﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JueWei.Bussiness
{
    public interface ICommunicationData
    {
        
    }

    public class StandablePdaData : ICommunicationData
    {
        public string materilano { get; set; }
        public string spec { get; set; }
        public string unit { get; set; }
        public decimal weight { get; set; }

        public override string ToString()
        {
            return string.Format("物料编号:{0},规格{1},重量{2}单位:{3}",materilano,spec,weight,unit);
        }
    }
}
