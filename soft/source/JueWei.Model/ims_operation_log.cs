using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    [Serializable]
    public class ims_operation_log
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// userid
        /// </summary>		
        private int _userid;
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        /// <summary>
        /// username
        /// </summary>		
        private string _username;
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }
        /// <summary>
        /// note
        /// </summary>		
        private string _note;
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }
        public ims_operation_log() { }
        public ims_operation_log(int id, int userid, string username, string note)
        {
            _id = id;
            _userid = userid;
            _username = username;
            _note = note;
        }

    }
}


