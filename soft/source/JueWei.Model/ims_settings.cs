using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    [Serializable]
    public class ims_settings
    {

        /// <summary>
        /// 设置项名字
        /// </summary>		
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        /// <summary>
        /// 设定的内容
        /// </summary>		
        private string _setting_data;
        public string Setting_data
        {
            get { return _setting_data; }
            set { _setting_data = value; }
        }
        public ims_settings() { }
        public ims_settings(string name, string setting_data)
        {
            _name = name;
            _setting_data = setting_data;
        }

    }
}


