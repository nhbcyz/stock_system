using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    [Serializable]
    public class ims_users
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 用户名
        /// </summary>		
        private string _username;
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }
        /// <summary>
        /// 密码
        /// </summary>		
        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        /// <summary>
        /// 盐
        /// </summary>		
        private string _salt;
        public string Salt
        {
            get { return _salt; }
            set { _salt = value; }
        }
        /// <summary>
        /// 账户创建时间
        /// </summary>		
        private DateTime _createtime;
        public DateTime Createtime
        {
            get { return _createtime; }
            set { _createtime = value; }
        }
        /// <summary>
        /// fullname
        /// </summary>		
        private string _fullname;
        public string Fullname
        {
            get { return _fullname; }
            set { _fullname = value; }
        }
        /// <summary>
        /// expireddate
        /// </summary>		
        private DateTime _expireddate;
        public DateTime Expireddate
        {
            get { return _expireddate; }
            set { _expireddate = value; }
        }
        public ims_users() { }
        public ims_users(int id, string username, string password, string salt, DateTime createtime, string fullname, DateTime expireddate)
        {
            _id = id;
            _username = username;
            _password = password;
            _salt = salt;
            _createtime = createtime;
            _fullname = fullname;
            _expireddate = expireddate;
        }

    }
}


