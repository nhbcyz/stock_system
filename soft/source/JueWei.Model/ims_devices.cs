using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    public class ims_devices
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 设备名字
        /// </summary>		
        private string _devicename;
        public string Devicename
        {
            get { return _devicename; }
            set { _devicename = value; }
        }
        /// <summary>
        /// 设备序列号
        /// </summary>		
        private string _serialno;
        public string Serialno
        {
            get { return _serialno; }
            set { _serialno = value; }
        }
        /// <summary>
        /// 设备MAC地址
        /// </summary>		
        private string _mac;
        public string Mac
        {
            get { return _mac; }
            set { _mac = value; }
        }
        /// <summary>
        /// 设备IP地址
        /// </summary>		
        private string _ipaddr;
        public string Ipaddr
        {
            get { return _ipaddr; }
            set { _ipaddr = value; }
        }
        /// <summary>
        /// 设备类型,0固定PDA,1手持PDA,2称重仪
        /// </summary>		
        private int _devicetype;
        public int Devicetype
        {
            get { return _devicetype; }
            set { _devicetype = value; }
        }
        /// <summary>
        /// 设备当前状态,0未激活,1设备损坏,2激活
        /// </summary>		
        private int _devicestate;
        public int Devicestate
        {
            get { return _devicestate; }
            set { _devicestate = value; }
        }
        /// <summary>
        /// 通讯端口
        /// </summary>		
        private int _commport;
        public int Commport
        {
            get { return _commport; }
            set { _commport = value; }
        }
        public ims_devices() { }
        public ims_devices(int id, string devicename, string serialno, string mac, string ipaddr, int devicetype, int devicestate, int commport)
        {
            _id = id;
            _devicename = devicename;
            _serialno = serialno;
            _mac = mac;
            _ipaddr = ipaddr;
            _devicetype = devicetype;
            _devicestate = devicestate;
            _commport = commport;
        }

    }
}


