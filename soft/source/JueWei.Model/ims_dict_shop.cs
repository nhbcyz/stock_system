using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    public class ims_dict_shop
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 门店编号
        /// </summary>		
        private string _shopno;
        public string Shopno
        {
            get { return _shopno; }
            set { _shopno = value; }
        }
        /// <summary>
        /// 店铺名字
        /// </summary>		
        private string _shopname;
        public string Shopname
        {
            get { return _shopname; }
            set { _shopname = value; }
        }
        /// <summary>
        /// 联系人
        /// </summary>		
        private string _contacter;
        public string Contacter
        {
            get { return _contacter; }
            set { _contacter = value; }
        }
        /// <summary>
        /// 联系电话
        /// </summary>		
        private string _tel;
        public string Tel
        {
            get { return _tel; }
            set { _tel = value; }
        }
        /// <summary>
        /// 线路编号
        /// </summary>		
        private string _vehiclelineno;
        public string Vehiclelineno
        {
            get { return _vehiclelineno; }
            set { _vehiclelineno = value; }
        }
        /// <summary>
        /// 线路描述
        /// </summary>		
        private string _vehiclename;
        public string Vehiclename
        {
            get { return _vehiclename; }
            set { _vehiclename = value; }
        }
        /// <summary>
        /// 装运顺序
        /// </summary>		
        private string _shiporder;
        public string Shiporder
        {
            get { return _shiporder; }
            set { _shiporder = value; }
        }

        /// <summary>
        /// 店铺地址
        /// </summary>		
        private string _addr;
        public string Addr
        {
            get { return _addr; }
            set { _addr = value; }
        }

        /// <summary>
        /// 自定义排序号
        /// </summary>		
        private int _sort_id;
        public int Sort_id
        {
            get { return _sort_id; }
            set { _sort_id = value; }
        }
     
        public ims_dict_shop() { }
        public ims_dict_shop(int id, string shopno, string shopname, string contacter, string tel, string vehiclelineno, string vehiclename, string shiporder,int sort_id)
        {
            _id = id;
            _shopno = shopno;
            _shopname = shopname;
            _contacter = contacter;
            _tel = tel;
            _vehiclelineno = vehiclelineno;
            _vehiclename = vehiclename;
            _shiporder = shiporder;
            _sort_id = sort_id;
        }

    }
}


