﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace JueWei.Common
{
    public class GlobalVar
    {
        public static AppConfig config = new AppConfig();
        private static GlobalVar _instance = new GlobalVar();


        public static GlobalVar Instance
        {
            get { 
                return _instance;
            }
        }

        private string _configFile;

        public string ConfigFile
        {
            get { return _configFile; }
            set 
            {
                if (value != _configFile)
                {
                    _configFile = value;
                    Update();
                }
               
            }
        }

        private void Update()
        {
            Object o = XmlSerializerCustom.LoadFromXml(_configFile, typeof(AppConfig));
            config = (AppConfig)o;
        }

        private GlobalVar()
        {
            
        }

        public static string DBConnString
        {
            get {

                return ConfigHelper.GetConfigString("DBConnStr");
            }
        }

        public string GetAppConfig(string key)
        {
            string val = "";
            if (config == null)
            {
                return "";
            }
            for (int i = 0; i < config.Items.Count; i++)
            {
                if (config.Items[i].key == key)
                {
                    val = config.Items[i].value;
                    break;
                }
            }
            return val;
        }

        public void SetAppConfig(string key,string value)
        {
            if (config == null)
            {
                return;
            }
            for (int i = 0; i < config.Items.Count; i++)
            {
                if (config.Items[i].key == key)
                {
                    config.Items[i].value = value;
                    break;
                }
            }
        }

        public void Save()
        {
            try
            {
                XmlSerializerCustom.SaveToXml(_configFile, config, typeof(AppConfig), "appSettings");
            }
            catch
            {
            }
           
        }


        public string AppDataQrDir { get; set; }

        public string AppRootDir { get; set; }

        public string QRRootDir { get; set; }

        public string ExportRootDir { get; set; }

        public decimal OrderWightDiff { get; set; }
    }
}
