using System;
using MySql.Data.MySqlClient;
using System.Data;
using JueWei.DBUtility;
//using Mysql.Data;
/**/
/// <summary>
/// 数据源提供
/// </summary>
public class PageHelper
{
    private int _PageSize = 10;
    private int _PageIndex = 1;
    private int _PageCount = 0;
    private int _TotalCount = 0;
    private string _TableName;//表名
    private string _QueryFieldName = "*";//表字段FieldStr
    private string _OrderStr = string.Empty; //排序_SortStr
    private string _QueryCondition = string.Empty;//查询的条件 RowFilter
    private string _PrimaryKey = string.Empty;//主键
    /**/
    /// <summary>
    /// 显示页数
    /// </summary>
    public int PageSize
    {
        get
        {
            return _PageSize;

        }
        set
        {
            _PageSize = value;
        }
    }
    /**/
    /// <summary>
    /// 当前页
    /// </summary>
    public int PageIndex
    {
        get
        {
            return _PageIndex;
        }
        set
        {
            _PageIndex = value;
        }
    }
    /**/
    /// <summary>
    /// 总页数
    /// </summary>
    public int PageCount
    {
        get
        {
            return _PageCount;
        }
    }
    /**/
    /// <summary>
    /// 总记录数
    /// </summary>
    public int TotalCount
    {
        get
        {
            return _TotalCount;
        }
    }
    /**/
    /// <summary>
    /// 表名，包括视图
    /// </summary>
    public string TableName
    {
        get
        {
            return _TableName;
        }
        set
        {
            _TableName = value;
        }
    }
    /**/
    /// <summary>
    /// 表字段FieldStr
    /// </summary>
    public string QueryFieldName
    {
        get
        {
            return _QueryFieldName;
        }
        set
        {
            _QueryFieldName = value;
        }
    }
    /**/
    /// <summary>
    /// 排序字段
    /// </summary>
    public string OrderStr
    {
        get
        {
            return _OrderStr;
        }
        set
        {
            _OrderStr = value;
        }
    }
    /**/
    /// <summary>
    /// 查询条件
    /// </summary>
    public string QueryCondition
    {
        get
        {
            return _QueryCondition;
        }
        set
        {
            _QueryCondition = value;
        }
    }
    /**/
    /// <summary>
    /// 主键
    /// </summary>
    public string PrimaryKey
    {
        get
        {
            return _PrimaryKey;
        }
        set
        {
            _PrimaryKey = value;
        }
    }
    public DataSet QueryDataTable()
    {
        MySqlParameter[] parameters = {
          new MySqlParameter("@Tables", MySqlDbType.VarChar, 255),
          new MySqlParameter("@PrimaryKey" , MySqlDbType.VarChar , 255),  
          new MySqlParameter("@Sort", MySqlDbType.VarChar , 255 ),
          new MySqlParameter("@CurrentPage", MySqlDbType.Int32),
          new MySqlParameter("@PageSize", MySqlDbType.Int32),                  
          new MySqlParameter("@Fields", MySqlDbType.VarChar, 255),
          new MySqlParameter("@Filter", MySqlDbType.VarChar,1000),
          new MySqlParameter("@Group" ,MySqlDbType.VarChar , 1000 )
          };
        parameters[0].Value = _TableName;
        parameters[1].Value = _PrimaryKey;
        parameters[2].Value = _OrderStr;
        parameters[3].Value = PageIndex;
        parameters[4].Value = PageSize;
        parameters[5].Value = _QueryFieldName;
        parameters[6].Value = _QueryCondition;
        parameters[7].Value = string.Empty;
        DataSet ds = DbHelperMySQL.RunProcedure("SP_Pagination", parameters, "dd");
        _TotalCount = GetTotalCount();
        if (_TotalCount == 0)
        {
            _PageIndex = 0;
            _PageCount = 0;
        }
        else
        {
            _PageCount = _TotalCount % _PageSize == 0 ? _TotalCount / _PageSize : _TotalCount / _PageSize + 1;
            if (_PageIndex > _PageCount)
            {
                _PageIndex = _PageCount;

                parameters[4].Value = _PageSize;

                ds = QueryDataTable();
            }
        }
        return ds;
    }

    public int GetTotalCount()
    {
        string strSql = " select count(1) from " + _TableName;
        if (_QueryCondition != string.Empty)
        {
            strSql += " where " + _QueryCondition;
        }
        return int.Parse(DbHelperSQL.GetSingle(strSql).ToString());
    }
}