using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using JueWei.Model;
using JueWei.DBUtility;
namespace JueWei.DAL
{
    public partial class ims_instock_record
    {
        #region  添加一条数据
        /// <summary>
        /// 添加一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否添加成功</returns>
        public bool Insert(Model.ims_instock_record SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@productid",SingleItem.Productid),            
                        new MySqlParameter("@materialno",SingleItem.Materialno),            
                        new MySqlParameter("@createtime",SingleItem.Createtime),            
                        new MySqlParameter("@spec",SingleItem.Spec),            
                        new MySqlParameter("@unit",SingleItem.Unit),            
                        new MySqlParameter("@weight",SingleItem.Weight),
                        new MySqlParameter("@state",SingleItem.State)
                        };
            string sql = "insert into ims_instock_record (productid,materialno,createtime,spec,unit,weight,state) values (@productid,@materialno,@createtime,@spec,@unit,@weight,@state)";


            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库插入操作
            {
                return true;
            }
            return false;   //插入不成功，或者数据已经存在 
        }
        #endregion

        #region 删除一条数据
        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        /// <param name="id">主键值</param>
        /// <returns>是否删除成功！</returns>
        public bool Delete(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "delete from ims_instock_record where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 更新一条数据
        /// <summary>
        /// 更新一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否修改成功</returns>
        public bool Update(Model.ims_instock_record SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@id",SingleItem.Id),            
                        new MySqlParameter("@productid",SingleItem.Productid),            
                        new MySqlParameter("@materialno",SingleItem.Materialno),            
                        new MySqlParameter("@createtime",SingleItem.Createtime),            
                        new MySqlParameter("@spec",SingleItem.Spec),            
                        new MySqlParameter("@unit",SingleItem.Unit),            
                        new MySqlParameter("@weight",SingleItem.Weight),
                        new MySqlParameter("@state",SingleItem.State)         
                        };
            string sql = "update ims_instock_record set  productid = @productid ,  materialno = @materialno ,  createtime = @createtime ,  spec = @spec ,  unit = @unit ,  weight = @weight ,state = @state  where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库修改操作
            {
                return true;
            }
            return false;   //修改不成功，或者数据不存在           
        }
        #endregion

        #region  检查记录是否已经存在
        /// <summary>
        /// 检查记录是否已经存在
        /// </summary>
        public bool Exists(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select count(1) from ims_instock_record where id=@id";
            if (Convert.ToInt32(DbHelperMySQL.GetSingle(sql, prams)) > 0)    //存在返回true
            {
                return true;
            }
            return false;   //不存在返回false
        }
        #endregion

        #region  根据主键取数据
        /// <summary>
        /// 根据主键取数据
        /// </summary>
        public Model.ims_instock_record SelectById(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select * from ims_instock_record where id=@id";
            Model.ims_instock_record SingleItem = new Model.ims_instock_record();
            using (MySqlDataReader reader = DbHelperMySQL.ExecuteReader(sql, prams))
            {
                if (reader.Read())
                {
                    if (reader["id"].ToString() != "")
                    {
                        SingleItem.Id = int.Parse(reader["id"].ToString());
                    }
                    if (reader["productid"].ToString() != "")
                    {
                        SingleItem.Productid = int.Parse(reader["productid"].ToString());
                    }
                    if (reader["materialno"].ToString() != "")
                    {
                        SingleItem.Materialno = reader["materialno"].ToString();
                    }
                    if (reader["createtime"].ToString() != "")
                    {
                        SingleItem.Createtime = DateTime.Parse(reader["createtime"].ToString());
                    }
                    SingleItem.Spec = reader["spec"].ToString();
                    SingleItem.Unit = reader["unit"].ToString();
                    if (reader["weight"].ToString() != "")
                    {
                        SingleItem.Weight = decimal.Parse(reader["weight"].ToString());
                    }
                    if (reader["state"].ToString() != "")
                    {
                        SingleItem.State = int.Parse(reader["state"].ToString());
                    }
                }
            }
            return SingleItem;
        }
        #endregion

        #region 根据条件取ID排序一条数据
        /// <summary>
        /// 根据条件取ID排序一条数据
        /// </summary>
        public Model.ims_instock_record SelectModelByWhereOrderByID(string strWhere, bool bAsc)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_instock_record ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by id ");
            if (bAsc) { strSql.Append(" asc "); }
            else { strSql.Append(" desc "); }
            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                Model.ims_instock_record SingleItem = new Model.ims_instock_record();
                {
                    SingleItem.Id = int.Parse(dr["id"].ToString());

                    SingleItem.Productid = int.Parse(dr["productid"].ToString());

                    SingleItem.Materialno = dr["materialno"].ToString();

                    SingleItem.Createtime = DateTime.Parse(dr["createtime"].ToString());

                    SingleItem.Spec = dr["spec"].ToString();
                    SingleItem.Unit = dr["unit"].ToString();
                    SingleItem.Weight = decimal.Parse(dr["weight"].ToString());
                    if (dr["state"].ToString() != "")
                    {
                        SingleItem.State = int.Parse(dr["state"].ToString());
                    }
                };
                return SingleItem;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region 取得所有数据的列表
        /// <summary>
        /// 取得所有数据的列表
        /// </summary>
        /// <returns>数据列表</returns>
        public IList<Model.ims_instock_record> GetAllList()
        {
            IList<Model.ims_instock_record> itemList = new List<Model.ims_instock_record>();
            using (MySqlDataReader reader = DbHelperMySQL.ExecuteReader("select * from ims_instock_record "))
            {
                while (reader.Read())
                {
                    Model.ims_instock_record SingleItem = new Model.ims_instock_record();
                    {
                        SingleItem.Id = int.Parse(reader["id"].ToString());
                        SingleItem.Productid = int.Parse(reader["productid"].ToString());
                        SingleItem.Materialno = reader["materialno"].ToString();
                        SingleItem.Createtime = DateTime.Parse(reader["createtime"].ToString());
                        SingleItem.Spec = reader["spec"].ToString();
                        SingleItem.Unit = reader["unit"].ToString();
                        SingleItem.Weight = decimal.Parse(reader["weight"].ToString());
                        if (reader["state"].ToString() != "")
                        {
                            SingleItem.State = int.Parse(reader["state"].ToString());
                        }
                    };
                    itemList.Add(SingleItem);
                }
            }
            return itemList;
        }
        #endregion

        #region  取得Where条件的列表
        /// <summary>
        /// 取得Where条件的列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_instock_record ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperMySQL.Query(strSql.ToString());
        }
        #endregion

        #region  获取数据条数
        /// <summary>
        /// 获取数据条数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) ");
            strSql.Append(" FROM ims_instock_record ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperMySQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        #endregion

        #region 根据选择的主键值执行批量删除
        /// <summary>
        /// 根据选择的主键值执行批量删除
        /// </summary>
        /// <param name="keys">选择的主键字符串</param>
        /// <returns>是否删除成功</returns>
        public bool DeleteChooseByKeys(string keys)
        {
            string sql = "delete from ims_instock_record where id in (" + keys + ")";   //执行批量删除语句, 主键为已选择的多个主键（用in的方式）

            if (DbHelperMySQL.ExecuteSql(sql) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_instock_record ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_instock_record ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables.Count == 2 && ds.Tables[1].Rows.Count > 0)
            {
                recordCount = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            }
            else
            {
                recordCount = 0;
            }

            return ds;
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetList(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }
        #endregion


        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整的，如：id desc）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_instock_record> GetListByPage(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_instock_record ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_instock_record ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());

            IList<Model.ims_instock_record> itemList = new List<Model.ims_instock_record>();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Model.ims_instock_record SingleItem = new Model.ims_instock_record
                    {
                        Id = int.Parse(ds.Tables[0].Rows[i]["id"].ToString()),

                        Productid = int.Parse(ds.Tables[0].Rows[i]["productid"].ToString()),

                        Materialno = ds.Tables[0].Rows[i]["materialno"].ToString(),

                        Createtime = DateTime.Parse(ds.Tables[0].Rows[i]["createtime"].ToString()),

                        Spec = ds.Tables[0].Rows[i]["spec"].ToString(),
                        Unit = ds.Tables[0].Rows[i]["unit"].ToString(),
                        Weight = decimal.Parse(ds.Tables[0].Rows[i]["weight"].ToString()),
                        State = int.Parse(ds.Tables[0].Rows[i]["state"].ToString())

                    };
                    itemList.Add(SingleItem);
                }
                recordCount = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                return itemList;
            }
            else
            {
                recordCount = 0;
                return null;
            }
        }
        #endregion

        #region 查询分页数据 （重载方法，省略了排序字段orderString）
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_instock_record> GetListByPage(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetListByPage(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }
        #endregion

        public DataSet GetProductSumWeightByDateTime(string strstart, string strend)
        {
            StringBuilder sbStr = new StringBuilder();
            sbStr.Append("SELECT p.productname,p.materialNo, SUM(ir.weight) as weights FROM ims_instock_record as ir LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON ir.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE ir.createtime >= '{0}' and ir.createtime <= '{1}' ",strstart,strend);
            sbStr.Append("GROUP BY p.id ");
            return DbHelperMySQL.Query(sbStr.ToString());
        }

        public DataSet GetHasScanedProductNum(DateTime dtime)
        {
            StringBuilder sbStr = new StringBuilder();
            sbStr.Append("SELECT p.shortname,COUNT(s.id) as num from ims_instock_record as s LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON s.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE DATE_FORMAT(s.createtime,'%Y-%m-%d') = '{0}' ",dtime.ToString("yyyy-MM-dd"));
            sbStr.Append("GROUP BY p.materialNo");
            return DbHelperMySQL.Query(sbStr.ToString());
        }

        public DataSet GetInstockRecord(DateTime dtime, int state)
        {
            StringBuilder sbStr = new StringBuilder();
            sbStr.Append("SELECT s.materialno,COUNT(s.id) as num from ims_instock_record as s LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON s.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE DATE_FORMAT(s.createtime,'%Y-%m-%d') = '{0}' and state = {1} ", dtime.ToString("yyyy-MM-dd"),state);
            sbStr.Append("GROUP BY p.materialNo");
            sbStr.Append(";SELECT s.* from ims_instock_record as s LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON s.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE DATE_FORMAT(s.createtime,'%Y-%m-%d') = '{0}' and state = {1} ", dtime.ToString("yyyy-MM-dd"), state);
            return DbHelperMySQL.Query(sbStr.ToString());
        }

        public DataSet GetInstockRecord(DateTime todytime, DateTime yesterday, int state)
        {
            StringBuilder sbStr = new StringBuilder();
            sbStr.Append("SELECT s.materialno,COUNT(s.id) as num from ims_instock_record as s LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON s.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE (DATE_FORMAT(s.createtime,'%Y-%m-%d') = '{0}' or (DATE_FORMAT(s.createtime,'%Y-%m-%d') = '{1}')) and state = {2} ", todytime.ToString("yyyy-MM-dd"), yesterday.ToString("yyyy-MM-dd"), state);
            sbStr.Append("GROUP BY p.materialNo");
            sbStr.Append(";SELECT s.* from ims_instock_record as s LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON s.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE (DATE_FORMAT(s.createtime,'%Y-%m-%d') = '{0}' or (DATE_FORMAT(s.createtime,'%Y-%m-%d') = '{1}')) and state = {2} ", todytime.ToString("yyyy-MM-dd"), yesterday.ToString("yyyy-MM-dd"), state);
            return DbHelperMySQL.Query(sbStr.ToString());
        }

        public int GetInstockRecordByMaterialnoAndDt(string materialno, DateTime dtime, int state)
        {
            StringBuilder sbStr = new StringBuilder();
            sbStr.Append("SELECT COUNT(s.id) as num from ims_instock_record as s LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON s.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE DATE_FORMAT(s.createtime,'%Y-%m-%d') = '{0}' and s.state = {1} and s.materialno='{2}'", dtime.ToString("yyyy-MM-dd"), state,materialno);
            DataSet ds = DbHelperMySQL.Query(sbStr.ToString());
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0][0] != null && ds.Tables[0].Rows[0][0].ToString() != "")
                {
                    return int.Parse(ds.Tables[0].Rows[0][0].ToString());
                }
            }
            return 0;
        }
    }
}

