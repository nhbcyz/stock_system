﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Aspose.Cells;
using System.IO;
using JueWei.Common;
using JueWei.Main.Common;
using JueWei.Main.Controls;
using ThoughtWorks.QRCode.Codec;
using System.Drawing.Imaging;
using ThoughtWorks.QRCode.Codec.Data;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class ProductInfoFrm : Form
    {
        private int _lasselectindex = 0;
        private bool _modifying = false;
        private string _qrpath = string.Empty;
        private BackgroundWorker worker = new BackgroundWorker();
        public ProductInfoFrm()
        {
            InitializeComponent();
            _qrpath = ConfigHelper.GetConfigString("DefaultQrPath");
            CheckQrPath();
            Init();
        }

        private void Init()
        {
        }
        private void ProductInfoFrm_Load(object sender, EventArgs e)
        {
            this.pager1.PageCurrent = 1;
            this.pager1.PageSize = 30;

            this.pager1.NMax = BindData();

            //激活OnPageChanged事件
            pager1.EventPaging += new EventPagingHandler(pager1_EventPaging);
            this.pager1.Bind();
        }
        /// <summary>
        /// 页数变化时调用绑定数据方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private int pager1_EventPaging(EventPagingArg e)
        {
            return BindData();
        }
        private void BindPage()
        {
            this.pager1.PageCurrent = 1;
            
            this.pager1.Bind();
        }
        private int BindData()
        {
            BLL.ims_product pro = new JueWei.BLL.ims_product();
            int recordcount = 0;
            DataSet ds = BLL.ims_product.GetList(this.pager1.PageCurrent,this.pager1.PageSize,"","",ref recordcount);
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = ds.Tables[0];
            dataGridViewX1.Rows[_lasselectindex].Selected = true;

            return recordcount;
        }

        private void btn_import_Click(object sender, EventArgs e)
        {
            ImportFromExcel();
        }

        private void ImportFromExcel()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.RestoreDirectory = false;
            openFileDialog.InitialDirectory = ConfigHelper.GetConfigString("LastImportPath");
            openFileDialog.Filter = "excel 03文件|*.xls|excel 07文件|*.xlsx";

            openFileDialog.FilterIndex = 1;
            string fName = string.Empty;
            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            
            fName = openFileDialog.FileName;
            if (string.IsNullOrEmpty(fName))
            {
                return;
            }
            FileInfo f = new FileInfo(fName);
            ConfigHelper.SetAppSetting("LastImportPath", Path.GetDirectoryName(fName),AppCommon.Instance._Config);
            
            Workbook workbook = new Workbook(fName);

            Cells cells = workbook.Worksheets[0].Cells;

            DataTable dt = cells.ExportDataTable(1, 0, cells.MaxDataRow, 18, false);

            ProductBussiness.Update(dt);
            BindData();
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void EditData()
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            BLL.ims_product pro = new BLL.ims_product();
            Model.ims_product mpro = BLL.ims_product.SelectById(int.Parse(id));

            if (mpro == null)
            {
                MessageBox.Show("获取数据失败");
                return;
            }
            ProductItemEditFrm frm = new ProductItemEditFrm(mpro, false);
            frm.ShowDialog();
            _modifying = true;
            BindData();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (!_modifying)
            {
                _modifying = false;
                _lasselectindex = dataGridViewX1.CurrentRow.Index;
            }
            ShowQrCode();
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            _lasselectindex = dataGridViewX1.CurrentRow.Index;
            EditData();
        }

        private void ShowQrCode()
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }
            string materalid = dataGridViewX1.CurrentRow.Cells[1].Value.ToString();
            string productname = dataGridViewX1.CurrentRow.Cells[2].Value.ToString();
            CheckQrPath();
            string qrpath = _qrpath + GenImgName(materalid, productname);
            if (File.Exists(qrpath))
            {
                this.qr_picbox.ImageLocation = qrpath;
                this.tbx_qrdata.Text = CodeDecoder(qrpath);
            }
            else {
                this.qr_picbox.ImageLocation = "";
                this.tbx_qrdata.Text = "";
            }
            
        }

        /// <summary>
        /// 生成二维码事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_gen_qr_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }
            Gen_Qr_Img();
        }

        private void Gen_Qr_Img()
        {
            string materalid = dataGridViewX1.CurrentRow.Cells[1].Value.ToString();
            string spec = dataGridViewX1.CurrentRow.Cells[3].Value.ToString();
            string weight = dataGridViewX1.CurrentRow.Cells[4].Value.ToString();
            string unit = dataGridViewX1.CurrentRow.Cells[5].Value.ToString();

            Gen_Qr_Img(GenQrData(materalid, spec, weight, unit), materalid, dataGridViewX1.CurrentRow.Cells[2].Value.ToString(),true);
            
        }
        private void Gen_Qr_Img(string qrdata, string materalid,string prodcutname,bool showqrdata)
        {
            //获取生成二维码的参数（规模、版本、链接地址）
            int size = Convert.ToInt32(this.qr_version_slide.Value);
            int version = Convert.ToInt32(this.qr_version_slide.Value);
            if (showqrdata)
            {
                this.tbx_qrdata.Text = qrdata;
            }
            
            //调用生成二维码图片的事件，并把图片显示在图片框中
            Image img = QrCodeBitmap(qrdata, size, version);
            SaveQrImage(img,materalid,prodcutname);
        }
        /// <summary>
        /// 生成二维码位图
        /// </summary>
        /// <param name="link">用于生成二维码的链接</param>4
        /// <param name="size">二维码规模大小</param>
        /// <param name="version">二维码版本</param>
        /// <returns>二维码图片对象</returns>
        private Image QrCodeBitmap(string link, int size, int version)
        {
            QRCodeEncoder qrcodeencoder = new QRCodeEncoder();//生成设置编码实例
            qrcodeencoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;//字节编码
            qrcodeencoder.QRCodeScale = size;//二维码规模大小
            qrcodeencoder.QRCodeVersion = version;//二维码版本
            qrcodeencoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;//设置错误校验（错误更正）的级别：中等M
            Image img = qrcodeencoder.Encode(link);//生成二维码image图片
            return img;
        }

        private void qr_size_slider_ValueChanged(object sender, EventArgs e)
        {
            Gen_Qr_Img();
        }

        private void qr_version_slide_ValueChanged(object sender, EventArgs e)
        {
            Gen_Qr_Img();
        }

        //private void SaveQrImage(Image img)
        //{
        //    try
        //    {
        //        if (dataGridViewX1.CurrentRow == null)
        //        {
        //            return;
        //        }
        //        if (img == null)
        //        {
        //            return;
        //        }
        //        CheckQrPath();
        //        string materalid = dataGridViewX1.CurrentRow.Cells[1].Value.ToString();
        //        string productname = dataGridViewX1.CurrentRow.Cells[2].Value.ToString();

        //        string qrpath = _qrpath + GenImgName(materalid, productname);

        //        img.Save(qrpath, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        img.Dispose();
        //        ShowQrCode();
        //    }
        //    catch (Exception ex)
        //    {
        //        LogHelper.WriteLog(typeof(ProductInfoFrm), ex);
        //    }

        //}

        private void SaveQrImage(Image img, string materalid, string productname)
        {
            try
            {
                if (dataGridViewX1.CurrentRow == null)
                {
                    return;
                }
                if (img == null)
                {
                    return;
                }
                CheckQrPath();
                string qrpath = _qrpath + GenImgName(materalid, productname);

                img.Save(qrpath, System.Drawing.Imaging.ImageFormat.Jpeg);
                img.Dispose();
                ShowQrCode();
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(ProductInfoFrm), ex);
            }

        }

        private string GenImgName(string materialNo, string productname)
        {
            return materialNo + "_" + productname + ".jpg";
        }

        private string GenQrData(string materialno, string spec, string weight, string unit)
        {
            return string.Format("{0},{1},{2},{3}", materialno, spec, weight, unit);
        }
        private void CheckQrPath()
        {
            if (!Directory.Exists(_qrpath))
            {
                Directory.CreateDirectory(_qrpath);
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }

            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            BLL.ims_product pro = new BLL.ims_product();
            if (!BLL.ims_product.Delete(int.Parse(id)))
            {
                MessageBox.Show("删除失败");
                return;
            }
            BindData();
        }

        /// <summary>
        /// 二维码解码
        /// </summary>
        /// <param name="filePath">图片路径</param>
        /// <returns></returns>
        public string CodeDecoder(string filePath)
        {
            if (!System.IO.File.Exists(filePath))
                return null;
            Bitmap myBitmap = new Bitmap(Image.FromFile(filePath));
            QRCodeDecoder decoder = new QRCodeDecoder();
            string decodedString = decoder.decode(new QRCodeBitmapImage(myBitmap));
            return decodedString;
        }

        private ProcessBarForm frm = null;
        private void btn_batch_gen_qr_Click(object sender, EventArgs e)
        {
            frm = new ProcessBarForm();
            string text = "正在生成,当前进度{0}%";
            frm.ShowProcess(0, string.Format(text, 0));

            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(Batch_Gen_Qr);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.RunWorkerAsync();


        }
        private void Batch_Gen_Qr(object sender, DoWorkEventArgs e)
        {
            this.qr_picbox.ImageLocation = "";
            IList<Model.ims_product> products = BLL.ims_product.GetAllList();
            if (products == null || products.Count <= 0)
            {
                return;
            }
            string text = "正在生成,当前进度{0}%";
            
            for (int i = 0; i < products.Count; i++)
            {
                Gen_Qr_Img(GenQrData(products[i].Materialno.ToString(), products[i].Spec, products[i].Weight.ToString(), products[i].Unit), products[i].Materialno.ToString(), products[i].Productname,false);
                frm.UpdateData(0, string.Format(text, i / products.Count));
            }
            

           
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            frm.Stop();
            ShowQrCode();
        }
    }
}
