namespace JueWei.Main
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabcontextmenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctx_Window_CloseOther = new System.Windows.Forms.ToolStripMenuItem();
            this.ctx_Window_CloseAll = new System.Windows.Forms.ToolStripMenuItem();
            this.NavTabControl = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar6 = new DevComponents.DotNetBar.RibbonBar();
            this.btnitem_offstock = new DevComponents.DotNetBar.ButtonItem();
            this.btnitem_offstock_stastic = new DevComponents.DotNetBar.ButtonItem();
            this.btnitem_diffstock = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar4 = new DevComponents.DotNetBar.RibbonBar();
            this.btnitem_instock = new DevComponents.DotNetBar.ButtonItem();
            this.btnitem_instockstastic = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar2 = new DevComponents.DotNetBar.RibbonBar();
            this.btnitem_ord_instock = new DevComponents.DotNetBar.ButtonItem();
            this.btn_productorder_instock = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar5 = new DevComponents.DotNetBar.RibbonBar();
            this.btnitem_shopdict = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar3 = new DevComponents.DotNetBar.RibbonBar();
            this.btnitem_deviceinfo = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.btnitm_pro_info = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar8 = new DevComponents.DotNetBar.RibbonBar();
            this.btnitem_set = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar7 = new DevComponents.DotNetBar.RibbonBar();
            this.btnitem_usermgr = new DevComponents.DotNetBar.ButtonItem();
            this.btn_pwdedit = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonTabItem1 = new DevComponents.DotNetBar.RibbonTabItem();
            this.tabitm_stock = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbontab_sysinfo = new DevComponents.DotNetBar.RibbonTabItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.btnitem_exit = new DevComponents.DotNetBar.ButtonItem();
            this.btn_relogon = new DevComponents.DotNetBar.ButtonItem();
            this.qatCustomizeItem1 = new DevComponents.DotNetBar.QatCustomizeItem();
            this.ribbonDetailPanel = new DevComponents.DotNetBar.Ribbon.RibbonClientPanel();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.notifyMenu_About = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyMenu_Show = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyMenu_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.statusbar = new DevComponents.DotNetBar.Bar();
            this.lblCommandStatus = new DevComponents.DotNetBar.LabelItem();
            this.lblCalendar = new DevComponents.DotNetBar.LabelItem();
            this.lblCurrentUser = new DevComponents.DotNetBar.LabelItem();
            this.progressBar = new DevComponents.DotNetBar.ProgressBarItem();
            this.styleManager = new DevComponents.DotNetBar.StyleManager(this.components);
            this.ribbonBar9 = new DevComponents.DotNetBar.RibbonBar();
            this.btn_reg = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.tabcontextmenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NavTabControl)).BeginInit();
            this.NavTabControl.SuspendLayout();
            this.ribbonControl1.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.ribbonDetailPanel.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusbar)).BeginInit();
            this.SuspendLayout();
            // 
            // tabcontextmenu
            // 
            this.tabcontextmenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctx_Window_CloseOther,
            this.ctx_Window_CloseAll});
            this.tabcontextmenu.Name = "tabcontextmenu";
            this.tabcontextmenu.Size = new System.Drawing.Size(149, 48);
            // 
            // ctx_Window_CloseOther
            // 
            this.ctx_Window_CloseOther.Name = "ctx_Window_CloseOther";
            this.ctx_Window_CloseOther.Size = new System.Drawing.Size(148, 22);
            this.ctx_Window_CloseOther.Text = "关闭其他窗口";
            // 
            // ctx_Window_CloseAll
            // 
            this.ctx_Window_CloseAll.Name = "ctx_Window_CloseAll";
            this.ctx_Window_CloseAll.Size = new System.Drawing.Size(148, 22);
            this.ctx_Window_CloseAll.Text = "关闭所有";
            // 
            // NavTabControl
            // 
            this.NavTabControl.CloseButtonOnTabsVisible = true;
            this.NavTabControl.ContextMenuStrip = this.tabcontextmenu;
            // 
            // 
            // 
            // 
            // 
            // 
            this.NavTabControl.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.NavTabControl.ControlBox.MenuBox.Name = "";
            this.NavTabControl.ControlBox.Name = "";
            this.NavTabControl.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.NavTabControl.ControlBox.MenuBox,
            this.NavTabControl.ControlBox.CloseBox});
            this.NavTabControl.Controls.Add(this.superTabControlPanel1);
            this.NavTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NavTabControl.Location = new System.Drawing.Point(0, 0);
            this.NavTabControl.Name = "NavTabControl";
            this.NavTabControl.ReorderTabsEnabled = true;
            this.NavTabControl.SelectedTabFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.NavTabControl.SelectedTabIndex = 0;
            this.NavTabControl.Size = new System.Drawing.Size(682, 178);
            this.NavTabControl.TabFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.NavTabControl.TabIndex = 7;
            this.NavTabControl.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem1});
            this.NavTabControl.Text = "superTabControl1";
            this.NavTabControl.TabItemClose += new System.EventHandler<DevComponents.DotNetBar.SuperTabStripTabItemCloseEventArgs>(this.NavTabControl_TabItemClose);
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 30);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(682, 148);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "superTabItem1";
            // 
            // ribbonControl1
            // 
            // 
            // 
            // 
            this.ribbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribbonPanel3);
            this.ribbonControl1.Controls.Add(this.ribbonPanel2);
            this.ribbonControl1.Controls.Add(this.ribbonPanel1);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ribbonTabItem1,
            this.tabitm_stock,
            this.ribbontab_sysinfo});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl1.Location = new System.Drawing.Point(5, 1);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.ribbonControl1.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem1,
            this.qatCustomizeItem1});
            this.ribbonControl1.Size = new System.Drawing.Size(682, 154);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl1.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl1.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl1.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabIndex = 13;
            this.ribbonControl1.Text = "ribbonControl1";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel2.Controls.Add(this.ribbonBar6);
            this.ribbonPanel2.Controls.Add(this.ribbonBar4);
            this.ribbonPanel2.Controls.Add(this.ribbonBar2);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel2.Size = new System.Drawing.Size(682, 95);
            // 
            // 
            // 
            this.ribbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel2.TabIndex = 2;
            this.ribbonPanel2.Visible = false;
            // 
            // ribbonBar6
            // 
            this.ribbonBar6.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar6.ContainerControlProcessDialogKey = true;
            this.ribbonBar6.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar6.DragDropSupport = true;
            this.ribbonBar6.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnitem_offstock,
            this.btnitem_offstock_stastic,
            this.btnitem_diffstock});
            this.ribbonBar6.Location = new System.Drawing.Point(278, 0);
            this.ribbonBar6.Name = "ribbonBar6";
            this.ribbonBar6.Size = new System.Drawing.Size(191, 92);
            this.ribbonBar6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar6.TabIndex = 2;
            this.ribbonBar6.Text = "订单出库";
            // 
            // 
            // 
            this.ribbonBar6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnitem_offstock
            // 
            this.btnitem_offstock.Name = "btnitem_offstock";
            this.btnitem_offstock.SubItemsExpandWidth = 14;
            this.btnitem_offstock.Text = "出库流程";
            this.btnitem_offstock.Click += new System.EventHandler(this.btnitem_offstock_Click);
            // 
            // btnitem_offstock_stastic
            // 
            this.btnitem_offstock_stastic.Name = "btnitem_offstock_stastic";
            this.btnitem_offstock_stastic.SubItemsExpandWidth = 14;
            this.btnitem_offstock_stastic.Text = "出库统计";
            this.btnitem_offstock_stastic.Click += new System.EventHandler(this.btnitem_offstock_stastic_Click);
            // 
            // btnitem_diffstock
            // 
            this.btnitem_diffstock.Name = "btnitem_diffstock";
            this.btnitem_diffstock.SubItemsExpandWidth = 14;
            this.btnitem_diffstock.Text = "库存统计";
            this.btnitem_diffstock.Click += new System.EventHandler(this.btnitem_diffstock_Click);
            // 
            // ribbonBar4
            // 
            this.ribbonBar4.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar4.ContainerControlProcessDialogKey = true;
            this.ribbonBar4.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar4.DragDropSupport = true;
            this.ribbonBar4.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnitem_instock,
            this.btnitem_instockstastic});
            this.ribbonBar4.Location = new System.Drawing.Point(139, 0);
            this.ribbonBar4.Name = "ribbonBar4";
            this.ribbonBar4.Size = new System.Drawing.Size(139, 92);
            this.ribbonBar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar4.TabIndex = 1;
            this.ribbonBar4.Text = "商品入库";
            // 
            // 
            // 
            this.ribbonBar4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnitem_instock
            // 
            this.btnitem_instock.Name = "btnitem_instock";
            this.btnitem_instock.SubItemsExpandWidth = 14;
            this.btnitem_instock.Text = "入库详情";
            this.btnitem_instock.Click += new System.EventHandler(this.btnitem_instock_Click);
            // 
            // btnitem_instockstastic
            // 
            this.btnitem_instockstastic.Name = "btnitem_instockstastic";
            this.btnitem_instockstastic.SubItemsExpandWidth = 14;
            this.btnitem_instockstastic.Text = "入库统计";
            this.btnitem_instockstastic.Click += new System.EventHandler(this.btnitem_instockstastic_Click);
            // 
            // ribbonBar2
            // 
            this.ribbonBar2.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.ContainerControlProcessDialogKey = true;
            this.ribbonBar2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar2.DragDropSupport = true;
            this.ribbonBar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnitem_ord_instock,
            this.btn_productorder_instock});
            this.ribbonBar2.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar2.Name = "ribbonBar2";
            this.ribbonBar2.Size = new System.Drawing.Size(136, 92);
            this.ribbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar2.TabIndex = 0;
            this.ribbonBar2.Text = "入库管理";
            // 
            // 
            // 
            this.ribbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnitem_ord_instock
            // 
            this.btnitem_ord_instock.Name = "btnitem_ord_instock";
            this.btnitem_ord_instock.SubItemsExpandWidth = 14;
            this.btnitem_ord_instock.Text = "订单入库";
            this.btnitem_ord_instock.Click += new System.EventHandler(this.btnitem_ord_instock_Click);
            // 
            // btn_productorder_instock
            // 
            this.btn_productorder_instock.Name = "btn_productorder_instock";
            this.btn_productorder_instock.SubItemsExpandWidth = 14;
            this.btn_productorder_instock.Text = "生产订单";
            this.btn_productorder_instock.Click += new System.EventHandler(this.btn_productorder_instock_Click);
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel1.Controls.Add(this.ribbonBar5);
            this.ribbonPanel1.Controls.Add(this.ribbonBar3);
            this.ribbonPanel1.Controls.Add(this.ribbonBar1);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(682, 95);
            // 
            // 
            // 
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 1;
            this.ribbonPanel1.Visible = false;
            // 
            // ribbonBar5
            // 
            this.ribbonBar5.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.ContainerControlProcessDialogKey = true;
            this.ribbonBar5.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar5.DragDropSupport = true;
            this.ribbonBar5.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnitem_shopdict});
            this.ribbonBar5.Location = new System.Drawing.Point(203, 0);
            this.ribbonBar5.Name = "ribbonBar5";
            this.ribbonBar5.Size = new System.Drawing.Size(100, 92);
            this.ribbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar5.TabIndex = 2;
            this.ribbonBar5.Text = "门店信息";
            // 
            // 
            // 
            this.ribbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnitem_shopdict
            // 
            this.btnitem_shopdict.Name = "btnitem_shopdict";
            this.btnitem_shopdict.SubItemsExpandWidth = 14;
            this.btnitem_shopdict.Text = "门店字典";
            this.btnitem_shopdict.Click += new System.EventHandler(this.btnitem_shopdict_Click);
            // 
            // ribbonBar3
            // 
            this.ribbonBar3.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.ContainerControlProcessDialogKey = true;
            this.ribbonBar3.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar3.DragDropSupport = true;
            this.ribbonBar3.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnitem_deviceinfo});
            this.ribbonBar3.Location = new System.Drawing.Point(103, 0);
            this.ribbonBar3.Name = "ribbonBar3";
            this.ribbonBar3.Size = new System.Drawing.Size(100, 92);
            this.ribbonBar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar3.TabIndex = 1;
            this.ribbonBar3.Text = "设备信息";
            // 
            // 
            // 
            this.ribbonBar3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnitem_deviceinfo
            // 
            this.btnitem_deviceinfo.Name = "btnitem_deviceinfo";
            this.btnitem_deviceinfo.SubItemsExpandWidth = 14;
            this.btnitem_deviceinfo.Text = "设备编辑";
            this.btnitem_deviceinfo.Click += new System.EventHandler(this.btnitem_deviceinfo_Click);
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.ContainerControlProcessDialogKey = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.DragDropSupport = true;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnitm_pro_info});
            this.ribbonBar1.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(100, 92);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar1.TabIndex = 0;
            this.ribbonBar1.Text = "商品管理";
            // 
            // 
            // 
            this.ribbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnitm_pro_info
            // 
            this.btnitm_pro_info.Name = "btnitm_pro_info";
            this.btnitm_pro_info.SubItemsExpandWidth = 14;
            this.btnitm_pro_info.Text = "单品字典";
            this.btnitm_pro_info.Click += new System.EventHandler(this.productinfo_mi_Click);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel3.Controls.Add(this.ribbonBar9);
            this.ribbonPanel3.Controls.Add(this.ribbonBar8);
            this.ribbonPanel3.Controls.Add(this.ribbonBar7);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel3.Size = new System.Drawing.Size(682, 95);
            // 
            // 
            // 
            this.ribbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 3;
            // 
            // ribbonBar8
            // 
            this.ribbonBar8.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar8.ContainerControlProcessDialogKey = true;
            this.ribbonBar8.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar8.DragDropSupport = true;
            this.ribbonBar8.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnitem_set});
            this.ribbonBar8.Location = new System.Drawing.Point(145, 0);
            this.ribbonBar8.Name = "ribbonBar8";
            this.ribbonBar8.Size = new System.Drawing.Size(100, 92);
            this.ribbonBar8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar8.TabIndex = 1;
            this.ribbonBar8.Text = "其他配置";
            // 
            // 
            // 
            this.ribbonBar8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnitem_set
            // 
            this.btnitem_set.Name = "btnitem_set";
            this.btnitem_set.SubItemsExpandWidth = 14;
            this.btnitem_set.Text = "常规参数";
            this.btnitem_set.Click += new System.EventHandler(this.btnitem_set_Click);
            // 
            // ribbonBar7
            // 
            this.ribbonBar7.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar7.ContainerControlProcessDialogKey = true;
            this.ribbonBar7.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar7.DragDropSupport = true;
            this.ribbonBar7.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnitem_usermgr,
            this.btn_pwdedit});
            this.ribbonBar7.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar7.Name = "ribbonBar7";
            this.ribbonBar7.Size = new System.Drawing.Size(142, 92);
            this.ribbonBar7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar7.TabIndex = 0;
            this.ribbonBar7.Text = "用户管理";
            // 
            // 
            // 
            this.ribbonBar7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnitem_usermgr
            // 
            this.btnitem_usermgr.Name = "btnitem_usermgr";
            this.btnitem_usermgr.SubItemsExpandWidth = 14;
            this.btnitem_usermgr.Text = "用户列表";
            this.btnitem_usermgr.Click += new System.EventHandler(this.btnitem_usermgr_Click);
            // 
            // btn_pwdedit
            // 
            this.btn_pwdedit.Name = "btn_pwdedit";
            this.btn_pwdedit.SubItemsExpandWidth = 14;
            this.btn_pwdedit.Text = "密码修改";
            this.btn_pwdedit.Click += new System.EventHandler(this.btn_pwdedit_Click);
            // 
            // ribbonTabItem1
            // 
            this.ribbonTabItem1.Name = "ribbonTabItem1";
            this.ribbonTabItem1.Panel = this.ribbonPanel1;
            this.ribbonTabItem1.Text = "基础信息";
            // 
            // tabitm_stock
            // 
            this.tabitm_stock.Name = "tabitm_stock";
            this.tabitm_stock.Panel = this.ribbonPanel2;
            this.tabitm_stock.Text = "出入库";
            // 
            // ribbontab_sysinfo
            // 
            this.ribbontab_sysinfo.Checked = true;
            this.ribbontab_sysinfo.Name = "ribbontab_sysinfo";
            this.ribbontab_sysinfo.Panel = this.ribbonPanel3;
            this.ribbontab_sysinfo.Text = "系统信息";
            // 
            // buttonItem1
            // 
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnitem_exit,
            this.btn_relogon});
            this.buttonItem1.Text = "系统";
            // 
            // btnitem_exit
            // 
            this.btnitem_exit.Name = "btnitem_exit";
            this.btnitem_exit.Text = "退出系统";
            this.btnitem_exit.Click += new System.EventHandler(this.btnitem_exit_Click);
            // 
            // btn_relogon
            // 
            this.btn_relogon.Name = "btn_relogon";
            this.btn_relogon.Text = "重新登录";
            this.btn_relogon.Click += new System.EventHandler(this.btn_relogon_Click);
            // 
            // qatCustomizeItem1
            // 
            this.qatCustomizeItem1.Name = "qatCustomizeItem1";
            // 
            // ribbonDetailPanel
            // 
            this.ribbonDetailPanel.CanvasColor = System.Drawing.SystemColors.Control;
            this.ribbonDetailPanel.Controls.Add(this.NavTabControl);
            this.ribbonDetailPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonDetailPanel.Location = new System.Drawing.Point(5, 155);
            this.ribbonDetailPanel.Name = "ribbonDetailPanel";
            this.ribbonDetailPanel.Size = new System.Drawing.Size(682, 178);
            // 
            // 
            // 
            this.ribbonDetailPanel.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ribbonDetailPanel.Style.BackColorGradientAngle = 90;
            this.ribbonDetailPanel.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ribbonDetailPanel.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonDetailPanel.Style.BorderBottomWidth = 1;
            this.ribbonDetailPanel.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.ribbonDetailPanel.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonDetailPanel.Style.BorderLeftWidth = 1;
            this.ribbonDetailPanel.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonDetailPanel.Style.BorderRightWidth = 1;
            this.ribbonDetailPanel.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonDetailPanel.Style.BorderTopWidth = 1;
            this.ribbonDetailPanel.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonDetailPanel.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.ribbonDetailPanel.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            // 
            // 
            // 
            this.ribbonDetailPanel.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonDetailPanel.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonDetailPanel.TabIndex = 14;
            this.ribbonDetailPanel.Text = "ribbonClientPanel1";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "仓库分拣系统";
            this.notifyIcon1.BalloonTipTitle = "[技德科技]仓库分拣系统";
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.notifyMenu_About,
            this.notifyMenu_Show,
            this.notifyMenu_Exit});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 70);
            // 
            // notifyMenu_About
            // 
            this.notifyMenu_About.Name = "notifyMenu_About";
            this.notifyMenu_About.Size = new System.Drawing.Size(180, 22);
            this.notifyMenu_About.Text = "关于(&A)";
            // 
            // notifyMenu_Show
            // 
            this.notifyMenu_Show.Name = "notifyMenu_Show";
            this.notifyMenu_Show.Size = new System.Drawing.Size(180, 22);
            this.notifyMenu_Show.Text = "显示/隐藏主窗口(&S)";
            // 
            // notifyMenu_Exit
            // 
            this.notifyMenu_Exit.Name = "notifyMenu_Exit";
            this.notifyMenu_Exit.Size = new System.Drawing.Size(180, 22);
            this.notifyMenu_Exit.Text = "退出(&X)";
            // 
            // statusbar
            // 
            this.statusbar.AntiAlias = true;
            this.statusbar.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.statusbar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.statusbar.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.statusbar.IsMaximized = false;
            this.statusbar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblCommandStatus,
            this.lblCalendar,
            this.lblCurrentUser,
            this.progressBar});
            this.statusbar.Location = new System.Drawing.Point(5, 333);
            this.statusbar.Name = "statusbar";
            this.statusbar.Size = new System.Drawing.Size(682, 22);
            this.statusbar.Stretch = true;
            this.statusbar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.statusbar.TabIndex = 15;
            this.statusbar.TabStop = false;
            this.statusbar.Text = "bar1";
            // 
            // lblCommandStatus
            // 
            this.lblCommandStatus.Name = "lblCommandStatus";
            this.lblCommandStatus.Stretch = true;
            // 
            // lblCalendar
            // 
            this.lblCalendar.Name = "lblCalendar";
            this.lblCalendar.Text = "2010-11-12 0:00:00";
            // 
            // lblCurrentUser
            // 
            this.lblCurrentUser.Name = "lblCurrentUser";
            // 
            // progressBar
            // 
            // 
            // 
            // 
            this.progressBar.BackStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.progressBar.ChunkGradientAngle = 0F;
            this.progressBar.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.progressBar.Name = "progressBar";
            this.progressBar.RecentlyUsed = false;
            this.progressBar.Visible = false;
            // 
            // styleManager
            // 
            this.styleManager.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Blue;
            this.styleManager.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154))))));
            // 
            // ribbonBar9
            // 
            this.ribbonBar9.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar9.ContainerControlProcessDialogKey = true;
            this.ribbonBar9.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar9.DragDropSupport = true;
            this.ribbonBar9.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btn_reg,
            this.buttonItem3});
            this.ribbonBar9.Location = new System.Drawing.Point(245, 0);
            this.ribbonBar9.Name = "ribbonBar9";
            this.ribbonBar9.Size = new System.Drawing.Size(138, 92);
            this.ribbonBar9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar9.TabIndex = 2;
            this.ribbonBar9.Text = "帮助和关于";
            // 
            // 
            // 
            this.ribbonBar9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btn_reg
            // 
            this.btn_reg.Name = "btn_reg";
            this.btn_reg.SubItemsExpandWidth = 14;
            this.btn_reg.Text = "软件升级";
            this.btn_reg.Click += new System.EventHandler(this.btn_reg_Click);
            // 
            // buttonItem3
            // 
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.SubItemsExpandWidth = 14;
            this.buttonItem3.Text = "软件帮助";
            // 
            // MainFrm
            // 
            this.ClientSize = new System.Drawing.Size(692, 357);
            this.Controls.Add(this.ribbonDetailPanel);
            this.Controls.Add(this.statusbar);
            this.Controls.Add(this.ribbonControl1);
            this.IsMdiContainer = true;
            this.Name = "MainFrm";
            this.Text = "绝味分拣系统";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainFrm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFrm_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrm_FormClosing);
            this.tabcontextmenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NavTabControl)).EndInit();
            this.NavTabControl.ResumeLayout(false);
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribbonPanel2.ResumeLayout(false);
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonPanel3.ResumeLayout(false);
            this.ribbonDetailPanel.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statusbar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip tabcontextmenu;
        private DevComponents.DotNetBar.SuperTabControl NavTabControl;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private System.Windows.Forms.ToolStripMenuItem ctx_Window_CloseOther;
        private System.Windows.Forms.ToolStripMenuItem ctx_Window_CloseAll;
        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel2;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem1;
        private DevComponents.DotNetBar.RibbonTabItem tabitm_stock;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem btnitem_exit;
        private DevComponents.DotNetBar.QatCustomizeItem qatCustomizeItem1;
        private DevComponents.DotNetBar.ButtonItem btnitm_pro_info;
        private DevComponents.DotNetBar.RibbonBar ribbonBar2;
        private DevComponents.DotNetBar.ButtonItem btnitem_ord_instock;
        private DevComponents.DotNetBar.ButtonItem btn_relogon;
        private DevComponents.DotNetBar.Ribbon.RibbonClientPanel ribbonDetailPanel;
        private DevComponents.DotNetBar.RibbonBar ribbonBar3;
        private DevComponents.DotNetBar.ButtonItem btnitem_deviceinfo;
        private DevComponents.DotNetBar.RibbonBar ribbonBar4;
        private DevComponents.DotNetBar.ButtonItem btnitem_instock;
        private DevComponents.DotNetBar.RibbonBar ribbonBar5;
        private DevComponents.DotNetBar.ButtonItem btnitem_shopdict;
        private DevComponents.DotNetBar.ButtonItem btnitem_instockstastic;
        private DevComponents.DotNetBar.RibbonBar ribbonBar6;
        private DevComponents.DotNetBar.ButtonItem btnitem_offstock;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem notifyMenu_About;
        private System.Windows.Forms.ToolStripMenuItem notifyMenu_Show;
        private System.Windows.Forms.ToolStripMenuItem notifyMenu_Exit;
        private DevComponents.DotNetBar.Bar statusbar;
        private DevComponents.DotNetBar.LabelItem lblCommandStatus;
        private DevComponents.DotNetBar.LabelItem lblCalendar;
        private DevComponents.DotNetBar.LabelItem lblCurrentUser;
        private DevComponents.DotNetBar.ProgressBarItem progressBar;
        private DevComponents.DotNetBar.StyleManager styleManager;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.DotNetBar.RibbonBar ribbonBar7;
        private DevComponents.DotNetBar.ButtonItem btnitem_usermgr;
        private DevComponents.DotNetBar.RibbonTabItem ribbontab_sysinfo;
        private DevComponents.DotNetBar.ButtonItem btn_pwdedit;
        private DevComponents.DotNetBar.ButtonItem btnitem_offstock_stastic;
        private DevComponents.DotNetBar.ButtonItem btnitem_diffstock;
        private DevComponents.DotNetBar.RibbonBar ribbonBar8;
        private DevComponents.DotNetBar.ButtonItem btnitem_set;
        private DevComponents.DotNetBar.ButtonItem btn_productorder_instock;
        private DevComponents.DotNetBar.RibbonBar ribbonBar9;
        private DevComponents.DotNetBar.ButtonItem btn_reg;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
    }
}