namespace JueWei.Main
{
    using DevComponents.DotNetBar;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using WHC.OrderWater.Commons;

    public class BaseForm : OfficeForm
    {
        private IContainer components = null;
        protected bool permitclose = true;

        public BaseForm()
        {
            this.InitializeComponent();
            
        }

        private void BaseForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                this.FormOnLoad();
            }
        }

        private void BaseForm_Load(object sender, EventArgs e)
        {
            if (!base.DesignMode)
            {
                this.Cursor = Cursors.WaitCursor;
                try
                {
                    this.FormOnLoad();
                }
                catch (Exception exception)
                {
                    this.ProcessException(exception);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public virtual void FormOnLoad()
        {
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.ResumeLayout(false);

        }

        protected override void OnLoad(EventArgs e)
        {
            if (!base.DesignMode)
            {
                ComponentResourceManager manager = new ComponentResourceManager(typeof(BaseForm));
                //base.Icon = Resources.app;
                base.StartPosition = FormStartPosition.CenterScreen;
                base.OnLoad(e);
            }
        }

        public void ProcessException(Exception ex)
        {
            this.WriteException(ex);
            MessageBox.Show(ex.Message);
        }

        public void WriteException(Exception ex)
        {
            LogHelper.Error(ex);
            //MessageExUtil.ShowError(ex.Message);
        }

        public bool PermitClose
        {
            get { return permitclose; }
        }
    }
}

