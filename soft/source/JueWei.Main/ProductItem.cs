﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JueWei.Common;

namespace JueWei.Main
{
    public partial class ProductItem : Form
    {
        private Model.ims_product _mproduct = null;
        private bool _adding = false;
        public ProductItem()
        {
            InitializeComponent();
        }

        public ProductItem(Model.ims_product mproduct,bool adding)
        {
            this._mproduct = mproduct;
            this._adding = adding;
            InitializeComponent();
            UpdateUI();
        }

        private void UpdateUI()
        {
            if (_adding)
            {
                return;
            }
            if (this._mproduct == null)
            {
                return;
            }
            tb_materialno.Text = _mproduct.Materialno.ToString();
            tb_productname.Text = _mproduct.Productname.ToString();
            tb_spec.Text = _mproduct.Spec.ToString();
            tb_weight.Text = _mproduct.Weight.ToString();
            tb_unit.Text = _mproduct.Unit.ToString();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tb_productname.Text.Trim()) ||
    string.IsNullOrEmpty(tb_spec.Text.Trim()) ||
    string.IsNullOrEmpty(tb_weight.Text.Trim()) ||
    string.IsNullOrEmpty(tb_unit.Text.Trim())
    )
                {
                    MessageBox.Show("参数不能为空");
                    return;
                }
                decimal weight = 0.0m;
                if (!decimal.TryParse(tb_weight.Text.Trim(), out weight))
                {
                    MessageBox.Show("重量填写错误");
                    return;
                }
                _mproduct.Productname = tb_productname.Text.TrimEnd();
                _mproduct.Spec = tb_spec.Text.TrimEnd();
                _mproduct.Weight = weight;
                _mproduct.Unit = tb_unit.Text.TrimEnd();
                if (!BLL.ims_product.Update(_mproduct))
                {
                    MessageBox.Show("保存失败");
                    return;
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("保存失败");
                LogHelper.WriteLog(typeof(ProductItem), ex);
            }

            this.Close();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
