using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
namespace WHC.WareHouseMis.UI.SplashScreen
{
    public class Splasher
    {
        private static Form m_SplashForm = null;
        private static ISplashForm m_SplashInterface = null;
        private static System.Threading.Thread m_SplashThread = null;
        private static string m_TempStatus = string.Empty;

        public static void Close()
        {
            if ((m_SplashThread != null) && (m_SplashForm != null))
            {
                try
                {
                    m_SplashForm.Invoke(new MethodInvoker(m_SplashForm.Close));
                }
                catch (Exception)
                {
                }
                m_SplashThread = null;
                m_SplashForm = null;
            }
        }

        private static void CreateInstance(System.Type FormType)
        {
            object obj2 = FormType.InvokeMember(null, BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly, null, null, null);
            m_SplashForm = obj2 as Form;
            m_SplashInterface = obj2 as ISplashForm;
            if (m_SplashForm == null)
            {
                throw new Exception("Splash Screen must inherit from System.Windows.Forms.Form");
            }
            if (m_SplashInterface == null)
            {
                throw new Exception("must implement interface ISplashForm");
            }
            if (!string.IsNullOrEmpty(m_TempStatus))
            {
                m_SplashInterface.SetStatusInfo(m_TempStatus);
            }
        }

        public static void Show(System.Type splashFormType)
        {
            if (m_SplashThread == null)
            {
                if (splashFormType == null)
                {
                    throw new Exception("splashFormType is null");
                }
                m_SplashThread = new System.Threading.Thread(new ThreadStart(delegate
                {
                    CreateInstance(splashFormType);
                    Application.Run(m_SplashForm);
                }));
                m_SplashThread.IsBackground = true;
                m_SplashThread.SetApartmentState(ApartmentState.STA);
                m_SplashThread.Start();
            }
        }

        public static string Status
        {
            set
            {
                if ((m_SplashInterface == null) || (m_SplashForm == null))
                {
                    m_TempStatus = value;
                }
                else
                {
                    m_SplashForm.Invoke(new MethodInvoker(delegate { m_SplashInterface.SetStatusInfo(value); }));
                }
            }
        }

        private delegate void SplashStatusChangedHandle(string NewStatusInfo);
    }
}

