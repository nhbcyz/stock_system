namespace WHC.WareHouseMis.UI.SplashScreen
{
    using DevComponents.DotNetBar;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class frmSplash : Form, ISplashForm
    {
        private IContainer components = null;
        private Label label1;
        private LabelX lbStatusInfo;

        public frmSplash()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.lbStatusInfo = new DevComponents.DotNetBar.LabelX();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbStatusInfo
            // 
            // 
            // 
            // 
            this.lbStatusInfo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbStatusInfo.Location = new System.Drawing.Point(0, 0);
            this.lbStatusInfo.Name = "lbStatusInfo";
            this.lbStatusInfo.Size = new System.Drawing.Size(0, 0);
            this.lbStatusInfo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 173);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "正在加载程序所需资源,请稍后...";
            // 
            // frmSplash
            // 
            this.ClientSize = new System.Drawing.Size(461, 262);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSplash";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        void ISplashForm.SetStatusInfo(string NewStatusInfo)
        {
            this.lbStatusInfo.Text = NewStatusInfo;
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {








        }

    }
}

