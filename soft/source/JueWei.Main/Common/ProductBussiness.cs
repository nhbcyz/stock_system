﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using JueWei.DBUtility;
using WHC.OrderWater.Commons;
using JueWei.Bussiness;

namespace JueWei.Common
{
    class ProductBussiness
    {
        public static int Update(DataTable dt)
        {
            if (dt == null)
            {
                return 0;
            }
            BLL.ims_dict_product pro = new JueWei.BLL.ims_dict_product();
            List<string> sqllist = new List<string>();
            string sqlStr = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string materialno = dt.Rows[i][1].ToString();
                if (string.IsNullOrEmpty(materialno))
                {
                    continue;
                }
                if (string.IsNullOrEmpty(dt.Rows[i][5].ToString()) || string.IsNullOrEmpty(dt.Rows[i][3].ToString()))
                {
                    continue;
                }
                DataSet ds = BLL.ims_dict_product.GetList(" materialNo ='" + materialno + "' ");
                string spec = dt.Rows[i][3].ToString();
                string productname = dt.Rows[i][4].ToString();
                string shortname = productname;
                if (productname.StartsWith("绝味牌散1类"))
                {
                    shortname = productname.Substring(0,6);
                }
                else
                {
                    if (productname.Length >= 10)
                    {
                        shortname = productname.Substring(0, 10);
                    }
                    
                }
                decimal weight = 0;
                if (spec.IndexOf('J') > 0) //单品按斤称的
                {
                    weight = Convert.ToDecimal(spec.Substring(0, spec.IndexOf('J')));
                    weight = weight / 2;
                }
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    //已存在物料编号
                    sqlStr = "update ims_dict_product set ";
                    sqlStr += "productname ='" + productname + "',";
                    sqlStr += "shortname ='" + shortname + "',";
                    sqlStr += "spec ='" + dt.Rows[i][3].ToString() + "',";
                    sqlStr += "packetweight ='" + dt.Rows[i][5].ToString() + "',";
                    sqlStr += "modifytime ='" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "' ";
                    sqlStr += " where materialNo = " + dt.Rows[i][1].ToString();
                    sqllist.Add(sqlStr);
                }
                else
                {
                    //不存在物料编号,插入
                    sqlStr = "insert into ims_dict_product (materialNo,productname,shortname,spec,unit,weight,packetweight,createtime,modifytime) values ";
                    sqlStr += "('" + dt.Rows[i][1].ToString() + "','";
                    sqlStr += "" + productname + "','";
                    sqlStr += "" + shortname + "','";
                    sqlStr += "" + dt.Rows[i][3].ToString() + "','";
                    sqlStr += "" + "0" + "','";
                    sqlStr += "" + weight.ToString() + "','";
                    sqlStr += "" + dt.Rows[i][5].ToString() + "','";
                    sqlStr += "" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "','";
                    sqlStr += "" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "')";
                    sqllist.Add(sqlStr);
                }
            }
            int k = DbHelperMySQL.ExecuteSqlTran(sqllist);
            return k;
        }

        public static int Update(List<RFC_BaseObj> goods)
        {
            if (goods == null)
            {
                return 0;
            }

            BLL.ims_dict_product pro = new JueWei.BLL.ims_dict_product();
            List<string> sqllist = new List<string>();
            string sqlStr = string.Empty;
            for (int i = 0; i < goods.Count; i++)
            {
                RFC_MATNR matnr = goods[i] as RFC_MATNR;
                string materialno = matnr.MATNR;
                DataSet ds = BLL.ims_dict_product.GetList(" materialNo ='" + materialno + "' ");
                string spec = matnr.VRKME;
                string productname = matnr.MAKTX;
                string shortname = productname;
                string packweight = matnr.ZWEIGHT.ToString("f2");
                if (productname.StartsWith("绝味牌散1类"))
                {
                    shortname = productname.Substring(6);
                }

                decimal weight = 0;
                if (spec.IndexOf('J') > 0) //单品按斤称的
                {
                    weight = Convert.ToDecimal(spec.Substring(0, spec.IndexOf('J')));
                    weight = weight / 2;
                }
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    //已存在物料编号
                    sqlStr = "update ims_dict_product set ";
                    sqlStr += "productname ='" + productname + "',";
                    sqlStr += "shortname ='" + shortname + "',";
                    sqlStr += "spec ='" + spec + "',";
                    sqlStr += "unit ='" + matnr.MEINS + "',";
                    sqlStr += "packetweight ='" + packweight + "',";
                    sqlStr += "modifytime ='" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "' ";
                    sqlStr += " where materialNo = " + materialno;
                    sqllist.Add(sqlStr);
                }
                else
                {
                    //不存在物料编号,插入
                    sqlStr = "insert into ims_dict_product (materialNo,productname,shortname,spec,unit,weight,packetweight,createtime,modifytime) values ";
                    sqlStr += "('" + materialno + "','";
                    sqlStr += "" + productname + "','";
                    sqlStr += "" + shortname + "','";
                    sqlStr += "" + spec + "','";
                    sqlStr += "" + matnr.MEINS + "','";
                    sqlStr += "" + weight.ToString() + "','";
                    sqlStr += "" + packweight + "','";
                    sqlStr += "" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "','";
                    sqlStr += "" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "')";
                    sqllist.Add(sqlStr);
                }
            }
            int k = DbHelperMySQL.ExecuteSqlTran(sqllist);
            return k;
        }
        /// <summary>
        /// 给商品分配设备
        /// </summary>
        /// <param name="orderid"></param>
        /// <param name="deviceid"></param>
        /// <returns></returns>
        public static bool SceduleToDevice(int productid, int deviceid)
        {
            try
            {
                Model.ims_dict_product morder = BLL.ims_dict_product.SelectById(productid);
                if (morder == null)
                {
                    return false;
                }
                morder.Deviceid = deviceid;
                if (!BLL.ims_dict_product.Update(morder))
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(OrderBussiness), ex);
                return false;
            }

            return true;
        }
    }
}
