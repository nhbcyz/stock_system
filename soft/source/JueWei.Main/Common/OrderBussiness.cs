using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using JueWei.DBUtility;
using WHC.OrderWater.Commons;
using JueWei.Bussiness;

namespace JueWei.Common
{
    class OrderBussiness
    {
        /// <summary>
        /// 导入订单时插入
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int Insert(DataTable dt)
        {
            if (dt == null)
            {
                return 0;
            }
            IList<Model.ims_dict_product> products = BLL.ims_dict_product.GetAllList();
            List<string> sqllist = new List<string>();
            string sqlStr = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //商品名字为空时,本订单不写入数据库
                if (dt.Rows[i][5] == null || dt.Rows[i][5].ToString() == "")
                {
                    continue;
                }
                string materialno = dt.Rows[i][22].ToString();
                string spec = dt.Rows[i][40].ToString();
                int deviceid = GetDeviceIDByMaterialNo(materialno, products);
                StringBuilder strsql = new StringBuilder();
                strsql.Append("insert into ims_orders (orderno,orderexplain,verifydate,verifynum,exchangedate,");
                strsql.Append("materialno,unit,shopname,expressdate,shopaddrno,weight,shopno,exchangeno,exchangeaddr,deviceid,state) values ");
                strsql.Append("(");
                strsql.AppendFormat("'{0}',", dt.Rows[i][1].ToString());//凭证
                strsql.AppendFormat("'{0}',", dt.Rows[i][5].ToString());//订单描述
                strsql.AppendFormat("'{0}',", dt.Rows[i][7].ToString());//凭证日期
                strsql.AppendFormat("'{0}',", dt.Rows[i][8].ToString()); //个数
                strsql.AppendFormat("'{0}',", dt.Rows[i][14].ToString()); //出货日期
                strsql.AppendFormat("'{0}',", dt.Rows[i][22].ToString());//物料编号
                strsql.AppendFormat("'{0}',", dt.Rows[i][23].ToString());//单位
                strsql.AppendFormat("'{0}',", dt.Rows[i][24].ToString()); //店铺名字
                strsql.AppendFormat("'{0}',", dt.Rows[i][43].ToString()); //发货日期
                strsql.AppendFormat("'{0}',", dt.Rows[i][51].ToString()); //店铺地址码
               // decimal weight = 0;
                decimal proweight = GetWeightyMaterialNo(materialno, products);
                strsql.AppendFormat("'{0}',", proweight);//重量
                //if (spec.IndexOf("J") > 0)
                //{
                //    weight = Convert.ToDecimal(spec.Substring(0, spec.IndexOf('J')));
                //    weight = weight / 2;
                //    strsql.AppendFormat("'{0}',", weight);//重量
                //}
                //else
                //{
                //    strsql.AppendFormat("'{0}',", dt.Rows[i][8].ToString());//如果规格不是J，则重量直接默认为订单个数
                //}
                
                strsql.AppendFormat("'{0}',", dt.Rows[i][17].ToString());//店铺编号
                strsql.AppendFormat("'{0}',", dt.Rows[i][79].ToString());// 交货单号
                strsql.AppendFormat("'{0}',", dt.Rows[i][80].ToString());//地址

                strsql.AppendFormat("'{0}',", deviceid);//设备ID
                strsql.AppendFormat("'{0}'", 0); //状态
                strsql.Append(")");
                sqllist.Add(strsql.ToString());
            }
            int k = DbHelperMySQL.ExecuteSqlTran(sqllist);
            return k;
        }


        /// <summary>
        /// 插入生产订单
        /// </summary>
        /// <param name="goods"></param>
        /// <returns></returns>
        public static int Insert(List<RFC_BaseObj> goods)
        {
            if (goods == null)
            {
                return 0;
            }
            List<string> sqllist = new List<string>();
            string sqlStr = string.Empty;
            for (int i = 0; i < goods.Count; i++)
            {
                RFC_AUFNR aufnr = goods[i] as RFC_AUFNR;
                string materialno = aufnr.MATNR;
                string mobiletype = aufnr.BWART;
                string stockaddr = aufnr.LGORT;
                string factory = aufnr.WERKS;
                int basicnum = (int)aufnr.SBMNG;
                string basicunit = aufnr.SBMEH;
                string orderno = aufnr.AUFNR;
                DataSet ds = BLL.ims_product_order.GetList(" orderno ='" + orderno + "' ");
                StringBuilder strsql = new StringBuilder();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    strsql.Append("update ims_product_order set ");
                    strsql.AppendFormat("completedate ='{0}',", aufnr.GLTRP);
                    strsql.AppendFormat("materialno ='{0}',", materialno);
                    strsql.AppendFormat("mobiletype ='{0}',", mobiletype);
                    strsql.AppendFormat("stockaddr ='{0}',", stockaddr);
                    strsql.AppendFormat("factory ='{0}',", factory);
                    strsql.AppendFormat("basicnum ='{0}',", basicnum);
                    strsql.AppendFormat("basicunit ='{0}'", basicunit);
                    strsql.AppendFormat(" where orderno ='{0}' ", orderno);
                    sqllist.Add(strsql.ToString());
                }
                else
                {
                    strsql.Append("insert into ims_product_order (orderno,completedate,materialno,mobiletype,stockaddr,factory,");
                    strsql.Append("basicnum,basicunit,scannum) values ");
                    strsql.Append("(");
                    strsql.AppendFormat("'{0}',", orderno);//凭证
                    strsql.AppendFormat("'{0}',", aufnr.GLTRP);//订单描述
                    strsql.AppendFormat("'{0}',", materialno);//凭证日期
                    strsql.AppendFormat("'{0}',", mobiletype); //个数
                    strsql.AppendFormat("'{0}',", stockaddr); //出货日期
                    strsql.AppendFormat("'{0}',", factory);//物料编号
                    strsql.AppendFormat("'{0}',", basicnum);//单位
                    strsql.AppendFormat("'{0}',", basicunit); //店铺名字
                    strsql.AppendFormat("'{0}'", 0); //发货日期
                    strsql.Append(")");
                    sqllist.Add(strsql.ToString());
                }
            }
            int k = DbHelperMySQL.ExecuteSqlTran(sqllist);
            return k;
        }

        public static int UpdateProductOrder(List<RFC_BaseObj> goods)
        {
            if (goods == null)
            {
                return 0;
            }
            List<string> sqllist = new List<string>();
            string sqlStr = string.Empty;
            for (int i = 0; i < goods.Count; i++)
            {
                RFC_AUFNR_OUT aufnr = goods[i] as RFC_AUFNR_OUT;
                if (string.IsNullOrEmpty(aufnr.MBLNR) || !string.IsNullOrEmpty(aufnr.Error))
                {
                    continue;
                }
                StringBuilder strsql = new StringBuilder();
                string orderno = aufnr.AUFNR;
                strsql.Append("update ims_product_order set ");
                strsql.AppendFormat("sapverifyno ='{0}',", aufnr.MBLNR);
                strsql.AppendFormat("sapverifytime ='{0}' ", aufnr.MJAHR);
                strsql.AppendFormat(" where orderno ='{0}' ", orderno);
                sqllist.Add(strsql.ToString());
            }

            int k = DbHelperMySQL.ExecuteSqlTran(sqllist);
            return k;
        }

        public static int InsertExchangeOrder(List<RFC_BaseObj> goods)
        {
            if (goods == null)
            {
                return 0;
            }
            IList<Model.ims_dict_product> products = BLL.ims_dict_product.GetAllList();
            List<string> sqllist = new List<string>();
            string sqlStr = string.Empty;
            for (int i = 0; i < goods.Count; i++)
            {

                RFC_EBELN ebeln = goods[i] as RFC_EBELN;
                string productname = ebeln.MAKTX;
                if (string.IsNullOrEmpty(productname))
                {
                    continue;
                }
                string materialno = ebeln.MATNR;
                string spec = ebeln.VRKME;
                int deviceid = GetDeviceIDByMaterialNo(materialno, products);
                StringBuilder strsql = new StringBuilder();
                strsql.Append("insert into ims_orders (orderno,orderexplain,verifydate,verifynum,exchangedate,");
                strsql.Append("materialno,unit,shopname,expressdate,shopaddrno,weight,shopno,exchangeno,exchangeaddr,deviceid,state,posnr) values ");
                strsql.Append("(");
                strsql.AppendFormat("'{0}',", "0");//凭证
                strsql.AppendFormat("'{0}',", ebeln.MAKTX);//订单描述
                strsql.AppendFormat("'{0}',", ebeln.BLDAT);//凭证日期
                strsql.AppendFormat("'{0}',", ebeln.LFIMG); //个数
                strsql.AppendFormat("'{0}',", ebeln.LFDAT); //出货日期
                strsql.AppendFormat("'{0}',", ebeln.MATNR);//物料编号
                strsql.AppendFormat("'{0}',", ebeln.VRKME);//单位
                strsql.AppendFormat("'{0}',", ebeln.NAME1); //店铺名字
                strsql.AppendFormat("'{0}',", ebeln.WADAT); //发货日期
                strsql.AppendFormat("'{0}',", ""); //店铺地址码
                // decimal weight = 0;
                decimal proweight = GetWeightyMaterialNo(materialno, products);
                strsql.AppendFormat("'{0}',", proweight);//重量
                strsql.AppendFormat("'{0}',", ebeln.KUNNR);//店铺编号
                strsql.AppendFormat("'{0}',", ebeln.EBELN);// 交货单号
                strsql.AppendFormat("'{0}',", ebeln.STREET);//地址

                strsql.AppendFormat("'{0}',", deviceid);//设备ID
                strsql.AppendFormat("'{0}',", 0); //状态
                strsql.AppendFormat("'{0}'", ebeln.POSNR);
                strsql.Append(")");
                sqllist.Add(strsql.ToString());
            }
            int k = DbHelperMySQL.ExecuteSqlTran(sqllist);
            return k;
        }


        public static int GetDeviceIDByMaterialNo(string materialno, IList<Model.ims_dict_product> products)
        {
            foreach (Model.ims_dict_product item in products)
            {
                if (item.Materialno.ToString() == materialno)
                {
                    return item.Deviceid;
                }
            }
            return 0;
        }

        public static decimal GetWeightyMaterialNo(string materialno, IList<Model.ims_dict_product> products)
        {
            foreach (Model.ims_dict_product item in products)
            {
                if (item.Materialno.ToString() == materialno)
                {
                    return item.Weight;
                }
            }
            return 0;
        }
        /// <summary>
        /// 给订单分配设备
        /// </summary>
        /// <param name="orderid"></param>
        /// <param name="deviceid"></param>
        /// <returns></returns>
        public static bool SceduleOrder(int orderid, int deviceid)
        {
            try
            {
                Model.ims_orders morder = BLL.ims_orders.SelectById(orderid);
                if (morder == null)
                {
                    return false;
                }
                morder.Deviceid = deviceid;
                if (!BLL.ims_orders.Update(morder))
                {
                    return false;
                }
                Model.ims_schedule temp = BLL.ims_schedule.SelectModelByWhereOrderByID(" order_id = " + orderid, true);
                if (temp == null)
                {
                    temp = new JueWei.Model.ims_schedule();
                    temp.Dev_id = deviceid;
                    temp.State = 0;
                    temp.Createdate = DateTime.Now;
                    temp.Expiredate = temp.Createdate.AddDays(1);
                    temp.Order_id = orderid;
                    if (!BLL.ims_schedule.Insert(temp))
                    {
                        return false;
                    }
                }
                else
                {
                    temp.Order_id = orderid;
                    if (!BLL.ims_schedule.Update(temp))
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(OrderBussiness), ex);
                return false;
            }

            return true;
        }


        public static void DeleteAllOrder()
        {
            string sql = "delete from ims_orders";
            DbHelperMySQL.ExecuteSql(sql);
        }
    }
}
