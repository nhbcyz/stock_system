namespace JueWei.Main
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows.Forms;
    using JueWei.Main;

    public class GlobalControl
    {
        public Dictionary<string, Model.ims_acl_function> FunctionDict = new Dictionary<string, Model.ims_acl_function>();
        public string gAppMsgboxTitle = string.Empty;
        public string gAppUnit = string.Empty;
        public string gAppWholeName = "";
        public string Login_Name_Key = "WareHouseMis_LoginName";
        public Model.ims_users LoginInfo = null;
        public MainFrm MainDialog;
       // public List<WHC.WareHouseMis.Entity.CListItem> ManagedWareHouse = new List<WHC.WareHouseMis.Entity.CListItem>();

        public void About()
        {
          //  new AboutBox { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
        }

        //public List<WHC.WareHouseMis.Entity.CListItem> GetWareHouse(UserInfo loginInfo)
        //{
        //    return null;
        //    //List<WHC.WareHouseMis.Entity.CListItem> list = new List<WHC.WareHouseMis.Entity.CListItem>();
        //    //List<WareHouseInfo> mangedList = new List<WareHouseInfo>();
        //    //List<RoleInfo> rolesByUser = WHC.Security.BLL.BLLFactory<Role>.Instance.GetRolesByUser(loginInfo.ID);
        //    //bool flag = false;
        //    //foreach (RoleInfo info in rolesByUser)
        //    //{
        //    //    if (((info.Name == "组长") || (info.Name == "管理员")) || (info.Name == "普通用户"))
        //    //    {
        //    //        flag = true;
        //    //    }
        //    //}
        //    //if (flag)
        //    //{
        //    //    mangedList = WHC.WareHouseMis.BLL.BLLFactory<WareHouse>.Instance.GetAll();
        //    //    foreach (WareHouseInfo info2 in mangedList)
        //    //    {
        //    //        list.Add(new WHC.WareHouseMis.Entity.CListItem(info2.Name, info2.Name));
        //    //    }
        //    //    return list;
        //    //}
        //    //mangedList = WHC.WareHouseMis.BLL.BLLFactory<WareHouse>.Instance.GetMangedList(loginInfo.Name);
        //    //foreach (WareHouseInfo info2 in mangedList)
        //    //{
        //    //    list.Add(new WHC.WareHouseMis.Entity.CListItem(info2.Name, info2.Name));
        //    //}
        //    //return list;
        //}

        public bool HasFunction(string controlID)
        {
            bool flag = false;
            if (this.FunctionDict.ContainsKey(controlID))
            {
                flag = true;
            }
            return flag;
        }

        public void Help()
        {
            //try
            //{
            //    Process.Start("Help.chm");
            //}
            //catch (Exception)
            //{
            //    MessageExUtil.ShowWarning("文件打开失败");
            //}
        }

        public void Notify(string caption, string content)
        {
            this.Notify(caption, content, 400, 200, 0x1388);
        }

        public void Notify(string caption, string content, int width, int height, int waitTime)
        {
            NotifyWindow window = new NotifyWindow(caption, content);
            window.TitleClicked += new EventHandler(this.notifyWindowClick);
            window.TextClicked += new EventHandler(this.notifyWindowClick);
            window.SetDimensions(width, height);
            window.WaitTime = waitTime;
            window.Notify();
        }

        private void notifyWindowClick(object sender, EventArgs e)
        {

        }

        public void Quit()
        {
            if (Portal.gc.MainDialog != null)
            {
                Portal.gc.MainDialog.Hide();
            }
            Model.ims_users loginInfo = Portal.gc.LoginInfo;
            if (loginInfo != null)
            {
                Model.ims_logon_log info2 = new Model.ims_logon_log
                {
                    Logonname = loginInfo.Username,
                    Realname = loginInfo.Fullname,
                    Userid = loginInfo.Id,
                    Note = "用户退出"
                };
                BLL.ims_logon_log.Insert(info2);
            }
            Application.Exit();
        }
    }
}

