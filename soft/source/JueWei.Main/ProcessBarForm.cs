﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JueWei.Main
{
    public partial class ProcessBarForm : Form
    {
        int processPercent = 0;
        string message = "";
        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                //this.labelX1.Text = message;
            }
        }
        public int ProcessPercent
        {
            get { return processPercent; }
            set
            {
                processPercent = value;
                //if (processPercent >= 100)
                //    this.Close();
                //this.progressBarX1.Value = processPercent;
            }
        }
        
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        public ProcessBarForm()
        {
            InitializeComponent();
            timer.Interval = 100;
            timer.Tick += new EventHandler(timer_Tick);

            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (this.progressBarX1.Value < this.progressBarX1.Maximum)
            {
                this.progressBarX1.PerformStep();
            }
        }

        public void Stop()
        {
            timer.Stop();
            this.progressBarX1.Value = this.progressBarX1.Maximum;
           // MessageBox.Show("完成");
            this.Close();
        }


        /// <summary>
        /// 更新进度
        /// </summary>
        /// <param name="percent">进度,小于等于100</param>
        /// <param name="message">消息</param>
        public void ShowProcess(int percent, string message)
        {
            this.Show();
            this.ProcessPercent = percent;
            this.Message = message;
            this.progressBarX1.Refresh();
        }
        public void UpdateData(int percent, string message)
        {
            this.ProcessPercent = percent;
            this.Message = message;
        }

        private void ProcessBarForm_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
        }
        private void ProcessBarForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Cursor = Cursors.Default;
        }
    }
}
