namespace JueWei.Main
{
    partial class ProductItemEditFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.tb_materialno = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_productname = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_spec = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_weight = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_unit = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btn_ok = new DevComponents.DotNetBar.ButtonX();
            this.btn_cancel = new DevComponents.DotNetBar.ButtonX();
            this.tb_shortname = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(130, 6);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "物料编号";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(130, 50);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "产品名称";
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(130, 121);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "规格";
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(130, 162);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 3;
            this.labelX4.Text = "重量";
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(130, 208);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(75, 23);
            this.labelX5.TabIndex = 4;
            this.labelX5.Text = "单位";
            // 
            // tb_materialno
            // 
            // 
            // 
            // 
            this.tb_materialno.Border.Class = "TextBoxBorder";
            this.tb_materialno.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_materialno.Location = new System.Drawing.Point(222, 6);
            this.tb_materialno.Name = "tb_materialno";
            this.tb_materialno.PreventEnterBeep = true;
            this.tb_materialno.ReadOnly = true;
            this.tb_materialno.Size = new System.Drawing.Size(111, 21);
            this.tb_materialno.TabIndex = 5;
            // 
            // tb_productname
            // 
            // 
            // 
            // 
            this.tb_productname.Border.Class = "TextBoxBorder";
            this.tb_productname.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_productname.Location = new System.Drawing.Point(222, 50);
            this.tb_productname.Name = "tb_productname";
            this.tb_productname.PreventEnterBeep = true;
            this.tb_productname.Size = new System.Drawing.Size(111, 21);
            this.tb_productname.TabIndex = 6;
            // 
            // tb_spec
            // 
            // 
            // 
            // 
            this.tb_spec.Border.Class = "TextBoxBorder";
            this.tb_spec.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_spec.Location = new System.Drawing.Point(222, 121);
            this.tb_spec.Name = "tb_spec";
            this.tb_spec.PreventEnterBeep = true;
            this.tb_spec.Size = new System.Drawing.Size(111, 21);
            this.tb_spec.TabIndex = 7;
            // 
            // tb_weight
            // 
            // 
            // 
            // 
            this.tb_weight.Border.Class = "TextBoxBorder";
            this.tb_weight.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_weight.Location = new System.Drawing.Point(222, 162);
            this.tb_weight.Name = "tb_weight";
            this.tb_weight.PreventEnterBeep = true;
            this.tb_weight.Size = new System.Drawing.Size(111, 21);
            this.tb_weight.TabIndex = 8;
            // 
            // tb_unit
            // 
            // 
            // 
            // 
            this.tb_unit.Border.Class = "TextBoxBorder";
            this.tb_unit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_unit.Location = new System.Drawing.Point(222, 208);
            this.tb_unit.Name = "tb_unit";
            this.tb_unit.PreventEnterBeep = true;
            this.tb_unit.Size = new System.Drawing.Size(111, 21);
            this.tb_unit.TabIndex = 9;
            // 
            // btn_ok
            // 
            this.btn_ok.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ok.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_ok.Location = new System.Drawing.Point(263, 277);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ok.TabIndex = 10;
            this.btn_ok.Text = "确定";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Location = new System.Drawing.Point(392, 277);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cancel.TabIndex = 11;
            this.btn_cancel.Text = "取消";
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // tb_shortname
            // 
            // 
            // 
            // 
            this.tb_shortname.Border.Class = "TextBoxBorder";
            this.tb_shortname.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_shortname.Location = new System.Drawing.Point(222, 85);
            this.tb_shortname.Name = "tb_shortname";
            this.tb_shortname.PreventEnterBeep = true;
            this.tb_shortname.Size = new System.Drawing.Size(111, 21);
            this.tb_shortname.TabIndex = 13;
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(130, 85);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(75, 23);
            this.labelX6.TabIndex = 12;
            this.labelX6.Text = "简写名";
            // 
            // ProductItemEditFrm
            // 
            this.AcceptButton = this.btn_ok;
            this.CancelButton = this.btn_cancel;
            this.ClientSize = new System.Drawing.Size(523, 328);
            this.Controls.Add(this.tb_shortname);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.tb_unit);
            this.Controls.Add(this.tb_weight);
            this.Controls.Add(this.tb_spec);
            this.Controls.Add(this.tb_productname);
            this.Controls.Add(this.tb_materialno);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Name = "ProductItemEditFrm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "编辑商品";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_materialno;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_productname;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_spec;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_weight;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_unit;
        private DevComponents.DotNetBar.ButtonX btn_ok;
        private DevComponents.DotNetBar.ButtonX btn_cancel;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_shortname;
        private DevComponents.DotNetBar.LabelX labelX6;
    }
}