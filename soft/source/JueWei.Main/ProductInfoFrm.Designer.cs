﻿namespace JueWei.Main
{
    partial class ProductInfoFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_delete = new DevComponents.DotNetBar.ButtonX();
            this.btn_edit = new DevComponents.DotNetBar.ButtonX();
            this.btn_import = new DevComponents.DotNetBar.ButtonX();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.qr_picbox = new System.Windows.Forms.PictureBox();
            this.qr_size_slider = new DevComponents.DotNetBar.Controls.Slider();
            this.btn_gen_qr = new DevComponents.DotNetBar.ButtonX();
            this.qr_version_slide = new DevComponents.DotNetBar.Controls.Slider();
            this.tbx_qrdata = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.pager1 = new JueWei.Main.Controls.Pager();
            this.btn_batch_gen_qr = new DevComponents.DotNetBar.ButtonX();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qr_picbox)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btn_batch_gen_qr);
            this.splitContainer1.Panel1.Controls.Add(this.btn_gen_qr);
            this.splitContainer1.Panel1.Controls.Add(this.btn_delete);
            this.splitContainer1.Panel1.Controls.Add(this.btn_edit);
            this.splitContainer1.Panel1.Controls.Add(this.btn_import);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(887, 341);
            this.splitContainer1.SplitterDistance = 37;
            this.splitContainer1.TabIndex = 0;
            // 
            // btn_delete
            // 
            this.btn_delete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_delete.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_delete.Location = new System.Drawing.Point(225, 3);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_delete.TabIndex = 5;
            this.btn_delete.Text = "删除";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_edit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_edit.Location = new System.Drawing.Point(117, 3);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(75, 23);
            this.btn_edit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_edit.TabIndex = 4;
            this.btn_edit.Text = "编辑";
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // btn_import
            // 
            this.btn_import.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_import.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_import.Location = new System.Drawing.Point(12, 3);
            this.btn_import.Name = "btn_import";
            this.btn_import.Size = new System.Drawing.Size(75, 23);
            this.btn_import.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_import.TabIndex = 3;
            this.btn_import.Text = "导入";
            this.btn_import.Click += new System.EventHandler(this.btn_import_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridViewX1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.pager1);
            this.splitContainer2.Size = new System.Drawing.Size(579, 300);
            this.splitContainer2.SplitterDistance = 251;
            this.splitContainer2.TabIndex = 1;
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.materialNo,
            this.productname,
            this.spec,
            this.weight,
            this.unit});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.RowTemplate.Height = 23;
            this.dataGridViewX1.Size = new System.Drawing.Size(579, 251);
            this.dataGridViewX1.TabIndex = 1;
            this.dataGridViewX1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            this.dataGridViewX1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "编号";
            this.id.Name = "id";
            // 
            // materialNo
            // 
            this.materialNo.DataPropertyName = "materialNo";
            this.materialNo.HeaderText = "物料编号";
            this.materialNo.Name = "materialNo";
            // 
            // productname
            // 
            this.productname.DataPropertyName = "productname";
            this.productname.HeaderText = "产品名字";
            this.productname.Name = "productname";
            // 
            // spec
            // 
            this.spec.DataPropertyName = "spec";
            this.spec.HeaderText = "规格";
            this.spec.Name = "spec";
            // 
            // weight
            // 
            this.weight.DataPropertyName = "weight";
            this.weight.HeaderText = "重量";
            this.weight.Name = "weight";
            // 
            // unit
            // 
            this.unit.DataPropertyName = "unit";
            this.unit.HeaderText = "单位";
            this.unit.Name = "unit";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.tbx_qrdata);
            this.splitContainer3.Panel2.Controls.Add(this.qr_version_slide);
            this.splitContainer3.Panel2.Controls.Add(this.qr_size_slider);
            this.splitContainer3.Panel2.Controls.Add(this.qr_picbox);
            this.splitContainer3.Panel2MinSize = 60;
            this.splitContainer3.Size = new System.Drawing.Size(887, 300);
            this.splitContainer3.SplitterDistance = 579;
            this.splitContainer3.TabIndex = 2;
            // 
            // qr_picbox
            // 
            this.qr_picbox.Location = new System.Drawing.Point(18, 75);
            this.qr_picbox.Name = "qr_picbox";
            this.qr_picbox.Size = new System.Drawing.Size(263, 191);
            this.qr_picbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.qr_picbox.TabIndex = 0;
            this.qr_picbox.TabStop = false;
            // 
            // qr_size_slider
            // 
            // 
            // 
            // 
            this.qr_size_slider.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.qr_size_slider.LabelWidth = 70;
            this.qr_size_slider.Location = new System.Drawing.Point(29, 3);
            this.qr_size_slider.Maximum = 60;
            this.qr_size_slider.Minimum = 40;
            this.qr_size_slider.Name = "qr_size_slider";
            this.qr_size_slider.Size = new System.Drawing.Size(238, 23);
            this.qr_size_slider.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.qr_size_slider.TabIndex = 1;
            this.qr_size_slider.Text = "二维码规模";
            this.qr_size_slider.Value = 40;
            this.qr_size_slider.ValueChanged += new System.EventHandler(this.qr_size_slider_ValueChanged);
            // 
            // btn_gen_qr
            // 
            this.btn_gen_qr.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_gen_qr.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_gen_qr.Location = new System.Drawing.Point(359, 4);
            this.btn_gen_qr.Name = "btn_gen_qr";
            this.btn_gen_qr.Size = new System.Drawing.Size(75, 23);
            this.btn_gen_qr.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_gen_qr.TabIndex = 6;
            this.btn_gen_qr.Text = "生成二维码";
            this.btn_gen_qr.Click += new System.EventHandler(this.btn_gen_qr_Click);
            // 
            // qr_version_slide
            // 
            // 
            // 
            // 
            this.qr_version_slide.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.qr_version_slide.LabelWidth = 70;
            this.qr_version_slide.Location = new System.Drawing.Point(29, 32);
            this.qr_version_slide.Maximum = 40;
            this.qr_version_slide.Minimum = 1;
            this.qr_version_slide.Name = "qr_version_slide";
            this.qr_version_slide.Size = new System.Drawing.Size(238, 23);
            this.qr_version_slide.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.qr_version_slide.TabIndex = 2;
            this.qr_version_slide.Text = "二维码版本";
            this.qr_version_slide.Value = 8;
            this.qr_version_slide.ValueChanged += new System.EventHandler(this.qr_version_slide_ValueChanged);
            // 
            // tbx_qrdata
            // 
            // 
            // 
            // 
            this.tbx_qrdata.Border.Class = "TextBoxBorder";
            this.tbx_qrdata.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbx_qrdata.Location = new System.Drawing.Point(29, 272);
            this.tbx_qrdata.Name = "tbx_qrdata";
            this.tbx_qrdata.PreventEnterBeep = true;
            this.tbx_qrdata.Size = new System.Drawing.Size(224, 21);
            this.tbx_qrdata.TabIndex = 3;
            // 
            // pager1
            // 
            this.pager1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pager1.Location = new System.Drawing.Point(0, 0);
            this.pager1.Name = "pager1";
            this.pager1.NMax = 0;
            this.pager1.PageCount = 0;
            this.pager1.PageCurrent = 0;
            this.pager1.PageSize = 50;
            this.pager1.Size = new System.Drawing.Size(579, 45);
            this.pager1.TabIndex = 0;
            // 
            // btn_batch_gen_qr
            // 
            this.btn_batch_gen_qr.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_batch_gen_qr.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_batch_gen_qr.Location = new System.Drawing.Point(470, 3);
            this.btn_batch_gen_qr.Name = "btn_batch_gen_qr";
            this.btn_batch_gen_qr.Size = new System.Drawing.Size(75, 23);
            this.btn_batch_gen_qr.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_batch_gen_qr.TabIndex = 7;
            this.btn_batch_gen_qr.Text = "批量生成";
            this.btn_batch_gen_qr.Click += new System.EventHandler(this.btn_batch_gen_qr_Click);
            // 
            // ProductInfoFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 341);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProductInfoFrm";
            this.ShowInTaskbar = false;
            this.Text = "ProductInfoFrm";
            this.Load += new System.EventHandler(this.ProductInfoFrm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.qr_picbox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private JueWei.Main.Controls.Pager pager1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn productname;
        private System.Windows.Forms.DataGridViewTextBoxColumn spec;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private DevComponents.DotNetBar.ButtonX btn_import;
        private DevComponents.DotNetBar.ButtonX btn_edit;
        private DevComponents.DotNetBar.ButtonX btn_delete;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.PictureBox qr_picbox;
        private DevComponents.DotNetBar.Controls.Slider qr_size_slider;
        private DevComponents.DotNetBar.ButtonX btn_gen_qr;
        private DevComponents.DotNetBar.Controls.Slider qr_version_slide;
        private DevComponents.DotNetBar.Controls.TextBoxX tbx_qrdata;
        private DevComponents.DotNetBar.ButtonX btn_batch_gen_qr;
    }
}