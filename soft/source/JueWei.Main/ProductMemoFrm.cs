using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Aspose.Cells;
using System.IO;
using JueWei.Common;
using JueWei.Main.Common;
using JueWei.Main.Controls;
using ThoughtWorks.QRCode.Codec;
using System.Drawing.Imaging;
using ThoughtWorks.QRCode.Codec.Data;
using WHC.OrderWater.Commons;
using JueWei.Bussiness;

namespace JueWei.Main
{
    public partial class ProductMemoFrm : BaseForm
    {
        private int _lasselectindex = 0;
        private bool _modifying = false;
      //  private string _qrpath = string.Empty;
        private BackgroundWorker worker = new BackgroundWorker();
        private DataSet _alldevices = null;
        public ProductMemoFrm()
        {
            InitializeComponent();
            //_qrpath = ConfigHelper.GetConfigString("DefaultQrPath");
            CheckQrPath();
            Init();
        }

        private void Init()
        {

        }


        private void ProductInfoFrm_Load(object sender, EventArgs e)
        {
            InitDevices();
            //初始化右键菜单
            InitContextMenu();

            this.pager1.PageCurrent = 1;
            this.pager1.PageSize = 30;

            this.pager1.NMax = BLL.ims_dict_product.GetRecordCount(""); ;

            //激活OnPageChanged事件
            pager1.EventPaging += new EventPagingHandler(pager1_EventPaging);
            this.pager1.Bind();
        }
        /// <summary>
        /// 页数变化时调用绑定数据方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private int pager1_EventPaging(EventPagingArg e)
        {
            return BindData();
        }
        private void BindPage()
        {
            this.pager1.PageCurrent = 1;
            
            this.pager1.Bind();
        }
        private int BindData()
        {
            int recordcount = 0;
            int pageindex = 1;
            if (this.pager1.PageCurrent != 0)
            {
                pageindex = this.pager1.PageCurrent;
            }
            DataSet ds = BLL.ims_dict_product.GetList(pageindex, this.pager1.PageSize, "", "", ref recordcount);
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = ds.Tables[0];
            if (dataGridViewX1.RowCount < _lasselectindex)
            {
                _lasselectindex = 0;
            }
            dataGridViewX1.Rows[_lasselectindex].Selected = true;

            return recordcount;
        }

        private void btn_import_Click(object sender, EventArgs e)
        {
          //  ImportFromExcel();
            
            if (ConfigHelper.GetConfigInt("DataSource") == 0)
            {
                ImportFromExcel();
            }
            else
            {
                ImportFromSAP();
            }
        }

        private void ImportFromSAP()
        {
            try
            {
                IRFCComm rfccomm = new RFCComm(ConfigHelper.GetConfigString("RFCHost"),
                                                ConfigHelper.GetConfigString("RFCSN"),
                                                ConfigHelper.GetConfigString("RFCUser"),
                                                EncodeHelper.DesDecrypt(ConfigHelper.GetConfigString("RFCPassword")),
                                                ConfigHelper.GetConfigString("RFCClient"));

                RFC_Comm_Obj_Out rfcdata = rfccomm.ZMM_GET_MATNR(ConfigHelper.GetConfigString("RFCFactoryNo"), DateTime.Now.ToString("yyyyMMdd"));
                if (rfcdata == null)
                {
                    MessageUtil.ShowTips("从SAP导入数据失败");
                    return;
                }
                if (!rfcdata.Error)
                {
                    MessageUtil.ShowTips(string.Format("从SAP导入数据失败,错误:{0}",rfcdata.ErrorMsg));
                    return;
                }
                if (rfcdata.Data == null)
                {
                    MessageUtil.ShowTips(string.Format("从SAP导入数据失败,错误:{0}", "SAP没有返回数据"));
                    return;
                }
                ProductBussiness.Update(rfcdata.Data);
                BindData();
                MessageUtil.ShowTips("导入成功");
                
            }
            catch (System.Exception ex)
            {
                MessageUtil.ShowTips(ex.Message);
            }


        }

        private void ImportFromExcel()
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.RestoreDirectory = false;
                openFileDialog.InitialDirectory = ConfigHelper.GetConfigString("LastImportPath");
                openFileDialog.Filter = "excel 03文件|*.xls|excel 07文件|*.xlsx";

                openFileDialog.FilterIndex = 1;
                string fName = string.Empty;
                if (openFileDialog.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                fName = openFileDialog.FileName;
                if (string.IsNullOrEmpty(fName))
                {
                    return;
                }
                FileInfo f = new FileInfo(fName);
                ConfigHelper.SetAppSetting("LastImportPath", Path.GetDirectoryName(fName), AppCommon.Instance._Config);

                Workbook workbook = new Workbook(fName);

                Cells cells = workbook.Worksheets[0].Cells;

                DataTable dt = cells.ExportDataTableAsString(1, 0, cells.MaxDataRow, cells.MaxColumn + 1, false);

                int k = ProductBussiness.Update(dt);
                BindData();
                if (k <= 0)
                {
                    MessageUtil.ShowTips("导入失败");
                }
                else
                {
                    MessageUtil.ShowTips("导入成功");
                } 
            }
            catch (System.Exception ex)
            {
                MessageUtil.ShowError("导入失败");
            }

        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void EditData()
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            Model.ims_dict_product mpro = BLL.ims_dict_product.SelectById(int.Parse(id));

            if (mpro == null)
            {
                MessageBox.Show("获取数据失败");
                return;
            }
            ProductItemEditFrm frm = new ProductItemEditFrm(mpro, false);
            frm.ShowDialog();
            _modifying = true;
            BindData();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (!_modifying)
            {
                _modifying = false;
                if (dataGridViewX1.CurrentRow != null)
                {
                    _lasselectindex = dataGridViewX1.CurrentRow.Index;
                }
                
            }
            ShowQrCode();
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            _lasselectindex = dataGridViewX1.CurrentRow.Index;
            EditData();
        }

        private void ShowQrCode()
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }
            string materalid = dataGridViewX1.CurrentRow.Cells[1].Value.ToString();
            string productname = dataGridViewX1.CurrentRow.Cells[2].Value.ToString();
            CheckQrPath();
            string qrpath = ConfigHelper.GetConfigString("DefaultQrPath") + GenImgName(materalid, productname);
            if (File.Exists(qrpath))
            {
                this.qr_picbox.ImageLocation = qrpath;
                this.tbx_qrdata.Text = CodeDecoder(qrpath);
            }
            else {
                this.qr_picbox.ImageLocation = "";
                this.tbx_qrdata.Text = "";
            }
            
        }

        /// <summary>
        /// 生成二维码事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_gen_qr_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }
            Gen_Qr_Img();
        }

        private void Gen_Qr_Img()
        {
            string materalid = dataGridViewX1.CurrentRow.Cells[1].Value.ToString();
            string spec = dataGridViewX1.CurrentRow.Cells[3].Value.ToString();
            string weight = dataGridViewX1.CurrentRow.Cells[4].Value.ToString();
            string unit = dataGridViewX1.CurrentRow.Cells[5].Value.ToString();

            Gen_Qr_Img(GenQrData(materalid, spec, weight, unit), materalid, dataGridViewX1.CurrentRow.Cells[2].Value.ToString(),true);
            
        }
        private void Gen_Qr_Img(string qrdata, string materalid,string prodcutname,bool showqrdata)
        {
            //获取生成二维码的参数（规模、版本、链接地址）
            int size = Convert.ToInt32(this.qr_version_slide.Value);
            int version = Convert.ToInt32(this.qr_version_slide.Value);
            if (showqrdata)
            {
                this.tbx_qrdata.Text = qrdata;
            }
            
            //调用生成二维码图片的事件，并把图片显示在图片框中
            Image img = QrCodeBitmap(qrdata, size, version);
            SaveQrImage(img,materalid,prodcutname);
        }

        private void Gen_Qr_ImgNotShow(string qrdata, string materalid, string prodcutname, bool showqrdata)
        {
            //获取生成二维码的参数（规模、版本、链接地址）
            int size = Convert.ToInt32(this.qr_version_slide.Value);
            int version = Convert.ToInt32(this.qr_version_slide.Value);
            if (showqrdata)
            {
                this.tbx_qrdata.Text = qrdata;
            }

            //调用生成二维码图片的事件，并把图片显示在图片框中
            Image img = QrCodeBitmap(qrdata, size, version);
            SaveQrImageNotShow(img, materalid, prodcutname);
        }
        /// <summary>
        /// 生成二维码位图
        /// </summary>
        /// <param name="link">用于生成二维码的链接</param>4
        /// <param name="size">二维码规模大小</param>
        /// <param name="version">二维码版本</param>
        /// <returns>二维码图片对象</returns>
        private Image QrCodeBitmap(string link, int size, int version)
        {
            QRCodeEncoder qrcodeencoder = new QRCodeEncoder();//生成设置编码实例
            qrcodeencoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;//字节编码
            qrcodeencoder.QRCodeScale = size;//二维码规模大小
            qrcodeencoder.QRCodeVersion = version;//二维码版本
            qrcodeencoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;//设置错误校验（错误更正）的级别：中等M
            Image img = qrcodeencoder.Encode(link);//生成二维码image图片
            return img;
        }

        private void qr_size_slider_ValueChanged(object sender, EventArgs e)
        {
            Gen_Qr_Img();
        }

        private void qr_version_slide_ValueChanged(object sender, EventArgs e)
        {
            Gen_Qr_Img();
        }

        //private void SaveQrImage(Image img)
        //{
        //    try
        //    {
        //        if (dataGridViewX1.CurrentRow == null)
        //        {
        //            return;
        //        }
        //        if (img == null)
        //        {
        //            return;
        //        }
        //        CheckQrPath();
        //        string materalid = dataGridViewX1.CurrentRow.Cells[1].Value.ToString();
        //        string productname = dataGridViewX1.CurrentRow.Cells[2].Value.ToString();

        //        string qrpath = _qrpath + GenImgName(materalid, productname);

        //        img.Save(qrpath, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        img.Dispose();
        //        ShowQrCode();
        //    }
        //    catch (Exception ex)
        //    {
        //        LogHelper.WriteLog(typeof(ProductInfoFrm), ex);
        //    }

        //}
        private void SaveQrImage(Image img, string materalid, string productname)
        {
            try
            {
                if (dataGridViewX1.CurrentRow == null)
                {
                    return;
                }
                if (img == null)
                {
                    return;
                }
                CheckQrPath();
                string qrpath = ConfigHelper.GetConfigString("DefaultQrPath") + GenImgName(materalid, productname);

                img.Save(qrpath, System.Drawing.Imaging.ImageFormat.Jpeg);
                img.Dispose();
                ShowQrCode();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowTips("生成二维码出错,请重新生成");
                LogHelper.Error(typeof(ProductMemoFrm), ex);
            }

        }
        private void SaveQrImageNotShow(Image img, string materalid, string productname)
        {
            try
            {
                if (dataGridViewX1.CurrentRow == null)
                {
                    return;
                }
                if (img == null)
                {
                    return;
                }
                CheckQrPath();
                string qrpath = ConfigHelper.GetConfigString("DefaultQrPath") + GenImgName(materalid, productname);

                img.Save(qrpath, System.Drawing.Imaging.ImageFormat.Jpeg);
                img.Dispose();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowTips("生成二维码出错,请重新生成");
                LogHelper.Error(typeof(ProductMemoFrm), ex);
            }

        }

        private string GenImgName(string materialNo, string productname)
        {
            return materialNo + "_" + productname + ".jpg";
        }

        private string GenQrData(string materialno, string spec, string weight, string unit)
        {
            return string.Format("{0},{1},{2},{3}", materialno, spec, weight, unit);
        }
        private void CheckQrPath()
        {
            if (!Directory.Exists(ConfigHelper.GetConfigString("DefaultQrPath")))
            {
                Directory.CreateDirectory(ConfigHelper.GetConfigString("DefaultQrPath"));
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }

            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            if (!BLL.ims_dict_product.Delete(int.Parse(id)))
            {
                MessageBox.Show("删除失败");
                return;
            }
            BindData();
        }

        /// <summary>
        /// 二维码解码
        /// </summary>
        /// <param name="filePath">图片路径</param>
        /// <returns></returns>
        public string CodeDecoder(string filePath)
        {
            if (!System.IO.File.Exists(filePath))
                return null;
            try
            {
                Bitmap myBitmap = new Bitmap(Image.FromFile(filePath));
                QRCodeDecoder decoder = new QRCodeDecoder();
                string decodedString = decoder.decode(new QRCodeBitmapImage(myBitmap));
                return decodedString;
            }
            catch (System.Exception ex)
            {
            	
            }
            return "";
        }

        private ProcessBarForm frm = null;
        private void btn_batch_gen_qr_Click(object sender, EventArgs e)
        {
            frm = new ProcessBarForm();
            string text = "正在生成,当前进度{0}%";
            frm.ShowProcess(0, string.Format(text, 0));
            Control.CheckForIllegalCrossThreadCalls = false;
            worker.WorkerReportsProgress = true;
            worker.DoWork += new DoWorkEventHandler(Batch_Gen_Qr);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.RunWorkerAsync();
            

        }
        private void Batch_Gen_Qr(object sender, DoWorkEventArgs e)
        {
            this.qr_picbox.ImageLocation = "";
            IList<Model.ims_dict_product> products = BLL.ims_dict_product.GetAllList();
            if (products == null || products.Count <= 0)
            {
                return;
            }
            string text = "正在生成,当前进度{0}%";
            
            for (int i = 0; i < products.Count; i++)
            {
                Gen_Qr_ImgNotShow(GenQrData(products[i].Materialno.ToString(), products[i].Spec, products[i].Weight.ToString(), products[i].Unit), products[i].Materialno.ToString(), products[i].Productname, false);
                frm.UpdateData(0, string.Format(text, i / products.Count));
            }
            

           
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            frm.Stop();
            ShowQrCode();
            Control.CheckForIllegalCrossThreadCalls = true;
            MessageUtil.ShowTips("批量生成二维码成功");
        }


        private void InitContextMenu()
        {
            if (_alldevices == null || _alldevices.Tables[0].Rows.Count <= 0)
            {
                return;
            }

            foreach (DataRow dr in _alldevices.Tables[0].Rows)
            {
                ToolStripMenuItem tsmi = new System.Windows.Forms.ToolStripMenuItem();
                tsmi.Name = "tsmi_device_" + dr["id"];
                tsmi.Size = new System.Drawing.Size(152, 22);
                tsmi.Text = dr["devicename"].ToString();
                tsmi.Tag = dr["id"].ToString();
                tsmi.Click += new EventHandler(tsmi_Click);
                this.tsmi_alloc.DropDownItems.Add(tsmi);
            }
        }
        private void tsmi_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.SelectedRows.Count <= 0)
            {
                MessageUtil.ShowTips("请先选择要分配的数据");
                return;
            }
            int nsucccount = 0, nfailedcount = 0; //成功更新条数

            ToolStripMenuItem tsmi = sender as ToolStripMenuItem;
            if (tsmi != null && tsmi.Tag != null)
            {
                int deviceid = Convert.ToInt32(tsmi.Tag.ToString());
                if (deviceid > 0)
                {
                    for (int i = 0; i < dataGridViewX1.SelectedRows.Count; i++)
                    {
                        DataGridViewRow row = dataGridViewX1.SelectedRows[i];
                        int productid = Convert.ToInt32(row.Cells[0].Value);
                        //业务层分配订单
                        if (ProductBussiness.SceduleToDevice(productid, deviceid))
                        {
                            nsucccount++;
                        }
                        else { nfailedcount++; }
                    }
                }
            }
            this.pager1.PageCount = BLL.ims_orders.GetRecordCount("");
            this.pager1.Bind();
            MessageUtil.ShowTips(string.Format("分配成功,分配结果,选中{0}条,成功{1}条,失败{2}条", dataGridViewX1.SelectedRows.Count, nsucccount, nfailedcount));
        }

        private void InitDevices()
        {
            _alldevices = BLL.ims_devices.GetList(" devicestate <> 1 and devicetype <> 2");
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (deviceid.Index == e.ColumnIndex)
            {
                if (_alldevices != null && e.Value != null &&!string.IsNullOrEmpty(e.Value.ToString()))
                {
                    int thedeviceid = Convert.ToInt32(e.Value);
                    if (thedeviceid != 0)
                    {
                        DataRow[] drs = _alldevices.Tables[0].Select(" id=" + thedeviceid);
                        if (drs != null && drs.Length == 1)
                        {
                            e.Value = drs[0]["devicename"].ToString();
                            return;
                        }
                    }
                }
                e.Value = "未关联";
            }
            
        }
    }
}
