﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Data.SQLite;
using JueWei.Common;

namespace JueWei.PdaMain
{
    class AppCommon
    {
        private static AppCommon _instance = null;

        private ConfigData AppData = new ConfigData();
        private string _configFile = "";
        private string _dbfile = "";
        private AppCommon()
        {
            string aurl = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase.ToString()) + "\\";
            _configFile = aurl + "app.xml";
            object o = MyXmlSerializer.LoadFromXml(_configFile, typeof(ConfigData));
            if (o != null)
            {
                AppData = o as ConfigData;  
            }
            _dbfile = aurl + GetAppConfig("dbpath");
        }

        public string GetAppConfig(string key)
        {
            string val = "";
            if (AppData == null)
            {
                return "";
            }
            for (int i = 0; i < AppData.Items.Count; i++)
            {
                if (AppData.Items[i].key == key)
                {
                    val = AppData.Items[i].value;
                    break;
                }
            }
            return val;
        }

        public void SetAppConfig(string key, string value)
        {
            if (AppData == null)
            {
                return;
            }
            for (int i = 0; i < AppData.Items.Count; i++)
            {
                if (AppData.Items[i].key == key)
                {
                    AppData.Items[i].value = value;
                    break;
                }
            }
        }

        public void Save()
        {
            try
            {
                MyXmlSerializer.SaveToXml(_configFile, AppData, typeof(ConfigData), "app");
            }
            catch
            {
            }

        }

        public string DbFile
        {
            get {
                return _dbfile;
            }
        }

        public string DbConnString
        {
            get
            {
                return string.Format("data source={0};", _dbfile);
            }
        }

        public static AppCommon Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = new AppCommon();
                }
                return _instance;
            }
        }

        public void InitDb()
        {
            DbHelperSQLite.connectionString = DbConnString;
            string strsql = string.Empty;
            if (!File.Exists(DbFile))
            {
                SQLiteConnection.CreateFile(DbFile);
                strsql = "CREATE TABLE [dayschedule] ([id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "[order_id] INT(11) NOT NULL DEFAULT 0, " +
                "[product_id] INT(11) NOT NULL DEFAULT 0, " +
                "[materialno] CHAR(50) NOT NULL, " +
                "[state] int(11) NOT NULL DEFAULT 0);";
                DbHelperSQLite.ExecuteSql(strsql);
                strsql = "CREATE TABLE [productinstock] ([id] INTEGER PRIMARY KEY AUTOINCREMENT,[productid] INT NOT NULL,[materialno] CHAR(50) NOT NULL, [createtime] CHAR(30) NOT NULL, [spec] CHAR(10) NOT NULL,   [weight] FLOAT NOT NULL);";
                DbHelperSQLite.ExecuteSql(strsql);
            }

        }
    }
}
