﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using JueWei.PdaMain.JueWei.WebService;
using JueWei.Common;
using NLSCAN.MacCtrl;
using System.Runtime.InteropServices;

namespace JueWei.PdaMain
{
    public partial class OrderInStockFrm : Form
    {
        private AppCommon theApp = null;
        private int readTimes = 0;
        private NLSScanner scanTest = new NLSScanner();
        private string currentshopno = string.Empty;
        private bool permitscan = true;

        public OrderInStockFrm()
        {
            InitializeComponent();
            scanTest.OnDecodeEvent += new DecodeEventHandler(scan_OnDecodeEvent);
            theApp = AppCommon.Instance;
        }

        private void scan_OnDecodeEvent(object sender, ScannerEventArgs e)
        {
            if (!permitscan)
            {
                this.lb_sbar.Text = string.Format("当前状态:数据无效");
                return;
            }
            readTimes++;
            for (int i = 0; i < e.DataLen; i++)
            {
                if (e.ByteData[i] == 0)
                {//if the data include 0, then change 0 to 0x20
                    e.ByteData[i] = 0x20;
                    NLSSysCtrl.NKDbgPrintfW("ByteData[" + i.ToString() + "] = 0\r\n");
                }
                if (e.ByteData[i] < 0x20)
                {
                    NLSSysCtrl.NKDbgPrintfW("ByteData[" + i.ToString() + "] = " + e.ByteData[i].ToString() + "\r\n");
                }
            }

            string output = e.Data.Replace("\n", string.Empty).Replace("\r", string.Empty);
            PostData(output);
        }

        private void PostData(string data)
        {
            permitscan = false;
            try
            {
                if (string.IsNullOrEmpty(currentshopno))
                {
                    lb_sbar.Text = string.Format("当前状态:当前没有需要出库的订单");
                    return;
                }
                ReqSchedule svs = new ReqSchedule();
                svs.Url = theApp.GetAppConfig("reqdayschedule");
                string ret = svs.PostOrder(theApp.GetAppConfig("deviceid"),currentshopno,data);
                ParseResult(ret);
            }
            catch (System.Exception ex)
            {
                lb_sbar.Text = string.Format("当前状态:网络异常");
            }
            finally
            {
                permitscan = true;
            }
        }

        private void GetNextOrder()
        {
            permitscan = false;
            try
            {
                ReqSchedule svs = new ReqSchedule();
                svs.Url = theApp.GetAppConfig("reqdayschedule");
                string data = svs.GetNextShopOrder(theApp.GetAppConfig("deviceid"));
                ParseResult(data);
            }
            catch
            {
                lb_sbar.Text = string.Format("当前状态:网络异常");
            }
            finally
            {
                permitscan = true;
            }
        }

        private void ParseResult(string data)
        {
            try
            {
                this.dataGrid1.DataSource = null;
                if (data == "1")
                {
                    lb_sbar.Text = string.Format("当前状态:服务器未启动扫码流程");
                }
                else if (data == "5")
                {
                    lb_sbar.Text = string.Format("当前状态:服务器异常");
                }
                else if (data == "6")
                {
                    lb_sbar.Text = string.Format("当前状态:当前单品已扫码完成,请扫下个单品");
                    GetNextOrder();
                }
                else if (data == "0")
                {
                    this.currentshopno = "";
                    lb_sbar.Text = string.Format("当前状态:当前路线扫码已完成");
                }
                else if (data == "-4")
                {
                    lb_sbar.Text = string.Format("当前状态:服务器异常");
                }
                else if (data == "-1")
                {
                    lb_sbar.Text = string.Format("当前状态:传递参数有误");
                }
                else if (data == "-2")
                {
                    lb_sbar.Text = string.Format("当前状态:扫码数据有误");
                }
                else if (data == "-3")
                {
                    lb_sbar.Text = string.Format("当前状态:入库失败,请重新扫码");
                }
                else
                {
                    if (string.IsNullOrEmpty(data))
                    {
                        lb_sbar.Text = string.Format("当前状态:请求失败,请重试");
                        return;
                    }
                    DataTable dt = null;
                    try
                    {
                        dt = JsonHelper.JsonToDataTable(data);
                    }
                    catch (System.Exception ex)
                    {
                        dt = null;
                    }
                    if (dt == null)
                    {
                        lb_sbar.Text = string.Format("当前状态:请求失败,请重试");
                        return;
                    }
                    lb_shop.Text = dt.Rows[0]["shopname"].ToString();
                    currentshopno = dt.Rows[0]["shopno"].ToString() ;
                    //this.lb_products.Items.Clear();
                    //foreach (DataRow item in dt.Rows)
                    //{
                    //    this.lb_products.Items.Add(item["productname"].ToString());
                    //}
                    if (dt != null)
                    {
                        dt.TableName = "Table";
                        this.dataGrid1.DataSource = dt;
                    }

                    lb_scanstatus.Text = "扫码成功";
                    lb_sbar.Text = string.Format("当前状态:请进行下一次扫码");
                }
            }
            catch (System.Exception ex)
            {
                lb_sbar.Text = string.Format("当前状态:网络异常");
            }
            finally
            {
                permitscan = true;
            }
        }

        private void btn_reqactive_Click(object sender, EventArgs e)
        {
            ActiveDevice();        
        }

        private void ActiveDevice()
        {
            ReqSchedule svs = new ReqSchedule();
            svs.Url = theApp.GetAppConfig("reqdayschedule");
            int state = svs.GetDeviceState(theApp.GetAppConfig("deviceid"));
            if (state == 2)
            {
                if (scanTest.Open() == 0)
                {
                    this.btn_reqactive.Enabled = false;
                    this.btn_stopwork.Enabled = true;
                    btn_refresh_Click(null, null);
                    return;
                }
            }
            this.btn_reqactive.Enabled = true;
            this.btn_stopwork.Enabled = false;
        }

        private void DeactiveDevice()
        {
            ReqSchedule svs = new ReqSchedule();
            svs.Url = theApp.GetAppConfig("reqdayschedule");
            svs.DeviceReqStop(theApp.GetAppConfig("deviceid"));
            this.btn_reqactive.Enabled = true;
            this.btn_stopwork.Enabled = false;
        }

        private void btn_stopwork_Click(object sender, EventArgs e)
        {
            if (scanTest.Close() == 0)
            {
                DeactiveDevice();
                // MessageBox.Show("Scan Close success!");
            }
            else
            {
                ActiveDevice();
                // MessageBox.Show("Scan Close false!");
            }
        }

        private void OrderInStockFrm_Load(object sender, EventArgs e)
        {
            int swidth = GetSystemMetrics(0);
            int sheight = GetSystemMetrics(1);
            this.Width = swidth;
            this.Height = sheight;

            ActiveDevice();
            string[] dgColumns = new string[3] { "单品名字", "总数目","已扫数目" };

        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            GetNextOrder();
        }
        [DllImport("coredll.dll")]
        public static extern int GetSystemMetrics(int nIndex);

        private void OrderInStockFrm_Resize(object sender, EventArgs e)
        {
            int swidth = GetSystemMetrics(0);
            int sheight = GetSystemMetrics(1);
            this.Width = swidth;
            this.Height = sheight;
        }
    }
}