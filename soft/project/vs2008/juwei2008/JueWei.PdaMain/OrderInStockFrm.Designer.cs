﻿namespace JueWei.PdaMain
{
    partial class OrderInStockFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_reqactive = new System.Windows.Forms.Button();
            this.btn_stopwork = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_shop = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lb_scanstatus = new System.Windows.Forms.Label();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.lb_sbar = new System.Windows.Forms.StatusBar();
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
            this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn3 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.SuspendLayout();
            // 
            // btn_reqactive
            // 
            this.btn_reqactive.Location = new System.Drawing.Point(62, 7);
            this.btn_reqactive.Name = "btn_reqactive";
            this.btn_reqactive.Size = new System.Drawing.Size(67, 30);
            this.btn_reqactive.TabIndex = 0;
            this.btn_reqactive.Text = "请求激活";
            this.btn_reqactive.Click += new System.EventHandler(this.btn_reqactive_Click);
            // 
            // btn_stopwork
            // 
            this.btn_stopwork.Location = new System.Drawing.Point(138, 7);
            this.btn_stopwork.Name = "btn_stopwork";
            this.btn_stopwork.Size = new System.Drawing.Size(78, 30);
            this.btn_stopwork.TabIndex = 5;
            this.btn_stopwork.Text = "停止工作";
            this.btn_stopwork.Click += new System.EventHandler(this.btn_stopwork_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 189);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.Text = "当前门店";
            // 
            // lb_shop
            // 
            this.lb_shop.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lb_shop.Location = new System.Drawing.Point(70, 178);
            this.lb_shop.Name = "lb_shop";
            this.lb_shop.Size = new System.Drawing.Size(146, 40);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(0, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 20);
            this.label2.Text = "上次扫码";
            // 
            // lb_scanstatus
            // 
            this.lb_scanstatus.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lb_scanstatus.Location = new System.Drawing.Point(74, 222);
            this.lb_scanstatus.Name = "lb_scanstatus";
            this.lb_scanstatus.Size = new System.Drawing.Size(144, 33);
            // 
            // btn_refresh
            // 
            this.btn_refresh.Location = new System.Drawing.Point(3, 7);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(53, 30);
            this.btn_refresh.TabIndex = 6;
            this.btn_refresh.Text = "刷新";
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // lb_sbar
            // 
            this.lb_sbar.Location = new System.Drawing.Point(0, 431);
            this.lb_sbar.Name = "lb_sbar";
            this.lb_sbar.Size = new System.Drawing.Size(638, 24);
            this.lb_sbar.Text = "statusBar1";
            // 
            // dataGrid1
            // 
            this.dataGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dataGrid1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular);
            this.dataGrid1.Location = new System.Drawing.Point(5, 43);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.Size = new System.Drawing.Size(213, 130);
            this.dataGrid1.TabIndex = 17;
            this.dataGrid1.TableStyles.Add(this.dataGridTableStyle1);
            // 
            // dataGridTableStyle1
            // 
            this.dataGridTableStyle1.GridColumnStyles.Add(this.dataGridTextBoxColumn1);
            this.dataGridTableStyle1.GridColumnStyles.Add(this.dataGridTextBoxColumn2);
            this.dataGridTableStyle1.GridColumnStyles.Add(this.dataGridTextBoxColumn3);
            this.dataGridTableStyle1.MappingName = "Table";
            // 
            // dataGridTextBoxColumn1
            // 
            this.dataGridTextBoxColumn1.Format = "";
            this.dataGridTextBoxColumn1.FormatInfo = null;
            this.dataGridTextBoxColumn1.HeaderText = "单品名";
            this.dataGridTextBoxColumn1.MappingName = "shortname";
            this.dataGridTextBoxColumn1.Width = 135;
            // 
            // dataGridTextBoxColumn2
            // 
            this.dataGridTextBoxColumn2.Format = "";
            this.dataGridTextBoxColumn2.FormatInfo = null;
            this.dataGridTextBoxColumn2.HeaderText = "总";
            this.dataGridTextBoxColumn2.MappingName = "verifynum";
            this.dataGridTextBoxColumn2.Width = 20;
            // 
            // dataGridTextBoxColumn3
            // 
            this.dataGridTextBoxColumn3.Format = "";
            this.dataGridTextBoxColumn3.FormatInfo = null;
            this.dataGridTextBoxColumn3.HeaderText = "已";
            this.dataGridTextBoxColumn3.MappingName = "scannum";
            this.dataGridTextBoxColumn3.Width = 20;
            // 
            // OrderInStockFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(638, 455);
            this.Controls.Add(this.dataGrid1);
            this.Controls.Add(this.lb_sbar);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.lb_scanstatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lb_shop);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_stopwork);
            this.Controls.Add(this.btn_reqactive);
            this.Name = "OrderInStockFrm";
            this.Text = "出库扫码";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OrderInStockFrm_Load);
            this.Resize += new System.EventHandler(this.OrderInStockFrm_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_reqactive;
        private System.Windows.Forms.Button btn_stopwork;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_shop;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lb_scanstatus;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.StatusBar lb_sbar;
        private System.Windows.Forms.DataGrid dataGrid1;
        private System.Windows.Forms.DataGridTableStyle dataGridTableStyle1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn2;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn3;
    }
}

