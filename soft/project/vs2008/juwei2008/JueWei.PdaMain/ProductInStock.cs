﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NLSCAN.MacCtrl;
using JueWei.PdaMain.JueWei.WebService;
using System.Runtime.InteropServices;


namespace JueWei.PdaMain
{
    public partial class ProductInStock : Form
    {

        private AppCommon theApp = null;
        private int readTimes = 0;
        private NLSScanner scanTest = new NLSScanner();
        private bool _devicestate = false;
        public ProductInStock()
        {
            InitializeComponent();
            scanTest.OnDecodeEvent += new DecodeEventHandler(scan_OnDecodeEvent);
            theApp = AppCommon.Instance;
        }
        private void scan_OnDecodeEvent(object sender, ScannerEventArgs e)
        {
            readTimes++;
            for (int i = 0; i < e.DataLen; i++)
            {
                if (e.ByteData[i] == 0)
                {//if the data include 0, then change 0 to 0x20
                    e.ByteData[i] = 0x20;
                    NLSSysCtrl.NKDbgPrintfW("ByteData[" + i.ToString() + "] = 0\r\n");
                }
                if (e.ByteData[i] < 0x20)
                {
                    NLSSysCtrl.NKDbgPrintfW("ByteData[" + i.ToString() + "] = " + e.ByteData[i].ToString() + "\r\n");
                }
            }
            if (!_devicestate)
            {
                textBox1.Text = "设备已停止,扫码失败";
                return;
            }
            string output = e.Data.Replace("\n", string.Empty).Replace("\r", string.Empty);
            ReqSchedule svs = new ReqSchedule();
            svs.Url = theApp.GetAppConfig("reqdayschedule");
            string ret = svs.PostProduct(output);
            if (ret == "0")
            {
                textBox1.Text = "扫码成功";
            }
            else
            {
                textBox1.Text = "扫码失败";
            }
            ReqHasScanProduct();
        }

        private void btn_stopwork_Click(object sender, EventArgs e)
        {
            if (scanTest.Close() == 0)
            {
                DeactiveDevice();
               // MessageBox.Show("Scan Close success!");
            }
            else
            {
                ActiveDevice();
               // MessageBox.Show("Scan Close false!");
            }
        }

        private void ProductInStock_Load(object sender, EventArgs e)
        {
            int swidth = GetSystemMetrics(0);
            int sheight = GetSystemMetrics(1);
            this.Width = swidth;
            this.Height = sheight;
            ActiveDevice();
            ReqHasScanProduct();
        }

        private void ActiveDevice()
        {
            ReqSchedule svs = new ReqSchedule();
            svs.Url = theApp.GetAppConfig("reqdayschedule");
            int state = svs.GetDeviceState(theApp.GetAppConfig("deviceid"));
            if (state == 2)
            {
                if (scanTest.Open() == 0)
                {
                    this.btn_reqactive.Enabled = false;
                    this.btn_stopwork.Enabled = true;
                    this._devicestate = true;
                    return;
                }
            }
            this._devicestate = false;
            this.btn_reqactive.Enabled = true;
            this.btn_stopwork.Enabled = false;
        }

        private void DeactiveDevice()
        {
            ReqSchedule svs = new ReqSchedule();
            svs.Url = theApp.GetAppConfig("reqdayschedule");
            svs.DeviceReqStop(theApp.GetAppConfig("deviceid"));
            this.btn_reqactive.Enabled = true;
            this.btn_stopwork.Enabled = false;
            this._devicestate = false;
        }

        private void btn_reqactive_Click(object sender, EventArgs e)
        {
            ActiveDevice();

        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            ActiveDevice();
            ReqHasScanProduct();
        }

        private void ReqHasScanProduct()
        {
            ReqSchedule svs = new ReqSchedule();
            svs.Url = theApp.GetAppConfig("reqdayschedule");
            string data = svs.ReqHasScanedProduct();
            this.dataGrid1.DataSource = null;
            DataTable dt = null;
            try
            {
                dt = JsonHelper.JsonToDataTable(data);
            }
            catch (System.Exception ex)
            {
                textBox1.Text = "获取已入库数据失败";
                dt = null;
            }
            if (dt != null)
            {
                dt.TableName = "Table";
                this.dataGrid1.DataSource = dt;
            }


        }
        [DllImport("coredll.dll")]
        public static extern int GetSystemMetrics(int nIndex);

        private void ProductInStock_Resize(object sender, EventArgs e)
        {
            int swidth = GetSystemMetrics(0);
            int sheight = GetSystemMetrics(1);
            this.Width = swidth;
            this.Height = sheight;
        }
    }
}