﻿namespace JueWei.PdaMain
{
    partial class ProductInStock
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_reqactive = new System.Windows.Forms.Button();
            this.btn_stopwork = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
            this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.SuspendLayout();
            // 
            // btn_reqactive
            // 
            this.btn_reqactive.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.btn_reqactive.Location = new System.Drawing.Point(137, 4);
            this.btn_reqactive.Name = "btn_reqactive";
            this.btn_reqactive.Size = new System.Drawing.Size(61, 35);
            this.btn_reqactive.TabIndex = 1;
            this.btn_reqactive.Text = "激活";
            this.btn_reqactive.Click += new System.EventHandler(this.btn_reqactive_Click);
            // 
            // btn_stopwork
            // 
            this.btn_stopwork.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.btn_stopwork.Location = new System.Drawing.Point(70, 4);
            this.btn_stopwork.Name = "btn_stopwork";
            this.btn_stopwork.Size = new System.Drawing.Size(61, 34);
            this.btn_stopwork.TabIndex = 6;
            this.btn_stopwork.Text = "停止";
            this.btn_stopwork.Click += new System.EventHandler(this.btn_stopwork_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Regular);
            this.textBox1.Location = new System.Drawing.Point(3, 192);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(193, 39);
            this.textBox1.TabIndex = 9;
            // 
            // btn_refresh
            // 
            this.btn_refresh.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.btn_refresh.Location = new System.Drawing.Point(3, 3);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(61, 35);
            this.btn_refresh.TabIndex = 10;
            this.btn_refresh.Text = "刷新";
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // dataGrid1
            // 
            this.dataGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dataGrid1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular);
            this.dataGrid1.Location = new System.Drawing.Point(3, 45);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.Size = new System.Drawing.Size(213, 130);
            this.dataGrid1.TabIndex = 18;
            this.dataGrid1.TableStyles.Add(this.dataGridTableStyle1);
            // 
            // dataGridTableStyle1
            // 
            this.dataGridTableStyle1.GridColumnStyles.Add(this.dataGridTextBoxColumn1);
            this.dataGridTableStyle1.GridColumnStyles.Add(this.dataGridTextBoxColumn2);
            this.dataGridTableStyle1.MappingName = "Table";
            // 
            // dataGridTextBoxColumn1
            // 
            this.dataGridTextBoxColumn1.Format = "";
            this.dataGridTextBoxColumn1.FormatInfo = null;
            this.dataGridTextBoxColumn1.HeaderText = "单品名";
            this.dataGridTextBoxColumn1.MappingName = "shortname";
            this.dataGridTextBoxColumn1.Width = 125;
            // 
            // dataGridTextBoxColumn2
            // 
            this.dataGridTextBoxColumn2.Format = "";
            this.dataGridTextBoxColumn2.FormatInfo = null;
            this.dataGridTextBoxColumn2.HeaderText = "已扫";
            this.dataGridTextBoxColumn2.MappingName = "num";
            // 
            // ProductInStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(638, 455);
            this.Controls.Add(this.dataGrid1);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_stopwork);
            this.Controls.Add(this.btn_reqactive);
            this.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Regular);
            this.Name = "ProductInStock";
            this.Text = "入库扫码";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProductInStock_Load);
            this.Resize += new System.EventHandler(this.ProductInStock_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_reqactive;
        private System.Windows.Forms.Button btn_stopwork;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.DataGrid dataGrid1;
        private System.Windows.Forms.DataGridTableStyle dataGridTableStyle1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn2;
    }
}