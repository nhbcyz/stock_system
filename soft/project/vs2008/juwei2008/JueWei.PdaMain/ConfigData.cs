﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace JueWei.PdaMain
{
    [Serializable]
    [XmlRoot("app")]
    public class ConfigData
    {
        [XmlArray("items"), XmlArrayItem("item")]
        public List<ConfigItem> Items = new List<ConfigItem>();
    }

    public class ConfigItem
    {
        [XmlAttribute("key")]
        public string key;
        [XmlAttribute("value")]
        public string value;
    }
}
