﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
namespace JueWei.PdaMain
{
    public class DBUtil
    {
        private string connStr = string.Empty;
        private MySqlConnection conn;
        private MySqlCommand cmd;
        private MySqlTransaction tran;
        public DBUtil()
        {

        }

        public DBUtil(string dbType, string dbIP, string dbUser, string dbPass, string dbName, string dbPort)
        {
            if (dbType.ToUpper().Equals("MYSQL"))
            {
                this.connStr = string.Concat(new string[] 
                { 
                    "database=", dbName, 
                    ";Port=", dbPort, 
                    ";Password=", dbPass, 
                    ";User ID=", dbUser, 
                    ";server=", dbIP, 
                    ";Character Set=utf8;" 
                });
            }
        }
        public bool TestConn()
        {
            bool result;
            try
            {
                if (this.conn == null)
                {
                    this.conn = new MySqlConnection(this.connStr);
                } 
                if (this.conn.State == ConnectionState.Closed)
                { 
                    this.conn.Open();
                    this.conn.Close(); 
                }
                result = true;
            }
            catch { 
                result = false;
            } 
            return result;
        }
        public void Open()
        {
            try
            {
                if (this.conn == null)
                {
                    this.conn = new MySqlConnection(this.connStr);
                }
                if (this.conn.State == ConnectionState.Closed)
                {
                    this.conn.Open();
                }
            }
            catch (DbException ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void Close()
        {
            if (this.conn.State == ConnectionState.Open)
            { 
                this.conn.Close();
            }
        }
        public DataSet GetDataSet(string sql)
        {
            this.Open();
            MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter();
            this.cmd = this.conn.CreateCommand();
            this.cmd.CommandText = sql;
            mySqlDataAdapter.SelectCommand = this.cmd;
            DataSet dataSet = new DataSet();
            mySqlDataAdapter.Fill(dataSet);
            this.Close(); 
            return dataSet;
        }
        public MySqlCommand getCommand()
        {
            return conn.CreateCommand();
        }
        public DataTable GetDataTable(string sql)
        {
            this.Open();
            MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter();
            this.cmd = this.conn.CreateCommand();
            this.cmd.CommandText = sql;
            mySqlDataAdapter.SelectCommand = this.cmd;
            DataSet dataSet = new DataSet();
            mySqlDataAdapter.Fill(dataSet); 
            this.Close(); 
            return dataSet.Tables[0];
        }
        public int Execute(string sql)
        {
            int res = 0; this.Open(); this.cmd = this.conn.CreateCommand();
            this.tran = this.conn.BeginTransaction();
            try
            {
                this.cmd.Transaction = this.tran;
                this.cmd.CommandText = sql;
                res = this.cmd.ExecuteNonQuery(); this.tran.Commit();
            }
            catch (Exception ex)
            {
                res = -1;
                this.tran.Rollback(); throw new Exception(ex.Message);
            }
            finally { this.conn.Close(); } return res;
        }
        public void Transaction()
        {
            this.Open(); this.cmd = this.conn.CreateCommand();
            this.tran = this.conn.BeginTransaction();
        }
        public DataTable GetDataTableNotClose(string sql)
        {
            MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(); 
            this.cmd = this.conn.CreateCommand();
            this.cmd.CommandText = sql; 
            mySqlDataAdapter.SelectCommand = this.cmd; 
            DataSet dataSet = new DataSet();
            mySqlDataAdapter.Fill(dataSet); 
            return dataSet.Tables[0];
        }
        public void ExecuteNotTran(string sql)
        {
            this.cmd.Transaction = this.tran; 
            this.cmd.CommandText = sql; 
            this.cmd.ExecuteNonQuery();
        }
        public void Commit() { 
            this.tran.Commit(); 
            this.Close(); 
        }
        public void Rollback()
        {
            this.tran.Rollback();
            this.Close();
        }
    }
}
