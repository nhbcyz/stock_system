﻿namespace JueWei.PdaMain
{
    partial class ScanDataFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tx_materialno = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tx_name = new System.Windows.Forms.TextBox();
            this.tx_spec = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(56, 180);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(72, 20);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "确定入库";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(175, 180);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(72, 20);
            this.btn_cancel.TabIndex = 1;
            this.btn_cancel.Text = "取消";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tx_spec);
            this.panel1.Controls.Add(this.tx_name);
            this.panel1.Controls.Add(this.tx_materialno);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(3, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(244, 131);
            // 
            // tx_materialno
            // 
            this.tx_materialno.Location = new System.Drawing.Point(94, 11);
            this.tx_materialno.Name = "tx_materialno";
            this.tx_materialno.ReadOnly = true;
            this.tx_materialno.Size = new System.Drawing.Size(100, 23);
            this.tx_materialno.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(17, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 20);
            this.label5.Text = "规格";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(17, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 20);
            this.label4.Text = "名字";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(17, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 20);
            this.label3.Text = "编号";
            // 
            // tx_name
            // 
            this.tx_name.Location = new System.Drawing.Point(94, 42);
            this.tx_name.Name = "tx_name";
            this.tx_name.ReadOnly = true;
            this.tx_name.Size = new System.Drawing.Size(100, 23);
            this.tx_name.TabIndex = 9;
            // 
            // tx_spec
            // 
            this.tx_spec.Location = new System.Drawing.Point(94, 71);
            this.tx_spec.Name = "tx_spec";
            this.tx_spec.ReadOnly = true;
            this.tx_spec.Size = new System.Drawing.Size(100, 23);
            this.tx_spec.TabIndex = 10;
            // 
            // ScanDataFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(259, 218);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScanDataFrm";
            this.Text = "本次数据";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tx_materialno;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tx_name;
        private System.Windows.Forms.TextBox tx_spec;
    }
}