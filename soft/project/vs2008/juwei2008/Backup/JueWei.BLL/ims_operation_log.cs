using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.BLL
{
    public partial class ims_operation_log
    {
        private static readonly DAL.ims_operation_log dal = new DAL.ims_operation_log();

        #region  增加一条数据
        /// <summary>
        /// 添加一条数据
        /// </summary>
        public static bool Insert(Model.ims_operation_log SingleItem)
        {
            if (dal.Exists(SingleItem.Id))   //如果已经存在相应的记录
            {
                return false;
            }
            if (dal.Insert(SingleItem))
            {
                return true;
            }
            return false;
        }
        #endregion

        #region  删除一条数据
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public static bool Delete(int id)
        {
            return dal.Delete(id);
        }
        #endregion

        #region  更新一条数据
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public static bool Update(Model.ims_operation_log SingleItem)
        {
            return dal.Update(SingleItem);
        }
        #endregion

        #region  检查记录是否已经存在
        /// <summary>
        /// 检查记录是否已经存在
        /// </summary>
        public static bool Exists(int id)
        {
            return dal.Exists(id);
        }
        #endregion

        #region  根据主键取数据
        /// <summary>
        /// 根据主键取数据
        /// </summary>
        public static Model.ims_operation_log SelectById(int id)
        {
            return dal.SelectById(id);
        }
        #endregion

        #region 根据条件取ID排序一条数据
        /// <summary>
        /// 根据条件取ID排序一条数据
        /// </summary>
        public static Model.ims_operation_log SelectModelByWhereOrderByID(string strWhere, bool bAsc)
        {
            return dal.SelectModelByWhereOrderByID(strWhere, bAsc);
        }
        #endregion

        #region  取得所有数据的列表
        /// <summary>
        /// 取得所有数据的列表
        /// </summary>
        public static IList<Model.ims_operation_log> GetAllList()
        {
            return dal.GetAllList();
        }
        #endregion

        #region  取得Where条件的列表
        /// <summary>
        /// 取得Where条件的列表
        /// </summary>
        public static DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        #endregion

        #region  获取数据条数
        /// <summary>
        /// 获取数据条数
        /// </summary>
        public static int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        #endregion
        #region 根据选择的主键值执行批量删除
        /// <summary>
        /// 根据选择的主键值执行批量删除
        /// </summary>
        /// <param name="keys">选择的主键字符串</param>
        /// <returns>是否删除成功！</returns>
        public static bool DeleteChooseByKeys(string keys)
        {
            return dal.DeleteChooseByKeys(keys);
        }
        #endregion


        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static DataSet GetList(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            return dal.GetList(pageIndex, pageSize, orderString, strWhere, ref recordCount);
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static DataSet GetList(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return dal.GetList(pageIndex, pageSize, strWhere, ref recordCount);
        }
        #endregion

        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static IList<Model.ims_operation_log> GetListByPage(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            return dal.GetListByPage(pageIndex, pageSize, orderString, strWhere, ref recordCount);
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static IList<Model.ims_operation_log> GetListByPage(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return dal.GetListByPage(pageIndex, pageSize, strWhere, ref recordCount);
        }

        #endregion

    }
}
