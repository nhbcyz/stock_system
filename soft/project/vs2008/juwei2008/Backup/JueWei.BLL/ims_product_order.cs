﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace JueWei.BLL
{
    public partial class ims_product_order
    {
        private static readonly DAL.ims_product_order dal = new DAL.ims_product_order();

        #region  增加一条数据
        /// <summary>
        /// 添加一条数据
        /// </summary>
        public static bool Insert(Model.ims_product_order SingleItem)
        {
            if (dal.Exists(SingleItem.Id))   //如果已经存在相应的记录
            {
                return false;
            }
            if (dal.Insert(SingleItem))
            {
                return true;
            }
            return false;
        }
        #endregion

        #region  删除一条数据
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public static bool Delete(int id)
        {
            return dal.Delete(id);
        }
        #endregion

        #region  更新一条数据
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public static bool Update(Model.ims_product_order SingleItem)
        {
            return dal.Update(SingleItem);
        }
        #endregion

        #region  检查记录是否已经存在
        /// <summary>
        /// 检查记录是否已经存在
        /// </summary>
        public static bool Exists(int id)
        {
            return dal.Exists(id);
        }
        #endregion

        #region  根据主键取数据
        /// <summary>
        /// 根据主键取数据
        /// </summary>
        public static Model.ims_product_order SelectById(int id)
        {
            return dal.SelectById(id);
        }
        #endregion

        #region 根据条件取ID排序一条数据
        /// <summary>
        /// 根据条件取ID排序一条数据
        /// </summary>
        public static Model.ims_product_order SelectModelByWhereOrderByID(string strWhere, bool bAsc)
        {
            return dal.SelectModelByWhereOrderByID(strWhere, bAsc);
        }
        #endregion

        #region  取得所有数据的列表
        /// <summary>
        /// 取得所有数据的列表
        /// </summary>
        public static IList<Model.ims_product_order> GetAllList()
        {
            return dal.GetAllList();
        }
        #endregion

        #region  取得Where条件的列表
        /// <summary>
        /// 取得Where条件的列表
        /// </summary>
        public static DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        #endregion

        #region  获取数据条数
        /// <summary>
        /// 获取数据条数
        /// </summary>
        public static int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        #endregion
        #region 根据选择的主键值执行批量删除
        /// <summary>
        /// 根据选择的主键值执行批量删除
        /// </summary>
        /// <param name="keys">选择的主键字符串</param>
        /// <returns>是否删除成功！</returns>
        public static bool DeleteChooseByKeys(string keys)
        {
            return dal.DeleteChooseByKeys(keys);
        }
        #endregion


        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static DataSet GetList(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            return dal.GetList(pageIndex, pageSize, orderString, strWhere, ref recordCount);
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static DataSet GetList(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return dal.GetList(pageIndex, pageSize, strWhere, ref recordCount);
        }
        #endregion

        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static IList<Model.ims_product_order> GetListByPage(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            return dal.GetListByPage(pageIndex, pageSize, orderString, strWhere, ref recordCount);
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static IList<Model.ims_product_order> GetListByPage(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return dal.GetListByPage(pageIndex, pageSize, strWhere, ref recordCount);
        }


        /// <summary>
        /// 根据时间从订单中获取所有线路编号
        /// </summary>
        /// <param name="strTime"></param>
        /// <returns></returns>
        public static DataSet GetAllLinesByTime(string strTime)
        {
            return dal.GetAllLinesByTime(strTime);
        }

        /// <summary>
        /// 根据时间和线路编码从订单中获取所有店铺
        /// </summary>
        /// <param name="strTime"></param>
        /// <returns></returns>
        public static DataSet GetAllShopsByLineAndTime(string strTime, string lineno)
        {
            return dal.GetAllShopsByLineAndTime(strTime, lineno);
        }

        public static DataSet GetAllShopsByLineAndTimeEx(string strTime, string lineno)
        {
            return dal.GetAllShopsByLineAndTimeEx(strTime, lineno);
        }

        public static DataSet GetCurrentNoScanOrderByDeviceID(string strTime, string lineno, int deviceid)
        {
            return dal.GetCurrentNoScanOrderByDeviceID(strTime, lineno, deviceid);
        }

        public static decimal GetSumWeightByShopNo(string strTime, string shopno)
        {
            return dal.GetSumWeightByShopNo(strTime, shopno);
        }

        public static bool UpdateShopOrderStateByShopNoAndDt(int state, string strTime, string shopno, string exchangeno)
        {
            return dal.UpdateShopOrderStateByShopNoAndDt(state, strTime, shopno, exchangeno);
        }
        #endregion

        public static DataSet GetOrdersByWhere(string strWhere)
        {
            return dal.GetOrdersByWhere(strWhere);
        }

        public static DataSet GetOrdersByWhere(string exchangedate, string shopno)
        {
            return dal.GetOrdersByWhere(exchangedate, shopno);
        }


        #region 根据时间和店铺获取总重量(皮重(包含包装重量))
        /// <summary>
        /// 获取订单净重
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public static decimal GetSumRealWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            return dal.GetSumRealWeightByShopNoAndDt(strTime, shopno, exchangeno);
        }
        #endregion

        #region 根据时间和店铺获取总重量(净重(不含包装重量))
        /// <summary>
        /// 获取毛重
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public static decimal GetSumMaoWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            return dal.GetSumMaoWeightByShopNoAndDt(strTime, shopno, exchangeno);
        }
        #endregion

        #region 根据时间和店铺获取总的包装重量
        /// <summary>
        /// 获取包装重量
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public static decimal GetSumPacketWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            return dal.GetSumPacketWeightByShopNoAndDt(strTime, shopno, exchangeno);
        }
        #endregion

        public static bool CheckHasScrabedByLineAndDt(string strTime, string lineno)
        {
            return dal.CheckHasScrabedByLineAndDt(strTime, lineno);
        }

        public static DataSet GetProductRecord(DateTime dtime)
        {
            return dal.GetProductRecord(dtime);
        }
    }
}
