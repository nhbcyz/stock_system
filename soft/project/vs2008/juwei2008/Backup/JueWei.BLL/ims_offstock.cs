using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.BLL
{
    public partial class ims_offstock
    {
        private static readonly DAL.ims_offstock dal = new DAL.ims_offstock();

        #region  增加一条数据
        /// <summary>
        /// 添加一条数据
        /// </summary>
        public static bool Insert(Model.ims_offstock SingleItem)
        {
            if (dal.Exists(SingleItem.Id))   //如果已经存在相应的记录
            {
                return false;
            }
            if (dal.Insert(SingleItem))
            {
                return true;
            }
            return false;
        }
        #endregion

        #region  删除一条数据
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public static bool Delete(int id)
        {
            return dal.Delete(id);
        }
        #endregion

        #region  更新一条数据
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public static bool Update(Model.ims_offstock SingleItem)
        {
            return dal.Update(SingleItem);
        }
        #endregion

        #region  检查记录是否已经存在
        /// <summary>
        /// 检查记录是否已经存在
        /// </summary>
        public static bool Exists(int id)
        {
            return dal.Exists(id);
        }
        #endregion

        #region  根据主键取数据
        /// <summary>
        /// 根据主键取数据
        /// </summary>
        public static Model.ims_offstock SelectById(int id)
        {
            return dal.SelectById(id);
        }
        #endregion

        #region 根据条件取ID排序一条数据
        /// <summary>
        /// 根据条件取ID排序一条数据
        /// </summary>
        public static Model.ims_offstock SelectModelByWhereOrderByID(string strWhere, bool bAsc)
        {
            return dal.SelectModelByWhereOrderByID(strWhere, bAsc);
        }
        #endregion

        #region  取得所有数据的列表
        /// <summary>
        /// 取得所有数据的列表
        /// </summary>
        public static IList<Model.ims_offstock> GetAllList()
        {
            return dal.GetAllList();
        }
        #endregion

        #region  取得Where条件的列表
        /// <summary>
        /// 取得Where条件的列表
        /// </summary>
        public static DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        #endregion

        #region  获取数据条数
        /// <summary>
        /// 获取数据条数
        /// </summary>
        public static int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        #endregion
        #region 根据选择的主键值执行批量删除
        /// <summary>
        /// 根据选择的主键值执行批量删除
        /// </summary>
        /// <param name="keys">选择的主键字符串</param>
        /// <returns>是否删除成功！</returns>
        public static bool DeleteChooseByKeys(string keys)
        {
            return dal.DeleteChooseByKeys(keys);
        }
        #endregion


        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static DataSet GetList(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            return dal.GetList(pageIndex, pageSize, orderString, strWhere, ref recordCount);
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static DataSet GetList(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return dal.GetList(pageIndex, pageSize, strWhere, ref recordCount);
        }
        #endregion

        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static IList<Model.ims_offstock> GetListByPage(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            return dal.GetListByPage(pageIndex, pageSize, orderString, strWhere, ref recordCount);
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public static IList<Model.ims_offstock> GetListByPage(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return dal.GetListByPage(pageIndex, pageSize, strWhere, ref recordCount);
        }

        #endregion

        #region 根据时间和店铺获取数据

        public static DataSet GetListByShopNoAndDt(string strTime, string shopno)
        {
            return dal.GetListByShopNoAndDt(strTime, shopno);
        }

        #endregion

        #region 根据时间和出货单获取数据

        public static DataSet GetListByExchangeNoAndDt(string strTime,string shopno, string exchangeno)
        {
            return dal.GetListByExchangeNoAndDt(strTime, shopno, exchangeno);
        }

        #endregion

        #region 根据时间和店铺按出货单号分组统计

        public static DataSet GetListByShopNoAndDtGroupByExchange(string strTime, string shopno)
        {
            return null;
           // return dal.GetListByShopNoAndDtGroupByExchange(strTime, shopno);
        }

        #endregion

        #region 根据时间和店铺获取总重量(皮重(包含包装重量))
        /// <summary>
        /// 获取订单净重
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public static decimal GetSumRealWeightByShopNoAndDt(string strTime, string shopno,string exchangeno)
        {
            return dal.GetSumRealWeightByShopNoAndDt(strTime, shopno,exchangeno);
        }
        #endregion

        #region 根据时间和店铺获取总重量(净重(不含包装重量))
        /// <summary>
        /// 获取毛重
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public static decimal GetSumMaoWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            return dal.GetSumMaoWeightByShopNoAndDt(strTime, shopno,exchangeno);
        }
        #endregion

        #region 根据时间和店铺获取总的包装重量
        /// <summary>
        /// 获取包装重量
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public static decimal GetSumPacketWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            return dal.GetSumPacketWeightByShopNoAndDt(strTime, shopno,exchangeno);
        }
        #endregion

        /// <summary>
        /// 出库统计
        /// </summary>
        /// <param name="strstart"></param>
        /// <param name="strend"></param>
        /// <returns></returns>
        public static DataSet GetProductSumWeightByDateTime(string strstart, string strend)
        {
            return dal.GetProductSumWeightByDateTime(strstart, strend);
        }

        /// <summary>
        /// 统计库存
        /// </summary>
        /// <param name="strstart"></param>
        /// <param name="strend"></param>
        /// <returns></returns>
        public static DataSet GetStockDiffSumWeightByDateTime(string strstart, string strend)
        {
            return dal.GetStockDiffSumWeightByDateTime(strstart, strend);
        }



        /// <summary>
        /// 根据时间和线路编码从出货中获取所有店铺
        /// </summary>
        /// <param name="strTime"></param>
        /// <returns></returns>
        public static DataSet GetAllShopsByLineAndTime(string strTime, string lineno)
        {
            return dal.GetAllShopsByLineAndTime(strTime, lineno);
        }

        /// <summary>
        /// 根据时间和线路编码从出货中获取所有店铺,逆序
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="lineno"></param>
        /// <returns></returns>
        public static DataSet GetAllShopsByLineAndTimeDesc(string strTime, string lineno)
        {
            return dal.GetAllShopsByLineAndTimeDesc(strTime, lineno);
        }

        public static DataSet GetAllExchangeNoByShopNoAndTime(string strTime, string shopno)
        {
            return dal.GetAllExchangeNoByShopNoAndTime(strTime, shopno);
        }

        public static DataSet GetListByWhere(string shopno, string exchangeno, string strTime)
        {
            return dal.GetListByWhere( shopno,exchangeno,strTime);
        }

        public static DataSet GetListByWhere(string shopno, string exchangeno, string strTime,int state)
        {
            return dal.GetListByWhere(shopno, exchangeno, strTime,state);
        }

        public static DataSet CheckShopHasOffStockOrHasWeighted(string shopno, string exchangeno, string strTime, int state)
        {
            return dal.CheckShopHasOffStockOrHasWeighted(shopno, exchangeno, strTime, state);
        }

        public static DataSet CheckShopOrderHasWeighted(string shopno, string exchangeno, string strTime, int state)
        {
            return dal.CheckShopOrderHasWeighted(shopno, exchangeno, strTime, state);
        }

        public static bool DeleteData(string strWhere)
        {
            return dal.DeleteData(strWhere);
        }

        public static bool DeleteData(string shopno, string exchangeno, string strTime)
        {
            return dal.DeleteData(shopno, exchangeno, strTime);
        }

        public static bool CheckHasOffStockByLineAndDt(string strTime, string lineno)
        {
            return dal.CheckHasOffStockByLineAndDt(strTime, lineno);
        }

        public static DataSet QueryAllStocks(string strTime, string lineno)
        {
            return dal.QueryAllStocks(strTime, lineno);
        }

        public static int QueryOffStockNum(string strTime, string materialno, int state)
        {
            return dal.QueryOffStockNum(strTime, materialno, state);
        }
    }
}
