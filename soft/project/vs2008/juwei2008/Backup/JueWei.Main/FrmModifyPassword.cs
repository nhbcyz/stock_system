using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.WareHouseMis.UI;

namespace JueWei.Main
{
    public partial class FrmModifyPassword : BaseForm
    {
        public FrmModifyPassword()
        {
            InitializeComponent();
        }

        private void FrmModifyPassword_Load(object sender, EventArgs e)
        {
            this.txtLogin.Text = Portal.gc.LoginInfo.Username;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.txtRePassword.Text != this.txtPassword.Text)
            {
                MessageExUtil.ShowTips("两个新密码的输入不一致");
                this.txtRePassword.Focus();
            }
            else
            {
                try
                {
                    Portal.gc.LoginInfo.Password = this.txtPassword.Text;
                    if (BLL.ims_users.Update(Portal.gc.LoginInfo))
                    {
                        base.DialogResult = DialogResult.OK;
                        MessageExUtil.ShowTips("密码修改成功");
                    }
                    else
                    {
                        MessageExUtil.ShowWarning("用户密码资料不正确，请核对");
                    }
                }
                catch (Exception exception)
                {
                    MessageExUtil.ShowError(exception.Message);
                }
            }
        }
    }
}