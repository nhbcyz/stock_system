namespace JueWei.Main
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Windows.Forms;
    //using WHC.Dictionary;
    using WHC.OrderWater.Commons;
    using JueWei.Main;
    using WHC.WareHouseMis.UI.SplashScreen;
    using JueWei.Main.Common;
    //using WHC.Pager.WinControl;
    //using WHC.Security;
    //using WHC.Security.BLL;
    //using WHC.Security.Entity;
    //using WHC.WareHouseMis.BLL;
    //using WHC.WareHouseMis.Entity;
    //using WHC.WareHouseMis.UI.SplashScreen;

    public class Portal
    {
        public static GlobalControl gc = new GlobalControl();
        private static Mutex mutex = null;

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs ex)
        {
            LogHelper.Error(ex.Exception);
            //string message = string.Format("{0}\r\n操作发生错误，您需要退出系统么？", ex.Exception.Message);
            //if (DialogResult.Yes == MessageExUtil.ShowYesNoAndError(message))
            //{
            //    Application.Exit();
            //}
        }

        private static void GlobalMutex()
        {
            bool createdNew = false;
            string name = @"Global\WareHouseMis";
            try
            {
                mutex = new Mutex(false, name, out createdNew);
            }
            catch (Exception exception)
            {
                Console.Write(exception.Message);
                Thread.Sleep(0x3e8);
                Environment.Exit(1);
            }
            if (createdNew)
            {
                Console.WriteLine("程序已启动");
            }
            else
            {
                //MessageExUtil.ShowTips("另一个窗口已在运行，不能重复运行。");
                //Thread.Sleep(0x3e8);
                //Environment.Exit(1);
            }
        }

        private static void LoginByArgs(string[] args)
        {
            //CommandArgs args2 = CommandLine.Parse(args);
            //string str = args2.ArgPairs["U"];
            //string userPassword = args2.ArgPairs.ContainsKey("P") ? args2.ArgPairs["P"] : "";
            //if (!string.IsNullOrEmpty(str))
            //{
            //    if (!string.IsNullOrEmpty(WHC.Security.BLL.BLLFactory<User>.Instance.VerifyUser(str, userPassword, Guid.NewGuid().ToString())))
            //    {
            //        UserInfo userByName = WHC.Security.BLL.BLLFactory<User>.Instance.GetUserByName(str);
            //        List<FunctionInfo> functionsByUser = WHC.Security.BLL.BLLFactory<Function>.Instance.GetFunctionsByUser(userByName.ID, "WareMis");
            //        if ((functionsByUser != null) && (functionsByUser.Count > 0))
            //        {
            //            foreach (FunctionInfo info2 in functionsByUser)
            //            {
            //                if (!gc.FunctionDict.ContainsKey(info2.ControlID))
            //                {
            //                    gc.FunctionDict.Add(info2.ControlID, info2);
            //                }
            //            }
            //        }
            //        WHC.OrderWater.Commons.RegistryHelper.SaveValue(gc.Login_Name_Key, str);
            //        LoginLogInfo info3 = new LoginLogInfo {
            //            LoginName = userByName.Name,
            //            RealName = userByName.FullName,
            //            User_ID = userByName.ID.ToString(),
            //            Note = "用户登录"
            //        };
            //        WHC.WareHouseMis.BLL.BLLFactory<LoginLog>.Instance.Insert(info3);
            //        gc.ManagedWareHouse = gc.GetWareHouse(userByName);
            //        if ((gc.ManagedWareHouse != null) && (gc.ManagedWareHouse.Count > 0))
            //        {
            //            gc.LoginInfo = userByName;
            //            Splasher.Show(typeof(frmSplash));
            //            gc.MainDialog = new MainForm();
            //            gc.MainDialog.StartPosition = FormStartPosition.CenterScreen;
            //            Application.Run(gc.MainDialog);
            //        }
            //        else
            //        {
            //            MessageUtil.ShowTips("用户登录信息正确，但未授权管理库房信息，请联系组长授权管理");
            //            LoginNormal(args);
            //        }
            //    }
            //    else
            //    {
            //        MessageUtil.ShowTips("用户帐号密码不正确");
            //        LoginNormal(args);
            //    }
            //}
            //else
            //{
            //    MessageUtil.ShowTips("命令格式有误");
            //    LoginNormal(args);
            //}
        }

        private static void LoginNormal(string[] args)
        {
            Logon logon = new Logon
            {
                StartPosition = FormStartPosition.CenterScreen
            };
            if ((DialogResult.OK == logon.ShowDialog()) && logon.bLogin)
            {
                Splasher.Show(typeof(frmSplash));
                gc.MainDialog = new MainFrm();
                gc.MainDialog.StartPosition = FormStartPosition.CenterScreen;
                Application.Run(gc.MainDialog);
            }
            logon.Dispose();
        }

        [STAThread]
        private static void Main(string[] args)
        {
            AppCommon.Instance.Init();
            SetUIConstants();
            GlobalMutex();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new ThreadExceptionEventHandler(Portal.Application_ThreadException);
            if (args.Length >= 1)
            {
                LoginByArgs(args);
            }
            else
            {
                //Application.Run(new MainFrm());
                LoginNormal(args);
            }
        }

        private static void SetUIConstants()
        {
            //WHC.Security.MyConstants.License = "397cV0hDLlNlY3VybXR5fOS8jeWNjuiBqnzlua-lt57niLHlkK-o_6rmioDmnK-mnInpmZDlhbzlj7h8RmFsc2Uv";
            //WHC.Dictionary.MyConstants.License = "37c6V0hDLkRpY3Rpa25hcnl85LyN5Y2O6IGqfOW5_*W3nueIseWQr*i-qubKgObcr*bciemZkOWFrOWPuHxGYWxzZQvv";
            //WHC.Pager.WinControl.MyConstants.License = "070eV0hDLlBhZ2VyfOS8jeWNjuiBqnx8RmFsc2Uv";
        }
    }
}

