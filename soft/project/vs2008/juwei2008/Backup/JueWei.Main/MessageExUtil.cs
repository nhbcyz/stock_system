namespace JueWei.Main
{
    using DevComponents.DotNetBar;
    using System;
    using System.Windows.Forms;

    public class MessageExUtil
    {
        public static DialogResult ShowError(string message)
        {
            return MessageBoxEx.Show(message, "错误信息", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        public static DialogResult ShowTips(string message)
        {
            return MessageBoxEx.Show(message, "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public static DialogResult ShowWarning(string message)
        {
            return MessageBoxEx.Show(message, "警告信息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public static DialogResult ShowYesNoAndError(string message)
        {
            return MessageBoxEx.Show(message, "错误信息", MessageBoxButtons.YesNo, MessageBoxIcon.Hand);
        }

        public static DialogResult ShowYesNoAndTips(string message)
        {
            return MessageBoxEx.Show(message, "提示信息", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
        }

        public static DialogResult ShowYesNoAndWarning(string message)
        {
            return MessageBoxEx.Show(message, "警告信息", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
        }

        public static DialogResult ShowYesNoCancelAndTips(string message)
        {
            return MessageBoxEx.Show(message, "提示信息", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Asterisk);
        }
    }
}

