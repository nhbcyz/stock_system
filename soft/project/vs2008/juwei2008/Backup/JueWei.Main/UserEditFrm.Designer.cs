namespace JueWei.Main
{
    partial class UserEditFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_cancel = new DevComponents.DotNetBar.ButtonX();
            this.btn_ok = new DevComponents.DotNetBar.ButtonX();
            this.tb_secondpwd = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_pwd = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_user = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dtinput_expire = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.tb_fullname = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lb_fullname = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.cb_roles = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            ((System.ComponentModel.ISupportInitialize)(this.dtinput_expire)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_cancel
            // 
            this.btn_cancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cancel.Location = new System.Drawing.Point(301, 249);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cancel.TabIndex = 40;
            this.btn_cancel.Text = "取消";
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ok.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ok.Location = new System.Drawing.Point(172, 249);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ok.TabIndex = 39;
            this.btn_ok.Text = "确定";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // tb_secondpwd
            // 
            // 
            // 
            // 
            this.tb_secondpwd.Border.Class = "TextBoxBorder";
            this.tb_secondpwd.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_secondpwd.Location = new System.Drawing.Point(136, 94);
            this.tb_secondpwd.Name = "tb_secondpwd";
            this.tb_secondpwd.PreventEnterBeep = true;
            this.tb_secondpwd.Size = new System.Drawing.Size(111, 21);
            this.tb_secondpwd.TabIndex = 37;
            // 
            // tb_pwd
            // 
            // 
            // 
            // 
            this.tb_pwd.Border.Class = "TextBoxBorder";
            this.tb_pwd.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_pwd.Location = new System.Drawing.Point(136, 51);
            this.tb_pwd.Name = "tb_pwd";
            this.tb_pwd.PreventEnterBeep = true;
            this.tb_pwd.Size = new System.Drawing.Size(111, 21);
            this.tb_pwd.TabIndex = 36;
            // 
            // tb_user
            // 
            this.tb_user.AcceptsTab = true;
            // 
            // 
            // 
            this.tb_user.Border.Class = "TextBoxBorder";
            this.tb_user.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_user.Location = new System.Drawing.Point(136, 7);
            this.tb_user.Name = "tb_user";
            this.tb_user.PreventEnterBeep = true;
            this.tb_user.Size = new System.Drawing.Size(111, 21);
            this.tb_user.TabIndex = 35;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(33, 207);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(86, 23);
            this.labelX4.TabIndex = 33;
            this.labelX4.Text = "账号过期时间";
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(44, 94);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 32;
            this.labelX3.Text = "确认密码";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(44, 51);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 31;
            this.labelX2.Text = "密码";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(44, 7);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 30;
            this.labelX1.Text = "用户名";
            // 
            // dtinput_expire
            // 
            // 
            // 
            // 
            this.dtinput_expire.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtinput_expire.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_expire.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtinput_expire.ButtonDropDown.Visible = true;
            this.dtinput_expire.CustomFormat = "yyyy-MM-dd";
            this.dtinput_expire.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtinput_expire.IsPopupCalendarOpen = false;
            this.dtinput_expire.Location = new System.Drawing.Point(136, 207);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dtinput_expire.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_expire.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dtinput_expire.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtinput_expire.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtinput_expire.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtinput_expire.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtinput_expire.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtinput_expire.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtinput_expire.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtinput_expire.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_expire.MonthCalendar.DisplayMonth = new System.DateTime(2016, 10, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtinput_expire.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtinput_expire.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtinput_expire.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtinput_expire.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_expire.MonthCalendar.TodayButtonVisible = true;
            this.dtinput_expire.Name = "dtinput_expire";
            this.dtinput_expire.Size = new System.Drawing.Size(111, 21);
            this.dtinput_expire.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtinput_expire.TabIndex = 41;
            this.dtinput_expire.TimeSelectorTimeFormat = DevComponents.Editors.DateTimeAdv.eTimeSelectorFormat.Time24H;
            this.dtinput_expire.Value = new System.DateTime(2016, 10, 7, 19, 17, 59, 0);
            // 
            // tb_fullname
            // 
            // 
            // 
            // 
            this.tb_fullname.Border.Class = "TextBoxBorder";
            this.tb_fullname.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_fullname.Location = new System.Drawing.Point(136, 128);
            this.tb_fullname.Name = "tb_fullname";
            this.tb_fullname.PreventEnterBeep = true;
            this.tb_fullname.Size = new System.Drawing.Size(111, 21);
            this.tb_fullname.TabIndex = 43;
            // 
            // lb_fullname
            // 
            // 
            // 
            // 
            this.lb_fullname.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb_fullname.Location = new System.Drawing.Point(44, 128);
            this.lb_fullname.Name = "lb_fullname";
            this.lb_fullname.Size = new System.Drawing.Size(75, 23);
            this.lb_fullname.TabIndex = 42;
            this.lb_fullname.Text = "全名";
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(33, 166);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(75, 23);
            this.labelX5.TabIndex = 44;
            this.labelX5.Text = "角色分配";
            // 
            // cb_roles
            // 
            this.cb_roles.DisplayMember = "Text";
            this.cb_roles.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_roles.FormattingEnabled = true;
            this.cb_roles.ItemHeight = 15;
            this.cb_roles.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3});
            this.cb_roles.Location = new System.Drawing.Point(136, 166);
            this.cb_roles.Name = "cb_roles";
            this.cb_roles.Size = new System.Drawing.Size(111, 21);
            this.cb_roles.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_roles.TabIndex = 45;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "手持PDA";
            this.comboItem1.Value = "0";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "固定PDA";
            this.comboItem2.Value = "1";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "称重仪";
            this.comboItem3.Value = "2";
            // 
            // UserEditFrm
            // 
            this.ClientSize = new System.Drawing.Size(393, 306);
            this.Controls.Add(this.cb_roles);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.tb_fullname);
            this.Controls.Add(this.lb_fullname);
            this.Controls.Add(this.dtinput_expire);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.tb_secondpwd);
            this.Controls.Add(this.tb_pwd);
            this.Controls.Add(this.tb_user);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Name = "UserEditFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "用户编辑";
            this.Load += new System.EventHandler(this.UserEditFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtinput_expire)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btn_cancel;
        private DevComponents.DotNetBar.ButtonX btn_ok;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_secondpwd;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_pwd;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_user;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtinput_expire;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_fullname;
        private DevComponents.DotNetBar.LabelX lb_fullname;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_roles;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
    }
}