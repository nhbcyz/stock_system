namespace JueWei.Main
{
    partial class OffStockOrderEditFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_cancel = new DevComponents.DotNetBar.ButtonX();
            this.btn_ok = new DevComponents.DotNetBar.ButtonX();
            this.tb_weight = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.tb_num = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.SuspendLayout();
            // 
            // btn_cancel
            // 
            this.btn_cancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Location = new System.Drawing.Point(181, 171);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cancel.TabIndex = 29;
            this.btn_cancel.Text = "取消";
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ok.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ok.Location = new System.Drawing.Point(64, 171);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ok.TabIndex = 28;
            this.btn_ok.Text = "确定";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // tb_weight
            // 
            // 
            // 
            // 
            this.tb_weight.Border.Class = "TextBoxBorder";
            this.tb_weight.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_weight.Location = new System.Drawing.Point(145, 69);
            this.tb_weight.Name = "tb_weight";
            this.tb_weight.PreventEnterBeep = true;
            this.tb_weight.Size = new System.Drawing.Size(100, 21);
            this.tb_weight.TabIndex = 27;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(28, 68);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 26;
            this.labelX1.Text = "总重量";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(28, 36);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 30;
            this.labelX2.Text = "订单个数";
            // 
            // tb_num
            // 
            // 
            // 
            // 
            this.tb_num.Border.Class = "TextBoxBorder";
            this.tb_num.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_num.Location = new System.Drawing.Point(145, 36);
            this.tb_num.Name = "tb_num";
            this.tb_num.PreventEnterBeep = true;
            this.tb_num.ReadOnly = true;
            this.tb_num.Size = new System.Drawing.Size(100, 21);
            this.tb_num.TabIndex = 31;
            // 
            // OffStockOrderEditFrm
            // 
            this.AcceptButton = this.btn_ok;
            this.CancelButton = this.btn_cancel;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.tb_num);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.tb_weight);
            this.Controls.Add(this.labelX1);
            this.Name = "OffStockOrderEditFrm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "出库订单编辑";
            this.Load += new System.EventHandler(this.OrderEditFrm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OffStockOrderEditFrm_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btn_cancel;
        private DevComponents.DotNetBar.ButtonX btn_ok;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_weight;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_num;
    }
}