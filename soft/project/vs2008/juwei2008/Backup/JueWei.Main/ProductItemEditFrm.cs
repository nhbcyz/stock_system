using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using JueWei.Common;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class ProductItemEditFrm : BaseForm
    {
        private Model.ims_dict_product _mproduct = null;
        private bool _adding = false;
        public ProductItemEditFrm()
        {
            InitializeComponent();
        }

        public ProductItemEditFrm(Model.ims_dict_product mproduct, bool adding)
        {
            this._mproduct = mproduct;
            this._adding = adding;
            InitializeComponent();
            UpdateUI();
        }

        private void UpdateUI()
        {
            if (_adding)
            {
                return;
            }
            if (this._mproduct == null)
            {
                return;
            }
            tb_materialno.Text = _mproduct.Materialno.ToString();
            tb_productname.Text = _mproduct.Productname.ToString();
            tb_shortname.Text = _mproduct.Shortname.ToString();
            tb_spec.Text = _mproduct.Spec.ToString();
            tb_weight.Text = _mproduct.Weight.ToString();
            tb_unit.Text = _mproduct.Unit.ToString();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tb_productname.Text.Trim()) ||
                    string.IsNullOrEmpty(tb_spec.Text.Trim()) ||
                    string.IsNullOrEmpty(tb_weight.Text.Trim()) ||
                    string.IsNullOrEmpty(tb_unit.Text.Trim()) ||
                    string.IsNullOrEmpty(tb_shortname.Text.Trim())
                    )
                {
                    MessageExUtil.ShowWarning("参数不能为空");
                    return;
                }
                decimal weight = 0.0m;
                if (!decimal.TryParse(tb_weight.Text.Trim(), out weight))
                {
                    MessageExUtil.ShowWarning("重量填写错误");
                    return;
                }
                _mproduct.Productname = tb_productname.Text.TrimEnd();
                _mproduct.Spec = tb_spec.Text.TrimEnd();
                _mproduct.Weight = weight;
                _mproduct.Unit = tb_unit.Text.TrimEnd();
                _mproduct.Shortname = tb_shortname.Text.TrimEnd();
                if (!BLL.ims_dict_product.Update(_mproduct))
                {
                    MessageExUtil.ShowError("保存失败");
                    return;
                }
            }
            catch (System.Exception ex)
            {
                MessageExUtil.ShowError("保存失败");
                LogHelper.Error(typeof(ProductItemEditFrm), ex);
            }

            this.Close();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}