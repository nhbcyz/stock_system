namespace JueWei.Main
{
    partial class WaitWeightFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.tb_weight = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btn_ok = new DevComponents.DotNetBar.ButtonX();
            this.btn_cancel = new DevComponents.DotNetBar.ButtonX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.tb_orderweight = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.tb_boxnum = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_packetweight = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.btn_weight = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(13, 87);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "请输入重量";
            // 
            // tb_weight
            // 
            // 
            // 
            // 
            this.tb_weight.Border.Class = "TextBoxBorder";
            this.tb_weight.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_weight.Location = new System.Drawing.Point(155, 88);
            this.tb_weight.Name = "tb_weight";
            this.tb_weight.PreventEnterBeep = true;
            this.tb_weight.Size = new System.Drawing.Size(100, 21);
            this.tb_weight.TabIndex = 1;
            // 
            // btn_ok
            // 
            this.btn_ok.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ok.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ok.Location = new System.Drawing.Point(180, 192);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ok.TabIndex = 2;
            this.btn_ok.Text = "确定";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Location = new System.Drawing.Point(57, 192);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cancel.TabIndex = 3;
            this.btn_cancel.Text = "取消";
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(13, 16);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 4;
            this.labelX2.Text = "订单重量";
            // 
            // tb_orderweight
            // 
            // 
            // 
            // 
            this.tb_orderweight.Border.Class = "TextBoxBorder";
            this.tb_orderweight.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_orderweight.Location = new System.Drawing.Point(155, 16);
            this.tb_orderweight.Name = "tb_orderweight";
            this.tb_orderweight.PreventEnterBeep = true;
            this.tb_orderweight.ReadOnly = true;
            this.tb_orderweight.Size = new System.Drawing.Size(100, 21);
            this.tb_orderweight.TabIndex = 5;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(13, 125);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 6;
            this.labelX3.Text = "请输入框数";
            // 
            // tb_boxnum
            // 
            // 
            // 
            // 
            this.tb_boxnum.Border.Class = "TextBoxBorder";
            this.tb_boxnum.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_boxnum.Location = new System.Drawing.Point(155, 129);
            this.tb_boxnum.Name = "tb_boxnum";
            this.tb_boxnum.PreventEnterBeep = true;
            this.tb_boxnum.Size = new System.Drawing.Size(100, 21);
            this.tb_boxnum.TabIndex = 7;
            // 
            // tb_packetweight
            // 
            // 
            // 
            // 
            this.tb_packetweight.Border.Class = "TextBoxBorder";
            this.tb_packetweight.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_packetweight.Location = new System.Drawing.Point(155, 53);
            this.tb_packetweight.Name = "tb_packetweight";
            this.tb_packetweight.PreventEnterBeep = true;
            this.tb_packetweight.ReadOnly = true;
            this.tb_packetweight.Size = new System.Drawing.Size(100, 21);
            this.tb_packetweight.TabIndex = 9;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(13, 52);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 8;
            this.labelX4.Text = "包装重量";
            // 
            // btn_weight
            // 
            this.btn_weight.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_weight.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_weight.Location = new System.Drawing.Point(180, 227);
            this.btn_weight.Name = "btn_weight";
            this.btn_weight.Size = new System.Drawing.Size(75, 23);
            this.btn_weight.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_weight.TabIndex = 10;
            this.btn_weight.Text = "直接称重";
            this.btn_weight.Visible = false;
            this.btn_weight.Click += new System.EventHandler(this.btn_weight_Click);
            // 
            // WaitWeightFrm
            // 
            this.AcceptButton = this.btn_ok;
            this.CancelButton = this.btn_cancel;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btn_weight);
            this.Controls.Add(this.tb_packetweight);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.tb_boxnum);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.tb_orderweight);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.tb_weight);
            this.Controls.Add(this.labelX1);
            this.Name = "WaitWeightFrm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "称重页";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WaitWeightFrm_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_weight;
        private DevComponents.DotNetBar.ButtonX btn_ok;
        private DevComponents.DotNetBar.ButtonX btn_cancel;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_orderweight;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_boxnum;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_packetweight;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX btn_weight;
    }
}