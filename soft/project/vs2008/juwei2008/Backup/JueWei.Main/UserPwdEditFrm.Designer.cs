namespace JueWei.Main
{
    partial class UserPwdEditFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_oldpwd = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lb_oldpwd = new DevComponents.DotNetBar.LabelX();
            this.btn_cancel = new DevComponents.DotNetBar.ButtonX();
            this.btn_ok = new DevComponents.DotNetBar.ButtonX();
            this.tb_secondpwd = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_pwd = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_user = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // tb_oldpwd
            // 
            // 
            // 
            // 
            this.tb_oldpwd.Border.Class = "TextBoxBorder";
            this.tb_oldpwd.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_oldpwd.Location = new System.Drawing.Point(176, 64);
            this.tb_oldpwd.Name = "tb_oldpwd";
            this.tb_oldpwd.PasswordChar = '*';
            this.tb_oldpwd.PreventEnterBeep = true;
            this.tb_oldpwd.Size = new System.Drawing.Size(112, 21);
            this.tb_oldpwd.TabIndex = 57;
            // 
            // lb_oldpwd
            // 
            // 
            // 
            // 
            this.lb_oldpwd.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb_oldpwd.Location = new System.Drawing.Point(84, 64);
            this.lb_oldpwd.Name = "lb_oldpwd";
            this.lb_oldpwd.Size = new System.Drawing.Size(76, 23);
            this.lb_oldpwd.TabIndex = 56;
            this.lb_oldpwd.Text = "旧密码";
            // 
            // btn_cancel
            // 
            this.btn_cancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cancel.Location = new System.Drawing.Point(341, 271);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cancel.TabIndex = 54;
            this.btn_cancel.Text = "取消";
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ok.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ok.Location = new System.Drawing.Point(212, 271);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ok.TabIndex = 53;
            this.btn_ok.Text = "确定";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // tb_secondpwd
            // 
            // 
            // 
            // 
            this.tb_secondpwd.Border.Class = "TextBoxBorder";
            this.tb_secondpwd.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_secondpwd.Location = new System.Drawing.Point(176, 154);
            this.tb_secondpwd.Name = "tb_secondpwd";
            this.tb_secondpwd.PasswordChar = '*';
            this.tb_secondpwd.PreventEnterBeep = true;
            this.tb_secondpwd.Size = new System.Drawing.Size(111, 21);
            this.tb_secondpwd.TabIndex = 52;
            // 
            // tb_pwd
            // 
            // 
            // 
            // 
            this.tb_pwd.Border.Class = "TextBoxBorder";
            this.tb_pwd.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_pwd.Location = new System.Drawing.Point(176, 111);
            this.tb_pwd.Name = "tb_pwd";
            this.tb_pwd.PasswordChar = '*';
            this.tb_pwd.PreventEnterBeep = true;
            this.tb_pwd.Size = new System.Drawing.Size(111, 21);
            this.tb_pwd.TabIndex = 51;
            // 
            // tb_user
            // 
            this.tb_user.AcceptsTab = true;
            // 
            // 
            // 
            this.tb_user.Border.Class = "TextBoxBorder";
            this.tb_user.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_user.Location = new System.Drawing.Point(176, 29);
            this.tb_user.Name = "tb_user";
            this.tb_user.PreventEnterBeep = true;
            this.tb_user.ReadOnly = true;
            this.tb_user.Size = new System.Drawing.Size(111, 21);
            this.tb_user.TabIndex = 50;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(84, 154);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 48;
            this.labelX3.Text = "确认密码";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(84, 111);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 47;
            this.labelX2.Text = "新密码";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(84, 29);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 46;
            this.labelX1.Text = "用户名";
            // 
            // UserPwdEditFrm
            // 
            this.ClientSize = new System.Drawing.Size(488, 323);
            this.Controls.Add(this.tb_oldpwd);
            this.Controls.Add(this.lb_oldpwd);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.tb_secondpwd);
            this.Controls.Add(this.tb_pwd);
            this.Controls.Add(this.tb_user);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UserPwdEditFrm";
            this.Text = "密码修改";
            this.Load += new System.EventHandler(this.UserPwdEditFrm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.TextBoxX tb_oldpwd;
        private DevComponents.DotNetBar.LabelX lb_oldpwd;
        private DevComponents.DotNetBar.ButtonX btn_cancel;
        private DevComponents.DotNetBar.ButtonX btn_ok;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_secondpwd;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_pwd;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_user;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
    }
}