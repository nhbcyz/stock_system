﻿namespace JueWei.Main
{
    partial class RegForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_regnum = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_macnum = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnReg = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "注册码：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "机器码：";
            // 
            // tb_regnum
            // 
            // 
            // 
            // 
            this.tb_regnum.Border.Class = "TextBoxBorder";
            this.tb_regnum.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_regnum.Location = new System.Drawing.Point(63, 39);
            this.tb_regnum.Name = "tb_regnum";
            this.tb_regnum.PreventEnterBeep = true;
            this.tb_regnum.Size = new System.Drawing.Size(209, 21);
            this.tb_regnum.TabIndex = 6;
            // 
            // tb_macnum
            // 
            // 
            // 
            // 
            this.tb_macnum.Border.Class = "TextBoxBorder";
            this.tb_macnum.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_macnum.Location = new System.Drawing.Point(63, 12);
            this.tb_macnum.Name = "tb_macnum";
            this.tb_macnum.PreventEnterBeep = true;
            this.tb_macnum.Size = new System.Drawing.Size(209, 21);
            this.tb_macnum.TabIndex = 7;
            // 
            // btnReg
            // 
            this.btnReg.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnReg.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnReg.Location = new System.Drawing.Point(185, 72);
            this.btnReg.Name = "btnReg";
            this.btnReg.Size = new System.Drawing.Size(75, 23);
            this.btnReg.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnReg.TabIndex = 8;
            this.btnReg.Text = "注册";
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            // 
            // RegForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 107);
            this.Controls.Add(this.btnReg);
            this.Controls.Add(this.tb_macnum);
            this.Controls.Add(this.tb_regnum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "RegForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "RegForm";
            this.Load += new System.EventHandler(this.RegForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_regnum;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_macnum;
        private DevComponents.DotNetBar.ButtonX btnReg;
    }
}