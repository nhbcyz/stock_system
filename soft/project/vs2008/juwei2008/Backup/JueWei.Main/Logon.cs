using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;
using System.Diagnostics;
using JueWei.Main.Common;
using WHC.WareHouseMis.UI;
using JueWei.Bussiness;

namespace JueWei.Main
{
    public partial class Logon : BaseForm
    {
        public bool bLogin = false;
        private bool debug = true;
        //private RegisterHotKeyHelper hotKey1 = new RegisterHotKeyHelper();
        //private RegisterHotKeyHelper hotKey2 = new RegisterHotKeyHelper();
        
        private const string Login_Name_Key = "WareHouseMis_LoginName";
        
        private BackgroundWorker updateWorker;

        public Logon()
        {
            this.InitializeComponent();
            try
            {
                AppCommon.Instance.Init();
                this.InitLoginName();
                this.InitHistoryLoginName();
                this.SetHotKey();
            }
            catch (Exception exception)
            {
                // MessageExUtil.ShowError(exception.Message);
                LogHelper.Error(exception);
            }
            this.btExit.DialogResult = DialogResult.Cancel;
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
            Application.ExitThread();
        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            //if (debug)
            //{
            //    this.bLogin = true;
            //    base.DialogResult = DialogResult.OK;
            //    Portal.gc.LoginInfo = BLL.ims_users.SelectModelByWhereOrderByID(" username = 'admin' ", true);
            //    IList<Model.ims_acl_function> functionsByUser = BLL.ims_acl_function.GetListByUserName(Portal.gc.LoginInfo.Username);
            //    if ((functionsByUser != null) && (functionsByUser.Count > 0))
            //    {
            //        foreach (Model.ims_acl_function info2 in functionsByUser)
            //        {
            //            if (!Portal.gc.FunctionDict.ContainsKey(info2.Controlid))
            //            {
            //                Portal.gc.FunctionDict.Add(info2.Controlid, info2);
            //            }
            //        }
            //    }
            //}
            if (this.cmbzhanhao.Text.Length == 0)
            {
                MessageExUtil.ShowTips("请输入帐号");
                this.cmbzhanhao.Focus();
            }
            else
            {
                try
                {
                    string userName = this.cmbzhanhao.Text.Trim();
                    string pwd = this.tbPass.Text;
                    Model.ims_users user = BLL.ims_users.VerifyUser(userName, pwd);
                    if (user != null)
                    {
                        IList<Model.ims_acl_function> functionsByUser = BLL.ims_acl_function.GetListByUserName(user.Username);
                        if ((functionsByUser != null) && (functionsByUser.Count > 0))
                        {
                            foreach (Model.ims_acl_function info2 in functionsByUser)
                            {
                                if (!Portal.gc.FunctionDict.ContainsKey(info2.Controlid))
                                {
                                    Portal.gc.FunctionDict.Add(info2.Controlid, info2);
                                }
                            }
                        }
                        RegistryHelper.SaveValue("StockSys_LoginName", userName);
                        Model.ims_logon_log info3 = new Model.ims_logon_log
                        {
                            Logonname = user.Username,
                            Realname = user.Fullname,
                            Userid = user.Id,
                            Note = "用户登录"
                        };
                        BLL.ims_logon_log.Insert(info3);
                        Portal.gc.LoginInfo = user;
                        this.bLogin = true;
                        base.DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        MessageExUtil.ShowTips("用户帐号密码不正确");
                        this.tbPass.Text = "";
                    }
                }
                catch (Exception exception)
                {
                    MessageExUtil.ShowError(exception.Message);
                }
            }
        }

        private void cmbzhanhao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.tbPass.Focus();
            }
            if (e.KeyCode == Keys.F1)
            {
                this.linkHelp_LinkClicked(sender, null);
            }
        }

        private string EncodePassword(string passwordText)
        {
            return EncodeHelper.MD5Encrypt(passwordText);
        }

        private void hotKey1_HotKey()
        {
            this.linkHelp_LinkClicked(null, null);
        }

        private void hotKey2_HotKey()
        {
            this.lnkSecurity_LinkClicked(null, null);
        }

        private void InitHistoryLoginName()
        {
            //string str = RegistryHelper.GetValue("WareHouseMis_LoginName");
            //if (!string.IsNullOrEmpty(str))
            //{
            //    if (this.cmbzhanhao.Items.Count > 0)
            //    {
            //        this.cmbzhanhao.SelectedIndex = this.cmbzhanhao.FindString(str);
            //    }
            //    else
            //    {
            //        this.cmbzhanhao.Text = str;
            //    }
            //}
        }



        private void InitLoginName()
        {
            IList<Model.ims_users> all = BLL.ims_users.GetAllList();
            this.cmbzhanhao.Items.Clear();
            foreach (Model.ims_users info in all)
            {
                this.cmbzhanhao.Items.Add(info.Username);
            }
        }

        private void linkHelp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("Help.chm");
            }
            catch (Exception)
            {
                //MessageExUtil.ShowWarning("不能打开帮助！");
            }
        }

        private void lnkSecurity_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //WHC.Security.UI.Portal.StartLogin();
        }

        private void Logon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btLogin_Click(sender, null);
            }
            if (e.KeyCode == Keys.F1)
            {
                this.linkHelp_LinkClicked(sender, null);
            }
        }

        private void Logon_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip tip = new System.Windows.Forms.ToolTip();
            tip.SetToolTip(this.cmbzhanhao, "首次登录的默认账号为:admin");
            tip.SetToolTip(this.tbPass, "首次登录的默认密码为空, \r\n 以后请更改默认密码！");
            CCalendar calendar = new CCalendar();
            //this.lblCalendar.Text = calendar.GetDateInfo(DateTime.Now).Fullinfo;
            AppConfig config = new AppConfig();
            this.Text = config.AppName;
            this.lblTitle.Text = config.AppName;
            if (this.cmbzhanhao.Text != "")
            {
                this.tbPass.Focus();
            }
            else
            {
                this.cmbzhanhao.Focus();
            }
            this.updateWorker = new BackgroundWorker();
            this.updateWorker.DoWork += new DoWorkEventHandler(this.updateWorker_DoWork);
            this.updateWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.updateWorker_RunWorkerCompleted);
            string str = config.AppConfigGet("AutoUpdate");
            if (!string.IsNullOrEmpty(str))
            {
                bool result = false;
                bool.TryParse(str, out result);
                if (result)
                {
                    this.updateWorker.RunWorkerAsync();
                }
            }
        }

        private void SetHotKey()
        {
            //this.hotKey1.Keys = Keys.F1;
            //this.hotKey1.ModKey = (RegisterHotKeyHelper.MODKEY) 0;
            //this.hotKey1.WindowHandle = base.Handle;
            //this.hotKey1.WParam = 0x2711;
            //this.hotKey1.HotKey += new RegisterHotKeyHelper.HotKeyPass(this.hotKey1_HotKey);
            //this.hotKey1.StarHotKey();
            //this.hotKey2.Keys = Keys.F2;
            //this.hotKey2.ModKey = (RegisterHotKeyHelper.MODKEY) 0;
            //this.hotKey2.WindowHandle = base.Handle;
            //this.hotKey2.WParam = 0x2712;
            //this.hotKey2.HotKey += new RegisterHotKeyHelper.HotKeyPass(this.hotKey2_HotKey);
            //this.hotKey2.StarHotKey();
        }

        private void tbPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btLogin_Click(sender, null);
            }
            if (e.KeyCode == Keys.F1)
            {
                this.linkHelp_LinkClicked(sender, null);
            }
        }

        private void updateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //UpdateClass class2 = new UpdateClass();
                //if (class2.HasNewVersion && (MessageExUtil.ShowYesNoAndTips("有新的版本，是否需要更新") == DialogResult.Yes))
                //{
                //    Process.Start(Path.Combine(Application.StartupPath, "Updater.exe"), "121");
                //    Application.Exit();
                //}
            }
            catch (Exception exception)
            {
                //MessageExUtil.ShowError(exception.Message);
            }
        }

        private void updateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }
    }
}