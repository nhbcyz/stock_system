using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using JueWei.Main.Controls;

namespace JueWei.Main
{
    public partial class DeviceInfoFrm : BaseForm
    {
        private int _lasselectindex = 0; //最后选中索引
        private bool _modifying = false; //界面正在刷新

        public DeviceInfoFrm()
        {
            InitializeComponent();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            AddData();
        }
        private void InitAuthorizedUI()
        {
            this.btn_add.Visible = Portal.gc.HasFunction("EditDevice");
            this.btn_delete.Visible = Portal.gc.HasFunction("EditDevice");
            this.btn_edit.Visible = Portal.gc.HasFunction("EditDevice");

            this.btn_active.Visible = Portal.gc.HasFunction("ActiveDevice");
            this.btn_stop.Visible = Portal.gc.HasFunction("ActiveDevice");
        }
        private void AddData()
        {
            DeviceItemEditFrm frm = new DeviceItemEditFrm(null, true);
            frm.ShowDialog();
            _modifying = true;
            BindData();
        }
        private void btn_edit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }

            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            DeleteData(id);
        }

        private void DeleteData(string id)
        {
            if (!BLL.ims_devices.Delete(int.Parse(id)))
            {
                MessageBox.Show("删除失败");
                return;
            }
            BindData();
        }

        private void EditData()
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            Model.ims_devices mpro = BLL.ims_devices.SelectById(int.Parse(id));

            if (mpro == null)
            {
                MessageBox.Show("获取数据失败");
                return;
            }
            DeviceItemEditFrm frm = new DeviceItemEditFrm(mpro, false);
            frm.ShowDialog();
            _modifying = true;
            BindData();
        }

        private void DeviceInfoFrm_Load(object sender, EventArgs e)
        {
            this.pager1.PageCurrent = 1;
            this.pager1.PageSize = 30;
            this.pager1.NMax = BLL.ims_devices.GetRecordCount(""); ;
            //激活OnPageChanged事件
            pager1.EventPaging += new EventPagingHandler(pager1_EventPaging);
            this.pager1.Bind();
            InitAuthorizedUI();
        }

        /// <summary>
        /// 页数变化时调用绑定数据方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private int pager1_EventPaging(EventPagingArg e)
        {
            return BindData();
        }

        private void BindPage()
        {
            this.pager1.PageCurrent = 1;

            this.pager1.Bind();
        }

        private int BindData()
        {
            int recordcount = 0;
            DataSet ds = BLL.ims_devices.GetList(this.pager1.PageCurrent, this.pager1.PageSize, "", "", ref recordcount);
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = ds.Tables[0];
            dataGridViewX1.Rows[_lasselectindex].Selected = true;

            return recordcount;
        }

        private void dataGridViewX1_DoubleClick(object sender, EventArgs e)
        {
            if (Portal.gc.HasFunction("EditDevice"))
            {
                _lasselectindex = dataGridViewX1.CurrentRow.Index;
                EditData();
            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (devicetype.Index == e.ColumnIndex)
            {
                if (e.Value == null)
                {
                    return;
                }
                if (e.Value.ToString() == "0")
                {
                    e.Value = "手持PDA";
                }
                else if (e.Value.ToString() == "1")
                {
                    e.Value = "固定式PDA";
                }
                else {
                    e.Value = "称重仪PDA";
                }
            }

            if (devicestate.Index == e.ColumnIndex)
            {
                if (e.Value == null)
                {
                    return;
                }
                if (e.Value.ToString() == "0")
                {
                    e.Value = "未激活";
                }
                else if(e.Value.ToString() == "1")
                {
                    e.Value = "已停用";
                }
                else
                {
                    e.Value = "已激活";
                }
            }
        }

        private void dataGridViewX1_SelectionChanged(object sender, EventArgs e)
        {
            if (!_modifying)
            {
                _modifying = false;
                if (this.dataGridViewX1.CurrentRow == null)
                {
                    _lasselectindex = 0;
                    return;
                }
                _lasselectindex = dataGridViewX1.CurrentRow.Index;
            }
        }

        private void btn_active_Click(object sender, EventArgs e)
        {
            SwitchDeviceState(2);
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            SwitchDeviceState(0);
        }

        private void SwitchDeviceState(int state)
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            Model.ims_devices mpro = BLL.ims_devices.SelectById(int.Parse(id));

            if (mpro == null)
            {
                MessageBox.Show("获取数据失败");
                return;
            }
            mpro.Devicestate = state;
            if (!BLL.ims_devices.Update(mpro))
            {
                MessageBox.Show("切换设备状态失败");
                return;
            }
            BindData();
        }
    }
}