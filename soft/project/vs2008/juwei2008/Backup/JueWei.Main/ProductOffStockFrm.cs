using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Newtonsoft.Json;
using WHC.OrderWater.Commons;
using WHC.OrderWater.Commons.Threading;
using System.Threading;
using JueWei.Bussiness.protocols;
using JueWei.Common;
using JueWei.Main.Common;
using System.IO;
using Aspose.Cells;
using JueWei.Bussiness;

namespace JueWei.Main
{
    public partial class ProductOffStockFrm : BaseForm
    {
        private JueWei.Bussiness.OrderSetting _order_setting = null;
        private Model.ims_settings _ims_settings = null;
        private DataTable _all_lines = null;
        private DataTable _all_shops = null;
        private DataTable _all_scanned_shops = null;
        private string _line_no = "";
        private QueuedBackgroundWorker worker;
        private bool querylineing = false; //是否正在查询中
        private ProcessBarForm proFrm = null;
        protocol_yaohua_weight_communication _port_protocl = new protocol_yaohua_weight_communication();
        private QueuedBackgroundWorker queryshopworker;
        private DateTime? _scantime = null;
        private string offstocktempfilename = string.Empty;
        private string shiptempfilename = string.Empty;
        private bool formHasClose = false;
        private object thisobj = new object();
        private int _last_hasweigth_selected_index = 0;
        private bool _is_updateui = false; //
        public ProductOffStockFrm()
        {
            InitializeComponent();
            this.permitclose = false;
            offstocktempfilename = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\templates\\交货单格式.xls";
            shiptempfilename = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\templates\\物流交接模板.xls";
        }

        private void ProductOffStockFrm_Load(object sender, EventArgs e)
        {
            _ims_settings = BLL.ims_settings.SelectById("current_order");
            if (_ims_settings != null && !string.IsNullOrEmpty(_ims_settings.Setting_data))
            {
                _order_setting = JsonConvert.DeserializeObject<Bussiness.OrderSetting>(_ims_settings.Setting_data);
            }
            else
            {
                _ims_settings = new Model.ims_settings();
                _ims_settings.Name = "current_order";
                if (!BLL.ims_settings.Exists("current_order"))
                {
                    BLL.ims_settings.Insert(_ims_settings);
                }
            }
            if (_order_setting != null)
            {
                dtinput_select.Value = Convert.ToDateTime(_order_setting.SelectDate);
            }
            else
            {
                dtinput_select.Value = DateTime.Now;
            }
            bool ret = _port_protocl.OpenSerialPort(ConfigHelper.GetConfigInt("WeightDeviceComm"), ConfigHelper.GetConfigInt("WeightDeviceBaund"));
            if (!ret)
            {
                MessageUtil.ShowError("串口打开失败!");
            }
            queryshopworker = new QueuedBackgroundWorker();
            queryshopworker.IsBackground = true;
            queryshopworker.Threads = 1;
            queryshopworker.ProcessingMode = ProcessingMode.FIFO;
            queryshopworker.DoWork += new QueuedWorkerDoWorkEventHandler(QueryShops_worker_DoWork);
            queryshopworker.RunWorkerCompleted += new RunQueuedWorkerCompletedEventHandler(QueryShops_worker_RunWorkerCompleted);
            queryshopworker.RunWorkerAsync("queryshops");

            RefreshWeightCommbox();
        }

        private void btn_selectorder_Click(object sender, EventArgs e)
        {
            if (Math.Abs(dtinput_select.Value.Subtract(DateTime.Now).Days) > 30)
            {
                return;
            }
            if (querylineing)
            {
                MessageUtil.ShowTips("正在查询,请稍后");
                return;
            }
            if (_order_setting == null)
            {
                _order_setting = new Bussiness.OrderSetting();
            }
            _order_setting.SelectDate = dtinput_select.Value.ToString("yyyy-MM-dd");
            string _json = JsonConvert.SerializeObject(_order_setting);
            _ims_settings.Setting_data = _json;
            BLL.ims_settings.Update(_ims_settings);
            proFrm = new ProcessBarForm();
            string text = "正在查询...";
            proFrm.ShowProcess(0, text);

            worker = new QueuedBackgroundWorker();
            worker.IsBackground = true;
            worker.Threads = 1;
            worker.ProcessingMode = ProcessingMode.FIFO;
            worker.DoWork += new QueuedWorkerDoWorkEventHandler(QueryLine_worker_DoWork);
            worker.RunWorkerCompleted += new RunQueuedWorkerCompletedEventHandler(QueryLine_worker_RunWorkerCompleted);
            worker.RunWorkerAsync("queryline");
            //QueryLine();
        }

        private void QueryLine_worker_DoWork(object sender, QueuedWorkerDoWorkEventArgs e)
        {
            querylineing = true;
           DataTable dt =  QueryLine();
            if (dt != null)
            {
                e.Result = dt;
            }

        }

        private DataTable QueryLine()
        {
            if (_order_setting == null || string.IsNullOrEmpty(_order_setting.SelectDate))
            {
                MessageUtil.ShowTips("请先选择时间");
                return null;
            }
            
            DataSet ds = BLL.ims_orders.GetAllLinesByTime(_order_setting.SelectDate);
            if (ds == null)
            {
                MessageUtil.ShowTips("没有可装运的路线");
                return null;
            }
            _all_lines = ds.Tables[0];
            return ds.Tables[0];


        }

        private void QueryLine_worker_RunWorkerCompleted(object sender, QueuedWorkerCompletedEventArgs e)
        {
            
            querylineing = false;
            proFrm.Stop();
            DataTable dt = e.Result as DataTable;
            if (dt != null)
            {
                cb_vechele.DataSource = dt;
                cb_vechele.DisplayMember = "vehiclename";
                cb_vechele.ValueMember = "vehiclelineno";
            }
            worker.CancelAsync();
            worker.Dispose();
            worker = null;
        }

        private void btn_crab_product_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_vechele.Items.Count <= 0)
                {
                    return;
                }
                cb_shops.DataSource = null;

                DataRowView dr = (DataRowView)cb_vechele.Items[cb_vechele.SelectedIndex];
                _line_no = dr["vehiclelineno"].ToString();
                if (!string.IsNullOrEmpty(_line_no))
                {
                    _order_setting.LineNo = _line_no;
                    string _json = JsonConvert.SerializeObject(_order_setting);
                    _ims_settings.Setting_data = _json;
                    BLL.ims_settings.Update(_ims_settings);
                    QueryShops();
                    RefreshWeightCommbox();
                }
                
                
            }
            catch (System.Exception ex)
            {
                MessageUtil.ShowTips("出现一个小问题");
                return;
            }
            
        }

        private void QueryShops()
        {
            DataSet ds = BLL.ims_orders.GetAllShopsByLineAndTime(_order_setting.SelectDate,_line_no);
            if (ds == null)
            {
                MessageUtil.ShowTips("没有该线路对应的店铺");
                return;
            }
            _all_shops = ds.Tables[0];
            cb_shops.DataSource = _all_shops;
            cb_shops.DisplayMember = "shopname";
            cb_shops.ValueMember = "shopno";
            UpdateUI();
        }


        private void QueryShops_worker_DoWork(object sender, QueuedWorkerDoWorkEventArgs e)
        {
            while (!formHasClose)
            {
                
                if (_all_shops == null || _all_shops.Rows.Count == 0 || _order_setting == null || string.IsNullOrEmpty(_order_setting.SelectDate) || string.IsNullOrEmpty(_order_setting.LineNo))
                {
                    Thread.Sleep(50);
                    continue;
                }
                //查询店铺是否拣货完成
                foreach (DataRow dr in _all_shops.Rows)
                {
                    DataSet ds = BLL.ims_orders.GetList(string.Format(" DATE_FORMAT(exchangedate,'%Y-%m-%d') = '{0}' and shopno = '{1}' and exchangeno='{2}' ", _order_setting.SelectDate, dr["shopno"],dr["exchangeno"]));
                    bool instockstate = CheckInStockComplete(ds);
                    if (!instockstate)
                    {
                        dr["status"] = "未拣货";
                        dr["statusno"] = "0";
                    }
                    else
                    {
                        if (ds.Tables[0].Rows[0]["state"].ToString() == "1")
                        {
                            dr["status"] = "已扫码";
                            dr["statusno"] = "1";
                        }
                        else if (ds.Tables[0].Rows[0]["state"].ToString() == "2")
                        {
                            dr["status"] = "已称重";
                            dr["statusno"] = "2";
                        }
                        else
                        {
                            dr["status"] = "已出库";
                            dr["statusno"] = "3";
                        }
                    }
                    lock (this.thisobj)
                    {
                        UpdateUI();
                    }
                }
                Thread.Sleep(1000); //10秒钟轮询一次
            }
        }
        private delegate void UpdateDataGridCallBack();

        private void UpdateUI()
        {
            try
            {
                if (formHasClose)
                {
                    return;
                }
                if (this.dataGridViewX1.InvokeRequired)
                {
                    this.Invoke(new UpdateDataGridCallBack(UpdateUI));
                }
                else
                {
                    this.dataGridViewX1.DataSource = _all_shops;
                    for (int i = 0; i < _all_shops.Rows.Count&& i < this.dataGridViewX1.Rows.Count; i++)
                    {
                        if (_all_shops.Rows[i]["statusno"].ToString() == "0")
                        {
                            this.dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.OrangeRed;
                        }
                        else
                        {
                            this.dataGridViewX1.Rows[i].DefaultCellStyle.BackColor = Color.LightCyan;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

        }

        private void QueryShops_worker_RunWorkerCompleted(object sender, QueuedWorkerCompletedEventArgs e)
        {

        }

        private void ProductOffStockFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            lock (thisobj)
            {
                DialogResult result;

                result = MessageBox.Show("确定退出吗？", "退出", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (result == DialogResult.OK)
                {
                    _port_protocl.CloseSerialPort();
                    if (this.queryshopworker != null)
                    {
                        this.queryshopworker.CancelAsync();
                    }

                    permitclose = true;
                    formHasClose = true;

                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private bool CheckInStockComplete(DataSet ds)
        {
            bool bfound = true;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                if (item["state"].ToString() == "0")
                {
                    return false;
                }
            }
            return bfound;
        }

        private bool CheckShopScanned(string shopno)
        {
            foreach (DataRow dr in _all_shops.Rows)
            {
                if (dr["shopno"].ToString() == shopno)
                {
                    if (dr["statusno"].ToString() == "1")
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void btn_offstock_Click(object sender, EventArgs e)
        {
            try
            {
                if (_scantime == null)
                {
                    _scantime = Convert.ToDateTime(_order_setting.SelectDate);
                }
                if (cb_weightshops.Items.Count <= 0 || cbExchangeNo.Items.Count <= 0)
                {
                    return;
                }

                DataRowView dr = (DataRowView)this.cb_weightshops.Items[cb_weightshops.SelectedIndex];
                string shopno = dr["shopno"].ToString();

                DataRowView dr2 = (DataRowView)cbExchangeNo.Items[cbExchangeNo.SelectedIndex];
                string exchangeo = dr2["exchangeno"].ToString();
                bool hasweight = CheckShopOrderHasWeighted(shopno, exchangeo, Convert.ToDateTime(_order_setting.SelectDate));
                if (!hasweight)
                {
                    MessageUtil.ShowTips("称重未完成,不允许出库");
                    return;
                }
                if(CheckShopHasOffStockEx(shopno, exchangeo, Convert.ToDateTime(_order_setting.SelectDate)))
                {
                    GenerateOffStockPaper(shopno, exchangeo, Convert.ToDateTime(_order_setting.SelectDate));
                    MessageUtil.ShowTips("该商品已出货完成,直接生成出货单");
                    return;
                }
                if (!CheckStock(shopno,exchangeo))
                {
                    return;
                }
 

                DataSet ds = CheckShopHasOffStock(shopno, exchangeo, Convert.ToDateTime(_order_setting.SelectDate));
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    //出库未完成,需更新出货状态
                    UpdateShopOrderToOffStockState(ds);
                }
                
               // UpdateShopOrderToOffStockState(dr["shopno"].ToString(), (DateTime)_scantime);
                
                //if (!ExchangeDataToSap(shopno, exchangeo, Convert.ToDateTime(_order_setting.SelectDate)))
                //{
                //    return;
                //}
                GenerateOffStockPaper(shopno,exchangeo, Convert.ToDateTime(_order_setting.SelectDate));
                
            }
            catch (System.Exception ex)
            {
                MessageUtil.ShowTips("请先选择店铺");
            }
        }

        private void GetShopWeightSum(string shopno)
        {
            if (_order_setting == null || string.IsNullOrEmpty(_order_setting.SelectDate))
            {
                MessageUtil.ShowTips("请先选择时间");
                return;
            }

            DataSet ds = BLL.ims_orders.GetAllLinesByTime(_order_setting.SelectDate);
            if (ds == null)
            {
                MessageUtil.ShowTips("没有可装运的路线");
                return;
            }

        }

        private void btn_entercomplete_Click(object sender, EventArgs e)
        {
            if (cb_shops.Items.Count <= 0)
            {
                return;
            }
            DataRowView dr = (DataRowView)cb_shops.Items[cb_shops.SelectedIndex];
            string shopno = dr["shopno"].ToString();
            bool hasscan = CheckShopScanned(dr["shopno"].ToString());
            if (!hasscan)
            {
                MessageUtil.ShowTips("扫码未完成,请等扫码完成之后再进行确认");
                return;
            }

            string strWhere = string.Format(" DATE_FORMAT(exchangedate, '%Y-%m-%d') = '{0}' and state = 1 and shopno ='{1}' ", _order_setting.SelectDate, shopno);
            DataSet ds = BLL.ims_orders.GetList(strWhere);
            _scantime = DateTime.Now;
            if (!ProductOffStockBussiness.BatchInsertReocrds(ds, ((DateTime)_scantime).ToString("yyyy-MM-dd HH:mm:ss"), _order_setting.SelectDate))
            {
                MessageUtil.ShowError("生成出库记录时出现错误");
                return;
            }
            ProductOffStockBussiness.BatchChangeOrderState(ds, 2);

            #region MyRegion
            //foreach (DataRow item in ds.Tables[0].Rows)
            //{
            //    Model.ims_offstock stock = new JueWei.Model.ims_offstock();
            //    stock.Deviceid = Convert.ToInt32(item["deviceid"]);
            //    stock.Materialno = Convert.ToInt32(item["materialno"]);
            //    stock.Shopno = item["shopno"].ToString();
            //    stock.Orderid = Convert.ToInt32(item["id"]);
            //    stock.Scandate = (DateTime)_scantime;
            //    stock.State = 0;
            //    stock.Orderdate = Convert.ToDateTime(_order_setting.SelectDate);
            //    stock.Weight = Convert.ToDecimal(item["weight"]);
            //    stock.Productnum = Convert.ToInt32(item["verifynum"]);
            //    if (!BLL.ims_offstock.Insert(stock))
            //    {
            //        MessageUtil.ShowError("生成出库记录时出现错误");
            //        return;
            //    }
            //} 
            #endregion

            //foreach (DataRow item in ds.Tables[0].Rows)
            //{
            //    int id = Convert.ToInt32(item["id"]);
            //    Model.ims_orders morder = BLL.ims_orders.SelectById(id);
            //    if (morder!= null)
            //    {
            //        morder.State = 2;
            //        BLL.ims_orders.Update(morder);
            //    }
            //}
            QueryShops();
            //btn_selectorder_Click(null, null);
            QueryOffOrderShops();
            UpdateExchangeNo();
            btn_query_Click(null, null);
            //RefreshWeightGrid(shopno, (DateTime)_scantime);
        }

        //刷新待称重的门店
        private void RefreshWeightCommbox()
        {
            QueryOffOrderShops();
            UpdateExchangeNo();
            if (cb_weightshops.Items.Count <= 0)
            {
                return;
            }
            DataRowView dr = (DataRowView)cb_weightshops.Items[cb_weightshops.SelectedIndex];
            string shopno = dr["shopno"].ToString();
            if (cbExchangeNo.Items.Count <= 0)
            {
                return;
            }
            DataRowView dr2 = (DataRowView)cbExchangeNo.Items[cbExchangeNo.SelectedIndex];
            string exchangeo = dr2["exchangeno"].ToString();

            RefreshWeightGridByExchangeNo(shopno, exchangeo, Convert.ToDateTime(_order_setting.SelectDate));
        }
        /// <summary>
        /// 刷新出货单里面的店铺
        /// </summary>
        private void QueryOffOrderShops()
        {
            _is_updateui = true;
            if (cb_weightshops.Items.Count > 0)
            {
                _last_hasweigth_selected_index = cb_weightshops.SelectedIndex;
            }

            this.cb_weightshops.DataSource = null;
            _all_scanned_shops = null;
            this.cbExchangeNo.DataSource = null;

            DataSet ds = BLL.ims_offstock.GetAllShopsByLineAndTimeDesc(_order_setting.SelectDate, _order_setting.LineNo);
            if (ds == null || ds.Tables[0].Rows.Count <= 0)
            {
                return;
            }
            _all_scanned_shops = ds.Tables[0];
            this.cb_weightshops.DataSource = _all_scanned_shops;
            cb_weightshops.DisplayMember = "shopname";
            cb_weightshops.ValueMember = "shopno";
            if (cb_weightshops.Items.Count > _last_hasweigth_selected_index)
            {
                cb_weightshops.SelectedIndex = _last_hasweigth_selected_index;
            }
            //DataRowView dr = (DataRowView)cb_weightshops.Items[cb_weightshops.SelectedIndex];
            //string shopno = dr["shopno"].ToString();

            //if (string.IsNullOrEmpty(shopno))
            //{
            //    return;
            //}

           // cb_weightshops.SelectedIndex = 0;
            //RefreshWeightGridByExchangeNo(ds.Tables[0].Rows[0]["shopno"].ToString(), Convert.ToDateTime(_order_setting.SelectDate));
            _is_updateui = false;
        }

        //刷新待称重的订单表格Grid
        //private void RefreshWeightGrid(string shopno,DateTime scandate)
        //{
        //    this.dataGridViewX2.DataSource = null;
        //    if (string.IsNullOrEmpty(shopno))
        //    {
        //        MessageUtil.ShowError("未选择店铺");
        //        return;
        //    }
        //    dataGridViewX2.AutoGenerateColumns = false;
        //    this.dataGridViewX2.DataSource = BLL.ims_offstock.GetListByShopNoAndDt(scandate.ToString("yyyy-MM-dd"),shopno).Tables[0];
        //}

        //刷新待称重的订单表格Grid
        private void RefreshWeightGridByExchangeNo(string shopno,string exchangeno, DateTime scandate)
        {
            this.dataGridViewX2.DataSource = null;
            if (string.IsNullOrEmpty(exchangeno))
            {
                MessageUtil.ShowError("未选择出货单");
                return;
            }
            dataGridViewX2.AutoGenerateColumns = false;
            this.dataGridViewX2.DataSource = BLL.ims_offstock.GetListByExchangeNoAndDt(scandate.ToString("yyyy-MM-dd"),shopno, exchangeno).Tables[0];
        }

        private void btn_weight_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_weightshops.Items.Count <= 0)
                {
                    return;
                }
                DataRowView dr = (DataRowView)cb_weightshops.Items[cb_weightshops.SelectedIndex];
                string shopno = dr["shopno"].ToString();

                DataRowView dr2 = (DataRowView)cbExchangeNo.Items[cbExchangeNo.SelectedIndex];
                string exchangeo = dr2["exchangeno"].ToString();

                bool hasweighted = CheckShopHasOffStockOrHasWeighted(dr["shopno"].ToString(), exchangeo,Convert.ToDateTime(_order_setting.SelectDate));
                if (hasweighted)
                {
                    MessageUtil.ShowTips("所选店铺的出货单已称重,不允许再次称重");
                    return;
                }
                //float v = 0.0f;
                //int datatype = 0;
                //int unit = 0;
                //byte[] err = new byte[255];
                //bool ret = _port_protocl.GetVal(ref v, ref datatype, ref unit, err);
                //string error = System.Text.Encoding.Default.GetString(err);
                //if (!ret)
                //{
                //    MessageUtil.ShowError("称重失败,错误为:" + error);
                //    return;
                //}
                decimal orderweight = BLL.ims_offstock.GetSumRealWeightByShopNoAndDt(_order_setting.SelectDate, shopno,exchangeo);
                WaitWeightFrm frm = new WaitWeightFrm(orderweight,_order_setting.SelectDate,shopno,exchangeo);
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    MessageUtil.ShowTips("称重失败");
                    return;
                }
                //if (Math.Abs((decimal)v / orderweight - 1) > weightdiff)
                //{
                //    MessageUtil.ShowTips(String.Format("实际重量{0},订单重量{1},相差超过设定值,请重新称重",v,orderweight));
                //    return;
                //}
                //更新所有订单为称重状态
                
                UpdateShopOrderToWeightState(shopno,exchangeo, Convert.ToDateTime(_order_setting.SelectDate));
                btn_query_Click(null, null);
                MessageUtil.ShowTips("称重完成");
            }
            catch (System.Exception ex)
            {
                MessageUtil.ShowTips("出现错误," + ex.Message);
            }

        }

        //检查店铺是否称重完成
        private bool CheckShopOrderHasWeighted(string shopno,string exchangeno, DateTime scandate)
        {
            DateTime weightdt = DateTime.Now;
            //string strWhere = string.Format(" DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' and state < 1 and shopno ='{1}' and exchangeno='{2}' ", scandate.ToString("yyyy-MM-dd"), shopno,exchangeno);

           // DataSet ds = BLL.ims_offstock.GetList(strWhere);
            DataSet ds = BLL.ims_offstock.CheckShopOrderHasWeighted(shopno, exchangeno, scandate.ToString("yyyy-MM-dd"), 1);
            if (ds != null && ds.Tables[0].Rows.Count == 0)
            {
                return true;
            }
            return false;
        }

        private void UpdateShopOrderToWeightState(string shopno,string exchangeno, DateTime scandate)
        {
            DateTime weightdt = DateTime.Now;
           // string strWhere = string.Format(" DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' and state = 0 and shopno ='{1}' and exchangeno ='{2}' ", scandate.ToString("yyyy-MM-dd"), shopno,exchangeno);
           // int recordcount = 0;
           // IList<Model.ims_offstock> stocks = BLL.ims_offstock.GetListByPage(1,1000,strWhere,ref recordcount);
            DataSet ds = BLL.ims_offstock.GetListByWhere(shopno, exchangeno, scandate.ToString("yyyy-MM-dd"), 0);
            if (ds == null || ds.Tables[0].Rows.Count <= 0)
            {
                return;
            }
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                int id = Convert.ToInt32(dr["id"]);
                Model.ims_offstock item = BLL.ims_offstock.SelectById(id);
                if (item != null)
                {
                    item.State = 1;
                    item.Weightdate = weightdt;
                    if (!BLL.ims_offstock.Update(item))
                    {
                        MessageUtil.ShowError("出现错误");
                        return;
                    }
                }
            }

            //foreach (Model.ims_offstock item in stocks)
            //{
            //    item.State = 1;
            //    item.Weightdate = weightdt;
            //    if (!BLL.ims_offstock.Update(item))
            //    {
            //        MessageUtil.ShowError("出现错误");
            //        return;
            //    }
            //}
        }

        private void dataGridViewX2_DoubleClick(object sender, EventArgs e)
        {
            if (!AppCommon.Instance.FunctionDict.ContainsKey("ModifyWeight"))
            {
                WeightLogonFrm logonfrm = new WeightLogonFrm();
                if (logonfrm.ShowDialog() != DialogResult.OK)
                {
                    MessageBox.Show("没有修改重量的权限");
                    return;
                }
                //MessageUtil.ShowError("该用户不具备称重权限,请重新登陆");
                //return;
            }

            if (dataGridViewX2.CurrentRow == null || dataGridViewX2.CurrentRow.Cells[0].Value == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX2.CurrentRow.Cells[0].Value.ToString();
            Model.ims_offstock mpro = BLL.ims_offstock.SelectById(int.Parse(id));

            if (mpro == null)
            {
                MessageBox.Show("获取数据失败");
                return;
            }
            if (mpro.State > 0)
            {
                MessageBox.Show("订单称重后不允许修改重量");
                return;
            }

            OffStockOrderEditFrm frm = new OffStockOrderEditFrm(mpro, false);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                QueryOffOrderShops();
                UpdateExchangeNo();
                btn_query_Click(null, null);
            }
 
        }


        private void UpdateShopOrderToOffStockState(string shopno,string exchangeno, DateTime scandate)
        {
            DataSet ds = CheckShopHasOffStock(shopno,exchangeno, scandate);
            if (ds == null || ds.Tables[0].Rows.Count <= 0)
            {
                MessageUtil.ShowTips("本店铺所选订单已经出库完成,直接生成出货单");
                return;
            }
            UpdateShopOrderToOffStockState(ds);
        }

        private void UpdateShopOrderToOffStockState(DataSet ds)
        {
            if (ds == null || ds.Tables[0].Rows.Count <= 0)
            {
                return;
            }
            ProductOffStockBussiness.UpdateShopOrderToOffStockState(ds);

            #region 更新出库,废除代码
            //DateTime offstockdt = DateTime.Now;
            //foreach (DataRow dr in ds.Tables[0].Rows)
            //{
            //    Model.ims_offstock item = BLL.ims_offstock.SelectById(int.Parse(dr["id"].ToString()));
            //    if (item == null)
            //    {
            //        continue;
            //    }
            //    item.State = 2;
            //    item.Offstockdate = offstockdt;
            //    if (!BLL.ims_offstock.Update(item))
            //    {
            //        MessageUtil.ShowError("更新出货订单时出现错误");
            //        return;
            //    }
            //} 
            #endregion
        }

        private DataSet CheckShopHasOffStock(string shopno,string exchangeno, DateTime scandate)
        {
            
           // string strWhere = string.Format(" DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' and state = 1 and shopno ='{1}' and exchangeno ='{2}' ", scandate.ToString("yyyy-MM-dd"), shopno,exchangeno);
           // int recordcount = 0;
           // IList<Model.ims_offstock> stocks = BLL.ims_offstock.GetListByPage(1, 1000, strWhere, ref recordcount);
        //    if (stocks == null || stocks.Count == 0)
         //   {
         //       
         //       return null;
          //  }
            DataSet ds = BLL.ims_offstock.GetListByWhere(shopno, exchangeno, scandate.ToString("yyyy-MM-dd"), 1);
            return ds;
        }

        private bool CheckShopHasOffStockEx(string shopno, string exchangeno, DateTime scandate)
        {
            DataSet ds = BLL.ims_offstock.GetListByWhere(shopno, exchangeno, scandate.ToString("yyyy-MM-dd"), 2);
            if (ds != null && ds.Tables.Count >0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        private void GenerateOffStockPaper(string shopno,string exchangeno,DateTime scandate)
        {
            try
            {
                string exportpath = ConfigHelper.GetConfigString("DefaultExportPath");
                
                string datepath = Convert.ToDateTime(_order_setting.SelectDate).ToString("yyyyMMdd");

                exportpath = exportpath + "\\" + datepath + "\\";

                CheckExportPath(exportpath);

                exportpath = exportpath + exchangeno + ".xls";

                #region MyRegion
                //SaveFileDialog dlg = new SaveFileDialog();
                //dlg.InitialDirectory = "c:\\";
                //dlg.RestoreDirectory = false;
                //dlg.Filter = "excel 03文件|*.xls|excel 07文件|*.xlsx";
                //string fName = string.Empty;
                //if (dlg.ShowDialog() != DialogResult.OK)
                //{
                //    return;
                //}

                //fName = dlg.FileName;
                //if (string.IsNullOrEmpty(fName))
                //{
                //    return;
                //} 
                #endregion
               
                if (ExportOffStockPapaer(exportpath, shopno, exchangeno, scandate))
                {
                    MessageUtil.ShowTips("生成成功");
                    return;
                }
                MessageUtil.ShowTips("生成失败");
            }
            catch
            {

            }
        }

        private void CheckExportPath(string exportpath)
        {
            if (!Directory.Exists(exportpath))
            {
                Directory.CreateDirectory(exportpath);
            }
        }

        private bool ExportOffStockPapaer(string path, string shopno,string exchangeno, DateTime scandate)
        {
            string filepath = Path.GetDirectoryName(path);
            string filename = System.IO.Path.GetFileName(path);

            try
            {

                Model.ims_dict_shop mshop = BLL.ims_dict_shop.SelectModelByWhereOrderByID(string.Format(" shopno='{0}' ", shopno), true);
                if (mshop == null)
                {
                    MessageUtil.ShowTips("未选择店铺");
                    return false;
                }
                DataTable dtorders = BLL.ims_offstock.GetListByExchangeNoAndDt(scandate.ToString("yyyy-MM-dd"), shopno,exchangeno).Tables[0];
                if (mshop == null || dtorders.Rows.Count <= 0)
                {
                    MessageUtil.ShowTips("未查询到该店铺对应的出货单下的订单");
                    return false;
                }
                Model.ims_orders morder = BLL.ims_orders.SelectById(Convert.ToInt32(dtorders.Rows[0]["orderid"]));
                if (morder == null)
                {
                    MessageUtil.ShowTips("未查询订单");
                    return false;
                }
                File.Delete(path);
                File.Copy(offstocktempfilename, path);
                Workbook workbook = new Workbook(path);

                Cells cells = workbook.Worksheets[0].Cells;

                //装运顺序
                cells[1, 9].PutValue(mshop.Shiporder);
                //交货单号
                cells[4, 1].PutValue(morder.Exchangeno);
                //客户编号
                cells[4, 3].PutValue(mshop.Shopno);
                //客户名称
                //cells.Merge(4, 2, 1, 2);
                cells[4, 5].PutValue(mshop.Shopname);
                //cells[4, 4].SetStyle(new Style() { HorizontalAlignment = TextAlignmentType.Center }); //设置单元格合并后垂直居中显示
                //制单日期
                cells[4, 8].PutValue(DateTime.Now.ToShortDateString());
                //客户地址
                cells[5, 1].PutValue(morder.Exchangeaddr);
                //客户电话
                cells[5, 5].PutValue(mshop.Tel);
                //订单日期
                cells[5, 8].PutValue(morder.Verifydate);
                //发货工厂

                //运输区域
                cells[6, 2].PutValue(cells[6, 2].Value.ToString() + mshop.Vehiclename);
                //物流电话不填

                //发货日期
                cells[6, 8].PutValue(DateTime.Now.ToShortDateString());

                #region 填充订单
                //填充订单
                int startrow = 8;
                for (int i = 0; i < dtorders.Rows.Count; i++)
                {
                    //序号
                    cells[startrow + i, 0].PutValue(i + 1);
                    //厂品编码
                    cells[startrow + i, 1].PutValue(dtorders.Rows[i]["materialno"]);
                    //产品名称及规格
                    cells[startrow + i, 2].PutValue(dtorders.Rows[i]["productname"].ToString() + dtorders.Rows[i]["spec"].ToString());
                    string spec = dtorders.Rows[i]["spec"].ToString();
                    //交货量
                    cells[startrow + i, 3].PutValue(Convert.ToDecimal(dtorders.Rows[i]["productnum"].ToString()));


                    //规格
                    decimal weight = 0;

                    if (spec.IndexOf("J") > 0)
                    {
                        weight = Convert.ToDecimal(spec.Substring(0, spec.IndexOf('J')));
                        cells[startrow + i, 6].PutValue(weight.ToString() + "斤");
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dtorders.Rows[i]["productnum"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("包");
                    }
                    else if (spec.IndexOf("P") > 0)
                    {
                        weight = Convert.ToDecimal(spec.Substring(0, spec.IndexOf('P')));
                        cells[startrow + i, 6].PutValue(weight.ToString() + "个");
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dtorders.Rows[i]["weight"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("KG");
                    }
                    else if (spec.IndexOf("ZHI") >= 0)
                    {
                        cells[startrow + i, 6].PutValue("只");
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dtorders.Rows[i]["weight"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("KG");
                    }
                    else if (spec.IndexOf("KG") >= 0)
                    {
                        if (Convert.ToDecimal(dtorders.Rows[i]["weight"].ToString()) == 0.5m)
                        {
                            cells[startrow + i, 6].PutValue("1斤");
                        }
                        else
                        {
                            cells[startrow + i, 6].PutValue("2斤");
                        }
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dtorders.Rows[i]["weight"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("KG");
                    }
                    else if (spec.IndexOf("L21") >= 0)
                    {
                        cells[startrow + i, 6].PutValue("L21");
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dtorders.Rows[i]["weight"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("KG");
                    }
                    else if (spec.IndexOf("HE") >= 0)
                    {
                        cells[startrow + i, 6].PutValue("HE");
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dtorders.Rows[i]["productnum"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("盒");
                    }
                    else
                    {
                        cells[startrow + i, 6].PutValue(spec);
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dtorders.Rows[i]["weight"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("KG");
                    }
                    //批次号
                    cells[startrow + i, 7].PutValue(DateTime.Now.ToString("yyMMdd") + "0001");

                } 
                #endregion

                //填统计信息
                //总框数
                cells[42, 7].PutValue(dtorders.Rows[0]["boxnum"].ToString());


                //订单总重量
                decimal orderweight = BLL.ims_offstock.GetSumRealWeightByShopNoAndDt(_order_setting.SelectDate, shopno,exchangeno);
                decimal maoweight = BLL.ims_offstock.GetSumMaoWeightByShopNoAndDt(_order_setting.SelectDate, shopno,exchangeno);
                decimal packweight = BLL.ims_offstock.GetSumPacketWeightByShopNoAndDt(_order_setting.SelectDate, shopno,exchangeno);


                int boxnum = Convert.ToInt32(dtorders.Rows[0]["boxnum"]);
                decimal weightOfBox = ConfigHelper.GetConfigDecimal("BoxHeavy");

                //毛重,订单重量+框重+袋子重量
                decimal toweight = orderweight + packweight + weightOfBox * boxnum;
                cells[43, 2].PutValue(toweight.ToString());
                //净重
                cells[43, 7].PutValue(orderweight.ToString());
                //实收总毛重
                cells[44, 4].PutValue(dtorders.Rows[0]["offsumweight"].ToString());

                //制单人
                cells[46, 1].PutValue(Portal.gc.LoginInfo.Username);

                workbook.Save(path);
                System.Diagnostics.Process.Start(path);


            }
            catch (System.Exception ex)
            {
                return false;
            }
            return true;
        }

        private void btn_query_Click(object sender, EventArgs e)
        {
            try
            {
                if (cb_weightshops.Items.Count == 0)
                {
                    this.dataGridViewX2.DataSource = null;
                    this.cb_weightshops.Text = "";
                    return;
                }
                if (cb_weightshops.SelectedIndex == -1)
                {
                    cb_weightshops.SelectedIndex = 0;
                }
                DataRowView dr = (DataRowView)cb_weightshops.Items[cb_weightshops.SelectedIndex];
                string shopno = dr["shopno"].ToString();

                if (cbExchangeNo.SelectedIndex == -1)
                {
                    cbExchangeNo.SelectedIndex = 0;
                }
                DataRowView dr2 = (DataRowView)cbExchangeNo.Items[cbExchangeNo.SelectedIndex];
                string exchangeo = dr2["exchangeno"].ToString();
                RefreshWeightGridByExchangeNo(shopno,exchangeo, Convert.ToDateTime(_order_setting.SelectDate));
            }
            catch (System.Exception ex)
            {

            }
        }

        private bool DeleteOffStockByShopNoAndDt(string shopno,string exchangeno, DateTime scandate)
        {
            //string strWhere = string.Format(" DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' and shopno ='{1}' and exchangeno = '{2}' ", scandate.ToString("yyyy-MM-dd"), shopno,exchangeno);

            return BLL.ims_offstock.DeleteData(shopno, exchangeno, scandate.ToString("yyyy-MM-dd"));
        }

        private bool UpdateShopOrderToNoScanState(string shopno, string exchangeno,DateTime scandate)
        {
            string strWhere = string.Format(" DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' and shopno ='{1}' and exchangeno='{2}' ", scandate.ToString("yyyy-MM-dd"), shopno,exchangeno);
            return BLL.ims_orders.UpdateShopOrderStateByShopNoAndDt(0, scandate.ToString("yyyy-MM-dd"), shopno,exchangeno);
        }
        /// <summary>
        /// 检查出货单是否已称重或是是否已称重
        /// </summary>
        /// <param name="shopno"></param>
        /// <param name="scandate"></param>
        /// <returns></returns>
        private bool CheckShopHasOffStockOrHasWeighted(string shopno,string exchangeno, DateTime scandate)
        {
           // string strWhere = string.Format(" DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' and state > 0 and shopno ='{1}' and exchangeno = '{2}' ", scandate.ToString("yyyy-MM-dd"), shopno,exchangeno);
            DataSet ds = BLL.ims_offstock.CheckShopHasOffStockOrHasWeighted(shopno, exchangeno, scandate.ToString("yyyy-MM-dd"), 0);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            return true;
        }

        private void btn_reselectOrder_Click(object sender, EventArgs e)
        {
            try
            {
                if (_scantime == null)
                {
                    _scantime = Convert.ToDateTime(_order_setting.SelectDate);
                }
                DataRowView dr = (DataRowView)cb_weightshops.Items[cb_weightshops.SelectedIndex];
                string shopno = dr["shopno"].ToString();

                DataRowView dr2 = (DataRowView)cbExchangeNo.Items[cbExchangeNo.SelectedIndex];
                string exchangeo = dr2["exchangeno"].ToString();
                RefreshWeightGridByExchangeNo(shopno, exchangeo, Convert.ToDateTime(_order_setting.SelectDate));


                bool hasweight = CheckShopHasOffStockOrHasWeighted(dr["shopno"].ToString(),exchangeo, Convert.ToDateTime(_order_setting.SelectDate));
                if (hasweight)
                {
                    MessageUtil.ShowTips("称重已完成,不允许该操作");
                    return;
                }

                //重新拣货前先将该店铺的出库数据删除
                if (!DeleteOffStockByShopNoAndDt(shopno,exchangeo, Convert.ToDateTime(_order_setting.SelectDate)))
                {
                    MessageUtil.ShowTips("删除出库单失败,不允许进行该操作");
                    return;
                }

                //更新订单状态为未扫码状态
                if (!UpdateShopOrderToNoScanState(shopno,exchangeo, Convert.ToDateTime(_order_setting.SelectDate)))
                {
                    MessageUtil.ShowTips("更新订单为未扫码状态操作失败,执行重新拣货操作失败");
                    return;
                }

                //刷新上方的店铺订单信息
                QueryShops();
                RefreshWeightCommbox();
                //刷新下方的店铺出货信息
                QueryOffOrderShops();
                btn_query_Click(null, null);


                MessageUtil.ShowTips("执行重新拣货操作成功");

            }
            catch (System.Exception ex)
            {
            	
            }

        }

        private void cb_weightshops_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_is_updateui)
            {
                UpdateExchangeNo();
               // btn_query_Click(null, null);
            }
            
        }

        private void dataGridViewX1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            foreach (DataGridViewRow dr in this.dataGridViewX1.Rows)
            {
                dr.Cells[0].Value = dr.Index + 1;
            }
        }

        private void UpdateExchangeNo()
        {
            if (cb_weightshops.Items.Count == 0)
            {
                this.dataGridViewX2.DataSource = null;
                this.cb_weightshops.Text = "";
                return;
            }
            if (cb_weightshops.SelectedIndex == -1)
            {
                cb_weightshops.SelectedIndex = 0;
            }

            DataRowView dr = (DataRowView)cb_weightshops.Items[cb_weightshops.SelectedIndex];
            string shopno = dr["shopno"].ToString();

            if (string.IsNullOrEmpty(shopno))
            {
                return;
            }
            DataSet ds = BLL.ims_offstock.GetAllExchangeNoByShopNoAndTime(_order_setting.SelectDate, shopno);
            if (ds == null || ds.Tables[0].Rows.Count <= 0)
            {
                return;
            }

            this.cbExchangeNo.DataSource = ds.Tables[0];
            cbExchangeNo.DisplayMember = "exchangeno";
            cbExchangeNo.ValueMember = "exchangeno";
            cbExchangeNo.SelectedIndex = 0;
            RefreshWeightGridByExchangeNo(shopno,ds.Tables[0].Rows[0]["exchangeno"].ToString(), Convert.ToDateTime(_order_setting.SelectDate));
        }
        private void cbExchangeNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_is_updateui)
            {
                btn_query_Click(null, null);
            }
        }

        private void btn_exportall_Click(object sender, EventArgs e)
        {
            if (MessageUtil.ShowYesNoAndTips("导出所有订单会花费较长时间,是否继续?") != DialogResult.Yes)
            {
                return;
            }
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.ShowNewFolderButton = true;
            dlg.RootFolder = System.Environment.SpecialFolder.Desktop;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                if (dlg.SelectedPath.Trim() == "")
                {
                    return;
                }
                string fpath = dlg.SelectedPath;

                DataSet ds = BLL.ims_orders.GetAllLinesByTime(_order_setting.SelectDate);
                if (ds == null || ds.Tables[0].Rows.Count <= 0)
                {
                    MessageUtil.ShowTips("没有可装运的路线");
                    return;
                }

                proFrm = new ProcessBarForm();
                string text = "正在导出...";
                proFrm.ShowProcess(0, text);

                QueuedBackgroundWorker theworker = new QueuedBackgroundWorker();
                theworker.IsBackground = true;
                theworker.Threads = 1;
                theworker.ProcessingMode = ProcessingMode.FIFO;
                theworker.DoWork += new QueuedWorkerDoWorkEventHandler(ExportOrders_worker_DoWork);
                theworker.RunWorkerCompleted += new RunQueuedWorkerCompletedEventHandler(ExportOrders_worker_RunWorkerCompleted);
                theworker.RunWorkerAsync(fpath);
            }
        }

        private void ExportOrders_worker_DoWork(object sender, QueuedWorkerDoWorkEventArgs e)
        {
            try
            {
                if (e.Argument == null)
                {
                    e.Result = false;
                    return;
                }
                string fpath = e.Argument.ToString();
                if (string.IsNullOrEmpty(fpath))
                {
                    e.Result = false;
                    return;
                }
                querylineing = true;
                DataTable dt = QueryLine();
                if (dt == null || dt.Rows.Count <= 0)
                {
                    e.Result = false;
                    return;
                }
                foreach (DataRow dr in dt.Rows)
                {
                    string vechileno = dr["vehiclelineno"].ToString();
                    string vecpath = fpath + "\\" + vechileno + "\\";
                    if (!Directory.Exists(vecpath))
                    {
                        Directory.CreateDirectory(vecpath);
                    }
                    DataSet ds = BLL.ims_orders.GetAllShopsByLineAndTimeEx(_order_setting.SelectDate, vechileno);
                    if (ds == null || ds.Tables[0].Rows.Count <= 0)
                    {
                        continue;
                    }
                    foreach (DataRow drshop in ds.Tables[0].Rows)
                    {
                        string shopno = drshop["shopno"].ToString();
                        string shopxlspath = vecpath + shopno + ".xls";
                        if (File.Exists(shopxlspath))
                        {
                            File.Delete(shopxlspath);
                        }
                        //string strWhere = string.Format(" DATE_FORMAT(exchangedate, '%Y-%m-%d') = '{0}' and shopno ='{1}' ", _order_setting.SelectDate, shopno);


                        DataSet dsorders = BLL.ims_orders.GetOrdersByWhere(_order_setting.SelectDate,shopno);
                        if (dsorders == null || dsorders.Tables[0].Rows.Count <= 0)
                        {
                            continue;
                        }
                        ExportSingleShopOrder(shopxlspath, dsorders.Tables[0]);
                    }
                }
            }
            catch (System.Exception ex)
            {
                e.Result = false;
            }
            e.Result = true;
        }

        private void ExportOrders_worker_RunWorkerCompleted(object sender, QueuedWorkerCompletedEventArgs e)
        {
            proFrm.Stop();
            bool ret = (bool)e.Result;
            if (ret)
            {
                MessageUtil.ShowTips("导出成功");
            }
            else
            {
                MessageUtil.ShowError("导出失败");
            }
        }

        private void ExportSingleShopOrder(string path,DataTable dt)
        {
            try
            {
                string filepath = Path.GetDirectoryName(path);
                string filename = System.IO.Path.GetFileName(path);

                File.Copy(offstocktempfilename, path);
                Workbook workbook = new Workbook(path);
                Worksheet sheet = workbook.Worksheets[0];
                Cells cells = sheet.Cells;
                int Colnum = dt.Columns.Count;//表格列数 
                int Rownum = dt.Rows.Count;//表格行数 
                Model.ims_dict_shop mshop = BLL.ims_dict_shop.SelectModelByWhereOrderByID(string.Format(" shopno='{0}' ", dt.Rows[0]["shopno"].ToString()), true);
                if (mshop == null)
                {
                    MessageUtil.ShowTips("未选择店铺");
                    return;
                }
                Model.ims_orders morder = BLL.ims_orders.SelectById(Convert.ToInt32(dt.Rows[0]["id"]));
                if (morder == null)
                {
                    MessageUtil.ShowTips("未查询到订单");
                    return;
                }

                //交货单号
                cells[4, 1].PutValue(morder.Exchangeno);
                //客户编号
                cells[4, 3].PutValue(mshop.Shopno);
                //客户名称
                //cells.Merge(4, 2, 1, 2);
                cells[4, 5].PutValue(mshop.Shopname);
                //cells[4, 4].SetStyle(new Style() { HorizontalAlignment = TextAlignmentType.Center }); //设置单元格合并后垂直居中显示
                //制单日期
                cells[4, 8].PutValue(DateTime.Now.ToShortDateString());
                //客户地址
                cells[5, 1].PutValue(morder.Exchangeaddr);
                //客户电话
                cells[5, 5].PutValue(mshop.Tel);
                //订单日期
                cells[5, 8].PutValue(morder.Exchangedate.ToShortDateString());
                //发货工厂

                //运输区域
                cells[6, 2].PutValue(cells[6, 2].Value.ToString() + mshop.Vehiclename);
                //物流电话不填

                //发货日期
                cells[6, 8].PutValue(DateTime.Now.ToShortDateString());

                #region 填充订单
                //填充订单
                int startrow = 8;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //序号
                    cells[startrow + i, 0].PutValue(i + 1);
                    //厂品编码
                    cells[startrow + i, 1].PutValue(dt.Rows[i]["materialno"]);
                    //产品名称及规格
                    cells[startrow + i, 2].PutValue(dt.Rows[i]["orderexplain"].ToString() + dt.Rows[i]["spec"].ToString());
                    string spec = dt.Rows[i]["spec"].ToString();
                    //交货量
                    cells[startrow + i, 3].PutValue(Convert.ToDecimal(dt.Rows[i]["verifynum"].ToString()));


                    //规格
                    decimal weight = 0;

                    if (spec.IndexOf("J") > 0)
                    {
                        weight = Convert.ToDecimal(spec.Substring(0, spec.IndexOf('J')));
                        cells[startrow + i, 6].PutValue(weight.ToString() + "斤");
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dt.Rows[i]["verifynum"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("包");
                    }
                    else if (spec.IndexOf("P") > 0)
                    {
                        weight = Convert.ToDecimal(spec.Substring(0, spec.IndexOf('P')));
                        cells[startrow + i, 6].PutValue(weight.ToString() + "个");
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dt.Rows[i]["weight"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("KG");
                    }
                    else if (spec.IndexOf("ZHI") >= 0)
                    {
                        cells[startrow + i, 6].PutValue("只");
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dt.Rows[i]["weight"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("KG");
                    }
                    else if (spec.IndexOf("KG") >= 0)
                    {
                        if (Convert.ToDecimal(dt.Rows[i]["weight"].ToString()) == 0.5m)
                        {
                            cells[startrow + i, 6].PutValue("1斤");
                        }
                        else
                        {
                            cells[startrow + i, 6].PutValue("2斤");
                        }
                        
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dt.Rows[i]["weight"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("KG");
                    }
                    else if (spec.IndexOf("L21") >= 0)
                    {
                        cells[startrow + i, 6].PutValue("L21");
                        //出厂数
                        cells[startrow + i, 4].PutValue(Convert.ToDecimal(dt.Rows[i]["weight"].ToString()));
                        //单位
                        cells[startrow + i, 5].PutValue("KG");
                    }
                    //批次号
                    cells[startrow + i, 7].PutValue(DateTime.Now.ToString("yyMMdd") + "0001");

                }
                #endregion

                //填统计信息
                //总框数
                //cells[42, 7].PutValue(dt.Rows[0]["boxnum"].ToString());


                //订单总重量
                decimal orderweight = BLL.ims_orders.GetSumRealWeightByShopNoAndDt(_order_setting.SelectDate, morder.Shopno, morder.Exchangeno);
              //  decimal maoweight = BLL.ims_offstock.GetSumMaoWeightByShopNoAndDt(_order_setting.SelectDate, morder.Shopno, morder.Exchangeno);
               // decimal packweight = BLL.ims_offstock.GetSumPacketWeightByShopNoAndDt(_order_setting.SelectDate, morder.Shopno, morder.Exchangeno);


               // int boxnum = Convert.ToInt32(dt.Rows[0]["boxnum"]);
              //  decimal weightOfBox = ConfigHelper.GetConfigDecimal("BoxHeavy");

                //毛重,订单重量+框重+袋子重量
               // decimal toweight = orderweight + packweight + weightOfBox * boxnum;
               // cells[43, 2].PutValue(toweight.ToString());
                //净重
                cells[43, 7].PutValue(orderweight.ToString());


                //int icol = 0;
                //cells[0, icol++].PutValue("序号");
                //cells[0, icol++].PutValue("订单号码");
                //cells[0, icol++].PutValue("单品名字");
                //cells[0, icol++].PutValue("确认日期");
                //cells[0, icol++].PutValue("订单数量");
                //cells[0, icol++].PutValue("出货日期");
                //cells[0, icol++].PutValue("物料编号");
                //cells[0, icol++].PutValue("单位");
                //cells[0, icol++].PutValue("发货日期");
                //cells[0, icol++].PutValue("店铺地址码");
                //cells[0, icol++].PutValue("店铺名字");
                //cells[0, icol++].PutValue("重量");
                //cells[0, icol++].PutValue("店铺编号");
                //cells[0, icol++].PutValue("出货单号");
                //cells[0, icol++].PutValue("出货地址");

                ////生成数据行 
                //for (int i = 0; i < Rownum; i++)
                //{
                //    for (int k = 0; k < Colnum; k++)
                //    {
                //        cells[1 + i, k].PutValue(dt.Rows[i][k].ToString());
                //    }
                //}
                workbook.Save(path);
            }
            catch (System.Exception ex)
            {
            	
            }

        }

        private void ProductOffStockFrm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Oem1: // 分号
                    // ..
                    btn_weight_Click(null,null);
                    break;

                case Keys.Oem7: // 引号
                    // ...
                    btn_offstock_Click(null, null);
                    break;
                default:
                    break;
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Oem1: // 分号
                    // ..
                    btn_weight_Click(null, null);
                    return true;

                case Keys.Oem7: // 引号
                    // ...
                    btn_offstock_Click(null, null);
                    return true;
                case Keys.OemPeriod:
                    btn_entercomplete_Click(null, null);
                    return true;
                default:
                    return false;
            }

            return false;
        }

        private void btn_generateship_Click(object sender, EventArgs e)
        {
            GenerateShipPaper();
        }

        /// <summary>
        /// 检查线路是否拣货完成
        /// </summary>
        /// <param name="lineno"></param>
        /// <param name="scandate"></param>
        /// <returns></returns>
        private bool CheckLineHasScrab(string lineno,DateTime scandate)
        {
            return BLL.ims_orders.CheckHasScrabedByLineAndDt(scandate.ToString("yyyy-MM-dd"), lineno);
        }

        /// <summary>
        /// 检查线路是否出库完成
        /// </summary>
        /// <param name="lineno"></param>
        /// <param name="scandate"></param>
        /// <returns></returns>
        private bool CheckLineHasOffStock(string lineno, DateTime scandate)
        {
            return BLL.ims_offstock.CheckHasOffStockByLineAndDt(scandate.ToString("yyyy-MM-dd"), lineno);
        }

        private void GenerateShipPaper()
        {
            if (_scantime == null)
            {
                _scantime = Convert.ToDateTime(_order_setting.SelectDate);
            }
            if (this.cb_vechele.Items.Count <= 0)
            {
                return;
            }
            DataRowView dr = (DataRowView)this.cb_vechele.Items[cb_vechele.SelectedIndex];
            string vehiclelineno = dr["vehiclelineno"].ToString();

            bool hasweight = CheckLineHasScrab(vehiclelineno, Convert.ToDateTime(_order_setting.SelectDate));
            if (!hasweight)
            {
                MessageUtil.ShowTips("拣货未完成,不允许生成物流控制记录表");
                return;
            }
            bool hasoffstock = CheckLineHasOffStock(vehiclelineno, Convert.ToDateTime(_order_setting.SelectDate));
            if (!hasoffstock)
            {
                MessageUtil.ShowTips("出库未完成,不允许生成物流控制记录表");
                return;
            }
            GenerateShipPaper(vehiclelineno, Convert.ToDateTime(_order_setting.SelectDate));
        }

        private void GenerateShipPaper(string lineno, DateTime scandate)
        {
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.InitialDirectory = "c:\\";
                dlg.RestoreDirectory = false;
                dlg.Filter = "excel 03文件|*.xls|excel 07文件|*.xlsx";
                string fName = string.Empty;
                if (dlg.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                fName = dlg.FileName;
                if (string.IsNullOrEmpty(fName))
                {
                    return;
                }

                if (ExportShipPapaer(fName, lineno, scandate))
                {
                    MessageUtil.ShowTips("生成成功");
                    return;
                }
                MessageUtil.ShowTips("生成失败");
            }
            catch
            {

            }
        }
        private bool ExportShipPapaer(string path, string lineno, DateTime scandate)
        {
            string filepath = Path.GetDirectoryName(path);
            string filename = System.IO.Path.GetFileName(path);

            try
            {

                DataSet ds = BLL.ims_offstock.QueryAllStocks(scandate.ToString("yyyy-MM-dd"),lineno);
                if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
                {
                    MessageUtil.ShowTips("没有出货记录");
                    return false;
                }

                File.Delete(path);
                File.Copy(shiptempfilename, path);
                Workbook workbook = new Workbook(path);

                Cells cells = workbook.Worksheets[0].Cells;
                DataTable dt = ds.Tables[0];
                DataTable dtshops = dt.DefaultView.ToTable(true, "shopno", "shopname", "boxnum");
                //线路名称
                cells[1, 3].PutValue(dt.Rows[0]["vehiclename"].ToString());
                //出厂时间
                cells[2, 3].PutValue(DateTime.Now.ToString("yyyy-MM-dd"));

                cells[1, 8].PutValue("");

                int sumboxnum = 0; ;
                #region 填充门店
                //填充订单
                int startrow = 6;
                for (int i = 0; i < dtshops.Rows.Count; i++)
                {
                    string shopno = dtshops.Rows[i]["shopno"].ToString();
                    //门店编码
                    cells[startrow + i, 1].PutValue(shopno);
                    //门店名称
                    cells[startrow + i, 2].PutValue(dtshops.Rows[i]["shopname"]);
                    //产品种类
                    object countpro = dt.Compute("COUNT(materialno)", "shopno='" + shopno + "'");
                    cells[startrow + i, 3].PutValue(countpro);
                    //重量数
                    object sumweight = dt.Compute("SUM(weights)", "shopno='" + shopno + "'");
                    cells[startrow + i, 4].PutValue(sumweight);
                    //出厂框数
                    cells[startrow + i, 5].PutValue(dtshops.Rows[i]["boxnum"]);
                    sumboxnum += Convert.ToInt32(dtshops.Rows[i]["boxnum"]);
                }

                #endregion
                //出厂总框数
               
                cells[3, 3].PutValue(sumboxnum);
                //司机及跟车人员
                //发货时间
                cells[105, 3].PutValue(DateTime.Now.ToString("yyyy-MM-dd"));
                //删除多余的行
                if (dtshops.Rows.Count < 99)
                {
                    cells.DeleteRows(startrow + dtshops.Rows.Count, 99 - dtshops.Rows.Count);
                }
                
                workbook.Save(path);
                System.Diagnostics.Process.Start(path);


            }
            catch (System.Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 检查库存
        /// </summary>
        /// <param name="shopno"></param>
        /// <returns></returns>
        private bool CheckStock(string shopno,string exchangeno)
        {
            if (string.IsNullOrEmpty(shopno) || string.IsNullOrEmpty(exchangeno))
            {
                return false;
            }
            try
            {
                DataSet ds = BLL.ims_orders.GetList(string.Format(" DATE_FORMAT(exchangedate,'%Y-%m-%d') = '{0}' and shopno = '{1}' and exchangeno='{2}' ", _order_setting.SelectDate, shopno,exchangeno));
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string materialno = dr["materialno"].ToString();
                        int verifynum = int.Parse(dr["verifynum"].ToString());
                        if (!CheckStockByMaterialno(materialno, verifynum, DateTime.Now))
                        {
                            MessageUtil.ShowTips(string.Format("单品:{0}库存不足,物料编号为{1}", dr["orderexplain"], materialno));
                            return false;
                        }
                    }
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                LogHelper.Error(ex.Message);
            }
            return false;
        }

        private bool CheckStockByMaterialno(string materialno,int productnum,DateTime dt)
        {
            try
            {
                if (string.IsNullOrEmpty(materialno))
                {
                    return false;
                }
                int stocknum = StockBussiness.GetStockByMaterialNo(materialno, dt);
                int offstocknum = StockBussiness.GetOffStockByMaterialNo(materialno, dt);
                if (offstocknum + productnum <= stocknum)
                {
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                LogHelper.Error(ex.Message);
            }
            return false;
        }

        private bool ExchangeDataToSap(string shopno, string exchangeno, DateTime scandate)
        {
            Model.ims_dict_shop mshop = BLL.ims_dict_shop.SelectModelByWhereOrderByID(string.Format(" shopno='{0}' ", shopno), true);
            if (mshop == null)
            {
                MessageUtil.ShowTips("未选择店铺");
                return false;
            }
            DataTable dtorders = BLL.ims_offstock.GetListByExchangeNoAndDt(scandate.ToString("yyyy-MM-dd"), shopno, exchangeno).Tables[0];
            if (mshop == null || dtorders.Rows.Count <= 0)
            {
                MessageUtil.ShowTips("未查询到该店铺对应的出货单下的订单");
                return false;
            }
            DataSet dsorder = BLL.ims_orders.GetList(string.Format(" exchangeno = '{0}'", dtorders.Rows[0]["exchangeno"]));
            List<RFC_EBELN> datas = new List<RFC_EBELN>();
            foreach (DataRow dr in dsorder.Tables[0].Rows)
            {
                RFC_EBELN data = new RFC_EBELN();
                data.EBELN = dr["exchangeno"].ToString();
                data.POSNR = dr["posnr"].ToString();
                data.MATNR = dr["materialno"].ToString();
                data.MAKTX = dr["orderexplain"].ToString();
                data.LFDAT = Convert.ToDateTime(dr["verifydate"].ToString()).ToString("yyyyMMdd"); ;
                data.BLDAT = Convert.ToDateTime(dr["exchangedate"].ToString()).ToString("yyyyMMdd"); ;
                data.WADAT = Convert.ToDateTime(dr["expressdate"].ToString()).ToString("yyyyMMdd"); ;
                data.LFIMG = float.Parse(dr["verifynum"].ToString());
                data.VRKME = dr["unit"].ToString();
                data.KUNNR = dr["shopno"].ToString();
                data.NAME1 = dr["shopname"].ToString();
                data.STREET = dr["exchangeaddr"].ToString();
                
                datas.Add(data);
            }
            IRFCComm rfccomm = new RFCComm(ConfigHelper.GetConfigString("RFCHost"),
                                ConfigHelper.GetConfigString("RFCSN"),
                                ConfigHelper.GetConfigString("RFCUser"),
                                EncodeHelper.DesDecrypt(ConfigHelper.GetConfigString("RFCPassword")),
                                ConfigHelper.GetConfigString("RFCClient"));

            RFC_Comm_Obj_Out rfcdata = rfccomm.ZMM_VL02N(datas);
            if (rfcdata == null)
            {
                MessageUtil.ShowTips("同步到SAP失败");
                return false;
            }
            if (!rfcdata.Error)
            {
                MessageUtil.ShowTips(string.Format("同步到SAP失败,错误:{0}", rfcdata.ErrorMsg));
                return false;
            }
            return true;

        }

        private void btn_exchange2sap_Click(object sender, EventArgs e)
        {
            if (_scantime == null)
            {
                _scantime = Convert.ToDateTime(_order_setting.SelectDate);
            }
            if (cb_weightshops.Items.Count <= 0 || cbExchangeNo.Items.Count <= 0)
            {
                return;
            }

            DataRowView dr = (DataRowView)this.cb_weightshops.Items[cb_weightshops.SelectedIndex];
            string shopno = dr["shopno"].ToString();

            DataRowView dr2 = (DataRowView)cbExchangeNo.Items[cbExchangeNo.SelectedIndex];
            string exchangeno = dr2["exchangeno"].ToString();
            bool hasweight = CheckShopOrderHasWeighted(shopno, exchangeno, Convert.ToDateTime(_order_setting.SelectDate));
            if (!hasweight)
            {
                MessageUtil.ShowTips("称重未完成,不允许过账");
                return;
            }

            if (CheckShopHasOffStockEx(shopno, exchangeno, Convert.ToDateTime(_order_setting.SelectDate)))
            {

            }
            else
            {
                if (!CheckStock(shopno, exchangeno))
                {
                    return;
                }
            }



            if (ExchangeDataToSap(shopno, exchangeno, Convert.ToDateTime(_order_setting.SelectDate)))
            {
                MessageUtil.ShowTips("过账成功");
            }
        }

        private void btn_tosapall_Click(object sender, EventArgs e)
        {
            if (MessageUtil.ShowYesNoAndTips("导出所有订单会花费较长时间,是否继续?") != DialogResult.Yes)
            {
                return;
            }

                DataSet ds = BLL.ims_orders.GetAllLinesByTime(_order_setting.SelectDate);
                if (ds == null || ds.Tables[0].Rows.Count <= 0)
                {
                    MessageUtil.ShowTips("没有可过账的路线");
                    return;
                }

                proFrm = new ProcessBarForm();
                string text = "正在过账...";
                proFrm.ShowProcess(0, text);

                QueuedBackgroundWorker theworker = new QueuedBackgroundWorker();
                theworker.IsBackground = true;
                theworker.Threads = 1;
                theworker.ProcessingMode = ProcessingMode.FIFO;
                theworker.DoWork += new QueuedWorkerDoWorkEventHandler(ToSAPAll_worker_DoWork);
                theworker.RunWorkerCompleted += new RunQueuedWorkerCompletedEventHandler(ToSAPAll_worker_RunWorkerCompleted);
                theworker.RunWorkerAsync("");

        }
        private void ToSAPAll_worker_DoWork(object sender, QueuedWorkerDoWorkEventArgs e)
        {
            try
            {
                OperationResult op = null;
                DataSet dsexchanges = BLL.ims_orders.GetOrdersGroupByExchangeno(_order_setting.SelectDate);
                if (dsexchanges == null || dsexchanges.Tables.Count <= 0 || dsexchanges.Tables[0].Rows.Count <=0 )
                {
                    op = new OperationResult()
                    {
                        Result = false,
                        ErrorMsg = "没有查到能过账的数据"
                    };
                    e.Result = op;
                    return;
                }
                int nsucc = 0, nfailed = 0;
                string errormsg = string.Empty;
              
                foreach (DataRow dr in dsexchanges.Tables[0].Rows)
                {
                    string exchangeno = dr["exchangeno"].ToString();
                    string shopno = dr["shopno"].ToString();


                    bool hasweight = CheckShopOrderHasWeighted(shopno, exchangeno, Convert.ToDateTime(_order_setting.SelectDate));
                    if (!hasweight)
                    {
                        errormsg = string.Format("称重未完成,不允许过账,出货单号为:{0},门店编号:{1}", exchangeno, shopno);
                        op = new OperationResult()
                        {
                            Result = false,
                            ErrorMsg = errormsg
                        };
                        e.Result = op;
                        return;
                    }
                    if (CheckShopHasOffStockEx(shopno, exchangeno, Convert.ToDateTime(_order_setting.SelectDate)))
                    {

                    }
                    else
                    {
                        if (!CheckStock(shopno, exchangeno))
                        {
                            errormsg = string.Format("出货单库存不足,出货单号为:{0},门店编号:{1}", exchangeno, shopno);
                            op = new OperationResult()
                            {
                                Result = false,
                                ErrorMsg = errormsg
                            };
                            e.Result = op;
                            return;
                        }
                    }


                    DataTable dtorders = BLL.ims_offstock.GetListByExchangeNoAndDt(_order_setting.SelectDate, shopno, exchangeno).Tables[0];
                    if (dtorders.Rows.Count <= 0)
                    {
                        errormsg = string.Format("未查询到该店铺对应的出货单下的订单,出货单号为:{0},门店编号:{1}", exchangeno, shopno);
                        op = new OperationResult()
                        {
                            Result = false,
                            ErrorMsg = errormsg
                        };
                        e.Result = op;
                        return;
                    }

                    if (!ExchangeDataToSap(shopno, exchangeno, Convert.ToDateTime(_order_setting.SelectDate)))
                    {
                        nfailed++;
                        Model.ims_dict_shop mshop = BLL.ims_dict_shop.SelectModelByWhereOrderByID(string.Format("shopno = '{0}'", shopno), true);
                        if (mshop != null)
                        {
                            MessageUtil.ShowError(string.Format("同步线路编号{0}门店{1}出货单号{2}出货单出错", mshop.Vehiclename,mshop.Shopname,exchangeno));
                        }
                        
                        continue;
                    }

                    nsucc++;
                    
                }
                errormsg = string.Format("过账结果--成功个数:{0},失败个数:{1}", nsucc, nfailed);
                op = new OperationResult()
                {
                    Result = true,
                    ErrorMsg = errormsg
                };
                e.Result = op;
                return;
            }
            catch (System.Exception ex)
            {
                string errormsg = string.Format("过账过程出现异常:{0}", ex.Message);
                OperationResult op = new OperationResult()
                {
                    Result = false,
                    ErrorMsg = errormsg
                };
                e.Result = op;
                return;
            }
        }

        private void ToSAPAll_worker_RunWorkerCompleted(object sender, QueuedWorkerCompletedEventArgs e)
        {
            try
            {
                proFrm.Stop();
                OperationResult ret = (OperationResult)e.Result;
                if (ret.Result)
                {
                    MessageUtil.ShowTips(ret.ErrorMsg);
                }
                else
                {
                    MessageUtil.ShowError("导出失败,错误详情为:" + ret.ErrorMsg);
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            

        }

    }
}