using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using JueWei.Main.Controls;
using System.IO;
using JueWei.Common;
using Aspose.Cells;
using JueWei.Main.Common;
using WHC.OrderWater.Commons;
using JueWei.Bussiness;

namespace JueWei.Main
{
    public partial class ShopDictFrm : BaseForm
    {
        private int _lasselectindex = 0; //最后选中索引
        private bool _modifying = false; //界面正在刷新

        public ShopDictFrm()
        {
            InitializeComponent();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            AddData();
        }

        private void AddData()
        {
            DeviceItemEditFrm frm = new DeviceItemEditFrm(null, true);
            frm.ShowDialog();
            _modifying = true;
            BindData();
        }
        private void btn_edit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }

            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            DeleteData(id);
        }

        private void DeleteData(string id)
        {
            if (!BLL.ims_dict_shop.Delete(int.Parse(id)))
            {
                MessageBox.Show("删除失败");
                return;
            }
            BindData();
        }
        private void EditData()
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            Model.ims_devices mpro = BLL.ims_devices.SelectById(int.Parse(id));

            if (mpro == null)
            {
                MessageBox.Show("获取数据失败");
                return;
            }
            DeviceItemEditFrm frm = new DeviceItemEditFrm(mpro, false);
            frm.ShowDialog();
            _modifying = true;
            BindData();
        }
        private void DeviceInfoFrm_Load(object sender, EventArgs e)
        {
            this.pager1.PageCurrent = 1;
            this.pager1.PageSize = 30;
            this.pager1.NMax = BLL.ims_devices.GetRecordCount(""); ;
            //激活OnPageChanged事件
            pager1.EventPaging += new EventPagingHandler(pager1_EventPaging);
            this.pager1.Bind();
        }

        /// <summary>
        /// 页数变化时调用绑定数据方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private int pager1_EventPaging(EventPagingArg e)
        {
            return BindData();
        }

        private void BindPage()
        {
            this.pager1.PageCurrent = 1;

            this.pager1.Bind();
        }
        private void btn_import_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConfigHelper.GetConfigInt("DataSource") == 0)
                {
                    ImportFromExcel();
                }
                else
                {
                    ImportFromSAP();
                }
               // ImportFromExcel();
               // ImportFromSAP();
            }
            catch (System.Exception ex)
            {
                MessageUtil.ShowError("导入失败");
            }

        }
        private void ImportFromExcel()
        {
            string filename = OpenFile();
            if (string.IsNullOrEmpty(filename))
            {
                return;
            }
            DataTable dt = ImportFromExcel(filename);
            ShopBussiness.Update(dt);
            this.pager1.PageCount = BindData();
            MessageUtil.ShowTips("导入成功");

        }

        private void ImportFromSAP()
        {
            try
            {
                IRFCComm rfccomm = new RFCComm(ConfigHelper.GetConfigString("RFCHost"),
                                                ConfigHelper.GetConfigString("RFCSN"),
                                                ConfigHelper.GetConfigString("RFCUser"),
                                                EncodeHelper.DesDecrypt(ConfigHelper.GetConfigString("RFCPassword")),
                                                ConfigHelper.GetConfigString("RFCClient"));

                RFC_Comm_Obj_Out rfcdata = rfccomm.ZMM_GET_KUNNR(ConfigHelper.GetConfigString("RFCFactoryNo"), "");
                if (rfcdata == null)
                {
                    MessageUtil.ShowTips("从SAP导入数据失败");
                    return;
                }
                if (!rfcdata.Error)
                {
                    MessageUtil.ShowTips(string.Format("从SAP导入数据失败,错误:{0}", rfcdata.ErrorMsg));
                    return;
                }
                if (rfcdata.Data == null)
                {
                    MessageUtil.ShowTips(string.Format("从SAP导入数据失败,错误:{0}", "SAP没有返回数据"));
                    return;
                }
                ShopBussiness.Update(rfcdata.Data);
                this.pager1.PageCount = BindData();
                MessageUtil.ShowTips("导入成功");

            }
            catch (System.Exception ex)
            {
                MessageUtil.ShowTips(ex.Message);
            }


        }


        private string OpenFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.RestoreDirectory = false;
            openFileDialog.InitialDirectory = ConfigHelper.GetConfigString("LastImportPath");
            openFileDialog.Filter = "excel 03文件|*.xls|excel 07文件|*.xlsx";

            openFileDialog.FilterIndex = 1;

            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return "";
            }
            FileInfo f = new FileInfo(openFileDialog.FileName);
            ConfigHelper.SetAppSetting("LastImportPath", Path.GetDirectoryName(openFileDialog.FileName), AppCommon.Instance._Config);
            return openFileDialog.FileName;
        }
        private DataTable ImportFromExcel(string filename)
        {
            Workbook workbook = new Workbook(filename);

            Cells cells = workbook.Worksheets[0].Cells;

            DataTable dt = cells.ExportDataTableAsString(6, 1, cells.MaxDataRow, cells.MaxColumn, false);
            return dt;

        }
        private int BindData()
        {
            int recordcount = 0;
            if (this.pager1.PageCurrent == 0)
            {
                this.pager1.PageCurrent = 1;
            }

            DataSet ds = BLL.ims_dict_shop.GetList(this.pager1.PageCurrent, this.pager1.PageSize, "", "", ref recordcount);
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = ds.Tables[0];
            if (dataGridViewX1.RowCount <= _lasselectindex)
            {
                _lasselectindex = 0;
            }
            dataGridViewX1.Rows[_lasselectindex].Selected = true;

            return recordcount;
        }

        private void dataGridViewX1_DoubleClick(object sender, EventArgs e)
        {
            _lasselectindex = dataGridViewX1.CurrentRow.Index;
            EditData();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

        }

        private void dataGridViewX1_SelectionChanged(object sender, EventArgs e)
        {
            if (!_modifying)
            {
                _modifying = false;
                if (dataGridViewX1.CurrentRow != null && dataGridViewX1.Rows.Count < _lasselectindex)
                {
                    _lasselectindex = dataGridViewX1.CurrentRow.Index;
                }
                
            }
        }

    }
}