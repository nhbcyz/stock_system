using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class UserPwdEditFrm : BaseForm
    {
        public UserPwdEditFrm()
        {
            InitializeComponent();
        }
        private void UpdateUI()
        {
            this.tb_user.Text = Portal.gc.LoginInfo.Username;
            this.tb_oldpwd.Text = "";
            this.tb_pwd.Text = "";
            this.tb_secondpwd.Text = "";
        }
        private void UserPwdEditFrm_Load(object sender, EventArgs e)
        {
            UpdateUI();
        }


        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {

                {
                    if (string.IsNullOrEmpty(tb_pwd.Text.Trim()) ||
                    string.IsNullOrEmpty(tb_oldpwd.Text.Trim()) ||
                        string.IsNullOrEmpty(this.tb_secondpwd.Text.Trim())
                    )
                    {
                        MessageExUtil.ShowWarning("��������Ϊ��");
                        return;
                    }
                    //�޸�����
                    if (tb_pwd.Text.Trim() != this.tb_secondpwd.Text.Trim())
                    {
                         MessageExUtil.ShowWarning("�������벻һ��");
                         return;
                    }

                    string pwd = MD5Util.GetMD5_32(this.tb_oldpwd.Text.Trim());
                    string oldpwd= MD5Util.GetMD5_32(pwd + Portal.gc.LoginInfo.Salt);
                    if (oldpwd != Portal.gc.LoginInfo.Password)
                    {
                        MessageExUtil.ShowWarning("��������֤ʧ��");
                        return;
                    }
                    string newpwd = MD5Util.GetMD5_32(this.tb_pwd.Text.Trim());
                    string newPassword = MD5Util.GetMD5_32(newpwd + Portal.gc.LoginInfo.Salt);
                    Portal.gc.LoginInfo.Password = newPassword;
                    //���޸����������
                    if (!BLL.ims_users.Update(Portal.gc.LoginInfo))
                    {
                        MessageExUtil.ShowError("�޸�����ʧ��");
                        return;
                    }
                    MessageExUtil.ShowError("�޸�����ɹ�");
                }

            }
            catch (System.Exception ex)
            {
                MessageExUtil.ShowError("����ʧ��");
                LogHelper.Error(typeof(DeviceItemEditFrm), ex);
            }
        }
    }
}