using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;
using System.IO;
using Aspose.Cells;

namespace JueWei.Main
{
    public partial class ProductOffStockStasticFrm : BaseForm
    {
        private int _lasselectindex = 0; //最后选中索引
        private bool _modifying = false; //界面正在刷新
        private SearchCondition _conditions = new SearchCondition();

        private DataSet _alldevices = null;
        public ProductOffStockStasticFrm()
        {
            InitializeComponent();
        }


        private void BindData()
        {
            DateTime start = dtinput_start.Value;
            DateTime end = dtinput_end.Value;

            DataSet ds = BLL.ims_offstock.GetProductSumWeightByDateTime(start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = ds.Tables[0];
            if (dataGridViewX1.RowCount <= _lasselectindex)
            {
                _lasselectindex = 0;
            }

            dataGridViewX1.Rows[_lasselectindex].Selected = true;
        }

        private DataTable ComputeStock()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("productname");
            datatable.Columns.Add("materialno");
            datatable.Columns.Add("diffweights");

            DateTime dt = dtinput_start.Value;

            DateTime start = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            DateTime end = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
            DataSet dsoffstock = BLL.ims_offstock.GetProductSumWeightByDateTime(start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));
            DataSet dsinstock = BLL.ims_instock_record.GetProductSumWeightByDateTime(start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));

            foreach (DataRow dr in dsinstock.Tables[0].Rows)
            {
                
            }

            return datatable;
        }
        private void dataGridViewX1_SelectionChanged(object sender, EventArgs e)
        {
            if (!_modifying)
            {
                _modifying = false;
                if (dataGridViewX1.CurrentRow != null)
                {
                    _lasselectindex = dataGridViewX1.CurrentRow.Index;
                }
                
            }
        }

        private void InitQueryParams()
        {
            DateTime dt = DateTime.Now;
            dtinput_start.Value = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            dtinput_end.Value = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }

        /// <summary>
        /// 搜索订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_search_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private string CombineSql()
        {
            StringBuilder sbstr = new StringBuilder();

            DateTime dt = dtinput_start.Value;

            DateTime start = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            DateTime end = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
            sbstr.AppendFormat(" createtime >= '{0}' and createtime <= '{1}' ", start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));
            return sbstr.ToString();
        }


        private void btn_export_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.RestoreDirectory = false;
            dlg.Filter = "excel 03文件|*.xls|excel 07文件|*.xlsx";
            string fName = string.Empty;
            if (dlg.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            fName = dlg.FileName;
            if (string.IsNullOrEmpty(fName))
            {
                return;
            }
            //ExcelHelper.DataSetToExcel(this.dataGridViewX1.DataSource as DataTable, fName);
            if (ExportExcelWithAspose(this.dataGridViewX1.DataSource as DataTable, fName))
            {
                MessageUtil.ShowTips("导出成功");
                return;
            }
            MessageUtil.ShowTips("导出失败");
        }

        public bool ExportExcelWithAspose(DataTable dt, string path)
        {

            bool succeed = false;
            if (dt != null)
            {
                try
                {
                    Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                    Aspose.Cells.Worksheet cellSheet = workbook.Worksheets[0];
                    //为单元格添加样式    
                    Aspose.Cells.Style style = workbook.Styles[workbook.Styles.Add()];
                    //设置居中
                    style.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
                    //设置背景颜色
                    style.ForegroundColor = System.Drawing.Color.FromArgb(153, 204, 0);
                    style.Pattern = BackgroundType.Solid;
                    style.Font.IsBold = true;
                    int rowIndex = 0;
                    int colIndex = 0;
                    int colCount = 2;
                    int rowCount = dt.Rows.Count;
                    //列名的处理
                    cellSheet.Cells[rowIndex, colIndex].PutValue("物料-描述");
                    colIndex++;
                    cellSheet.Cells[rowIndex, colIndex].PutValue("销售总量（kg）");
                    colIndex++;

                    //中间空行
                    rowIndex++;
                    rowIndex++;
                    for (int i = 0; i < rowCount; i++)
                    {
                        colIndex = 0;
                        for (int j = 0; j < colCount; j++)
                        {
                            if (j == 1)
                            {
                                cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Rows[i][j + 1].ToString());
                                colIndex++;
                                continue;
                            }
                            cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Rows[i][j].ToString());
                            colIndex++;
                        }
                        rowIndex++;
                    }

                    //总重量
                    rowIndex++;
                    string Sum = dt.Compute("sum(weights)", "true").ToString();
                    cellSheet.Cells[rowIndex, 1].PutValue(Sum);

                    cellSheet.AutoFitColumns();
                    path = Path.GetFullPath(path);
                    workbook.Save(path);
                    succeed = true;
                }
                catch (Exception ex)
                {
                    succeed = false;
                }
            }
            return succeed;
        }
        private void ProductOffStockStasticFrm_Load(object sender, EventArgs e)
        {
            //初始化查询参数
            InitQueryParams();
            BindData();
        }
    }
}