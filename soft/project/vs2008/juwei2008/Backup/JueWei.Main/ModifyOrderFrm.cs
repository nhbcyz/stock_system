using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class ModifyOrderFrm : BaseForm
    {
        private Model.ims_orders _mproduct = null;
        private bool _adding = false;
        public ModifyOrderFrm()
        {
            InitializeComponent();
        }

        public ModifyOrderFrm(Model.ims_orders mproduct, bool adding)
        {
            this._mproduct = mproduct;
            this._adding = adding;
            InitializeComponent();
        }

        private void UpdateUI()
        {
            if (!_adding)
            {
                if (this._mproduct == null)
                {
                    return;
                }
                this.tb_verifynum.Text = this._mproduct.Verifynum.ToString();
            }
            else {
                this._mproduct = new JueWei.Model.ims_orders();
                this.tb_verifynum.Text = "0";
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.tb_verifynum.Text.Trim())
                    )
                {
                    MessageExUtil.ShowWarning("参数不能为空");
                    return;
                }

                int num = 0;
                if (!int.TryParse(tb_verifynum.Text.Trim(), out num))
                {
                    MessageExUtil.ShowWarning("重量输入错误");
                    return;
                }
                _mproduct.Verifynum = num;
                if (_adding)
                {
                    if (!BLL.ims_orders.Insert(_mproduct))
                    {
                        MessageExUtil.ShowError("新增失败");
                        return;
                    }
                }
                else
                {
                    if (!BLL.ims_orders.Update(_mproduct))
                    {
                        MessageExUtil.ShowError("保存失败");
                        return;
                    }
                }

            }
            catch (System.Exception ex)
            {
                MessageExUtil.ShowError("保存失败");
                LogHelper.Error(typeof(DeviceItemEditFrm), ex);
            }

            this.Close();
        }


        private void ModifyOrderFrm_Load(object sender, EventArgs e)
        {
            UpdateUI();
        }
    }
}