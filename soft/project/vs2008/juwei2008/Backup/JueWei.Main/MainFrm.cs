using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using JueWei.Main.Common;
using WHC.OrderWater.Commons;
using WHC.WareHouseMis.UI;
using WHC.WareHouseMis.UI.SplashScreen;
using WHC.OrderWater.Commons.Threading;
using System.Threading;
using JueWei.Common;
using System.Management;

namespace JueWei.Main
{
    public partial class MainFrm : Office2007RibbonForm
    {
        private ProductMemoFrm _proFrm = null;
        private InStockBussiness _stockbuss = null;
        private WeightLoginBussiness _weightlogon = null;
        private Thread backworker;
        private bool _isClose = false;
        private DbMaintainMgr _dbMgr = null;
        private StockStasticMgr _stockmgr = null;
        public MainFrm()
        {
            InitializeComponent();
            
            Portal.gc.MainDialog = this;
           // CommnunicationMgr msr = new CommnunicationMgr("192.168.0.100",51236);
            //msr.Start();
            _stockbuss = new InStockBussiness();
            _stockbuss.Start();
            _weightlogon = new WeightLoginBussiness();
            _weightlogon.Start();
            _dbMgr = new DbMaintainMgr();
            _dbMgr.Start();
            _stockmgr = new StockStasticMgr();
            _stockmgr.Start();
        }


        /// <summary>
        /// 商品信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void productinfo_mi_Click(object sender, EventArgs e)
        {
            //ShowProductInfo();
            SetMdiForm("商品信息", typeof(ProductMemoFrm));
        }
        /// <summary>
        /// 显示商品信息
        /// </summary>
        private void ShowProductInfo()
        {
            if (_proFrm == null)
            {
                _proFrm = new ProductMemoFrm();
                _proFrm.Show();
                _proFrm.MdiParent = this;
                _proFrm.Dock = DockStyle.Fill;
            }
        }
        private void MainPage(object sender, EventArgs e)
        {
           // this.ribbonControl1.SelectedRibbonTabItem = this.itemQQ;
            //this.CommandStatus = string.Format("欢迎使用 {0}", WHC.WareHouseMis.UI.Portal.gc.gAppWholeName);
            //this.tool_ItemDetail_Click(null, null);
        }
        /// <summary>
        /// 显示商品信息业务数据
        /// </summary>
        private void ShowProductInfoBS()
        {

        }

        private void scandevice_mi_Click(object sender, EventArgs e)
        {

        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
            Init();

            //清空默认的Tab
            NavTabControl.Tabs.Clear();
            productinfo_mi_Click(null, null);
            Splasher.Close();
           
            InitBackWorker();
            InitAA();
        }
        private void InitAA()
        {
            Microsoft.Win32.RegistryKey retkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("software", true).CreateSubKey("WXK").CreateSubKey("WXK.INI").CreateSubKey(getMNum());
            object o = retkey.GetValue("Code");
            if (o != null)
            {
                string s = o.ToString();
                string mm = getMNum();
                string rm = getRNum(mm);
                if (s != rm)
                {
                    AP();
                }
            }
            else
            {
                AP();
            }
            
        }

        private void AP()
        {
            if (Math.Abs(DateTime.Now.Subtract(new DateTime(2017, 1, 1)).Days) > 200)
            {
                MessageUtil.ShowTips("第三方库权限到期,请联系服务提供商,QQ:1159265929");
                Environment.Exit(0);
            }
        }
        private void InitBackWorker()
        {
            backworker = new Thread(new ThreadStart(BackWorkerProcess));
            backworker.IsBackground = true;
            backworker.Start();
        }
        private delegate void UpdateUICallBack();
        private void BackWorkerProcess()
        {
            while (!_isClose)
            {
                UpdateUI();
                Thread.Sleep(1000);
            }
        }

        // 取得设备硬盘的卷标号
        public string GetDiskVolumeSerialNumber()
        {
            ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"d:\"");
            disk.Get();
            return disk.GetPropertyValue("VolumeSerialNumber").ToString();
        }

        //获得CPU的序列号
        public string getCpu()
        {
            string strCpu = null;
            ManagementClass myCpu = new ManagementClass("win32_Processor");
            ManagementObjectCollection myCpuConnection = myCpu.GetInstances();
            foreach (ManagementObject myObject in myCpuConnection)
            {
                strCpu = myObject.Properties["Processorid"].Value.ToString();
                break;
            }
            return strCpu;
        }

        //生成机器码
        public string getMNum()
        {
            string strNum = getCpu() + GetDiskVolumeSerialNumber();//获得24位Cpu和硬盘序列号
            string strMNum = strNum.Substring(0, 24);//从生成的字符串中取出前24个字符做为机器码
            return strMNum;
        }

        public int[] intCode = new int[127];//存储密钥
        public int[] intNumber = new int[25];//存机器码的Ascii值
        public char[] Charcode = new char[25];//存储机器码字

        public void setIntCode()//给数组赋值小于10的数
        {
            for (int i = 1; i < intCode.Length; i++)
            {
                intCode[i] = i % 9;
            }
        }

        //生成注册码
        public string getRNum(string str)
        {
            setIntCode();//初始化127位数组
            for (int i = 1; i < Charcode.Length; i++)//把机器码存入数组中
            {
                Charcode[i] = Convert.ToChar(str.Substring(i - 1, 1));
            }
            for (int j = 1; j < intNumber.Length; j++)//把字符的ASCII值存入一个整数组中。
            {
                intNumber[j] = intCode[Convert.ToInt32(Charcode[j])] + Convert.ToInt32(Charcode[j]);
            }
            string strAsciiName = "";//用于存储注册码
            for (int j = 1; j < intNumber.Length; j++)
            {
                if (intNumber[j] >= 48 && intNumber[j] <= 57)//判断字符ASCII值是否0－9之间
                {
                    strAsciiName += Convert.ToChar(intNumber[j]).ToString();
                }
                else if (intNumber[j] >= 65 && intNumber[j] <= 90)//判断字符ASCII值是否A－Z之间
                {
                    strAsciiName += Convert.ToChar(intNumber[j]).ToString();
                }
                else if (intNumber[j] >= 97 && intNumber[j] <= 122)//判断字符ASCII值是否a－z之间
                {
                    strAsciiName += Convert.ToChar(intNumber[j]).ToString();
                }
                else//判断字符ASCII值不在以上范围内
                {
                    if (intNumber[j] > 122)//判断字符ASCII值是否大于z
                    {
                        strAsciiName += Convert.ToChar(intNumber[j] - 10).ToString();
                    }
                    else
                    {
                        strAsciiName += Convert.ToChar(intNumber[j] - 9).ToString();
                    }
                }
            }
            return strAsciiName;
        }

        private void UpdateUI()
        {
            if (this.lblCalendar.InvokeRequired)
            {
                this.Invoke(new UpdateUICallBack(UpdateUI));
            }
            else {
                this.lblCalendar.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }
        private void Init()
        {
            this.InitUserRelated();
        }

        /// <summary>
        /// 创建或者显示一个多文档界面页面
        /// </summary>
        /// <param name="caption">窗体标题</param>
        /// <param name="formType">窗体类型</param>
        public void SetMdiForm(string caption, Type formType)
        {
            bool IsOpened = false;

            //遍历现有的Tab页面，如果存在，那么设置为选中即可
            foreach (SuperTabItem tabitem in NavTabControl.Tabs)
            {
                if (tabitem.Name == caption)
                {
                    NavTabControl.SelectedTab = tabitem;
                    IsOpened = true;
                    break;
                }
            }

            //如果在现有Tab页面中没有找到，那么就要初始化了Tab页面了
            if (!IsOpened)
            {
                //为了方便管理，调用LoadMdiForm函数来创建一个新的窗体，并作为MDI的子窗体
                //然后分配给SuperTab控件，创建一个SuperTabItem并显示
                DevComponents.DotNetBar.OfficeForm form = ChildWinManagement.LoadMdiForm(Portal.gc.MainDialog, formType)
                    as DevComponents.DotNetBar.OfficeForm;

                SuperTabItem tabItem = NavTabControl.CreateTab(caption);
                tabItem.Name = caption;
                tabItem.Text = caption;

                form.FormBorderStyle = FormBorderStyle.None;
                form.TopLevel = false;
                form.Visible = true;
                form.Dock = DockStyle.Fill;
                //tabItem.Icon = form.Icon;
                tabItem.AttachedControl.Controls.Add(form);

                NavTabControl.SelectedTab = tabItem;
            }
        }

        private void ctx_Window_CloseAll_Click(object sender, EventArgs e)
        {
            CloseAllDocuments();
        }

        private void ctx_Window_CloseOther_Click(object sender, EventArgs e)
        {
            CloseOthers();
        }

        public void CloseAllDocuments()
        {
            for (int i = NavTabControl.Tabs.Count - 1; i >= 0; i--)
            {
                SuperTabItem tabitem = NavTabControl.Tabs[i] as SuperTabItem;
                if (tabitem != null)
                {
                    
                    tabitem.Close();
                }
            }
        }

        public void CloseOthers()
        {
            if (ActiveMdiChild != null)
            {
                Type formType = ActiveMdiChild.GetType();
                for (int i = NavTabControl.Tabs.Count - 1; i >= 0; i--)
                {
                    SuperTabItem tabitem = NavTabControl.Tabs[i] as SuperTabItem;
                    if (tabitem != null && formType != tabitem.AttachedControl.Controls[0].GetType())
                    {
                        tabitem.Close();
                    }
                }
            }
        }

        private void btnitem_deviceinfo_Click(object sender, EventArgs e)
        {
            SetMdiForm("设备信息", typeof(DeviceInfoFrm));
        }

        private void btnitem_ord_instock_Click(object sender, EventArgs e)
        {
            SetMdiForm("订单入库", typeof(OrderInStockFrm));
        }

        private void btnitem_shopdict_Click(object sender, EventArgs e)
        {
            SetMdiForm("门店字典", typeof(ShopDictFrm));
        }

        private void btnitem_instock_Click(object sender, EventArgs e)
        {
            SetMdiForm("入库明细", typeof(ProductInStockQueryFrm));
        }

        private void btnitem_instockstastic_Click(object sender, EventArgs e)
        {
            SetMdiForm("入库统计", typeof(ProductInStockStasticFrm));
        }

        private void btnitem_offstock_Click(object sender, EventArgs e)
        {
            SetMdiForm("订单出库流程", typeof(ProductOffStockFrm));
        }

        private void NavTabControl_TabItemClose(object sender, SuperTabStripTabItemCloseEventArgs e)
        {
            string controlname = e.Tab.Text;
            SuperTabItem item = e.Tab as SuperTabItem;
            if (item.AttachedControl.Controls.Count <= 0)
            {
                return;
            }
            BaseForm frm = item.AttachedControl.Controls[0] as BaseForm;//获取内嵌的Form对象
            if (frm != null)
            {
                frm.Close(); //调用form的close事件，即触发了内嵌窗体的关闭事件
                if (!frm.PermitClose)
                {
                    e.Cancel = true;
                }
            }
        }

        private void MainFrm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_stockbuss != null)
            {
                _stockbuss.Stop();
               
            }
            if (_weightlogon != null)
            {
                _weightlogon.Stop();
            }
            if(_dbMgr != null)
            {
                _dbMgr.Stop();
            }
            if (_stockmgr != null)
            {
                _stockmgr.Stop();
            }
            _isClose = true;
        }

        private void btnitem_exit_Click(object sender, EventArgs e)
        {
            CloseAllDocuments();
            if (NavTabControl.Tabs.Count > 0)
            {
                MessageUtil.ShowError("请先关闭所有子窗口再退出");
                return;
            }
            this.Close();
        }

        private void MainFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _isClose = true;
            CloseAllDocuments();
            if (NavTabControl.Tabs.Count > 0)
            {
                MessageUtil.ShowError("请先关闭所有子窗口再退出");
                return;
            }
        }

        private void btn_relogon_Click(object sender, EventArgs e)
        {
            if (MessageExUtil.ShowYesNoAndWarning("您确定需要重新登录吗？") == DialogResult.Yes)
            {
                Portal.gc.MainDialog.Hide();
                Logon logon = new Logon
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                if ((DialogResult.OK == logon.ShowDialog()) && logon.bLogin)
                {
                    this.InitUserRelated();
                }
                logon.Dispose();
                Portal.gc.MainDialog.Show();
            }
        }

        private void InitUserRelated()
        {
            this.InitAuthorizedUI();
            //string str = this.config.AppConfigGet("UIStyle");
            //if (!string.IsNullOrEmpty(str))
            //{
            //    this.cbStyleManager.Text = str;
            //}
            string str2 = ConfigHelper.GetConfigString("Manufacturer");
            string str3 = ConfigHelper.GetConfigString("ApplicationName");
            string customer = ConfigHelper.GetConfigString("CustomerName");
            string weilcomstr = string.Format("{0}-{1}", str2, str3);
            
            Portal.gc.gAppUnit = str2;
            Portal.gc.gAppMsgboxTitle = weilcomstr;
            Portal.gc.gAppWholeName = weilcomstr;
            this.Text = string.Format("{0}-{1}", customer, str3);
            this.notifyIcon1.BalloonTipText = weilcomstr;
            this.notifyIcon1.BalloonTipTitle = weilcomstr;
            this.notifyIcon1.Text = weilcomstr;
            if (Portal.gc.LoginInfo != null)
            {
                this.UserStatus = string.Format("当前用户：{0}({1})", Portal.gc.LoginInfo.Fullname, Portal.gc.LoginInfo.Username);
            }
            else
            {
                this.UserStatus = "";
            }
            this.CommandStatus = string.Format("欢迎使用 {0}", Portal.gc.gAppWholeName);
            //this.ribbonControl.SelectedRibbonTabItem = this.itemQQ;
        }

        private void InitAuthorizedUI()
        {
            this.btnitem_usermgr.Enabled = Portal.gc.HasFunction("UserManage");
            //this.tool_Report.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("Report");
            //this.tool_Dict.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("Dictionary");
            //this.tool_ItemDetail.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("ItemDetail");
            //this.tool_Purchase.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("Purchase");
            //this.tool_StockSearch.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("StockSearch");
            //this.tool_TakeOut.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("TakeOut");
            //this.tool_WareHouse.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("WareHouse");
            //this.tool_Settings.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("Parameters");
            //this.tool_MonthlyStatistic.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("MonthlyStatistic");
            //this.tool_AnnualStatistic.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("AnnualStatistic");
            //this.tool_ClearAll.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("ClearAllData");
            //this.tool_ImportItemDetail.Enabled = WHC.WareHouseMis.UI.Portal.gc.HasFunction("ImportItemDetail");
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.notifyMenu_Show_Click(sender, e);
        }

        private void notifyMenu_Show_Click(object sender, EventArgs e)
        {
            if (base.WindowState == FormWindowState.Minimized)
            {
                base.WindowState = FormWindowState.Maximized;
                base.Show();
                base.BringToFront();
                base.Activate();
                base.Focus();
            }
            else
            {
                base.WindowState = FormWindowState.Minimized;
                base.Hide();
            }
        }

        public string UserStatus
        {
            get
            {
                return this.lblCurrentUser.Text;
            }
            set
            {
                this.lblCurrentUser.Text = value;
            }
        }

        public string CommandStatus
        {
            get
            {
                return this.lblCommandStatus.Text;
            }
            set
            {
                this.lblCommandStatus.Text = value;
            }
        }

        private void btnitem_usermgr_Click(object sender, EventArgs e)
        {
            SetMdiForm("用户列表", typeof(UserManageFrm));
        }

        private void btn_pwdedit_Click(object sender, EventArgs e)
        {
            SetMdiForm("密码修改", typeof(UserPwdEditFrm));
        }

        private void btnitem_offstock_stastic_Click(object sender, EventArgs e)
        {
            SetMdiForm("出库统计", typeof(ProductOffStockStasticFrm));
        }

        private void btnitem_diffstock_Click(object sender, EventArgs e)
        {
            SetMdiForm("库存统计", typeof(ProductDiffStockFrm));
        }

        private void btnitem_set_Click(object sender, EventArgs e)
        {
            SetMdiForm("参数设置", typeof(NormalSetFrm));
        }

        private void btn_productorder_instock_Click(object sender, EventArgs e)
        {
            SetMdiForm("生产订单管理", typeof(SapProductOrderInStockFrm));
        }

        private void btn_reg_Click(object sender, EventArgs e)
        {
            SetMdiForm("软件升级", typeof(RegForm));
        }
    }
}