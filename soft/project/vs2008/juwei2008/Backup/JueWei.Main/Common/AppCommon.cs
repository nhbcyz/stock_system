﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JueWei.DBUtility;
using JueWei.Common;
using System.Windows.Forms;

namespace JueWei.Main.Common
{
    public class AppCommon
    {
        private static AppCommon _instance = null;
        System.Configuration.Configuration _config = null;
        public Dictionary<string,Model.ims_acl_function> FunctionDict = new Dictionary<string,JueWei.Model.ims_acl_function>();
        public DateTime? WeightLogonDt = null;
        private AppCommon()
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;
        }

        public static AppCommon Instance
        {
            get
            {

                if (_instance == null)
                {
                    _instance = new AppCommon();
                }
                return _instance;
            }
        }

        public System.Configuration.Configuration _Config
        {
            get
            {
                if (_config == null)
                {
                    string fileName = System.IO.Path.GetFileName(Application.ExecutablePath);
                    _config = System.Configuration.ConfigurationManager.OpenExeConfiguration(fileName);
                }
                return _config;
            }
        }

        public void Init()
        {
 
        }
    }
}
