﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using JueWei.Common;
using System.IO;
using JueWei.DBUtility;

namespace JueWei.Main.Common
{
    public class DbMaintainMgr
    {
        private Thread theTh = null;
        private object theObj = new object();
        private bool _stoped = true;
        private string _bakPath = string.Empty;
        private DateTime _lastdeltime = DateTime.Now;
        public DbMaintainMgr()
        {
            theTh = new Thread(new ThreadStart(Process));
            _bakPath = ConfigHelper.GetConfigString("DelBakPath");
            CheckPath();
        }

        public void Start()
        {
            if (!_stoped)
            {
                return;
            }
            _stoped = false;
            if (theTh != null)
            {
                theTh.Start();
            }
        }

        private void Process()
        {
            //while (true)
            //{
                if (_stoped)
                {
                   // break;
                }
                CheckPath();
                if (_lastdeltime.AddDays(1) <= DateTime.Now)
                {
                    Thread.Sleep(1000);
                  //  continue;
                }
                DateTime dt = DateTime.Now.AddDays(-30);
                DbHelperMySQL.connectionString = GlobalVar.DBConnString;
                string sql = "Delete from ims_instock_record where createtime <= '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' ";

                DbHelperMySQL.ExecuteSql(sql);

                sql = "Delete from ims_orders where exchangedate <= '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' ";
                DbHelperMySQL.ExecuteSql(sql);

                sql = "Delete from ims_offstock where scandate <= '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' ";
                DbHelperMySQL.ExecuteSql(sql);
                _lastdeltime = DateTime.Now;
                Thread.Sleep(1000* 60);
           // }
        }

        public void Stop()
        {
            _stoped = true;
        }

        private void CheckPath()
        {
            if (!Directory.Exists(_bakPath))
            {
                Directory.CreateDirectory(_bakPath);
            }
            if (!Directory.Exists(_bakPath + "instock\\"))
            {
                Directory.CreateDirectory(_bakPath + "instock\\");
            }
            if (!Directory.Exists(_bakPath + "order\\"))
            {
                Directory.CreateDirectory(_bakPath + "order\\");
            }
            if (!Directory.Exists(_bakPath + "offstock\\"))
            {
                Directory.CreateDirectory(_bakPath + "offstock\\");
            }
        }
    }
}
