﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data;
using JueWei.DBUtility;

namespace JueWei.Main.Common
{
    public class StockStasticMgr
    {
        private Thread theTh = null;
        private object theObj = new object();
        private bool _stoped = false;
        private string _bakPath = string.Empty;
        private DateTime _lastdeltime = DateTime.Now;
        public StockStasticMgr()
        {
            theTh = new Thread(new ThreadStart(Process));
        }

        public void Start()
        {
            _stoped = false;
            if (theTh != null)
            {
                theTh.Start();
            }
        }

        private void Process()
        {
            while (true)
            {
                if (_stoped)
                {
                    break;
                }
                DataSet ds = BLL.ims_instock_record.GetInstockRecord(DateTime.Now,DateTime.Now.AddDays(-1), 0);

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string materialno = dr["materialno"].ToString();
                        int num = Convert.ToInt32(dr["num"].ToString());
                        DataSet dsmporder = BLL.ims_product_order.GetList(string.Format(" materialno = '{0}' and DATE_FORMAT(completedate,'%Y-%m-%d') = '{1}' ", materialno, DateTime.Now.ToString("yyyy-MM-dd")));
                        if (dsmporder != null & dsmporder.Tables.Count > 0 && dsmporder.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow drmp in dsmporder.Tables[0].Rows)
                            {
                                int basicnum = Convert.ToInt32(drmp["basicnum"].ToString());
                                int scannum = Convert.ToInt32(drmp["scannum"].ToString());
                                if (scannum < basicnum)
                                {
                                    int diff = basicnum - scannum;
                                    int pronum = 0;
                                    if (diff <= num)
                                    {
                                        pronum = diff;
                                    }
                                    else
                                    {
                                        pronum = num;
                                    }

                                    Model.ims_product_order mporder = BLL.ims_product_order.SelectById(int.Parse(drmp["id"].ToString()));
                                    if (mporder == null)
                                    {
                                        continue;
                                    }
                                    mporder.Scannum += pronum;
                                    if (BLL.ims_product_order.Update(mporder))
                                    {
                                        BatchInstock(ds.Tables[1],materialno,pronum);
                                        Thread.Sleep(1000);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                Thread.Sleep(10 * 1000);
            }
        }


        public void Stop()
        {
            _stoped = true;
        }


        private void BatchInstock(DataTable dt,string materialno,int ncount)
        {
            DataRow[] drs = dt.Select(string.Format("materialno = '{0}'", materialno));
            if (drs == null)
            {
                return;
            }
            List<string> sqllist = new List<string>();
            int i = 0;
            foreach (DataRow dr in drs)
            {
                if (dr["materialno"].ToString() != materialno)
                {
                    continue;
                }
                if (i >= ncount)
                {
                    break;
                }
                StringBuilder strsql = new StringBuilder();
                strsql.Append("update ims_instock_record set state = 1 where id = " + dr["id"].ToString() + ";");
                sqllist.Add(strsql.ToString());
                i++;
            }
            int k = DbHelperMySQL.ExecuteSqlTran(sqllist);
        }
    }
}
