﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class ProductOrderEditFrm : BaseForm
    {
        private Model.ims_product_order _mproduct = null;
        private bool _adding = false;
        public ProductOrderEditFrm()
        {
            InitializeComponent();
        }

        public ProductOrderEditFrm(Model.ims_product_order mproduct, bool adding)
        {
            this._mproduct = mproduct;
            this._adding = adding;
            InitializeComponent();
            UpdateUI();
        }

        private void UpdateUI()
        {
            if (_adding)
            {
                return;
            }
            if (this._mproduct == null)
            {
                return;
            }
            tb_materialno.Text = _mproduct.Materialno.ToString();
           // tb_productname.Text = _mproduct.Productname.ToString();
            tb_basicnum.Text = _mproduct.Basicnum.ToString();
            tb_scannum.Text = _mproduct.Scannum.ToString();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tb_scannum.Text.Trim()) )
                {
                    MessageExUtil.ShowWarning("参数不能为空");
                    return;
                }
                int scannum = 0;
                if (!int.TryParse(tb_scannum.Text.Trim(), out scannum))
                {
                    MessageExUtil.ShowWarning("扫码数量填写错误");
                    return;
                }
                if (_mproduct.Basicnum != scannum)
                {
                    MessageExUtil.ShowTips("扫码数量与基本扫码不相符合");
                    return;
                }
                _mproduct.Scannum = scannum;
                if (!BLL.ims_product_order.Update(_mproduct))
                {
                    MessageExUtil.ShowError("保存失败");
                    return;
                }
            }
            catch (System.Exception ex)
            {
                MessageExUtil.ShowError("保存失败");
                LogHelper.Error(typeof(ProductItemEditFrm), ex);
            }

            this.Close();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
