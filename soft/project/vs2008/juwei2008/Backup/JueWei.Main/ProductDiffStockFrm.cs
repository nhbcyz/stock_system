using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;
using Aspose.Cells;
using System.IO;

namespace JueWei.Main
{
    public partial class ProductDiffStockFrm : BaseForm
    {
        private int _lasselectindex = 0; //最后选中索引
        private bool _modifying = false; //界面正在刷新
        private SearchCondition _conditions = new SearchCondition();

        private DataSet _alldevices = null;
        public ProductDiffStockFrm()
        {
            InitializeComponent();
        }


        private void BindData()
        {
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = ComputeStock();
            if (dataGridViewX1.RowCount <= _lasselectindex)
            {
                _lasselectindex = 0;
            }

            dataGridViewX1.Rows[_lasselectindex].Selected = true;
        }

        private DataTable ComputeStock()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("productname");
            datatable.Columns.Add("materialno");
            datatable.Columns.Add(new DataColumn("diffweights",typeof(decimal)));


            DateTime start = dtinput_start.Value;
            DateTime end = dtinput_end.Value;
            DataSet dsoffstock = BLL.ims_offstock.GetProductSumWeightByDateTime(start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));
            DataSet dsinstock = BLL.ims_instock_record.GetProductSumWeightByDateTime(start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));

            ///从入库开始统计
            foreach (DataRow dr in dsinstock.Tables[0].Rows)
            {
                string martiealno = dr["materialno"].ToString();
                DataRow[] drs = dsoffstock.Tables[0].Select(" materialno = " + martiealno);
                DataRow newrow = datatable.NewRow();
                newrow["materialno"] = martiealno;
                newrow["productname"] = dr["productname"].ToString(); ;
                if (drs != null && drs.Length == 1)
                {//入库和出库表都有重量
                    newrow["diffweights"] = Convert.ToDecimal(dr["weights"]) - Convert.ToDecimal(drs[0]["weights"]);
                }
                else {
                    //入库有数据,出库没数据
                    newrow["diffweights"] = dr["weights"];
                }
                datatable.Rows.Add(newrow);
            }

            //从出库开始统计
            foreach (DataRow dr in dsoffstock.Tables[0].Rows)
            {
                string martiealno = dr["materialno"].ToString();
                DataRow[] drs = dsinstock.Tables[0].Select(" materialno = " + martiealno);

                if (drs != null && drs.Length == 1)
                {//入库和出库表都有重量，上次已统计,不再计算
                   // newrow["diffweights"] = Convert.ToDecimal(dr["weight"]) - Convert.ToDecimal(drs[0]["weight"]);
                }
                else
                {
                    //出库有数据,入库没数据
                    DataRow newrow = datatable.NewRow();
                    newrow["materialno"] = martiealno;
                    newrow["productname"] = dr["productname"].ToString();
                    newrow["diffweights"] =   - Convert.ToDecimal(dr["weights"]);
                    datatable.Rows.Add(newrow);
                }
            }

            return datatable;
        }
        private void InitQueryParams()
        {
            DateTime dt = DateTime.Now;
            dtinput_start.Value = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            dtinput_end.Value = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }

        /// <summary>
        /// 搜索订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_search_Click(object sender, EventArgs e)
        {
            BindData();
        }


        private void btn_export_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.RestoreDirectory = false;
            dlg.Filter = "excel 03文件|*.xls|excel 07文件|*.xlsx";
            string fName = string.Empty;
            if (dlg.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            fName = dlg.FileName;
            if (string.IsNullOrEmpty(fName))
            {
                return;
            }
            //ExcelHelper.DataSetToExcel(this.dataGridViewX1.DataSource as DataTable, fName);
            if (ExportExcelWithAspose(this.dataGridViewX1.DataSource as DataTable, fName))
            {
                MessageUtil.ShowTips("导出成功");
                return;
            }
            MessageUtil.ShowTips("导出失败");
        }

        public bool ExportExcelWithAspose(DataTable dt, string path)
        {

            bool succeed = false;
            if (dt != null)
            {
                try
                {
                    Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                    Aspose.Cells.Worksheet cellSheet = workbook.Worksheets[0];
                    //为单元格添加样式    
                    Aspose.Cells.Style style = workbook.Styles[workbook.Styles.Add()];
                    //设置居中
                    style.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
                    //设置背景颜色
                    style.ForegroundColor = System.Drawing.Color.FromArgb(153, 204, 0);
                    style.Pattern = BackgroundType.Solid;
                    style.Font.IsBold = true;
                    int rowIndex = 0;
                    int colIndex = 0;
                    int colCount = 2;
                    int rowCount = dt.Rows.Count;
                    //列名的处理
                    //列名的处理
                    cellSheet.Cells[rowIndex, colIndex].PutValue("物料-描述");
                    colIndex++;
                    cellSheet.Cells[rowIndex, colIndex].PutValue("库存总量（kg）");
                    colIndex++;

                    rowIndex++;
                    rowIndex++;
                    for (int i = 0; i < rowCount; i++)
                    {
                        colIndex = 0;
                        for (int j = 0; j < colCount; j++)
                        {
                            if (j == 1)
                            {
                                cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Rows[i][j + 1].ToString());
                                colIndex++;
                                continue;
                            }
                            cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Rows[i][j].ToString());
                            colIndex++;
                        }
                        rowIndex++;
                    }

                    //总重量
                    rowIndex++;
                    string Sum = dt.Compute("sum(diffweights)", "true").ToString();
                    cellSheet.Cells[rowIndex, 1].PutValue(Sum);


                    cellSheet.AutoFitColumns();
                    path = Path.GetFullPath(path);
                    workbook.Save(path);
                    succeed = true;
                }
                catch (Exception ex)
                {
                    succeed = false;
                }
            }
            return succeed;
        }

        private void ProductDiffStockFrm_Load(object sender, EventArgs e)
        {
            //初始化查询参数
            InitQueryParams();
            BindData();
        }
    }
}