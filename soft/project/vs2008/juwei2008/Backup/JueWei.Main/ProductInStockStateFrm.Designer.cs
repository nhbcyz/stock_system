namespace JueWei.Main
{
    partial class ProductInStockStateFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.rich_CurrentQrMsg = new DevComponents.DotNetBar.Controls.RichTextBoxEx();
            this.tx_materialno = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tx_spec = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_weight = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tx_unit = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.labelX1);
            this.splitContainer1.Panel2.Controls.Add(this.rich_CurrentQrMsg);
            this.splitContainer1.Size = new System.Drawing.Size(565, 318);
            this.splitContainer1.SplitterDistance = 151;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.tx_unit);
            this.groupPanel1.Controls.Add(this.tb_weight);
            this.groupPanel1.Controls.Add(this.tx_spec);
            this.groupPanel1.Controls.Add(this.tx_materialno);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupPanel1.Location = new System.Drawing.Point(0, 0);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(565, 151);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "当前数据";
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(305, 56);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(75, 23);
            this.labelX5.TabIndex = 3;
            this.labelX5.Text = "单位:";
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(305, 16);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 2;
            this.labelX4.Text = "重量:";
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(73, 57);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 1;
            this.labelX3.Text = "规格:";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(73, 16);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 0;
            this.labelX2.Text = "物料编号:";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(31, 5);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(106, 23);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "最新的5条数据:";
            // 
            // rich_CurrentQrMsg
            // 
            // 
            // 
            // 
            this.rich_CurrentQrMsg.BackgroundStyle.Class = "RichTextBoxBorder";
            this.rich_CurrentQrMsg.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rich_CurrentQrMsg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rich_CurrentQrMsg.Location = new System.Drawing.Point(0, 34);
            this.rich_CurrentQrMsg.Name = "rich_CurrentQrMsg";
            this.rich_CurrentQrMsg.Rtf = "{\\rtf1\\ansi\\ansicpg936\\deff0\\deflang1033\\deflangfe2052{\\fonttbl{\\f0\\fnil\\fcharset" +
                "134 \\\'cb\\\'ce\\\'cc\\\'e5;}}\r\n\\viewkind4\\uc1\\pard\\lang2052\\f0\\fs18\\par\r\n}\r\n";
            this.rich_CurrentQrMsg.Size = new System.Drawing.Size(565, 129);
            this.rich_CurrentQrMsg.TabIndex = 0;
            // 
            // tx_materialno
            // 
            // 
            // 
            // 
            this.tx_materialno.Border.Class = "TextBoxBorder";
            this.tx_materialno.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tx_materialno.Location = new System.Drawing.Point(167, 17);
            this.tx_materialno.Name = "tx_materialno";
            this.tx_materialno.PreventEnterBeep = true;
            this.tx_materialno.ReadOnly = true;
            this.tx_materialno.Size = new System.Drawing.Size(100, 21);
            this.tx_materialno.TabIndex = 4;
            // 
            // tx_spec
            // 
            // 
            // 
            // 
            this.tx_spec.Border.Class = "TextBoxBorder";
            this.tx_spec.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tx_spec.Location = new System.Drawing.Point(167, 57);
            this.tx_spec.Name = "tx_spec";
            this.tx_spec.PreventEnterBeep = true;
            this.tx_spec.ReadOnly = true;
            this.tx_spec.Size = new System.Drawing.Size(100, 21);
            this.tx_spec.TabIndex = 5;
            // 
            // tb_weight
            // 
            // 
            // 
            // 
            this.tb_weight.Border.Class = "TextBoxBorder";
            this.tb_weight.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_weight.Location = new System.Drawing.Point(412, 16);
            this.tb_weight.Name = "tb_weight";
            this.tb_weight.PreventEnterBeep = true;
            this.tb_weight.ReadOnly = true;
            this.tb_weight.Size = new System.Drawing.Size(100, 21);
            this.tb_weight.TabIndex = 6;
            // 
            // tx_unit
            // 
            // 
            // 
            // 
            this.tx_unit.Border.Class = "TextBoxBorder";
            this.tx_unit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tx_unit.Location = new System.Drawing.Point(412, 58);
            this.tx_unit.Name = "tx_unit";
            this.tx_unit.PreventEnterBeep = true;
            this.tx_unit.ReadOnly = true;
            this.tx_unit.Size = new System.Drawing.Size(100, 21);
            this.tx_unit.TabIndex = 7;
            // 
            // ProductInStock
            // 
            this.ClientSize = new System.Drawing.Size(565, 318);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProductInStock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ProductInStock";
            this.Load += new System.EventHandler(this.ProductInStock_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevComponents.DotNetBar.Controls.RichTextBoxEx rich_CurrentQrMsg;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX tx_unit;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_weight;
        private DevComponents.DotNetBar.Controls.TextBoxX tx_spec;
        private DevComponents.DotNetBar.Controls.TextBoxX tx_materialno;
    }
}