﻿namespace JueWei.Main
{
    partial class SapProductOrderInStockFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tb_materialno = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btn_exportall = new DevComponents.DotNetBar.ButtonX();
            this.btn_search = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btn_delete = new DevComponents.DotNetBar.ButtonX();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderexplain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.verifydate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.verifynum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exchangedate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.basicunit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.basicnum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scannum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_edit = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_import = new DevComponents.DotNetBar.ButtonX();
            this.pager1 = new JueWei.Main.Controls.Pager();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dtinput_select = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.btn_upload = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtinput_select)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_materialno
            // 
            // 
            // 
            // 
            this.tb_materialno.Border.Class = "TextBoxBorder";
            this.tb_materialno.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_materialno.Location = new System.Drawing.Point(112, 5);
            this.tb_materialno.Name = "tb_materialno";
            this.tb_materialno.PreventEnterBeep = true;
            this.tb_materialno.Size = new System.Drawing.Size(100, 21);
            this.tb_materialno.TabIndex = 12;
            // 
            // btn_exportall
            // 
            this.btn_exportall.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_exportall.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_exportall.Location = new System.Drawing.Point(542, 12);
            this.btn_exportall.Name = "btn_exportall";
            this.btn_exportall.Size = new System.Drawing.Size(75, 23);
            this.btn_exportall.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_exportall.TabIndex = 12;
            this.btn_exportall.Text = "导出所有";
            this.btn_exportall.Visible = false;
            this.btn_exportall.Click += new System.EventHandler(this.btn_exportall_Click);
            // 
            // btn_search
            // 
            this.btn_search.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_search.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_search.Location = new System.Drawing.Point(250, 2);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_search.TabIndex = 10;
            this.btn_search.Text = "查询";
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(12, 2);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 5;
            this.labelX1.Text = "商品种类";
            // 
            // btn_delete
            // 
            this.btn_delete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_delete.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_delete.Location = new System.Drawing.Point(442, 12);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_delete.TabIndex = 8;
            this.btn_delete.Text = "删除";
            this.btn_delete.Visible = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowDrop = true;
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.orderno,
            this.orderexplain,
            this.verifydate,
            this.verifynum,
            this.exchangedate,
            this.materialno,
            this.basicunit,
            this.basicnum,
            this.scannum});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.RowTemplate.Height = 23;
            this.dataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX1.Size = new System.Drawing.Size(736, 264);
            this.dataGridViewX1.TabIndex = 2;
            this.dataGridViewX1.DoubleClick += new System.EventHandler(this.dataGridViewX1_DoubleClick);
            this.dataGridViewX1.SelectionChanged += new System.EventHandler(this.dataGridViewX1_SelectionChanged);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "编号";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // orderno
            // 
            this.orderno.DataPropertyName = "orderno";
            this.orderno.HeaderText = "订单号";
            this.orderno.Name = "orderno";
            this.orderno.ReadOnly = true;
            // 
            // orderexplain
            // 
            this.orderexplain.DataPropertyName = "completedate";
            this.orderexplain.HeaderText = "完成日期";
            this.orderexplain.Name = "orderexplain";
            this.orderexplain.ReadOnly = true;
            // 
            // verifydate
            // 
            this.verifydate.DataPropertyName = "materialno";
            this.verifydate.HeaderText = "物料";
            this.verifydate.Name = "verifydate";
            this.verifydate.ReadOnly = true;
            // 
            // verifynum
            // 
            this.verifynum.DataPropertyName = "mobiletype";
            this.verifynum.HeaderText = "移动类型";
            this.verifynum.Name = "verifynum";
            this.verifynum.ReadOnly = true;
            // 
            // exchangedate
            // 
            this.exchangedate.DataPropertyName = "stockaddr";
            this.exchangedate.HeaderText = "仓库地址";
            this.exchangedate.Name = "exchangedate";
            this.exchangedate.ReadOnly = true;
            // 
            // materialno
            // 
            this.materialno.DataPropertyName = "factory";
            this.materialno.HeaderText = "工厂";
            this.materialno.Name = "materialno";
            this.materialno.ReadOnly = true;
            // 
            // basicunit
            // 
            this.basicunit.DataPropertyName = "basicunit";
            this.basicunit.HeaderText = "基本单位";
            this.basicunit.Name = "basicunit";
            this.basicunit.ReadOnly = true;
            // 
            // basicnum
            // 
            this.basicnum.DataPropertyName = "basicnum";
            this.basicnum.HeaderText = "订单数量";
            this.basicnum.Name = "basicnum";
            this.basicnum.ReadOnly = true;
            // 
            // scannum
            // 
            this.scannum.DataPropertyName = "scannum";
            this.scannum.HeaderText = "扫码数量";
            this.scannum.Name = "scannum";
            this.scannum.ReadOnly = true;
            // 
            // btn_edit
            // 
            this.btn_edit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_edit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_edit.Location = new System.Drawing.Point(347, 12);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(75, 23);
            this.btn_edit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_edit.TabIndex = 7;
            this.btn_edit.Text = "编辑";
            this.btn_edit.Visible = false;
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.tb_materialno);
            this.groupPanel1.Controls.Add(this.btn_search);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupPanel1.Location = new System.Drawing.Point(0, 41);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(736, 81);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 11;
            this.groupPanel1.Text = "查询条件";
            // 
            // btn_import
            // 
            this.btn_import.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_import.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_import.Location = new System.Drawing.Point(241, 12);
            this.btn_import.Name = "btn_import";
            this.btn_import.Size = new System.Drawing.Size(91, 23);
            this.btn_import.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_import.TabIndex = 6;
            this.btn_import.Text = "导入订单";
            this.btn_import.Click += new System.EventHandler(this.btn_import_Click);
            // 
            // pager1
            // 
            this.pager1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pager1.Location = new System.Drawing.Point(0, 231);
            this.pager1.Name = "pager1";
            this.pager1.NMax = 0;
            this.pager1.PageCount = 0;
            this.pager1.PageCurrent = 0;
            this.pager1.PageSize = 50;
            this.pager1.Size = new System.Drawing.Size(736, 33);
            this.pager1.TabIndex = 3;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dtinput_select);
            this.splitContainer1.Panel1.Controls.Add(this.labelX4);
            this.splitContainer1.Panel1.Controls.Add(this.btn_upload);
            this.splitContainer1.Panel1.Controls.Add(this.btn_exportall);
            this.splitContainer1.Panel1.Controls.Add(this.groupPanel1);
            this.splitContainer1.Panel1.Controls.Add(this.btn_delete);
            this.splitContainer1.Panel1.Controls.Add(this.btn_edit);
            this.splitContainer1.Panel1.Controls.Add(this.btn_import);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pager1);
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewX1);
            this.splitContainer1.Size = new System.Drawing.Size(736, 387);
            this.splitContainer1.SplitterDistance = 122;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 2;
            // 
            // dtinput_select
            // 
            // 
            // 
            // 
            this.dtinput_select.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtinput_select.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtinput_select.ButtonDropDown.Visible = true;
            this.dtinput_select.IsPopupCalendarOpen = false;
            this.dtinput_select.Location = new System.Drawing.Point(107, 12);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dtinput_select.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dtinput_select.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.MonthCalendar.DisplayMonth = new System.DateTime(2016, 10, 1, 0, 0, 0, 0);
            this.dtinput_select.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            // 
            // 
            // 
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.MonthCalendar.TodayButtonVisible = true;
            this.dtinput_select.Name = "dtinput_select";
            this.dtinput_select.Size = new System.Drawing.Size(107, 21);
            this.dtinput_select.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtinput_select.TabIndex = 16;
            this.dtinput_select.Value = new System.DateTime(2016, 10, 9, 20, 55, 14, 0);
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(36, 12);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(54, 23);
            this.labelX4.TabIndex = 15;
            this.labelX4.Text = "时间选择";
            // 
            // btn_upload
            // 
            this.btn_upload.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_upload.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_upload.Location = new System.Drawing.Point(644, 12);
            this.btn_upload.Name = "btn_upload";
            this.btn_upload.Size = new System.Drawing.Size(75, 23);
            this.btn_upload.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_upload.TabIndex = 13;
            this.btn_upload.Text = "交货";
            this.btn_upload.Visible = false;
            this.btn_upload.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // SapProductOrderInStockFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 387);
            this.Controls.Add(this.splitContainer1);
            this.Name = "SapProductOrderInStockFrm";
            this.Text = "生产订单管理";
            this.Load += new System.EventHandler(this.SapProductOrderInStockFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtinput_select)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.TextBoxX tb_materialno;
        private DevComponents.DotNetBar.ButtonX btn_exportall;
        private DevComponents.DotNetBar.ButtonX btn_search;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btn_delete;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private DevComponents.DotNetBar.ButtonX btn_edit;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btn_import;
        private JueWei.Main.Controls.Pager pager1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevComponents.DotNetBar.ButtonX btn_upload;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderno;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderexplain;
        private System.Windows.Forms.DataGridViewTextBoxColumn verifydate;
        private System.Windows.Forms.DataGridViewTextBoxColumn verifynum;
        private System.Windows.Forms.DataGridViewTextBoxColumn exchangedate;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialno;
        private System.Windows.Forms.DataGridViewTextBoxColumn basicunit;
        private System.Windows.Forms.DataGridViewTextBoxColumn basicnum;
        private System.Windows.Forms.DataGridViewTextBoxColumn scannum;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtinput_select;
    }
}