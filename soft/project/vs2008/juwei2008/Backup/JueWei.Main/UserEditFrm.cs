using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class UserEditFrm : BaseForm
    {
        private Model.ims_users _muser = null;
        private bool _adding = false;
        private IList<Model.ims_role> _roles;
        private int _selectroleid = 0;
        public UserEditFrm()
        {
            InitializeComponent();
        }

        public UserEditFrm(Model.ims_users muser, bool adding)
        {
            this._muser = muser;
            this._adding = adding;
            InitializeComponent();
        }

        private void UpdateUI()
        {
            InitRoles();
            this.tb_pwd.Text = "";
            this.tb_secondpwd.Text = "";
            if (!_adding)
            {
                if (this._muser == null)
                {
                    return;
                }
                this.tb_user.Text = this._muser.Username;
                
                this.tb_fullname.Text = this._muser.Fullname;
                this.dtinput_expire.Value = Convert.ToDateTime(this._muser.Expireddate);
            }
            else {
                this._muser = new JueWei.Model.ims_users();
                this.tb_user.Text = "";
                this.tb_fullname.Text = "";
                this.dtinput_expire.Value = DateTime.Now.AddMonths(12);
            }

        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                int roleid = _roles[cb_roles.SelectedIndex].Id;
                if (string.IsNullOrEmpty(tb_user.Text.Trim()) ||
                    string.IsNullOrEmpty(tb_fullname.Text.Trim())
                    )
                {
                    MessageExUtil.ShowWarning("参数不能为空");
                    return;
                }
                _muser.Username = tb_user.Text.TrimEnd();
                _muser.Fullname = tb_fullname.Text.TrimEnd();
                _muser.Expireddate = this.dtinput_expire.Value;
                if (_adding)
                {
                    if (string.IsNullOrEmpty(tb_pwd.Text.Trim()) ||
                         string.IsNullOrEmpty(this.tb_secondpwd.Text.Trim())
                         )
                    {
                        MessageExUtil.ShowWarning("密码不能为空");
                        return;
                    }
                    if (tb_pwd.Text.Trim() != this.tb_secondpwd.Text.Trim())
                    {
                        MessageExUtil.ShowWarning("密码输入不一致");
                        return;
                    }
                    string salt = MD5Util.GetMD5_8(DateTime.Now.Ticks.ToString());
                    _muser.Salt = salt;
                    string pwd = MD5Util.GetMD5_32(tb_pwd.Text.Trim());
                    _muser.Password = MD5Util.GetMD5_32(pwd + salt);
                    _muser.Createtime = DateTime.Now;
                    
                    if (BLL.ims_users.InsertUserAndRoleID(_muser,roleid) <= 0)
                    {
                        MessageExUtil.ShowError("新增失败");
                        return;
                    }
                    

                }
                else {
                    if (string.IsNullOrEmpty(tb_pwd.Text.Trim()) &&
                      string.IsNullOrEmpty(this.tb_secondpwd.Text.Trim())
                      )
                    {

                    }
                    else
                    {
                        //修改密码
                        if (tb_pwd.Text.Trim() != this.tb_secondpwd.Text.Trim())
                        {
                            MessageExUtil.ShowWarning("密码输入不一致");
                            return;
                        }
                        string pwd = MD5Util.GetMD5_32(tb_pwd.Text.Trim());
                        _muser.Password = MD5Util.GetMD5_32(pwd + _muser.Salt);

                    }
                    //不修改密码的流程
                    if (BLL.ims_users.UpdateUserAndRoleID(_muser,roleid) <= 0)
                    {
                        MessageExUtil.ShowError("保存失败");
                        return;
                    }
                }

            }
            catch (System.Exception ex)
            {
                MessageExUtil.ShowError("保存失败");
                LogHelper.Error(typeof(DeviceItemEditFrm), ex);
            }

            this.Close();
        }

        private void InitRoles()
        {
            cb_roles.Items.Clear();
            _roles = BLL.ims_role.GetAllList();
            foreach (var item in _roles)
            {
                cb_roles.Items.Add(item.Name);
            }
            if (_muser == null)
            {
                return;
            }
            _selectroleid = BLL.ims_users.GetRoleIdByUserId(_muser.Id);
            if (_selectroleid == -1)
            {
                return;
            }
            for (int i = 0; i < _roles.Count; i++ )
            {
                if (_roles[i].Id == _selectroleid)
                {
                    cb_roles.SelectedIndex = i;
                    return;
                }
            }
            cb_roles.SelectedIndex = 0;
        }

        private void UserEditFrm_Load(object sender, EventArgs e)
        {
            UpdateUI();
        }
    }
}