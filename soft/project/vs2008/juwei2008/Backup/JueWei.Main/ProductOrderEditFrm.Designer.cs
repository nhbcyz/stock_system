﻿namespace JueWei.Main
{
    partial class ProductOrderEditFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_cancel = new DevComponents.DotNetBar.ButtonX();
            this.btn_ok = new DevComponents.DotNetBar.ButtonX();
            this.tb_basicnum = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_materialno = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.tb_scannum = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // btn_cancel
            // 
            this.btn_cancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Location = new System.Drawing.Point(371, 318);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cancel.TabIndex = 25;
            this.btn_cancel.Text = "取消";
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ok.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_ok.Location = new System.Drawing.Point(242, 318);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ok.TabIndex = 24;
            this.btn_ok.Text = "确定";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // tb_basicnum
            // 
            // 
            // 
            // 
            this.tb_basicnum.Border.Class = "TextBoxBorder";
            this.tb_basicnum.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_basicnum.Location = new System.Drawing.Point(201, 133);
            this.tb_basicnum.Name = "tb_basicnum";
            this.tb_basicnum.PreventEnterBeep = true;
            this.tb_basicnum.ReadOnly = true;
            this.tb_basicnum.Size = new System.Drawing.Size(111, 21);
            this.tb_basicnum.TabIndex = 21;
            // 
            // tb_materialno
            // 
            // 
            // 
            // 
            this.tb_materialno.Border.Class = "TextBoxBorder";
            this.tb_materialno.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_materialno.Location = new System.Drawing.Point(201, 47);
            this.tb_materialno.Name = "tb_materialno";
            this.tb_materialno.PreventEnterBeep = true;
            this.tb_materialno.ReadOnly = true;
            this.tb_materialno.Size = new System.Drawing.Size(111, 21);
            this.tb_materialno.TabIndex = 19;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(109, 133);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 16;
            this.labelX3.Text = "基本数量";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(109, 47);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 14;
            this.labelX1.Text = "物料编号";
            // 
            // tb_scannum
            // 
            // 
            // 
            // 
            this.tb_scannum.Border.Class = "TextBoxBorder";
            this.tb_scannum.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_scannum.Location = new System.Drawing.Point(201, 170);
            this.tb_scannum.Name = "tb_scannum";
            this.tb_scannum.PreventEnterBeep = true;
            this.tb_scannum.Size = new System.Drawing.Size(111, 21);
            this.tb_scannum.TabIndex = 27;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(109, 170);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 26;
            this.labelX4.Text = "扫码数量";
            // 
            // ProductOrderEditFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 389);
            this.Controls.Add(this.tb_scannum);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.tb_basicnum);
            this.Controls.Add(this.tb_materialno);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX1);
            this.Name = "ProductOrderEditFrm";
            this.Text = "ProductOrderEditFrm";
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btn_cancel;
        private DevComponents.DotNetBar.ButtonX btn_ok;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_basicnum;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_materialno;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_scannum;
        private DevComponents.DotNetBar.LabelX labelX4;
    }
}