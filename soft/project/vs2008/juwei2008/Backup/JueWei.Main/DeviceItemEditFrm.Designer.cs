namespace JueWei.Main
{
    partial class DeviceItemEditFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_cancel = new DevComponents.DotNetBar.ButtonX();
            this.btn_ok = new DevComponents.DotNetBar.ButtonX();
            this.tb_ip = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_mac = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_devname = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_serialno = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.cb_devtype = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.cb_devstate = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.tx_port = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // btn_cancel
            // 
            this.btn_cancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Location = new System.Drawing.Point(364, 296);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cancel.TabIndex = 23;
            this.btn_cancel.Text = "取消";
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ok.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ok.Location = new System.Drawing.Point(235, 296);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ok.TabIndex = 22;
            this.btn_ok.Text = "确定";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // tb_ip
            // 
            // 
            // 
            // 
            this.tb_ip.Border.Class = "TextBoxBorder";
            this.tb_ip.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_ip.Location = new System.Drawing.Point(199, 141);
            this.tb_ip.Name = "tb_ip";
            this.tb_ip.PreventEnterBeep = true;
            this.tb_ip.Size = new System.Drawing.Size(111, 21);
            this.tb_ip.TabIndex = 20;
            // 
            // tb_mac
            // 
            // 
            // 
            // 
            this.tb_mac.Border.Class = "TextBoxBorder";
            this.tb_mac.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_mac.Location = new System.Drawing.Point(199, 100);
            this.tb_mac.Name = "tb_mac";
            this.tb_mac.PreventEnterBeep = true;
            this.tb_mac.Size = new System.Drawing.Size(111, 21);
            this.tb_mac.TabIndex = 19;
            // 
            // tb_devname
            // 
            // 
            // 
            // 
            this.tb_devname.Border.Class = "TextBoxBorder";
            this.tb_devname.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_devname.Location = new System.Drawing.Point(199, 57);
            this.tb_devname.Name = "tb_devname";
            this.tb_devname.PreventEnterBeep = true;
            this.tb_devname.Size = new System.Drawing.Size(111, 21);
            this.tb_devname.TabIndex = 18;
            // 
            // tb_serialno
            // 
            this.tb_serialno.AcceptsTab = true;
            // 
            // 
            // 
            this.tb_serialno.Border.Class = "TextBoxBorder";
            this.tb_serialno.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_serialno.Location = new System.Drawing.Point(199, 13);
            this.tb_serialno.Name = "tb_serialno";
            this.tb_serialno.PreventEnterBeep = true;
            this.tb_serialno.Size = new System.Drawing.Size(111, 21);
            this.tb_serialno.TabIndex = 17;
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(107, 210);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(75, 23);
            this.labelX5.TabIndex = 16;
            this.labelX5.Text = "设备类型";
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(107, 141);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 15;
            this.labelX4.Text = "IP地址";
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(107, 100);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 14;
            this.labelX3.Text = "Mac地址";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(107, 57);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 13;
            this.labelX2.Text = "设备名称";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(107, 13);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 12;
            this.labelX1.Text = "设备标识码";
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(107, 248);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(75, 23);
            this.labelX6.TabIndex = 24;
            this.labelX6.Text = "设备状态";
            // 
            // cb_devtype
            // 
            this.cb_devtype.DisplayMember = "Text";
            this.cb_devtype.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_devtype.FormattingEnabled = true;
            this.cb_devtype.ItemHeight = 15;
            this.cb_devtype.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3});
            this.cb_devtype.Location = new System.Drawing.Point(199, 212);
            this.cb_devtype.Name = "cb_devtype";
            this.cb_devtype.Size = new System.Drawing.Size(111, 21);
            this.cb_devtype.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_devtype.TabIndex = 26;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "手持PDA";
            this.comboItem1.Value = "0";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "固定PDA";
            this.comboItem2.Value = "1";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "称重仪";
            this.comboItem3.Value = "2";
            // 
            // cb_devstate
            // 
            this.cb_devstate.DisplayMember = "Text";
            this.cb_devstate.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_devstate.FormattingEnabled = true;
            this.cb_devstate.ItemHeight = 15;
            this.cb_devstate.Items.AddRange(new object[] {
            this.comboItem4,
            this.comboItem5,
            this.comboItem6});
            this.cb_devstate.Location = new System.Drawing.Point(199, 248);
            this.cb_devstate.Name = "cb_devstate";
            this.cb_devstate.Size = new System.Drawing.Size(111, 21);
            this.cb_devstate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_devstate.TabIndex = 27;
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "未激活";
            this.comboItem4.Value = "0";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "已停用";
            this.comboItem5.Value = "1";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "已激活";
            this.comboItem6.Value = "2";
            // 
            // tx_port
            // 
            // 
            // 
            // 
            this.tx_port.Border.Class = "TextBoxBorder";
            this.tx_port.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tx_port.Location = new System.Drawing.Point(199, 181);
            this.tx_port.Name = "tx_port";
            this.tx_port.PreventEnterBeep = true;
            this.tx_port.Size = new System.Drawing.Size(111, 21);
            this.tx_port.TabIndex = 29;
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Location = new System.Drawing.Point(107, 181);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(75, 23);
            this.labelX7.TabIndex = 28;
            this.labelX7.Text = "通讯端口";
            // 
            // DeviceItemEditFrm
            // 
            this.AcceptButton = this.btn_ok;
            this.CancelButton = this.btn_cancel;
            this.ClientSize = new System.Drawing.Size(467, 339);
            this.Controls.Add(this.tx_port);
            this.Controls.Add(this.labelX7);
            this.Controls.Add(this.cb_devstate);
            this.Controls.Add(this.cb_devtype);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.tb_ip);
            this.Controls.Add(this.tb_mac);
            this.Controls.Add(this.tb_devname);
            this.Controls.Add(this.tb_serialno);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Name = "DeviceItemEditFrm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "设备信息编辑";
            this.Load += new System.EventHandler(this.DeviceItemEditFrm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btn_cancel;
        private DevComponents.DotNetBar.ButtonX btn_ok;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_ip;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_mac;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_devname;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_serialno;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_devtype;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_devstate;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.DotNetBar.Controls.TextBoxX tx_port;
        private DevComponents.DotNetBar.LabelX labelX7;
    }
}