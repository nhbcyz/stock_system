using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using JueWei.Common;
using System.IO;
using WHC.OrderWater.Commons;
using JueWei.Main.Common;

namespace JueWei.Main
{
    public partial class NormalSetFrm : BaseForm
    {
        public NormalSetFrm()
        {
            InitializeComponent();
            this.tb_orderweightdiff.Text = ConfigHelper.GetConfigString("OrderDiffWeight");
            this.tb_qrpath.Text = ConfigHelper.GetConfigString("DefaultQrPath");
            this.tb_exportpath.Text = ConfigHelper.GetConfigString("DefaultExportPath");
            int dbsource = ConfigHelper.GetConfigInt("DataSource");
            if (dbsource >= 0 && dbsource < 2)
            {
                this.cbDataSource.SelectedIndex = ConfigHelper.GetConfigInt("DataSource");
            }
            else
            {
                this.cbDataSource.SelectedIndex = 0;
            }
            this.tb_host.Text = ConfigHelper.GetConfigString("RFCHost");
            this.tb_client.Text = ConfigHelper.GetConfigString("RFCClient");
            this.tb_rfcuser.Text = ConfigHelper.GetConfigString("RFCUser");
            string strpwd = string.Empty;
            try
            {
                strpwd = EncodeHelper.DesDecrypt(ConfigHelper.GetConfigString("RFCPassword"));
            }
            catch (System.Exception ex)
            {
                strpwd = "";
            }
            this.tb_rfcpwd.Text = strpwd;
        }

        private void btnqr_view_Click(object sender, EventArgs e)
        {
            this.tb_qrpath.Text = SelectDirPath(Path.GetDirectoryName(this.tb_qrpath.Text));
        }

        private string SelectDirPath(string orginalpath)
        {
            string dirpath = orginalpath;
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                dirpath = dialog.SelectedPath;
            }  
            return dirpath;
        }

        private void btn_exportview_Click(object sender, EventArgs e)
        {
            this.tb_exportpath.Text = SelectDirPath(Path.GetDirectoryName(this.tb_exportpath.Text));
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (SaveSettings())
            {
                MessageUtil.ShowTips("修改成功");
                return;
            }
            MessageUtil.ShowTips("修改失败");
        }

        private bool SaveSettings()
        {
            string strweight = this.tb_orderweightdiff.Text.TrimEnd();
            decimal v = 0.0M;
            if (!decimal.TryParse(strweight, out v))
            {
                return false;
            }
            int dbsource = 1;
            if (cbDataSource.SelectedIndex == 0)
            {
                dbsource = 0;
            }
            ConfigHelper.SetAppSetting("OrderDiffWeight", strweight, AppCommon.Instance._Config);
            ConfigHelper.SetAppSetting("DefaultQrPath", this.tb_qrpath.Text, AppCommon.Instance._Config);
            ConfigHelper.SetAppSetting("DefaultExportPath", this.tb_exportpath.Text, AppCommon.Instance._Config);
            ConfigHelper.SetAppSetting("DataSource", dbsource.ToString(), AppCommon.Instance._Config);

            ConfigHelper.SetAppSetting("RFCHost", this.tb_host.Text, AppCommon.Instance._Config);
            ConfigHelper.SetAppSetting("RFCClient", this.tb_client.Text, AppCommon.Instance._Config);
            ConfigHelper.SetAppSetting("RFCUser", this.tb_rfcuser.Text, AppCommon.Instance._Config);
            string strpwd = string.Empty;
            try
            {
                strpwd = EncodeHelper.DesEncrypt(this.tb_rfcpwd.Text);
            }
            catch (System.Exception ex)
            {
            	
            }
            ConfigHelper.SetAppSetting("RFCPassword", strpwd, AppCommon.Instance._Config);
            return true;
        }

        private void tb_orderweightdiff_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !Char.IsPunctuation(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;//消除不合适字符  
            }
            else if (Char.IsPunctuation(e.KeyChar))
            {
                if (e.KeyChar != '.' || this.tb_orderweightdiff.Text.Length == 0)//小数点  
                {
                    e.Handled = true;
                }
                if (tb_orderweightdiff.Text.LastIndexOf('.') != -1)
                {
                    e.Handled = true;
                }
            }   
        }
    }
}