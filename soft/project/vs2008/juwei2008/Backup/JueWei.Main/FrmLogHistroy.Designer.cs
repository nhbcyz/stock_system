﻿using DevComponents.DotNetBar;
using System.Windows.Forms;
using DevComponents.DotNetBar.Controls;
using System.Drawing;
using System;
using System.ComponentModel;
namespace JueWei.Main
{
    partial class FrmLogHistroy
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
            this.btnDeleteMonthLog = new ButtonX();
            this.chkUseDate = new CheckBox();
            this.btnSearch = new ButtonX();
            this.dateTimePicker2 = new DateTimePicker();
            this.dateTimePicker1 = new DateTimePicker();
            this.label4 = new LabelX();
            this.txtRealName = new TextBoxX();
            this.label3 = new LabelX();
            this.label2 = new LabelX();
            this.txtNote = new TextBoxX();
            this.label5 = new LabelX();
            this.txtLoginName = new TextBoxX();
            this.label1 = new LabelX();
            this.contextMenuStrip1 = new ContextMenuStrip(this.components);
            this.panelEx1 = new PanelEx();
            this.panelEx2 = new PanelEx();
            //this.winGridViewPager1 = new WinGridViewPager();
            this.panelEx1.SuspendLayout();
            this.panelEx2.SuspendLayout();
            base.SuspendLayout();
//            this.btnDeleteMonthLog.AccessibleRole = AccessibleRole.PushButton;
            this.btnDeleteMonthLog.Location = new Point(0x223, 0x27);
            this.btnDeleteMonthLog.Name = "btnDeleteMonthLog";
            this.btnDeleteMonthLog.Size = new Size(150, 0x17);
            this.btnDeleteMonthLog.TabIndex = 6;
            this.btnDeleteMonthLog.Text = "删除30天前的日志";
            this.btnDeleteMonthLog.Click += new EventHandler(this.btnDeleteMonthLog_Click);
            this.chkUseDate.AutoSize = true;
            this.chkUseDate.Location = new Point(0x2f6, 15);
            this.chkUseDate.Name = "chkUseDate";
            this.chkUseDate.Size = new Size(0x30, 0x10);
            this.chkUseDate.TabIndex = 4;
            this.chkUseDate.Text = "启用";
            this.chkUseDate.CheckedChanged += new EventHandler(this.chkUseDate_CheckedChanged);
            //this.btnSearch.AccessibleRole = AccessibleRole.PushButton;
            this.btnSearch.Location = new Point(0x2db, 0x27);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new Size(0x4b, 0x17);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "查询";
            this.btnSearch.Click += new EventHandler(this.btnSearch_Click);
            this.dateTimePicker2.Location = new Point(0x277, 12);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new Size(0x79, 0x15);
            this.dateTimePicker2.TabIndex = 3;
            this.dateTimePicker2.KeyUp += new KeyEventHandler(this.SearchControl_KeyUp);
            this.dateTimePicker1.Location = new Point(0x1bf, 12);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new Size(0x79, 0x15);
            this.dateTimePicker1.TabIndex = 2;
            this.dateTimePicker1.KeyUp += new KeyEventHandler(this.SearchControl_KeyUp);
            this.label4.AutoSize = true;
            this.label4.BackgroundStyle.Class = "";
            this.label4.Location = new Point(0x23e, 0x10);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x38, 0x12);
            this.label4.TabIndex = 2;
            this.label4.Text = "结束日期";
            this.txtRealName.Border.Class = "TextBoxBorder";
            this.txtRealName.Location = new Point(0x100, 13);
            this.txtRealName.Name = "txtRealName";
            this.txtRealName.Size = new Size(0x79, 0x15);
            this.txtRealName.TabIndex = 1;
            this.txtRealName.KeyUp += new KeyEventHandler(this.SearchControl_KeyUp);
            this.label3.AutoSize = true;
            this.label3.BackgroundStyle.Class = "";
            this.label3.Location = new Point(390, 0x10);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x38, 0x12);
            this.label3.TabIndex = 2;
            this.label3.Text = "开始日期";
            this.label2.AutoSize = true;
            this.label2.BackgroundStyle.Class = "";
            this.label2.Location = new Point(0xcb, 0x10);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x38, 0x12);
            this.label2.TabIndex = 2;
            this.label2.Text = "真实名称";
            this.txtNote.Border.Class = "TextBoxBorder";
            this.txtNote.Location = new Point(0x48, 0x29);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new Size(0x79, 0x15);
            this.txtNote.TabIndex = 0;
            this.txtNote.KeyUp += new KeyEventHandler(this.SearchControl_KeyUp);
            this.label5.AutoSize = true;
            this.label5.BackgroundStyle.Class = "";
            this.label5.Location = new Point(0x11, 0x2c);
            this.label5.Name = "label5";
            this.label5.Size = new Size(0x38, 0x12);
            this.label5.TabIndex = 2;
            this.label5.Text = "日志信息";
            this.txtLoginName.Border.Class = "TextBoxBorder";
            this.txtLoginName.Location = new Point(0x48, 13);
            this.txtLoginName.Name = "txtLoginName";
            this.txtLoginName.Size = new Size(0x79, 0x15);
            this.txtLoginName.TabIndex = 0;
            this.txtLoginName.KeyUp += new KeyEventHandler(this.SearchControl_KeyUp);
            this.label1.AutoSize = true;
            this.label1.BackgroundStyle.Class = "";
            this.label1.Location = new Point(0x11, 0x10);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x38, 0x12);
            this.label1.TabIndex = 2;
            this.label1.Text = "登录名称";
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new Size(0x3d, 4);
            this.panelEx1.CanvasColor = SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.btnDeleteMonthLog);
            this.panelEx1.Controls.Add(this.txtRealName);
            this.panelEx1.Controls.Add(this.chkUseDate);
            this.panelEx1.Controls.Add(this.label1);
            this.panelEx1.Controls.Add(this.btnSearch);
            this.panelEx1.Controls.Add(this.txtLoginName);
            this.panelEx1.Controls.Add(this.dateTimePicker2);
            this.panelEx1.Controls.Add(this.label5);
            this.panelEx1.Controls.Add(this.dateTimePicker1);
            this.panelEx1.Controls.Add(this.txtNote);
            this.panelEx1.Controls.Add(this.label4);
            this.panelEx1.Controls.Add(this.label2);
            this.panelEx1.Controls.Add(this.label3);
            this.panelEx1.Dock = DockStyle.Top;
            this.panelEx1.Location = new Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new Size(0x333, 0x5c);
            this.panelEx1.Style.Alignment = StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 11;
            this.panelEx2.CanvasColor = SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = eDotNetBarStyle.StyleManagerControlled;
            //this.panelEx2.Controls.Add(this.winGridViewPager1);
            this.panelEx2.Dock = DockStyle.Fill;
            this.panelEx2.Location = new Point(0, 0x5c);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new Size(0x333, 0x23b);
            this.panelEx2.Style.Alignment = StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.Border = eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 14;
            this.panelEx2.Text = "panelEx2";
            //this.winGridViewPager1.AppendedMenu = null;
            //this.winGridViewPager1.DataSource = null;
            //this.winGridViewPager1.DisplayColumns = "";
            //this.winGridViewPager1.Dock = DockStyle.Fill;
            //this.winGridViewPager1.Location = new Point(0, 0);
            //this.winGridViewPager1.MinimumSize = new Size(540, 0);
            //this.winGridViewPager1.Name = "winGridViewPager1";
            //this.winGridViewPager1.PrintTitle = "";
            //this.winGridViewPager1.Size = new Size(0x333, 0x23b);
            //this.winGridViewPager1.TabIndex = 12;
            base.AutoScaleDimensions = new SizeF(96f, 96f);
            //base.AutoScaleMode = AutoScaleMode.Dpi;
            this.AutoScroll = true;
            base.Controls.Add(this.panelEx2);
            base.Controls.Add(this.panelEx1);
            this.DoubleBuffered = true;
            base.Name = "FrmLogHistroy";
            base.Size = new Size(0x333, 0x297);
            this.panelEx1.ResumeLayout(false);
            this.panelEx1.PerformLayout();
            this.panelEx2.ResumeLayout(false);
            base.ResumeLayout(false);
        }
        private ButtonX btnDeleteMonthLog;
        private ButtonX btnSearch;
        private CheckBox chkUseDate;
        private ContextMenuStrip contextMenuStrip1;
        private DateTimePicker dateTimePicker1;
        private DateTimePicker dateTimePicker2;
        private LabelX label1;
        private LabelX label2;
        private LabelX label3;
        private LabelX label4;
        private LabelX label5;
        private PanelEx panelEx1;
        private PanelEx panelEx2;
        private TextBoxX txtLoginName;
        private TextBoxX txtNote;
        private TextBoxX txtRealName;
        #endregion
    }
}
