namespace JueWei.Main
{
    partial class OrderInStockFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderexplain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.verifydate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.verifynum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exchangedate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expressdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopaddrno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmi_alloc = new System.Windows.Forms.ToolStripMenuItem();
            this.pager1 = new JueWei.Main.Controls.Pager();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.btn_exportall = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.tb_materialno = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_shopname = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btn_search = new DevComponents.DotNetBar.ButtonX();
            this.cb_orderstate = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cb_devices = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.lb_allocstate = new DevComponents.DotNetBar.LabelX();
            this.cb_allocstate = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.lb_shopname = new DevComponents.DotNetBar.LabelX();
            this.btn_delete = new DevComponents.DotNetBar.ButtonX();
            this.btn_edit = new DevComponents.DotNetBar.ButtonX();
            this.btn_import = new DevComponents.DotNetBar.ButtonX();
            this.dtinput_select = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtinput_select)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowDrop = true;
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.orderno,
            this.orderexplain,
            this.verifydate,
            this.verifynum,
            this.exchangedate,
            this.materialno,
            this.weight,
            this.unit,
            this.expressdate,
            this.shopaddrno,
            this.shopname});
            this.dataGridViewX1.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.RowTemplate.Height = 23;
            this.dataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX1.Size = new System.Drawing.Size(726, 177);
            this.dataGridViewX1.TabIndex = 2;
            this.dataGridViewX1.DoubleClick += new System.EventHandler(this.dataGridViewX1_DoubleClick);
            this.dataGridViewX1.SelectionChanged += new System.EventHandler(this.dataGridViewX1_SelectionChanged);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "编号";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // orderno
            // 
            this.orderno.DataPropertyName = "orderno";
            this.orderno.HeaderText = "销售凭证";
            this.orderno.Name = "orderno";
            this.orderno.ReadOnly = true;
            // 
            // orderexplain
            // 
            this.orderexplain.DataPropertyName = "orderexplain";
            this.orderexplain.HeaderText = "订单描述";
            this.orderexplain.Name = "orderexplain";
            this.orderexplain.ReadOnly = true;
            // 
            // verifydate
            // 
            this.verifydate.DataPropertyName = "verifydate";
            this.verifydate.HeaderText = "凭证日期";
            this.verifydate.Name = "verifydate";
            this.verifydate.ReadOnly = true;
            // 
            // verifynum
            // 
            this.verifynum.DataPropertyName = "verifynum";
            this.verifynum.HeaderText = "确认数量";
            this.verifynum.Name = "verifynum";
            this.verifynum.ReadOnly = true;
            // 
            // exchangedate
            // 
            this.exchangedate.DataPropertyName = "exchangedate";
            this.exchangedate.HeaderText = "交货日期";
            this.exchangedate.Name = "exchangedate";
            this.exchangedate.ReadOnly = true;
            // 
            // materialno
            // 
            this.materialno.DataPropertyName = "materialno";
            this.materialno.HeaderText = "物料编号";
            this.materialno.Name = "materialno";
            this.materialno.ReadOnly = true;
            // 
            // weight
            // 
            this.weight.DataPropertyName = "weight";
            this.weight.HeaderText = "重量";
            this.weight.Name = "weight";
            this.weight.ReadOnly = true;
            // 
            // unit
            // 
            this.unit.DataPropertyName = "unit";
            this.unit.HeaderText = "单位";
            this.unit.Name = "unit";
            this.unit.ReadOnly = true;
            // 
            // expressdate
            // 
            this.expressdate.DataPropertyName = "expressdate";
            this.expressdate.HeaderText = "发货日期";
            this.expressdate.Name = "expressdate";
            this.expressdate.ReadOnly = true;
            // 
            // shopaddrno
            // 
            this.shopaddrno.DataPropertyName = "shopaddrno";
            this.shopaddrno.HeaderText = "店铺地址编号";
            this.shopaddrno.Name = "shopaddrno";
            this.shopaddrno.ReadOnly = true;
            // 
            // shopname
            // 
            this.shopname.DataPropertyName = "shopname";
            this.shopname.HeaderText = "店铺名字";
            this.shopname.Name = "shopname";
            this.shopname.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_alloc});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 26);
            // 
            // tsmi_alloc
            // 
            this.tsmi_alloc.Name = "tsmi_alloc";
            this.tsmi_alloc.Size = new System.Drawing.Size(100, 22);
            this.tsmi_alloc.Text = "分配";
            // 
            // pager1
            // 
            this.pager1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pager1.Location = new System.Drawing.Point(0, 132);
            this.pager1.Name = "pager1";
            this.pager1.NMax = 0;
            this.pager1.PageCount = 0;
            this.pager1.PageCurrent = 0;
            this.pager1.PageSize = 50;
            this.pager1.Size = new System.Drawing.Size(726, 45);
            this.pager1.TabIndex = 3;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dtinput_select);
            this.splitContainer1.Panel1.Controls.Add(this.labelX4);
            this.splitContainer1.Panel1.Controls.Add(this.btn_exportall);
            this.splitContainer1.Panel1.Controls.Add(this.groupPanel1);
            this.splitContainer1.Panel1.Controls.Add(this.btn_delete);
            this.splitContainer1.Panel1.Controls.Add(this.btn_edit);
            this.splitContainer1.Panel1.Controls.Add(this.btn_import);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pager1);
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewX1);
            this.splitContainer1.Size = new System.Drawing.Size(726, 359);
            this.splitContainer1.SplitterDistance = 181;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 1;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(12, 12);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(54, 23);
            this.labelX4.TabIndex = 13;
            this.labelX4.Text = "导入时间";
            // 
            // btn_exportall
            // 
            this.btn_exportall.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_exportall.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_exportall.Location = new System.Drawing.Point(502, 12);
            this.btn_exportall.Name = "btn_exportall";
            this.btn_exportall.Size = new System.Drawing.Size(75, 23);
            this.btn_exportall.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_exportall.TabIndex = 12;
            this.btn_exportall.Text = "导出所有";
            this.btn_exportall.Visible = false;
            this.btn_exportall.Click += new System.EventHandler(this.btn_exportall_Click);
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.tb_materialno);
            this.groupPanel1.Controls.Add(this.tb_shopname);
            this.groupPanel1.Controls.Add(this.btn_search);
            this.groupPanel1.Controls.Add(this.cb_orderstate);
            this.groupPanel1.Controls.Add(this.cb_devices);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.lb_allocstate);
            this.groupPanel1.Controls.Add(this.cb_allocstate);
            this.groupPanel1.Controls.Add(this.lb_shopname);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupPanel1.Location = new System.Drawing.Point(0, 53);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(726, 128);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 11;
            this.groupPanel1.Text = "查询条件";
            // 
            // tb_materialno
            // 
            // 
            // 
            // 
            this.tb_materialno.Border.Class = "TextBoxBorder";
            this.tb_materialno.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_materialno.Location = new System.Drawing.Point(545, 4);
            this.tb_materialno.Name = "tb_materialno";
            this.tb_materialno.PreventEnterBeep = true;
            this.tb_materialno.Size = new System.Drawing.Size(100, 21);
            this.tb_materialno.TabIndex = 12;
            // 
            // tb_shopname
            // 
            // 
            // 
            // 
            this.tb_shopname.Border.Class = "TextBoxBorder";
            this.tb_shopname.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_shopname.Location = new System.Drawing.Point(106, 4);
            this.tb_shopname.Name = "tb_shopname";
            this.tb_shopname.PreventEnterBeep = true;
            this.tb_shopname.Size = new System.Drawing.Size(100, 21);
            this.tb_shopname.TabIndex = 11;
            // 
            // btn_search
            // 
            this.btn_search.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_search.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_search.Location = new System.Drawing.Point(445, 49);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_search.TabIndex = 10;
            this.btn_search.Text = "查询";
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // cb_orderstate
            // 
            this.cb_orderstate.DisplayMember = "Text";
            this.cb_orderstate.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_orderstate.FormattingEnabled = true;
            this.cb_orderstate.ItemHeight = 15;
            this.cb_orderstate.Location = new System.Drawing.Point(319, 51);
            this.cb_orderstate.Name = "cb_orderstate";
            this.cb_orderstate.Size = new System.Drawing.Size(95, 21);
            this.cb_orderstate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_orderstate.TabIndex = 9;
            // 
            // cb_devices
            // 
            this.cb_devices.DisplayMember = "Text";
            this.cb_devices.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_devices.FormattingEnabled = true;
            this.cb_devices.ItemHeight = 15;
            this.cb_devices.Location = new System.Drawing.Point(106, 51);
            this.cb_devices.Name = "cb_devices";
            this.cb_devices.Size = new System.Drawing.Size(109, 21);
            this.cb_devices.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_devices.TabIndex = 8;
            this.cb_devices.SelectedIndexChanged += new System.EventHandler(this.cb_devices_SelectedIndexChanged);
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(237, 48);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 7;
            this.labelX3.Text = "订单状态";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(10, 49);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 6;
            this.labelX2.Text = "所属设备";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(445, 1);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 5;
            this.labelX1.Text = "商品种类";
            // 
            // lb_allocstate
            // 
            // 
            // 
            // 
            this.lb_allocstate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb_allocstate.Location = new System.Drawing.Point(237, 1);
            this.lb_allocstate.Name = "lb_allocstate";
            this.lb_allocstate.Size = new System.Drawing.Size(75, 23);
            this.lb_allocstate.TabIndex = 4;
            this.lb_allocstate.Text = "分配状态";
            // 
            // cb_allocstate
            // 
            this.cb_allocstate.DisplayMember = "Text";
            this.cb_allocstate.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_allocstate.FormattingEnabled = true;
            this.cb_allocstate.ItemHeight = 15;
            this.cb_allocstate.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3});
            this.cb_allocstate.Location = new System.Drawing.Point(318, 3);
            this.cb_allocstate.Name = "cb_allocstate";
            this.cb_allocstate.Size = new System.Drawing.Size(96, 21);
            this.cb_allocstate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_allocstate.TabIndex = 2;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "所有";
            this.comboItem1.Value = "0";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "未分配";
            this.comboItem2.Value = "1";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "已分配";
            this.comboItem3.Value = "2";
            // 
            // lb_shopname
            // 
            // 
            // 
            // 
            this.lb_shopname.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb_shopname.Location = new System.Drawing.Point(9, 3);
            this.lb_shopname.Name = "lb_shopname";
            this.lb_shopname.Size = new System.Drawing.Size(75, 23);
            this.lb_shopname.TabIndex = 0;
            this.lb_shopname.Text = "店铺名字";
            // 
            // btn_delete
            // 
            this.btn_delete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_delete.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_delete.Location = new System.Drawing.Point(398, 12);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_delete.TabIndex = 8;
            this.btn_delete.Text = "删除";
            this.btn_delete.Visible = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_edit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_edit.Location = new System.Drawing.Point(286, 12);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(75, 23);
            this.btn_edit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_edit.TabIndex = 7;
            this.btn_edit.Text = "编辑";
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // btn_import
            // 
            this.btn_import.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_import.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_import.Location = new System.Drawing.Point(200, 12);
            this.btn_import.Name = "btn_import";
            this.btn_import.Size = new System.Drawing.Size(75, 23);
            this.btn_import.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_import.TabIndex = 6;
            this.btn_import.Text = "导入";
            this.btn_import.Click += new System.EventHandler(this.btn_import_Click);
            // 
            // dtinput_select
            // 
            // 
            // 
            // 
            this.dtinput_select.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtinput_select.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtinput_select.ButtonDropDown.Visible = true;
            this.dtinput_select.IsPopupCalendarOpen = false;
            this.dtinput_select.Location = new System.Drawing.Point(72, 12);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dtinput_select.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dtinput_select.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.MonthCalendar.DisplayMonth = new System.DateTime(2016, 10, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.MonthCalendar.TodayButtonVisible = true;
            this.dtinput_select.Name = "dtinput_select";
            this.dtinput_select.Size = new System.Drawing.Size(107, 21);
            this.dtinput_select.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtinput_select.TabIndex = 14;
            this.dtinput_select.Value = new System.DateTime(2016, 10, 9, 20, 55, 14, 0);
            // 
            // OrderInStockFrm
            // 
            this.ClientSize = new System.Drawing.Size(726, 359);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "OrderInStockFrm";
            this.Text = "OrderInStockFrm";
            this.Load += new System.EventHandler(this.OrderInStockFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtinput_select)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private JueWei.Main.Controls.Pager pager1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevComponents.DotNetBar.ButtonX btn_delete;
        private DevComponents.DotNetBar.ButtonX btn_edit;
        private DevComponents.DotNetBar.ButtonX btn_import;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderno;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderexplain;
        private System.Windows.Forms.DataGridViewTextBoxColumn verifydate;
        private System.Windows.Forms.DataGridViewTextBoxColumn verifynum;
        private System.Windows.Forms.DataGridViewTextBoxColumn exchangedate;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialno;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn expressdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn shopaddrno;
        private System.Windows.Forms.DataGridViewTextBoxColumn shopname;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmi_alloc;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX lb_shopname;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_allocstate;
        private DevComponents.DotNetBar.LabelX lb_allocstate;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_devices;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_orderstate;
        private DevComponents.DotNetBar.ButtonX btn_search;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_shopname;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_materialno;
        private DevComponents.DotNetBar.ButtonX btn_exportall;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtinput_select;
    }
}