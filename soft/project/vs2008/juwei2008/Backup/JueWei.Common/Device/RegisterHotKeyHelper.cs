﻿using System;
using System.Windows.Forms;

namespace WHC.OrderWater.Commons
{
    public class RegisterHotKeyHelper
    {
        public RegisterHotKeyHelper()
        {

        }

        public Keys Keys { get; set; }
        public RegisterHotKeyHelper.MODKEY ModKey { get; set; }
        public IntPtr WindowHandle { get; set; }
        public int WParam { get; set; }

        public event RegisterHotKeyHelper.HotKeyPass HotKey;

        public extern static bool RegisterHotKey(IntPtr wnd, int id, RegisterHotKeyHelper.MODKEY mode, Keys vk);
        public void StarHotKey() {}
        public void StopHotKey() {}
        public extern static bool UnregisterHotKey(IntPtr wnd, int id);

        public enum MODKEY
        {
            MOD_ALT = 1,
            MOD_CONTROL = 2,
            MOD_SHIFT = 4,
            MOD_WIN = 8,
        }

        public delegate void HotKeyPass();
    }
}