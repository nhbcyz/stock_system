﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace JueWei.Common
{
    [Serializable]
    [XmlRoot("appSettings")]
    public class AppConfig
    {
        [XmlArray("items"),XmlArrayItem("item")]
        public List<ConfigItem> Items = new List<ConfigItem>();
    }
    
    public class ConfigItem
    {
        [XmlAttribute("key")]
        public string key;
        [XmlAttribute("value")]
        public string value;
    }
}
