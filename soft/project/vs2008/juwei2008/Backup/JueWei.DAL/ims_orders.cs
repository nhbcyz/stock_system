using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using JueWei.Model;
using JueWei.DBUtility;
namespace JueWei.DAL
{
    public partial class ims_orders
    {
        #region  添加一条数据
        /// <summary>
        /// 添加一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否添加成功</returns>
        public bool Insert(Model.ims_orders SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@shopaddrno",SingleItem.Shopaddrno),            
                        new MySqlParameter("@shopname",SingleItem.Shopname),            
                        new MySqlParameter("@weight",SingleItem.Weight),            
                        new MySqlParameter("@state",SingleItem.State),            
                        new MySqlParameter("@deviceid",SingleItem.Deviceid),            
                        new MySqlParameter("@shopno",SingleItem.Shopno),            
                        new MySqlParameter("@orderno",SingleItem.Orderno),            
                        new MySqlParameter("@orderexplain",SingleItem.Orderexplain),            
                        new MySqlParameter("@verifydate",SingleItem.Verifydate),            
                        new MySqlParameter("@verifynum",SingleItem.Verifynum),            
                        new MySqlParameter("@exchangedate",SingleItem.Exchangedate),            
                        new MySqlParameter("@materialno",SingleItem.Materialno),            
                        new MySqlParameter("@unit",SingleItem.Unit),                           
                        new MySqlParameter("@expressdate",SingleItem.Expressdate),
                        new MySqlParameter("@exchangeno",SingleItem.Exchangeno),
                        new MySqlParameter("@exchangeaddr",SingleItem.Exchangeaddr),
                        new MySqlParameter("@boxnum",SingleItem.Boxnum),
                        new MySqlParameter("@scannum",SingleItem.Scannum),
                        new MySqlParameter("@posnr",SingleItem.Posnr)
                        };
            string sql = "insert into ims_orders (shopaddrno,shopname,weight,state,deviceid,shopno,orderno,orderexplain,verifydate,verifynum,exchangedate,materialno,unit,expressdate,exchangeno,exchangeaddr,boxnum,scannum,posnr) values (@shopaddrno,@shopname,@weight,@state,@deviceid,@shopno,@orderno,@orderexplain,@verifydate,@verifynum,@exchangedate,@materialno,@unit,@expressdate,@exchangeno,@exchangeaddr,@boxnum,@scannum,@posnr)";


            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库插入操作
            {
                return true;
            }
            return false;   //插入不成功，或者数据已经存在 
        }
        #endregion

        #region 删除一条数据
        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        /// <param name="id">主键值</param>
        /// <returns>是否删除成功！</returns>
        public bool Delete(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "delete from ims_orders where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 更新一条数据
        /// <summary>
        /// 更新一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否修改成功</returns>
        public bool Update(Model.ims_orders SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@id",SingleItem.Id),            
                        new MySqlParameter("@shopaddrno",SingleItem.Shopaddrno),            
                        new MySqlParameter("@shopname",SingleItem.Shopname),            
                        new MySqlParameter("@weight",SingleItem.Weight),            
                        new MySqlParameter("@state",SingleItem.State),            
                        new MySqlParameter("@deviceid",SingleItem.Deviceid),            
                        new MySqlParameter("@shopno",SingleItem.Shopno),            
                        new MySqlParameter("@orderno",SingleItem.Orderno),            
                        new MySqlParameter("@orderexplain",SingleItem.Orderexplain),            
                        new MySqlParameter("@verifydate",SingleItem.Verifydate),            
                        new MySqlParameter("@verifynum",SingleItem.Verifynum),            
                        new MySqlParameter("@exchangedate",SingleItem.Exchangedate),            
                        new MySqlParameter("@materialno",SingleItem.Materialno),            
                        new MySqlParameter("@unit",SingleItem.Unit),            
                        new MySqlParameter("@expressdate",SingleItem.Expressdate),
                        new MySqlParameter("@exchangeno",SingleItem.Exchangeno),
                        new MySqlParameter("@exchangeaddr",SingleItem.Exchangeaddr),
                        new MySqlParameter("@boxnum",SingleItem.Boxnum),
                        new MySqlParameter("@scannum",SingleItem.Scannum),
                        new MySqlParameter("@posnr",SingleItem.Posnr)
                        };
            string sql = "update ims_orders set  shopaddrno = @shopaddrno ,  shopname = @shopname ,  weight = @weight ,  state = @state ,  deviceid = @deviceid ,  shopno = @shopno ,  orderno = @orderno ,  orderexplain = @orderexplain ,  verifydate = @verifydate ,  verifynum = @verifynum ,  exchangedate = @exchangedate ,  materialno = @materialno ,  unit = @unit ,  expressdate = @expressdate,exchangeno=@exchangeno,exchangeaddr=@exchangeaddr,boxnum=@boxnum, scannum=@scannum,posnr=@posnr   where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库修改操作
            {
                return true;
            }
            return false;   //修改不成功，或者数据不存在           
        }
        #endregion

        #region  检查记录是否已经存在
        /// <summary>
        /// 检查记录是否已经存在
        /// </summary>
        public bool Exists(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select count(1) from ims_orders where id=@id";
            if (Convert.ToInt32(DbHelperMySQL.GetSingle(sql, prams)) > 0)    //存在返回true
            {
                return true;
            }
            return false;   //不存在返回false
        }
        #endregion

        #region  根据主键取数据
        /// <summary>
        /// 根据主键取数据
        /// </summary>
        public Model.ims_orders SelectById(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select * from ims_orders where id=@id";
            Model.ims_orders SingleItem = new Model.ims_orders();
            using (MySqlDataReader reader = DbHelperMySQL.ExecuteReader(sql, prams))
            {
                if (reader.Read())
                {
                    if (reader["id"].ToString() != "")
                    {
                        SingleItem.Id = int.Parse(reader["id"].ToString());
                    }
                    SingleItem.Shopaddrno = reader["shopaddrno"].ToString();
                    SingleItem.Shopname = reader["shopname"].ToString();
                    if (reader["weight"].ToString() != "")
                    {
                        SingleItem.Weight = decimal.Parse(reader["weight"].ToString());
                    }
                    if (reader["state"].ToString() != "")
                    {
                        SingleItem.State = int.Parse(reader["state"].ToString());
                    }
                    if (reader["deviceid"].ToString() != "")
                    {
                        SingleItem.Deviceid = int.Parse(reader["deviceid"].ToString());
                    }
                    SingleItem.Shopno = reader["shopno"].ToString();
                    if (reader["orderno"].ToString() != "")
                    {
                        SingleItem.Orderno = int.Parse(reader["orderno"].ToString());
                    }
                    SingleItem.Orderexplain = reader["orderexplain"].ToString();
                    if (reader["verifydate"].ToString() != "")
                    {
                        SingleItem.Verifydate = DateTime.Parse(reader["verifydate"].ToString());
                    }
                    if (reader["verifynum"].ToString() != "")
                    {
                        SingleItem.Verifynum = int.Parse(reader["verifynum"].ToString());
                    }
                    if (reader["exchangedate"].ToString() != "")
                    {
                        SingleItem.Exchangedate = DateTime.Parse(reader["exchangedate"].ToString());
                    }
                    if (reader["materialno"].ToString() != "")
                    {
                        SingleItem.Materialno = int.Parse(reader["materialno"].ToString());
                    }
                    SingleItem.Unit = reader["unit"].ToString();
                    if (reader["expressdate"].ToString() != "")
                    {
                        SingleItem.Expressdate = DateTime.Parse(reader["expressdate"].ToString());
                    }
                    if (reader["expressdate"].ToString() != "")
                    {
                        SingleItem.Expressdate = DateTime.Parse(reader["expressdate"].ToString());
                    }

                    SingleItem.Exchangeno = reader["exchangeno"].ToString();
                    SingleItem.Exchangeaddr = reader["exchangeaddr"].ToString();

                    if (reader["boxnum"].ToString() != "")
                    {
                        SingleItem.Boxnum = int.Parse(reader["boxnum"].ToString());
                    }

                    if (reader["scannum"].ToString() != "")
                    {
                        SingleItem.Scannum = int.Parse(reader["scannum"].ToString());
                    }
                    SingleItem.Posnr = reader["posnr"].ToString();
                }
            }
            return SingleItem;
        }
        #endregion

        #region 根据条件取ID排序一条数据
        /// <summary>
        /// 根据条件取ID排序一条数据
        /// </summary>
        public Model.ims_orders SelectModelByWhereOrderByID(string strWhere, bool bAsc)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_orders ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by id ");
            if (bAsc) { strSql.Append(" asc "); }
            else { strSql.Append(" desc "); }
            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                Model.ims_orders SingleItem = new Model.ims_orders();
                    SingleItem.Id = int.Parse(dr["id"].ToString());

                    SingleItem.Shopaddrno = dr["shopaddrno"].ToString();
                    SingleItem.Shopname = dr["shopname"].ToString();
                    SingleItem.Weight = decimal.Parse(dr["weight"].ToString());

                    SingleItem.State = int.Parse(dr["state"].ToString());

                    SingleItem.Deviceid = int.Parse(dr["deviceid"].ToString());

                    SingleItem.Shopno = dr["shopno"].ToString();
                    SingleItem.Orderno = int.Parse(dr["orderno"].ToString());

                    SingleItem.Orderexplain = dr["orderexplain"].ToString();
                    SingleItem.Verifydate = DateTime.Parse(dr["verifydate"].ToString());

                    SingleItem.Verifynum = int.Parse(dr["verifynum"].ToString());

                    SingleItem.Exchangedate = DateTime.Parse(dr["exchangedate"].ToString());

                    SingleItem.Materialno = int.Parse(dr["materialno"].ToString());

                    SingleItem.Unit = dr["unit"].ToString();
                    SingleItem.Expressdate = DateTime.Parse(dr["expressdate"].ToString());

                    SingleItem.Exchangeno = dr["exchangeno"].ToString();
                    SingleItem.Exchangeaddr = dr["exchangeaddr"].ToString();

                    if (dr["boxnum"].ToString() != "")
                    {
                        SingleItem.Boxnum = int.Parse(dr["boxnum"].ToString());
                    }

                    if (dr["scannum"].ToString() != "")
                    {
                        SingleItem.Scannum = int.Parse(dr["scannum"].ToString());
                    }
                    SingleItem.Posnr = dr["posnr"].ToString();
                return SingleItem;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region 取得所有数据的列表
        /// <summary>
        /// 取得所有数据的列表
        /// </summary>
        /// <returns>数据列表</returns>
        public IList<Model.ims_orders> GetAllList()
        {
            IList<Model.ims_orders> itemList = new List<Model.ims_orders>();
            using (MySqlDataReader dr = DbHelperMySQL.ExecuteReader("select * from ims_orders "))
            {
                while (dr.Read())
                {
                    Model.ims_orders SingleItem = new Model.ims_orders();
                    SingleItem.Id = int.Parse(dr["id"].ToString());

                    SingleItem.Shopaddrno = dr["shopaddrno"].ToString();
                    SingleItem.Shopname = dr["shopname"].ToString();
                    SingleItem.Weight = decimal.Parse(dr["weight"].ToString());

                    SingleItem.State = int.Parse(dr["state"].ToString());

                    SingleItem.Deviceid = int.Parse(dr["deviceid"].ToString());

                    SingleItem.Shopno = dr["shopno"].ToString();
                    SingleItem.Orderno = int.Parse(dr["orderno"].ToString());

                    SingleItem.Orderexplain = dr["orderexplain"].ToString();
                    SingleItem.Verifydate = DateTime.Parse(dr["verifydate"].ToString());

                    SingleItem.Verifynum = int.Parse(dr["verifynum"].ToString());

                    SingleItem.Exchangedate = DateTime.Parse(dr["exchangedate"].ToString());

                    SingleItem.Materialno = int.Parse(dr["materialno"].ToString());

                    SingleItem.Unit = dr["unit"].ToString();
                    SingleItem.Expressdate = DateTime.Parse(dr["expressdate"].ToString());

                    SingleItem.Exchangeno = dr["exchangeno"].ToString();
                    SingleItem.Exchangeaddr = dr["exchangeaddr"].ToString();

                    if (dr["boxnum"].ToString() != "")
                    {
                        SingleItem.Boxnum = int.Parse(dr["boxnum"].ToString());
                    }

                    if (dr["scannum"].ToString() != "")
                    {
                        SingleItem.Scannum = int.Parse(dr["scannum"].ToString());
                    }
                    SingleItem.Posnr = dr["posnr"].ToString();
                    itemList.Add(SingleItem);
                }
            }
            return itemList;
        }
        #endregion

        #region  取得Where条件的列表
        /// <summary>
        /// 取得Where条件的列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_orders ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperMySQL.Query(strSql.ToString());
        }
        #endregion

        #region  获取数据条数
        /// <summary>
        /// 获取数据条数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) ");
            strSql.Append(" FROM ims_orders ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperMySQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        #endregion

        #region 根据选择的主键值执行批量删除
        /// <summary>
        /// 根据选择的主键值执行批量删除
        /// </summary>
        /// <param name="keys">选择的主键字符串</param>
        /// <returns>是否删除成功</returns>
        public bool DeleteChooseByKeys(string keys)
        {
            string sql = "delete from ims_orders where id in (" + keys + ")";   //执行批量删除语句, 主键为已选择的多个主键（用in的方式）

            if (DbHelperMySQL.ExecuteSql(sql) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_orders ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_orders ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables.Count == 2 && ds.Tables[1].Rows.Count > 0)
            {
                recordCount = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            }
            else
            {
                recordCount = 0;
            }

            return ds;
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetList(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }
        #endregion


        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整的，如：id desc）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_orders> GetListByPage(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_orders ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_orders ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());

            IList<Model.ims_orders> itemList = new List<Model.ims_orders>();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].Rows[i];

                    Model.ims_orders SingleItem = new Model.ims_orders();
                    {
                        SingleItem.Id = int.Parse(dr["id"].ToString());

                        SingleItem.Shopaddrno = dr["shopaddrno"].ToString();
                        SingleItem.Shopname = dr["shopname"].ToString();
                        SingleItem.Weight = decimal.Parse(dr["weight"].ToString());

                        SingleItem.State = int.Parse(dr["state"].ToString());

                        SingleItem.Deviceid = int.Parse(dr["deviceid"].ToString());

                        SingleItem.Shopno = dr["shopno"].ToString();
                        SingleItem.Orderno = int.Parse(dr["orderno"].ToString());

                        SingleItem.Orderexplain = dr["orderexplain"].ToString();
                        SingleItem.Verifydate = DateTime.Parse(dr["verifydate"].ToString());

                        SingleItem.Verifynum = int.Parse(dr["verifynum"].ToString());

                        SingleItem.Exchangedate = DateTime.Parse(dr["exchangedate"].ToString());

                        SingleItem.Materialno = int.Parse(dr["materialno"].ToString());

                        SingleItem.Unit = dr["unit"].ToString();
                        SingleItem.Expressdate = DateTime.Parse(dr["expressdate"].ToString());

                        SingleItem.Exchangeno = dr["exchangeno"].ToString();
                        SingleItem.Exchangeaddr = dr["exchangeaddr"].ToString();

                        if (dr["boxnum"].ToString() != "")
                        {
                             SingleItem.Boxnum = int.Parse(dr["boxnum"].ToString());
                        }

                        SingleItem.Scannum = int.Parse(dr["scannum"].ToString());
                        SingleItem.Posnr = dr["posnr"].ToString();
                    };
                    itemList.Add(SingleItem);
                }
                recordCount = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                return itemList;
            }
            else
            {
                recordCount = 0;
                return null;
            }
        }
        #endregion

        #region 查询分页数据 （重载方法，省略了排序字段orderString）
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_orders> GetListByPage(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetListByPage(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }


        #endregion


        public DataSet GetAllLinesByTime(string strTime)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select s.vehiclelineno,s.vehiclename,s.shiporder,s.sort_id from ims_orders as o ");
            strSql.Append("LEFT JOIN ims_dict_shop as s ON s.shopno = o.shopno ");
            strSql.Append("WHERE DATE_FORMAT(o.exchangedate,'%Y-%m-%d') = '");
            strSql.Append(strTime);
            strSql.Append("'  and s.vehiclelineno <> '' ");
            strSql.Append("GROUP BY s.vehiclelineno ");
            strSql.Append("order BY s.shiporder asc");
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public DataSet GetAllShopsByLineAndTime(string strTime, string lineno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select o.shopno,o.exchangeno,s.shopname,'' as status,'' as statusno from ims_orders as o ");
            strSql.Append("LEFT JOIN ims_dict_shop as s ON s.shopno = o.shopno ");
            strSql.Append("WHERE  state < 2 and DATE_FORMAT(o.exchangedate,'%Y-%m-%d') = '");
            strSql.Append(strTime);
            strSql.Append("'  and s.vehiclelineno = '");
            strSql.Append(lineno + "' ");
            strSql.Append("GROUP BY o.shopno  ");
            strSql.Append("order BY s.sort_id asc");
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public DataSet GetAllShopsByLineAndTimeEx(string strTime, string lineno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select o.shopno,o.exchangeno,s.shopname,'' as status,'' as statusno from ims_orders as o ");
            strSql.Append("LEFT JOIN ims_dict_shop as s ON s.shopno = o.shopno ");
            strSql.Append("WHERE  DATE_FORMAT(o.exchangedate,'%Y-%m-%d') = '");
            strSql.Append(strTime);
            strSql.Append("'  and s.vehiclelineno = '");
            strSql.Append(lineno + "' ");
            strSql.Append("GROUP BY o.shopno  ");
            strSql.Append("order BY s.sort_id asc");
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public DataSet GetCurrentNoScanOrderByDeviceID(string strTime, string lineno,int deviceid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT s.shopno,s.shopname,o.id,p.productname,p.shortname,o.verifynum,o.scannum FROM ims_orders as o LEFT JOIN ims_dict_shop as s ON s.shopno = o.shopno ");
            strSql.Append("LEFT JOIN ims_dict_product as p ON o.materialno = p.materialNo ");
            strSql.AppendFormat("WHERE DATE_FORMAT(o.exchangedate,'%Y-%m-%d') = '{0}' and s.vehiclelineno = '{1}' ",strTime,lineno);
            strSql.AppendFormat("and o.state = 0 and o.deviceid = {0} ", deviceid);
            strSql.Append("and s.sort_id = (SELECT s.sort_id FROM ims_orders as o LEFT JOIN ims_dict_shop as s ON s.shopno = o.shopno WHERE ");
            strSql.AppendFormat("DATE_FORMAT(o.exchangedate,'%Y-%m-%d') = '{0}' and s.vehiclelineno = '{1}' and o.state = 0 and o.deviceid = {2} ",strTime,lineno,deviceid);
            strSql.Append("GROUP BY s.sort_id order by s.sort_id ASC LIMIT 1) ");
            strSql.Append("ORDER BY s.sort_id ASC ");
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public decimal GetSumWeightByShopNo(string strTime, string shopno)
        {
            decimal weight = 0;


            return weight;
        }

        public bool UpdateShopOrderStateByShopNoAndDt(int state, string strTime, string shopno,string exchangeno)
        {
            string sql = string.Format("update ims_orders set state = {0} and scannum = 0 where DATE_FORMAT(exchangedate,'%Y-%m-%d') = '{1}' and shopno = '{2}' and exchangeno = '{3}'", state, strTime, shopno,exchangeno);
            return DbHelperMySQL.ExecuteSql(sql) > 0;
        }

        public DataSet GetOrdersByWhere(string strWhere)
        {
            StringBuilder sbsql = new StringBuilder();
            sbsql.Append("select id,orderno,orderexplain,verifydate,verifynum,exchangedate,materialno,unit,expressdate,shopaddrno,shopname,weight,shopno,exchangeno,exchangeaddr from ims_orders where ");
            sbsql.Append(strWhere);
            return DbHelperMySQL.Query(sbsql.ToString());
        }

        public DataSet GetOrdersByWhere(string exchangedate, string shopno)
        {
            StringBuilder sbsql = new StringBuilder();
            sbsql.Append("select o.id,o.orderno,o.orderexplain,o.verifydate,o.verifynum,o.exchangedate,o.materialno,o.unit,o.expressdate,o.shopaddrno,o.shopname,o.weight,o.shopno,o.exchangeno,o.exchangeaddr,p.spec from ims_orders as o left join ims_dict_product as p on o.materialno=p.materialNo where ");
            sbsql.AppendFormat(" DATE_FORMAT(o.exchangedate, '%Y-%m-%d') = '{0}' and o.shopno ='{1}' ", exchangedate, shopno);
            return DbHelperMySQL.Query(sbsql.ToString());
        }


        /// <summary>
        /// 获取订单重量,净重
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public decimal GetSumRealWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            string sql = string.Format("SELECT IFNULL(SUM(os.verifynum * os.weight),0) AS sw FROM ims_orders as os LEFT JOIN ims_dict_product as p ON p.materialNo = os.materialno WHERE DATE_FORMAT(exchangedate, '%Y-%m-%d') = '{0}' AND os.shopno = '{1}' and os.exchangeno='{2}' ", strTime, shopno, exchangeno);
            DataSet ds = DbHelperMySQL.Query(sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString());
            }
            return 0.0M;
        }

        /// <summary>
        /// 获取毛重
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public decimal GetSumMaoWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            string sql = string.Format("SELECT IFNULL(SUM(os.verifynum * (os.weight + p.packetweight)),0) AS sw FROM ims_orders as os LEFT JOIN ims_dict_product as p ON p.materialNo = os.materialno WHERE DATE_FORMAT(exchangedate, '%Y-%m-%d') = '{0}' AND os.shopno = '{1}' and os.exchangeno='{2}' ", strTime, shopno, exchangeno);
            DataSet ds = DbHelperMySQL.Query(sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString());
            }
            return 0.0M;
        }

        /// <summary>
        /// 获取包装重量
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public decimal GetSumPacketWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            string sql = string.Format("SELECT IFNULL(SUM(os.verifynum *  p.packetweight),0) AS sw FROM ims_orders as os LEFT JOIN ims_dict_product as p ON p.materialNo = os.materialno WHERE DATE_FORMAT(exchangedate, '%Y-%m-%d') = '{0}' AND os.shopno = '{1}' and os.exchangeno='{2}' ", strTime, shopno, exchangeno);
            DataSet ds = DbHelperMySQL.Query(sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString());
            }
            return 0.0M;
        }

        public bool CheckHasScrabedByLineAndDt(string strTime, string lineno)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT COUNT(1) FROM ims_orders as o WHERE ");
            sb.Append("o.shopno in ( SELECT s1.shopno from ( ");
            sb.AppendFormat("SELECT s2.shopno from ims_dict_shop as s2 WHERE s2.vehiclelineno = '{0}' ) AS s1) AND o.state < 2 ",lineno);
            sb.AppendFormat("AND DATE_FORMAT(o.exchangedate, '%Y-%m-%d') = '{0}'",strTime);
            DataSet ds = DbHelperMySQL.Query(sb.ToString());

            if (ds != null && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) <= 0;
            }
            return false;
        }

        public DataSet GetOrdersGroupByExchangeno(string strTime)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT o.exchangeno,o.shopno FROM ims_orders as o WHERE ");
            sb.AppendFormat(" DATE_FORMAT(o.exchangedate, '%Y-%m-%d') = '{0}' ", strTime);
            sb.Append("group by o.exchangeno;");
            return DbHelperMySQL.Query(sb.ToString());
        }
    }
}

