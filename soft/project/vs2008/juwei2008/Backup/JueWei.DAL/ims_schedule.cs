using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using JueWei.Model;
using JueWei.DBUtility;
namespace JueWei.DAL
{
    public partial class ims_schedule
    {
        #region  添加一条数据
        /// <summary>
        /// 添加一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否添加成功</returns>
        public bool Insert(Model.ims_schedule SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@order_id",SingleItem.Order_id),            
                        new MySqlParameter("@dev_id",SingleItem.Dev_id),            
                        new MySqlParameter("@state",SingleItem.State),            
                        new MySqlParameter("@createdate",SingleItem.Createdate),            
                        new MySqlParameter("@expiredate",SingleItem.Expiredate)            
                        };
            string sql = "insert into ims_schedule (order_id,dev_id,state,createdate,expiredate) values (@order_id,@dev_id,@state,@createdate,@expiredate)";


            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库插入操作
            {
                return true;
            }
            return false;   //插入不成功，或者数据已经存在 
        }
        #endregion

        #region 删除一条数据
        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        /// <param name="id">主键值</param>
        /// <returns>是否删除成功！</returns>
        public bool Delete(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "delete from ims_schedule where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 更新一条数据
        /// <summary>
        /// 更新一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否修改成功</returns>
        public bool Update(Model.ims_schedule SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@id",SingleItem.Id),            
                        new MySqlParameter("@order_id",SingleItem.Order_id),            
                        new MySqlParameter("@dev_id",SingleItem.Dev_id),            
                        new MySqlParameter("@state",SingleItem.State),            
                        new MySqlParameter("@createdate",SingleItem.Createdate),            
                        new MySqlParameter("@expiredate",SingleItem.Expiredate)            
                        };
            string sql = "update ims_schedule set  order_id = @order_id ,  dev_id = @dev_id ,  state = @state ,  createdate = @createdate ,  expiredate = @expiredate   where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库修改操作
            {
                return true;
            }
            return false;   //修改不成功，或者数据不存在           
        }
        #endregion

        #region  检查记录是否已经存在
        /// <summary>
        /// 检查记录是否已经存在
        /// </summary>
        public bool Exists(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select count(1) from ims_schedule where id=@id";
            if (Convert.ToInt32(DbHelperMySQL.GetSingle(sql, prams)) > 0)    //存在返回true
            {
                return true;
            }
            return false;   //不存在返回false
        }
        #endregion

        #region  根据主键取数据
        /// <summary>
        /// 根据主键取数据
        /// </summary>
        public Model.ims_schedule SelectById(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select * from ims_schedule where id=@id";
            Model.ims_schedule SingleItem = new Model.ims_schedule();
            using (MySqlDataReader reader = DbHelperMySQL.ExecuteReader(sql, prams))
            {
                if (reader.Read())
                {
                    if (reader["id"].ToString() != "")
                    {
                        SingleItem.Id = int.Parse(reader["id"].ToString());
                    }
                    if (reader["order_id"].ToString() != "")
                    {
                        SingleItem.Order_id = int.Parse(reader["order_id"].ToString());
                    }
                    if (reader["dev_id"].ToString() != "")
                    {
                        SingleItem.Dev_id = int.Parse(reader["dev_id"].ToString());
                    }
                    if (reader["state"].ToString() != "")
                    {
                        SingleItem.State = int.Parse(reader["state"].ToString());
                    }
                    if (reader["createdate"].ToString() != "")
                    {
                        SingleItem.Createdate = DateTime.Parse(reader["createdate"].ToString());
                    }
                    if (reader["expiredate"].ToString() != "")
                    {
                        SingleItem.Expiredate = DateTime.Parse(reader["expiredate"].ToString());
                    }

                }
            }
            return SingleItem;
        }
        #endregion

        #region 根据条件取ID排序一条数据
        /// <summary>
        /// 根据条件取ID排序一条数据
        /// </summary>
        public Model.ims_schedule SelectModelByWhereOrderByID(string strWhere, bool bAsc)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_schedule ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by id ");
            if (bAsc) { strSql.Append(" asc "); }
            else { strSql.Append(" desc "); }
            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                Model.ims_schedule SingleItem = new Model.ims_schedule
                {
                    Id = int.Parse(dr["id"].ToString()),

                    Order_id = int.Parse(dr["order_id"].ToString()),

                    Dev_id = int.Parse(dr["dev_id"].ToString()),

                    State = int.Parse(dr["state"].ToString()),

                    Createdate = DateTime.Parse(dr["createdate"].ToString()),

                    Expiredate = DateTime.Parse(dr["expiredate"].ToString())

                };
                return SingleItem;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region 取得所有数据的列表
        /// <summary>
        /// 取得所有数据的列表
        /// </summary>
        /// <returns>数据列表</returns>
        public IList<Model.ims_schedule> GetAllList()
        {
            IList<Model.ims_schedule> itemList = new List<Model.ims_schedule>();
            using (MySqlDataReader reader = DbHelperMySQL.ExecuteReader("select * from ims_schedule "))
            {
                while (reader.Read())
                {
                    Model.ims_schedule SingleItem = new Model.ims_schedule
                    {
                        Id = int.Parse(reader["id"].ToString()),
                        Order_id = int.Parse(reader["order_id"].ToString()),
                        Dev_id = int.Parse(reader["dev_id"].ToString()),
                        State = int.Parse(reader["state"].ToString()),
                        Createdate = DateTime.Parse(reader["createdate"].ToString()),
                        Expiredate = DateTime.Parse(reader["expiredate"].ToString())
                    };
                    itemList.Add(SingleItem);
                }
            }
            return itemList;
        }
        #endregion

        #region  取得Where条件的列表
        /// <summary>
        /// 取得Where条件的列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_schedule ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperMySQL.Query(strSql.ToString());
        }
        #endregion

        #region  获取数据条数
        /// <summary>
        /// 获取数据条数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) ");
            strSql.Append(" FROM ims_schedule ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperMySQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        #endregion

        #region 根据选择的主键值执行批量删除
        /// <summary>
        /// 根据选择的主键值执行批量删除
        /// </summary>
        /// <param name="keys">选择的主键字符串</param>
        /// <returns>是否删除成功</returns>
        public bool DeleteChooseByKeys(string keys)
        {
            string sql = "delete from ims_schedule where id in (" + keys + ")";   //执行批量删除语句, 主键为已选择的多个主键（用in的方式）

            if (DbHelperMySQL.ExecuteSql(sql) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_schedule ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_schedule ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables.Count == 2 && ds.Tables[1].Rows.Count > 0)
            {
                recordCount = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            }
            else
            {
                recordCount = 0;
            }

            return ds;
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetList(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }
        #endregion


        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整的，如：id desc）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_schedule> GetListByPage(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_schedule ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_schedule ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());

            IList<Model.ims_schedule> itemList = new List<Model.ims_schedule>();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Model.ims_schedule SingleItem = new Model.ims_schedule
                    {
                        Id = int.Parse(ds.Tables[0].Rows[i]["id"].ToString()),

                        Order_id = int.Parse(ds.Tables[0].Rows[i]["order_id"].ToString()),

                        Dev_id = int.Parse(ds.Tables[0].Rows[i]["dev_id"].ToString()),

                        State = int.Parse(ds.Tables[0].Rows[i]["state"].ToString()),

                        Createdate = DateTime.Parse(ds.Tables[0].Rows[i]["createdate"].ToString()),

                        Expiredate = DateTime.Parse(ds.Tables[0].Rows[i]["expiredate"].ToString())

                    };
                    itemList.Add(SingleItem);
                }
                recordCount = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                return itemList;
            }
            else
            {
                recordCount = 0;
                return null;
            }
        }
        #endregion

        #region 查询分页数据 （重载方法，省略了排序字段orderString）
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_schedule> GetListByPage(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetListByPage(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }
        #endregion
    }
}

