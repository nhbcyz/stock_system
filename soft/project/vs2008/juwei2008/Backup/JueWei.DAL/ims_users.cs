using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using JueWei.Model;
using JueWei.DBUtility;
using WHC.OrderWater.Commons;
namespace JueWei.DAL
{
    public partial class ims_users
    {
        #region  添加一条数据
        /// <summary>
        /// 添加一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否添加成功</returns>
        public bool Insert(Model.ims_users SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@username",SingleItem.Username),            
                        new MySqlParameter("@password",SingleItem.Password),            
                        new MySqlParameter("@salt",SingleItem.Salt),            
                        new MySqlParameter("@createtime",SingleItem.Createtime),            
                        new MySqlParameter("@fullname",SingleItem.Fullname),            
                        new MySqlParameter("@expireddate",SingleItem.Expireddate)            
                        };
            string sql = "insert into ims_users (username,password,salt,createtime,fullname,expireddate) values (@username,@password,@salt,@createtime,@fullname,@expireddate)";


            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库插入操作
            {
                return true;
            }
            return false;   //插入不成功，或者数据已经存在 
        }
        #endregion

        #region 删除一条数据
        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        /// <param name="id">主键值</param>
        /// <returns>是否删除成功！</returns>
        public bool Delete(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "delete from ims_users where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 更新一条数据
        /// <summary>
        /// 更新一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否修改成功</returns>
        public bool Update(Model.ims_users SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@id",SingleItem.Id),            
                        new MySqlParameter("@username",SingleItem.Username),            
                        new MySqlParameter("@password",SingleItem.Password),            
                        new MySqlParameter("@salt",SingleItem.Salt),            
                        new MySqlParameter("@createtime",SingleItem.Createtime),            
                        new MySqlParameter("@fullname",SingleItem.Fullname),            
                        new MySqlParameter("@expireddate",SingleItem.Expireddate)            
                        };
            string sql = "update ims_users set  username = @username ,  password = @password ,  salt = @salt ,  createtime = @createtime ,  fullname = @fullname ,  expireddate = @expireddate   where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库修改操作
            {
                return true;
            }
            return false;   //修改不成功，或者数据不存在           
        }
        #endregion

        #region  检查记录是否已经存在
        /// <summary>
        /// 检查记录是否已经存在
        /// </summary>
        public bool Exists(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select count(1) from ims_users where id=@id";
            if (Convert.ToInt32(DbHelperMySQL.GetSingle(sql, prams)) > 0)    //存在返回true
            {
                return true;
            }
            return false;   //不存在返回false
        }
        #endregion

        #region  根据主键取数据
        /// <summary>
        /// 根据主键取数据
        /// </summary>
        public Model.ims_users SelectById(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select * from ims_users where id=@id";
            Model.ims_users SingleItem = new Model.ims_users();
            using (MySqlDataReader reader = DbHelperMySQL.ExecuteReader(sql, prams))
            {
                if (reader.Read())
                {
                    if (reader["id"].ToString() != "")
                    {
                        SingleItem.Id = int.Parse(reader["id"].ToString());
                    }
                    SingleItem.Username = reader["username"].ToString();
                    SingleItem.Password = reader["password"].ToString();
                    SingleItem.Salt = reader["salt"].ToString();
                    if (reader["createtime"].ToString() != "")
                    {
                        SingleItem.Createtime = DateTime.Parse(reader["createtime"].ToString());
                    }
                    SingleItem.Fullname = reader["fullname"].ToString();
                    if (reader["expireddate"].ToString() != "")
                    {
                        SingleItem.Expireddate = DateTime.Parse(reader["expireddate"].ToString());
                    }

                }
            }
            return SingleItem;
        }
        #endregion

        #region 根据条件取ID排序一条数据
        /// <summary>
        /// 根据条件取ID排序一条数据
        /// </summary>
        public Model.ims_users SelectModelByWhereOrderByID(string strWhere, bool bAsc)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_users ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by id ");
            if (bAsc) { strSql.Append(" asc "); }
            else { strSql.Append(" desc "); }
            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                Model.ims_users SingleItem = new Model.ims_users
                {
                    Id = int.Parse(dr["id"].ToString()),

                    Username = dr["username"].ToString(),
                    Password = dr["password"].ToString(),
                    Salt = dr["salt"].ToString(),
                    Createtime = DateTime.Parse(dr["createtime"].ToString()),

                    Fullname = dr["fullname"].ToString(),
                    Expireddate = DateTime.Parse(dr["expireddate"].ToString())

                };
                return SingleItem;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region 取得所有数据的列表
        /// <summary>
        /// 取得所有数据的列表
        /// </summary>
        /// <returns>数据列表</returns>
        public IList<Model.ims_users> GetAllList()
        {
            IList<Model.ims_users> itemList = new List<Model.ims_users>();
            using (MySqlDataReader reader = DbHelperMySQL.ExecuteReader("select * from ims_users "))
            {
                while (reader.Read())
                {
                    Model.ims_users SingleItem = new Model.ims_users();
                    {
                        SingleItem.Id = int.Parse(reader["id"].ToString());
                        SingleItem.Username = reader["username"].ToString();
                        SingleItem.Password = reader["password"].ToString();
                        SingleItem.Salt = reader["salt"].ToString();
                        SingleItem.Createtime = DateTime.Parse(reader["createtime"].ToString());
                        if (reader["fullname"] != null)
                        {
                            SingleItem.Fullname = reader["fullname"].ToString();
                        }
                        if (reader["expireddate"] != null && reader["expireddate"].ToString() != "")
                        {
                            SingleItem.Expireddate = DateTime.Parse(reader["expireddate"].ToString());
                        }
                        
                    };
                    itemList.Add(SingleItem);
                }
            }
            return itemList;
        }
        #endregion

        #region  取得Where条件的列表
        /// <summary>
        /// 取得Where条件的列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_users ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperMySQL.Query(strSql.ToString());
        }
        #endregion

        #region  获取数据条数
        /// <summary>
        /// 获取数据条数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) ");
            strSql.Append(" FROM ims_users ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperMySQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        #endregion

        #region 根据选择的主键值执行批量删除
        /// <summary>
        /// 根据选择的主键值执行批量删除
        /// </summary>
        /// <param name="keys">选择的主键字符串</param>
        /// <returns>是否删除成功</returns>
        public bool DeleteChooseByKeys(string keys)
        {
            string sql = "delete from ims_users where id in (" + keys + ")";   //执行批量删除语句, 主键为已选择的多个主键（用in的方式）

            if (DbHelperMySQL.ExecuteSql(sql) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_users ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_users ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables.Count == 2 && ds.Tables[1].Rows.Count > 0)
            {
                recordCount = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            }
            else
            {
                recordCount = 0;
            }

            return ds;
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetList(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }
        #endregion


        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整的，如：id desc）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_users> GetListByPage(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_users ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_users ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());

            IList<Model.ims_users> itemList = new List<Model.ims_users>();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Model.ims_users SingleItem = new Model.ims_users
                    {
                        Id = int.Parse(ds.Tables[0].Rows[i]["id"].ToString()),

                        Username = ds.Tables[0].Rows[i]["username"].ToString(),
                        Password = ds.Tables[0].Rows[i]["password"].ToString(),
                        Salt = ds.Tables[0].Rows[i]["salt"].ToString(),
                        Createtime = DateTime.Parse(ds.Tables[0].Rows[i]["createtime"].ToString()),

                        Fullname = ds.Tables[0].Rows[i]["fullname"].ToString(),
                        Expireddate = DateTime.Parse(ds.Tables[0].Rows[i]["expireddate"].ToString())

                    };
                    itemList.Add(SingleItem);
                }
                recordCount = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                return itemList;
            }
            else
            {
                recordCount = 0;
                return null;
            }
        }
        #endregion

        #region 查询分页数据 （重载方法，省略了排序字段orderString）
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_users> GetListByPage(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetListByPage(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }
        #endregion


        public Model.ims_users VerifyUser(string username, string pwd)
        {
            string strSql = String.Format("select salt from ims_users where username = '{0}'",username);
            DataSet ds = DbHelperMySQL.Query(strSql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                string salt = ds.Tables[0].Rows[0][0].ToString();
                if (!string.IsNullOrEmpty(salt))
                {
                    string onepwd = MD5Util.GetMD5_32(pwd);
                    string twopwd = MD5Util.GetMD5_32(onepwd + salt);
                    return this.SelectModelByWhereOrderByID(string.Format(" username ='{0}' and password='{1}' and  expireddate > '{2}'", username, twopwd,DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")), true);
                }
            }
            return null;
        }

        public int GetRoleIdByUserId(int userid)
        {
            string strSql = "SELECT um.role_id FROM ims_users as u  LEFT JOIN ims_userrole_mapping as um ON u.id = um.user_id ";
            strSql += "WHERE u.id = " + userid;
            DataSet ds = DbHelperMySQL.Query(strSql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                string roleidstr = ds.Tables[0].Rows[0][0].ToString();
                if (!string.IsNullOrEmpty(roleidstr))
                {
                    return int.Parse(roleidstr);
                }
            }
            return -1;
        }

        public int InsertUserAndRoleID(Model.ims_users muser, int roleid)
        {
            if (!Insert(muser))
            {
                return -1;
            }
            muser = SelectModelByWhereOrderByID(" username = '" + muser.Username + "'",true);
            string sql = string.Format("insert into ims_userrole_mapping (user_id,role_id) values({0},{1})",muser.Id,roleid);
            return DbHelperMySQL.ExecuteSql(sql);
        }

        public int UpdateUserAndRoleID(Model.ims_users muser, int roleid)
        {
            if (!Update(muser))
            {
                return -1;
            }
            string sql = string.Format("Upate ims_userrole_mapping set role_id = {0} where user_id={1}", roleid, muser.Id);
            return DbHelperMySQL.ExecuteSql(sql);
        }
    }
}

