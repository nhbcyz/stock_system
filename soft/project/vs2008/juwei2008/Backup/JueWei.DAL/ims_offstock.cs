using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using JueWei.Model;
using JueWei.DBUtility;
namespace JueWei.DAL
{
    public partial class ims_offstock
    {
        #region  添加一条数据
        /// <summary>
        /// 添加一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否添加成功</returns>
        public bool Insert(Model.ims_offstock SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@state",SingleItem.State),            
                        new MySqlParameter("@orderid",SingleItem.Orderid),            
                        new MySqlParameter("@shopno",SingleItem.Shopno),            
                        new MySqlParameter("@deviceid",SingleItem.Deviceid),            
                        new MySqlParameter("@materialno",SingleItem.Materialno),            
                        new MySqlParameter("@scandate",SingleItem.Scandate),            
                        new MySqlParameter("@weightdate",SingleItem.Weightdate),            
                        new MySqlParameter("@offstockdate",SingleItem.Offstockdate),      
                        new MySqlParameter("@orderdate",SingleItem.Orderdate),            
                        new MySqlParameter("@weight",SingleItem.Weight),      
                        new MySqlParameter("@boxnum",SingleItem.Boxnum),
                        new MySqlParameter("@productnum",SingleItem.Productnum), 
                        new MySqlParameter("@exchangeno",SingleItem.Exchangeno)     
                        };
            string sql = "insert into ims_offstock (state,orderid,shopno,deviceid,materialno,scandate,weightdate,offstockdate,orderdate,weight,boxnum,productnum,exchangeno) values (@state,@orderid,@shopno,@deviceid,@materialno,@scandate,@weightdate,@offstockdate,@orderdate,@weight,@boxnum,@productnum,@exchangeno)";


            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库插入操作
            {
                return true;
            }
            return false;   //插入不成功，或者数据已经存在 
        }
        #endregion

        #region 删除一条数据
        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        /// <param name="id">主键值</param>
        /// <returns>是否删除成功！</returns>
        public bool Delete(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "delete from ims_offstock where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 更新一条数据
        /// <summary>
        /// 更新一条数据
        /// </summary>
        /// <param name="SingleItem">数据实体</param>
        /// <returns>bool值，是否修改成功</returns>
        public bool Update(Model.ims_offstock SingleItem)
        {
            MySqlParameter[] prams = new MySqlParameter[]{
                        new MySqlParameter("@id",SingleItem.Id),            
                        new MySqlParameter("@state",SingleItem.State),            
                        new MySqlParameter("@orderid",SingleItem.Orderid),            
                        new MySqlParameter("@shopno",SingleItem.Shopno),            
                        new MySqlParameter("@deviceid",SingleItem.Deviceid),            
                        new MySqlParameter("@materialno",SingleItem.Materialno),            
                        new MySqlParameter("@scandate",SingleItem.Scandate),            
                        new MySqlParameter("@weightdate",SingleItem.Weightdate),            
                        new MySqlParameter("@offstockdate",SingleItem.Offstockdate),   
                        new MySqlParameter("@orderdate",SingleItem.Orderdate),   
                        new MySqlParameter("@weight",SingleItem.Weight),
                        new MySqlParameter("@boxnum",SingleItem.Boxnum),
                        new MySqlParameter("@offsumweight",SingleItem.Offsumweight),
                        new MySqlParameter("@productnum",SingleItem.Productnum),
                        new MySqlParameter("@exchangeno",SingleItem.Exchangeno)
                        
                        };
            string sql = "update ims_offstock set  state = @state ,  orderid = @orderid ,  shopno = @shopno ,  deviceid = @deviceid ,  materialno = @materialno ,  scandate = @scandate ,  weightdate = @weightdate ,  offstockdate = @offstockdate , orderdate= @orderdate, weight = @weight, boxnum = @boxnum,offsumweight=@offsumweight,productnum=@productnum,exchangeno=@exchangeno  where id=@id";

            if (DbHelperMySQL.ExecuteSql(sql, prams) > 0)  //执行数据库修改操作
            {
                return true;
            }
            return false;   //修改不成功，或者数据不存在           
        }
        #endregion

        #region  检查记录是否已经存在
        /// <summary>
        /// 检查记录是否已经存在
        /// </summary>
        public bool Exists(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select count(1) from ims_offstock where id=@id";
            if (Convert.ToInt32(DbHelperMySQL.GetSingle(sql, prams)) > 0)    //存在返回true
            {
                return true;
            }
            return false;   //不存在返回false
        }
        #endregion

        #region  根据主键取数据
        /// <summary>
        /// 根据主键取数据
        /// </summary>
        public Model.ims_offstock SelectById(int id)
        {
            MySqlParameter[] prams = new MySqlParameter[] { 
                new MySqlParameter("@id",id)
            };
            string sql = "select * from ims_offstock where id=@id";
            Model.ims_offstock SingleItem = new Model.ims_offstock();
            using (MySqlDataReader dr = DbHelperMySQL.ExecuteReader(sql, prams))
            {
                if (dr.Read())
                {
                    if (dr["id"].ToString() != "")
                    {
                        SingleItem.Id = int.Parse(dr["id"].ToString());
                    }
                    if (dr["state"].ToString() != "")
                    {
                        SingleItem.State = int.Parse(dr["state"].ToString());
                    }
                    if (dr["orderid"].ToString() != "")
                    {
                        SingleItem.Orderid = int.Parse(dr["orderid"].ToString());
                    }
                    SingleItem.Shopno = dr["shopno"].ToString();
                    if (dr["deviceid"].ToString() != "")
                    {
                        SingleItem.Deviceid = int.Parse(dr["deviceid"].ToString());
                    }
                    if (dr["materialno"].ToString() != "")
                    {
                        SingleItem.Materialno = dr["materialno"].ToString();
                    }
                    if (dr["exchangeno"].ToString() != "")
                    {
                        SingleItem.Exchangeno = dr["exchangeno"].ToString();
                    }
                    
                    if (dr["scandate"].ToString() != "")
                    {
                        SingleItem.Scandate = DateTime.Parse(dr["scandate"].ToString());
                    }
                    if (dr["weightdate"] != null && dr["weightdate"].ToString() != "")
                    {
                        SingleItem.Weightdate = DateTime.Parse(dr["weightdate"].ToString());
                    }
                    if (dr["offstockdate"] != null && dr["offstockdate"].ToString() != "")
                    {
                        SingleItem.Offstockdate = DateTime.Parse(dr["offstockdate"].ToString());
                    }
                    if (dr["orderdate"] != null && dr["orderdate"].ToString() != "")
                    {
                        SingleItem.Orderdate = DateTime.Parse(dr["orderdate"].ToString());
                    }
                    if (dr["weight"].ToString() != "")
                    {
                        SingleItem.Weight = decimal.Parse(dr["weight"].ToString());
                    }
                    if (dr["boxnum"].ToString() != "")
                    {
                        SingleItem.Boxnum = int.Parse(dr["boxnum"].ToString());
                    }
                    if (dr["productnum"].ToString() != "")
                    {
                        SingleItem.Productnum = int.Parse(dr["productnum"].ToString());
                    }
                    if (dr["offsumweight"].ToString() != "")
                    {
                        SingleItem.Offsumweight = decimal.Parse(dr["offsumweight"].ToString());
                    }
                }
            }
            return SingleItem;
        }
        #endregion

        #region 根据条件取ID排序一条数据
        /// <summary>
        /// 根据条件取ID排序一条数据
        /// </summary>
        public Model.ims_offstock SelectModelByWhereOrderByID(string strWhere, bool bAsc)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_offstock ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by id ");
            if (bAsc) { strSql.Append(" asc "); }
            else { strSql.Append(" desc "); }
            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                Model.ims_offstock SingleItem = new Model.ims_offstock();
                {
                    SingleItem.Id = int.Parse(dr["id"].ToString());

                    SingleItem.State = int.Parse(dr["state"].ToString());

                    SingleItem.Orderid = int.Parse(dr["orderid"].ToString());

                    SingleItem.Shopno = dr["shopno"].ToString();
                    SingleItem.Deviceid = int.Parse(dr["deviceid"].ToString());
                    if (dr["exchangeno"].ToString() != "")
                    {
                        SingleItem.Exchangeno = dr["exchangeno"].ToString();
                    }
                    SingleItem.Materialno = dr["materialno"].ToString();

                    SingleItem.Scandate = DateTime.Parse(dr["scandate"].ToString());

                    if (dr["weightdate"] != null && dr["weightdate"].ToString() != "")
                    {
                        SingleItem.Weightdate = DateTime.Parse(dr["weightdate"].ToString());
                    }
                    if (dr["offstockdate"] != null && dr["offstockdate"].ToString() != "")
                    {
                        SingleItem.Offstockdate = DateTime.Parse(dr["offstockdate"].ToString());
                    }
                    if (dr["orderdate"] != null && dr["orderdate"].ToString() != "")
                    {
                        SingleItem.Orderdate = DateTime.Parse(dr["orderdate"].ToString());
                    }
                    if (dr["boxnum"].ToString() != "")
                    {
                        SingleItem.Boxnum = int.Parse(dr["boxnum"].ToString());
                    }
                    if (dr["productnum"].ToString() != "")
                    {
                        SingleItem.Productnum = int.Parse(dr["productnum"].ToString());
                    }
                    if (dr["offsumweight"].ToString() != "")
                    {
                        SingleItem.Offsumweight = decimal.Parse(dr["offsumweight"].ToString());
                    }
                    SingleItem.Weight = decimal.Parse(dr["weight"].ToString());

                };
                return SingleItem;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region 取得所有数据的列表
        /// <summary>
        /// 取得所有数据的列表
        /// </summary>
        /// <returns>数据列表</returns>
        public IList<Model.ims_offstock> GetAllList()
        {
            IList<Model.ims_offstock> itemList = new List<Model.ims_offstock>();
            using (MySqlDataReader dr = DbHelperMySQL.ExecuteReader("select * from ims_offstock "))
            {
                while (dr.Read())
                {
                    Model.ims_offstock SingleItem = new Model.ims_offstock();
                    {
                        SingleItem.Id = int.Parse(dr["id"].ToString());

                        SingleItem.State = int.Parse(dr["state"].ToString());

                        SingleItem.Orderid = int.Parse(dr["orderid"].ToString());

                        SingleItem.Shopno = dr["shopno"].ToString();
                        SingleItem.Deviceid = int.Parse(dr["deviceid"].ToString());

                        SingleItem.Materialno = dr["materialno"].ToString();

                        SingleItem.Scandate = DateTime.Parse(dr["scandate"].ToString());
                        if (dr["exchangeno"].ToString() != "")
                        {
                            SingleItem.Exchangeno = dr["exchangeno"].ToString();
                        }
                        if (dr["weightdate"] != null && dr["weightdate"].ToString() != "")
                        {
                            SingleItem.Weightdate = DateTime.Parse(dr["weightdate"].ToString());
                        }
                        if (dr["offstockdate"] != null && dr["offstockdate"].ToString() != "")
                        {
                            SingleItem.Offstockdate = DateTime.Parse(dr["offstockdate"].ToString());
                        }
                        if (dr["orderdate"] != null && dr["orderdate"].ToString() != "")
                        {
                            SingleItem.Orderdate = DateTime.Parse(dr["orderdate"].ToString());
                        }
                        if (dr["boxnum"].ToString() != "")
                        {
                            SingleItem.Boxnum = int.Parse(dr["boxnum"].ToString());
                        }
                        if (dr["productnum"].ToString() != "")
                        {
                            SingleItem.Productnum = int.Parse(dr["productnum"].ToString());
                        }
                        if (dr["offsumweight"].ToString() != "")
                        {
                            SingleItem.Offsumweight = decimal.Parse(dr["offsumweight"].ToString());
                        }
                        SingleItem.Weight = decimal.Parse(dr["weight"].ToString());

                    };
                    itemList.Add(SingleItem);
                }
            }
            return itemList;
        }
        #endregion

        #region  取得Where条件的列表
        /// <summary>
        /// 取得Where条件的列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM ims_offstock ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperMySQL.Query(strSql.ToString());
        }
        #endregion

        #region  获取数据条数
        /// <summary>
        /// 获取数据条数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) ");
            strSql.Append(" FROM ims_offstock ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperMySQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        #endregion

        #region 根据选择的主键值执行批量删除
        /// <summary>
        /// 根据选择的主键值执行批量删除
        /// </summary>
        /// <param name="keys">选择的主键字符串</param>
        /// <returns>是否删除成功</returns>
        public bool DeleteChooseByKeys(string keys)
        {
            string sql = "delete from ims_offstock where id in (" + keys + ")";   //执行批量删除语句, 主键为已选择的多个主键（用in的方式）

            if (DbHelperMySQL.ExecuteSql(sql) > 0)  //执行数据库删除操作
            {
                return true;
            }
            return false;   //没有删除成功，或者数据不存在
        }
        #endregion

        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整不用加where，如： "ID DESC"）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_offstock ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_offstock ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());
            if (ds.Tables.Count == 2 && ds.Tables[1].Rows.Count > 0)
            {
                recordCount = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            }
            else
            {
                recordCount = 0;
            }

            return ds;
        }

        /// <summary>
        /// 查询分页数据（重载方法，省略了排序字段orderString）
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public DataSet GetList(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetList(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }
        #endregion


        #region 查询分页数据
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderString">排序字段（写完整的，如：id desc）</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_offstock> GetListByPage(int pageIndex, int pageSize, string orderString, string strWhere, ref int recordCount)
        {
            //普通拼接SQL语句的方式
            int pageStart = (pageIndex - 1) * pageSize;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from ims_offstock ");

            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }
            if (orderString.Trim() != "")
            {
                strSql.Append(" order by ");
                strSql.Append(orderString);
            }
            strSql.Append(" limit ");
            strSql.Append(pageStart.ToString() + "," + pageSize.ToString());
            strSql.Append(";select count(1) from ims_offstock ");
            if (strWhere.Trim() != "")  //如果查询条件为空
            {
                strSql.Append(" where ");
                strSql.Append(strWhere);
            }

            DataSet ds = DbHelperMySQL.Query(strSql.ToString());

            IList<Model.ims_offstock> itemList = new List<Model.ims_offstock>();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].Rows[i];
                    Model.ims_offstock SingleItem = new Model.ims_offstock();
                    {
                        SingleItem.Id = int.Parse(dr["id"].ToString());

                        SingleItem.State = int.Parse(dr["state"].ToString());

                        SingleItem.Orderid = int.Parse(dr["orderid"].ToString());

                        SingleItem.Shopno = dr["shopno"].ToString();
                        SingleItem.Deviceid = int.Parse(dr["deviceid"].ToString());

                        SingleItem.Materialno = dr["materialno"].ToString();

                        SingleItem.Scandate = DateTime.Parse(dr["scandate"].ToString());
                        if (dr["exchangeno"].ToString() != "")
                        {
                            SingleItem.Exchangeno = dr["exchangeno"].ToString();
                        }
                        if (dr["weightdate"] != null && dr["weightdate"].ToString() != "")
                        {
                            SingleItem.Weightdate = DateTime.Parse(dr["weightdate"].ToString());
                        }
                        if (dr["offstockdate"] != null && dr["offstockdate"].ToString() != "")
                        {
                            SingleItem.Offstockdate = DateTime.Parse(dr["offstockdate"].ToString());
                        }
                        if (dr["orderdate"] != null && dr["orderdate"].ToString() != "")
                        {
                            SingleItem.Orderdate = DateTime.Parse(dr["orderdate"].ToString());
                        }
                        if (dr["boxnum"].ToString() != "")
                        {
                            SingleItem.Boxnum = int.Parse(dr["boxnum"].ToString());
                        }
                        if (dr["productnum"].ToString() != "")
                        {
                            SingleItem.Productnum = int.Parse(dr["productnum"].ToString());
                        }
                        if (dr["offsumweight"].ToString() != "")
                        {
                            SingleItem.Offsumweight = decimal.Parse(dr["offsumweight"].ToString());
                        }
                        SingleItem.Weight = decimal.Parse(dr["weight"].ToString());

                    };
                    itemList.Add(SingleItem);
                }
                recordCount = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                return itemList;
            }
            else
            {
                recordCount = 0;
                return null;
            }
        }
        #endregion

        #region 查询分页数据 （重载方法，省略了排序字段orderString）
        /// <summary>
        /// 查询分页数据
        /// </summary>
        /// <param name="pageIndex">当前页索引</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="strWhere">查询条件（不用加where）</param>
        /// <param name="recordCount">返回总的记录数</param>
        /// <returns>查询到的分页数据</returns>
        public IList<Model.ims_offstock> GetListByPage(int pageIndex, int pageSize, string strWhere, ref int recordCount)
        {
            return GetListByPage(pageIndex, pageSize, "id ASC", strWhere, ref recordCount);
        }
        #endregion


        /// <summary>
        /// 根据时间和店铺号码获取订单
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public DataSet GetListByShopNoAndDt(string strTime, string shopno)
        {
            DataSet ds = null;
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("select o.id,o.shopno,o.orderid,s.shopname,o.materialno,o.exchangeno,o.boxnum,p.productname,IFNULL(o.weight*o.productnum,0) as weight,o.productnum,p.spec,p.unit,if(o.state = 0 ,'未称重','已称重') as state from ims_offstock as o LEFT JOIN ");
            sbSql.Append("ims_dict_product as p ON o.materialno = p.materialNo LEFT JOIN ");
            sbSql.Append("ims_dict_shop as s ON o.shopno = s.shopno ");
            sbSql.AppendFormat("WHERE DATE_FORMAT(o.orderdate, '%Y-%m-%d') = '{0}' AND o.shopno = '{1}'",strTime,shopno);
            ds = DbHelperMySQL.Query(sbSql.ToString());
            return ds;
        }


        /// <summary>
        /// 根据时间和店铺号码获取订单
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public DataSet GetListByExchangeNoAndDt(string strTime,string shopno, string exchangeno)
        {
            DataSet ds = null;
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("select DISTINCT o.id,o.shopno,o.orderid,s.shopname,o.materialno,o.exchangeno,o.boxnum,o.offsumweight,p.productname,IFNULL(o.weight*o.productnum,0) as weight,o.productnum,p.spec,p.unit,if(o.state = 0 ,'未称重','已称重') as state from ims_offstock as o LEFT JOIN ");
            sbSql.Append("ims_dict_product as p ON o.materialno = p.materialNo LEFT JOIN ");
            sbSql.Append("ims_orders as od ON od.materialno = p.materialNo LEFT JOIN ");
            sbSql.Append("ims_dict_shop as s ON o.shopno = s.shopno ");
            sbSql.AppendFormat("WHERE DATE_FORMAT(o.orderdate, '%Y-%m-%d') = '{0}' AND o.shopno='{1}' AND od.exchangeno = '{2}' order by o.id asc", strTime,shopno, exchangeno);
            ds = DbHelperMySQL.Query(sbSql.ToString());
            return ds;
        }


        /// <summary>
        /// 获取订单重量,净重
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public decimal GetSumRealWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            string sql = string.Format("SELECT IFNULL(SUM(os.productnum * os.weight),0) AS sw FROM ims_offstock as os LEFT JOIN ims_dict_product as p ON p.materialNo = os.materialno LEFT JOIN ims_orders as od ON od.materialno = p.materialNo WHERE DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' AND os.shopno = '{1}' and od.exchangeno='{2}' ", strTime, shopno, exchangeno);
            DataSet ds = DbHelperMySQL.Query(sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString());
            }
            return 0.0M;
        }

        /// <summary>
        /// 获取毛重
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public decimal GetSumMaoWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            string sql = string.Format("SELECT IFNULL(SUM(os.productnum * (os.weight + p.packetweight)),0) AS sw FROM ims_offstock as os LEFT JOIN ims_dict_product as p ON p.materialNo = os.materialno LEFT JOIN ims_orders as od ON od.materialno = p.materialNo WHERE DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' AND os.shopno = '{1}' and od.exchangeno='{2}' ", strTime, shopno, exchangeno);
            DataSet ds = DbHelperMySQL.Query(sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString());
            }
            return 0.0M;
        }

        /// <summary>
        /// 获取包装重量
        /// </summary>
        /// <param name="strTime"></param>
        /// <param name="shopno"></param>
        /// <returns></returns>
        public decimal GetSumPacketWeightByShopNoAndDt(string strTime, string shopno, string exchangeno)
        {
            string sql = string.Format("SELECT IFNULL(SUM(os.productnum *  p.packetweight),0) AS sw FROM ims_offstock as os LEFT JOIN ims_dict_product as p ON p.materialNo = os.materialno LEFT JOIN ims_orders as od ON od.materialno = p.materialNo WHERE DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' AND os.shopno = '{1}' and od.exchangeno='{2}' ", strTime, shopno, exchangeno);
            DataSet ds = DbHelperMySQL.Query(sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString());
            }
            return 0.0M;
        }

        public DataSet GetProductSumWeightByDateTime(string strstart, string strend)
        {
            StringBuilder sbStr = new StringBuilder();
            sbStr.Append("SELECT p.productname,p.materialNo as materialno,SUM(os.weight*os.productnum) as weights FROM ims_offstock as os LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON os.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE os.state=2 and os.offstockdate >= '{0}' and os.offstockdate <= '{1}' ", strstart, strend);
            sbStr.Append("GROUP BY p.id ");
            return DbHelperMySQL.Query(sbStr.ToString());
        }

        public DataSet GetStockDiffSumWeightByDateTime(string strstart, string strend)
        {
            StringBuilder sbStr = new StringBuilder();
            sbStr.Append("SELECT p.productname,p.materialNo,SUM(os.weight*os.productnum) as weights FROM ims_offstock as os LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON ir.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE os.state=2 and os.offstockdate >= '{0}' and os.offstockdate <= '{1}' ", strstart, strend);
            sbStr.Append("GROUP BY p.id ");
            return DbHelperMySQL.Query(sbStr.ToString());
        }

        public DataSet GetAllShopsByLineAndTime(string strTime, string lineno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select o.shopno,s.shopname,'' as status,'' as statusno from ims_offstock as o ");
            strSql.Append("LEFT JOIN ims_dict_shop as s ON s.shopno = o.shopno ");
            strSql.Append("WHERE DATE_FORMAT(o.orderdate,'%Y-%m-%d') = '");
            strSql.Append(strTime);
            strSql.Append("'  and s.vehiclelineno = '");
            strSql.Append(lineno + "' ");
            strSql.Append("GROUP BY o.shopno  ");
            strSql.Append("order BY s.sort_id asc");
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public DataSet GetAllShopsByLineAndTimeDesc(string strTime, string lineno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select o.shopno,s.shopname,'' as status,'' as statusno from ims_offstock as o ");
            strSql.Append("LEFT JOIN ims_dict_shop as s ON s.shopno = o.shopno ");
            strSql.Append("WHERE DATE_FORMAT(o.orderdate,'%Y-%m-%d') = '");
            strSql.Append(strTime);
            strSql.Append("'  and s.vehiclelineno = '");
            strSql.Append(lineno + "' ");
            strSql.Append("GROUP BY o.shopno  ");
            strSql.Append("order BY s.sort_id desc");
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public DataSet GetAllExchangeNoByShopNoAndTime(string strTime, string shopno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select o.shopno,s.shopname,'' as status,'' as statusno,od.exchangeno from ims_offstock as o ");
            strSql.Append("LEFT JOIN ims_dict_shop as s ON s.shopno = o.shopno ");
            strSql.Append("LEFT JOIN ims_orders AS od ON o.shopno = od.shopno ");
            strSql.AppendFormat("WHERE DATE_FORMAT(o.orderdate,'%Y-%m-%d') = '{0}' ",strTime);
            strSql.AppendFormat("AND s.shopno='{0}' and NOT ISNULL(od.exchangeno) ", shopno);
            strSql.Append("GROUP BY od.exchangeno  ");
            strSql.Append("order BY od.exchangeno desc");
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public DataSet GetListByWhere(string shopno, string exchangeno, string strTime)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select o.* from ims_offstock as o left join ims_orders as od on o.shopno=od.shopno ");
            strSql.AppendFormat("Where o.shopno='{0}' and NOT ISNULL(od.exchangeno) ", shopno);
            strSql.AppendFormat("AND DATE_FORMAT(o.orderdate,'%Y-%m-%d') = '{0}' ", strTime);
            strSql.AppendFormat("AND od.exchangeno = '{0}' ", exchangeno);
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public DataSet GetListByWhere(string shopno, string exchangeno, string strTime,int state)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select o.* from ims_offstock as o left join ims_orders as od on o.shopno=od.shopno ");
            strSql.AppendFormat("Where o.shopno='{0}' and NOT ISNULL(od.exchangeno) ", shopno);
            strSql.AppendFormat("AND DATE_FORMAT(o.orderdate,'%Y-%m-%d') = '{0}' and o.state = {1} ", strTime,state);
            strSql.AppendFormat("AND od.exchangeno = '{0}' ", exchangeno);
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public DataSet CheckShopHasOffStockOrHasWeighted(string shopno, string exchangeno, string strTime, int state)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select o.* from ims_offstock as o left join ims_orders as od on o.shopno=od.shopno ");
            strSql.AppendFormat("Where o.shopno='{0}' and NOT ISNULL(od.exchangeno) ", shopno);
            strSql.AppendFormat("AND DATE_FORMAT(o.orderdate,'%Y-%m-%d') = '{0}' and o.state > {1} ", strTime, state);
            strSql.AppendFormat("AND od.exchangeno = '{0}' ", exchangeno);
            return DbHelperMySQL.Query(strSql.ToString());
        }

        public DataSet CheckShopOrderHasWeighted(string shopno, string exchangeno, string strTime, int state)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select o.* from ims_offstock as o left join ims_orders as od on o.shopno=od.shopno ");
            strSql.AppendFormat("Where o.shopno='{0}' and NOT ISNULL(od.exchangeno) ", shopno);
            strSql.AppendFormat("AND DATE_FORMAT(o.orderdate,'%Y-%m-%d') = '{0}' and o.state < {1} ", strTime, state);
            strSql.AppendFormat("AND od.exchangeno = '{0}' ", exchangeno);
            return DbHelperMySQL.Query(strSql.ToString());
        }


        public bool DeleteData(string strWhere)
        {
            if (string.IsNullOrEmpty(strWhere))
            {
                return false;
            }
            string sql = "delete from ims_offstock where " + strWhere;
            int nret =DbHelperMySQL.ExecuteSql(sql);
            return nret > 0;
        }

        public bool DeleteData(string shopno, string exchangeno, string strTime)
        {
            string sql = "delete o from ims_offstock as o left join ims_orders as od on o.materialno=od.materialno where ";
            sql += string.Format(" DATE_FORMAT(o.orderdate, '%Y-%m-%d') = '{0}' and o.shopno ='{1}' and od.exchangeno='{2}'",strTime,shopno,exchangeno);
            int nret = DbHelperMySQL.ExecuteSql(sql);
            return nret > 0;
        }

        public bool CheckHasOffStockByLineAndDt(string strTime, string lineno)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT COUNT(1) FROM ims_offstock as o WHERE ");
            sb.Append("o.shopno in ( SELECT s1.shopno from ( ");
            sb.AppendFormat("SELECT s2.shopno from ims_dict_shop as s2 WHERE s2.vehiclelineno = '{0}' ) AS s1) AND o.state < 2 ", lineno);
            sb.AppendFormat("AND DATE_FORMAT(o.orderdate, '%Y-%m-%d') = '{0}'", strTime);
            DataSet ds = DbHelperMySQL.Query(sb.ToString());

            if (ds != null && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) <= 0;
            }
            return false;
        }

        public DataSet QueryAllStocks(string strTime, string lineno)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT s.vehiclelineno,s.vehiclename,s.tel,s.shopname,s.shopno,a.materialno,a.weight,a.productnum,a.boxnum,a.productnum*a.weight as weights ");
            sb.Append("FROM ims_offstock AS a LEFT JOIN ims_dict_shop AS s ON a.shopno = s.shopno ");
            sb.AppendFormat("WHERE s.vehiclelineno = '{0}' AND DATE_FORMAT(a.orderdate, '%Y-%m-%d') = '{1}'", lineno,strTime);
            DataSet ds = DbHelperMySQL.Query(sb.ToString());
            return ds;
        }

        public int QueryOffStockNum(string strTime, string materialno, int state)
        {
            StringBuilder sbStr = new StringBuilder();
            sbStr.Append("SELECT sum(s.productnum) as num from ims_offstock as s LEFT JOIN ");
            sbStr.Append("ims_dict_product as p ON s.materialno = p.materialNo ");
            sbStr.AppendFormat("WHERE DATE_FORMAT(s.scandate,'%Y-%m-%d') = '{0}' and state = {1} and s.materialno = '{2}'", strTime, state,materialno);
            DataSet ds = DbHelperMySQL.Query(sbStr.ToString());
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0][0] != null && ds.Tables[0].Rows[0][0].ToString() != "")
                {
                    return int.Parse(ds.Tables[0].Rows[0][0].ToString());
                }
            }
            return 0;
        }
    }
}

