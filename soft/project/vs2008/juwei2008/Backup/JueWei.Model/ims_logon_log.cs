using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    [Serializable]
    public class ims_logon_log
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// �û�ID
        /// </summary>		
        private int _userid;
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        /// <summary>
        /// ��½�˺�
        /// </summary>		
        private string _logonname;
        public string Logonname
        {
            get { return _logonname; }
            set { _logonname = value; }
        }
        /// <summary>
        /// �û�ȫ��
        /// </summary>		
        private string _realname;
        public string Realname
        {
            get { return _realname; }
            set { _realname = value; }
        }
        /// <summary>
        /// ��½״̬����
        /// </summary>		
        private string _note;
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }
        public ims_logon_log() { }
        public ims_logon_log(int id, int userid, string logonname, string realname, string note)
        {
            _id = id;
            _userid = userid;
            _logonname = logonname;
            _realname = realname;
            _note = note;
        }

    }
}


