using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    [Serializable]
    public class ims_instock_record
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 对应的产品字典ID
        /// </summary>		
        private int _productid;
        public int Productid
        {
            get { return _productid; }
            set { _productid = value; }
        }
        /// <summary>
        /// 物料编码
        /// </summary>		
        private string _materialno;
        public string Materialno
        {
            get { return _materialno; }
            set { _materialno = value; }
        }
        /// <summary>
        /// 入库时间
        /// </summary>		
        private DateTime _createtime;
        public DateTime Createtime
        {
            get { return _createtime; }
            set { _createtime = value; }
        }
        /// <summary>
        /// 入库时规格
        /// </summary>		
        private string _spec;
        public string Spec
        {
            get { return _spec; }
            set { _spec = value; }
        }
        /// <summary>
        /// 入库时单位
        /// </summary>		
        private string _unit;
        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }
        /// <summary>
        /// 入库时重量
        /// </summary>		
        private decimal _weight;
        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        /// <summary>
        /// 入库时重量
        /// </summary>		
        private int _state = 0;
        public int State
        {
            get { return _state; }
            set { _state = value; }
        }

        public ims_instock_record() { }
        public ims_instock_record(int id, int productid, string materialno, DateTime createtime, string spec, string unit, decimal weight)
        {
            _id = id;
            _productid = productid;
            _materialno = materialno;
            _createtime = createtime;
            _spec = spec;
            _unit = unit;
            _weight = weight;
        }

    }
}


