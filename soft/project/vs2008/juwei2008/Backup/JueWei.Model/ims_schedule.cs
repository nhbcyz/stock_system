using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    public class ims_schedule
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 订单ID
        /// </summary>		
        private int _order_id;
        public int Order_id
        {
            get { return _order_id; }
            set { _order_id = value; }
        }
        /// <summary>
        /// 设备ID
        /// </summary>		
        private int _dev_id;
        public int Dev_id
        {
            get { return _dev_id; }
            set { _dev_id = value; }
        }
        /// <summary>
        /// 任务状态0未完成1已完成2已失效
        /// </summary>		
        private int _state;
        public int State
        {
            get { return _state; }
            set { _state = value; }
        }
        /// <summary>
        /// 任务创建时间
        /// </summary>		
        private DateTime _createdate;
        public DateTime Createdate
        {
            get { return _createdate; }
            set { _createdate = value; }
        }
        /// <summary>
        /// 任务过期时间
        /// </summary>		
        private DateTime _expiredate;
        public DateTime Expiredate
        {
            get { return _expiredate; }
            set { _expiredate = value; }
        }
        public ims_schedule() { }
        public ims_schedule(int id, int order_id, int dev_id, int state, DateTime createdate, DateTime expiredate)
        {
            _id = id;
            _order_id = order_id;
            _dev_id = dev_id;
            _state = state;
            _createdate = createdate;
            _expiredate = expiredate;
        }

    }
}


