using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    [Serializable]
    public class ims_orders
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 销售凭证号码
        /// </summary>		
        private int _orderno;
        public int Orderno
        {
            get { return _orderno; }
            set { _orderno = value; }
        }
        /// <summary>
        /// 订单描述
        /// </summary>		
        private string _orderexplain;
        public string Orderexplain
        {
            get { return _orderexplain; }
            set { _orderexplain = value; }
        }
        /// <summary>
        /// 凭证日期
        /// </summary>		
        private DateTime _verifydate;
        public DateTime Verifydate
        {
            get { return _verifydate; }
            set { _verifydate = value; }
        }
        /// <summary>
        /// 确认数量
        /// </summary>		
        private int _verifynum;
        public int Verifynum
        {
            get { return _verifynum; }
            set { _verifynum = value; }
        }
        /// <summary>
        /// 交货日期
        /// </summary>		
        private DateTime _exchangedate;
        public DateTime Exchangedate
        {
            get { return _exchangedate; }
            set { _exchangedate = value; }
        }
        /// <summary>
        /// 物料编号
        /// </summary>		
        private int _materialno;
        public int Materialno
        {
            get { return _materialno; }
            set { _materialno = value; }
        }
        /// <summary>
        /// 单位
        /// </summary>		
        private string _unit;
        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }
        /// <summary>
        /// 发货日期
        /// </summary>		
        private DateTime _expressdate;
        public DateTime Expressdate
        {
            get { return _expressdate; }
            set { _expressdate = value; }
        }
        /// <summary>
        /// 店铺地址编号
        /// </summary>		
        private string _shopaddrno;
        public string Shopaddrno
        {
            get { return _shopaddrno; }
            set { _shopaddrno = value; }
        }
        /// <summary>
        /// 店铺名字
        /// </summary>		
        private string _shopname;
        public string Shopname
        {
            get { return _shopname; }
            set { _shopname = value; }
        }
        /// <summary>
        /// 重量,单位为kg
        /// </summary>		
        private decimal _weight;
        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        /// <summary>
        /// 状态,0未入库,1已扫码,2已称重,3已出库
        /// </summary>		
        private int _state;
        public int State
        {
            get { return _state; }
            set { _state = value; }
        }
        /// <summary>
        /// 分配给的设备
        /// </summary>		
        private int _deviceid;
        public int Deviceid
        {
            get { return _deviceid; }
            set { _deviceid = value; }
        }
        /// <summary>
        /// 店铺编号
        /// </summary>		
        private string _shopno;
        public string Shopno
        {
            get { return _shopno; }
            set { _shopno = value; }
        }

        /// <summary>
        /// 已扫码数量
        /// </summary>
        private int _scannum;
        public int Scannum
        {
            get { return _scannum; }
            set { _scannum = value; }
        }

        /// <summary>
        /// 交货单号
        /// </summary>		
        private string _exchangeno;
        public string Exchangeno
        {
            get { return _exchangeno; }
            set { _exchangeno = value; }
        }

        /// <summary>
        /// 交货地址
        /// </summary>		
        private string _exchangeaddr;
        public string Exchangeaddr
        {
            get { return _exchangeaddr; }
            set { _exchangeaddr = value; }
        }

        /// <summary>
        /// 框个数
        /// </summary>
        private int _boxnum;
        public int Boxnum
        {
            get { return _boxnum; }
            set { _boxnum = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        private string _posnr;
        public string Posnr
        {
            get { return _posnr; }
            set { _posnr = value; }
        }

        public ims_orders() { }
        public ims_orders(int id, int orderno, string orderexplain, DateTime verifydate, int verifynum, 
            DateTime exchangedate, int materialno,int scannum,
            string unit, DateTime expressdate, string shopaddrno, string shopname, 
            decimal weight, int state, int deviceid, string shopno,string posnr)
        {
            _id = id;
            _orderno = orderno;
            _orderexplain = orderexplain;
            _verifydate = verifydate;
            _verifynum = verifynum;
            _exchangedate = exchangedate;
            _materialno = materialno;
            _unit = unit;
            _expressdate = expressdate;
            _shopaddrno = shopaddrno;
            _shopname = shopname;
            _weight = weight;
            _state = state;
            _deviceid = deviceid;
            _shopno = shopno;
            _scannum = scannum;
            _posnr = posnr;
        }

    }
}


