using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    [Serializable]
    public class ims_acl_function
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// pid
        /// </summary>		
        private int _pid;
        public int Pid
        {
            get { return _pid; }
            set { _pid = value; }
        }
        /// <summary>
        /// ��������
        /// </summary>		
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        /// <summary>
        /// �ؼ�ID
        /// </summary>		
        private string _controlid;
        public string Controlid
        {
            get { return _controlid; }
            set { _controlid = value; }
        }
        public ims_acl_function() { }
        public ims_acl_function(int id, int pid, string name, string controlid)
        {
            _id = id;
            _pid = pid;
            _name = name;
            _controlid = controlid;
        }

    }
}


