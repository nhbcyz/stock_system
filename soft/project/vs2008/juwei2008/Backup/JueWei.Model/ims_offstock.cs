using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    [Serializable]
    public class ims_offstock
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 订单编号
        /// </summary>		
        private int _orderid;
        public int Orderid
        {
            get { return _orderid; }
            set { _orderid = value; }
        }
        /// <summary>
        /// 店铺编号
        /// </summary>		
        private string _shopno;
        public string Shopno
        {
            get { return _shopno; }
            set { _shopno = value; }
        }

        /// <summary>
        /// 交货单号
        /// </summary>		
        private string _exchangeno;
        public string Exchangeno
        {
            get { return _exchangeno; }
            set { _exchangeno = value; }
        }


        /// <summary>
        /// 扫码的编号
        /// </summary>		
        private int _deviceid;
        public int Deviceid
        {
            get { return _deviceid; }
            set { _deviceid = value; }
        }
        /// <summary>
        /// materialno
        /// </summary>		
        private string _materialno;
        public string Materialno
        {
            get { return _materialno; }
            set { _materialno = value; }
        }
        /// <summary>
        /// 扫码入库日期
        /// </summary>		
        private DateTime _scandate;
        public DateTime Scandate
        {
            get { return _scandate; }
            set { _scandate = value; }
        }
        /// <summary>
        /// 称重的日期
        /// </summary>		
        private DateTime _weightdate;
        public DateTime Weightdate
        {
            get { return _weightdate; }
            set { _weightdate = value; }
        }
        /// <summary>
        /// 出库日期
        /// </summary>		
        private DateTime _offstockdate;
        public DateTime Offstockdate
        {
            get { return _offstockdate; }
            set { _offstockdate = value; }
        }

        /// <summary>
        /// 订单日期
        /// </summary>		
        private DateTime _orderdate;
        public DateTime Orderdate
        {
            get { return _orderdate; }
            set { _orderdate = value; }
        }

        /// <summary>
        /// 出库时重量
        /// </summary>		
        private decimal _weight;
        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        /// <summary>
        /// 0扫码1已称重2已出库
        /// </summary>		
        private int _state;
        public int State
        {
            get { return _state; }
            set { _state = value; }
        }

        private int _productnum;
        /// <summary>
        /// 产品个数
        /// </summary>
        public int Productnum
        {
            get { return _productnum; }
            set { _productnum = value; }
        }

        /// <summary>
        /// 框个数
        /// </summary>
        private int _boxnum;
        public int Boxnum
        {
            get { return _boxnum; }
            set { _boxnum = value; }
        }

        /// <summary>
        /// 出库门店订单总重量
        /// </summary>
        private decimal _offsumweight;
        public decimal Offsumweight
        {
            get { return _offsumweight; }
            set { _offsumweight = value; }
        }


        public ims_offstock() { }
        public ims_offstock(int id, int orderid, string shopno, int deviceid, string materialno, DateTime scandate, DateTime weightdate, DateTime offstockdate, decimal weight, int state,string exchangeno)
        {
            _id = id;
            _orderid = orderid;
            _shopno = shopno;
            _deviceid = deviceid;
            _materialno = materialno;
            _scandate = scandate;
            _weightdate = weightdate;
            _offstockdate = offstockdate;
            _weight = weight;
            _state = state;
            _exchangeno = exchangeno;
        }

    }
}


