﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
namespace JueWei.Model
{
    public class ims_dict_product
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 物料编号
        /// </summary>		
        private string _materialno;
        public string Materialno
        {
            get { return _materialno; }
            set { _materialno = value; }
        }
        /// <summary>
        /// 产品名字
        /// </summary>		
        private string _productname;
        public string Productname
        {
            get { return _productname; }
            set { _productname = value; }
        }
        /// <summary>
        /// 规格
        /// </summary>		
        private string _spec;
        public string Spec
        {
            get { return _spec; }
            set { _spec = value; }
        }
        /// <summary>
        /// 单品重量
        /// </summary>		
        private decimal _weight;
        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        /// <summary>
        /// 单位
        /// </summary>		
        private string _unit;
        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }
        /// <summary>
        /// 创建时间
        /// </summary>		
        private DateTime _createtime;
        public DateTime Createtime
        {
            get { return _createtime; }
            set { _createtime = value; }
        }
        /// <summary>
        /// 修改时间
        /// </summary>		
        private DateTime _modifytime;
        public DateTime Modifytime
        {
            get { return _modifytime; }
            set { _modifytime = value; }
        }
        /// <summary>
        /// 关联设备
        /// </summary>		
        private int _deviceid;
        public int Deviceid
        {
            get { return _deviceid; }
            set { _deviceid = value; }
        }

        /// <summary>
        /// 包装重量
        /// </summary>		
        private decimal _packetweight;
        public decimal Packetweight
        {
            get { return _packetweight; }
            set { _packetweight = value; }
        }

        /// <summary>
        /// 简写名字
        /// </summary>		
        private string _shortname;
        public string Shortname
        {
            get { return _shortname; }
            set { _shortname = value; }
        }

        public ims_dict_product() { }
        public ims_dict_product(int id, string materialno, string productname, string spec, decimal weight, string unit, DateTime createtime, DateTime modifytime, int deviceid)
        {
            _id = id;
            _materialno = materialno;
            _productname = productname;
            _spec = spec;
            _weight = weight;
            _unit = unit;
            _createtime = createtime;
            _modifytime = modifytime;
            _deviceid = deviceid;
        }

    }
}


