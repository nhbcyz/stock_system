using System;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Message event arguments for message events.
    /// </summary>
    public class MessageEventArgs : ConnectionEventArgs
    {

        #region Fields

        private byte[] FBuffer;

        private byte[] FMessageType;

        #endregion

        #region Constructor

        public MessageEventArgs(ISocketConnection connection, byte[] buffer)
            : base(connection)
        {
            FBuffer = buffer;
        }

        public MessageEventArgs(ISocketConnection connection, byte[] buffer,byte[] messageType)
            : base(connection)
        {
            FBuffer = buffer;
            FMessageType = messageType;
        }

        #endregion

        #region Properties

        public byte[] Buffer
        {
            get { return FBuffer; }
            internal set { FBuffer = value; }
        }

        public byte[] MessageType
        {
            get { return FMessageType; }
            internal set { FMessageType = value; }
        }

        #endregion

    }

}
