using System;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Base class for disposable objects.
    /// </summary>
    public abstract class BaseClass : IDisposable
    {

        #region Fields

        private bool FDisposed = false;

        //������
        public Object thislock = new Object();

        #endregion

        #region Methods

        /// <summary>
        /// This method is called when object is being disposed. Override this method to free resources.
        /// </summary>
        /// <param name="dispodedByUser">
        /// Indicates if was fired by user or GC.
        /// if disposedByUser = true you can dispose unmanaged and managed resources. if false, only unmanaged resources can be disposed.
        /// </param>
        protected virtual void Free(bool dispodedByUser)
        {
            //-----
        }

        /// <summary>
        /// Free the object.
        /// </summary>
        public void Dispose()
        {
            lock (this)
            {
                if (FDisposed == false)
                {
                    Free(true);
                    FDisposed = true;
                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Indicates is object is already disposed.
        /// </summary>
        protected bool Disposed
        {
            get
            {
              //  thislock = (Object)this;
                lock (this)
                {
                    return FDisposed;
                }
            }
        }

        #endregion

        #region Free

        /// <summary>
        /// Destructor. (Finalize)
        /// </summary>
        ~BaseClass()
        {
            Free(false);
        }

        #endregion

    }

}