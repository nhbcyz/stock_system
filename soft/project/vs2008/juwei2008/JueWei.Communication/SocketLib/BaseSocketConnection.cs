using System;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;
using System.Collections.Generic;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Base socket connection
    /// </summary>
    public abstract class BaseSocketConnection : BaseClass, ISocketConnection
    {

        #region Fields

        //----- Socket and Stream items!
        private Socket FSocket;
        private Stream FStream;

        //----- Connection Host!
        private BaseSocketConnectionHost FHost;

        private object FCustomData;
        private byte[] FHeader;
        private string FId;

        //----- Write items!
        private Queue<MessageBuffer> FWriteQueue;
        private bool FWriteQueueHasItems;

        //----- Read items!
        private object FSyncReadCount;
        private int FReadCount;
        private bool FReadCanEnqueue;

        private DateTime FLastAction;

        private ICryptoTransform FDecryptor;
        private ICryptoTransform FEncryptor;

        private EncryptType FEncryptType;
        private CompressionType FCompressionType;

        private ushort FSendSeq;//pzg 080702
        private Dictionary<int, byte[]> FBufferDataList;//pzg 091216

        #endregion

        #region Constructor

        internal BaseSocketConnection(BaseSocketConnectionHost host, Socket socket, EncryptType encryptType, CompressionType compressionType, byte[] header)
        {

            //----- Connection Id!
            FId = Guid.NewGuid().ToString();

            FSocket = socket;

            FEncryptType = encryptType;
            FCompressionType = compressionType;
            
            FWriteQueue = new Queue<MessageBuffer>();
            FWriteQueueHasItems = false;
            FReadCanEnqueue = true;

            FReadCount = 0;
            FSyncReadCount = new object();

            FHeader = header;
            FHost = host;

            FLastAction = DateTime.Now;

            FCustomData = null;
            FEncryptor = null;
            FDecryptor = null;

            FSendSeq = 0;//pzg 080702
            FBufferDataList=new Dictionary<int,byte[]>();//pzg 091216

        }

        #endregion

        #region Free

        protected override void Free(bool dispodedByUser)
        {

            if (dispodedByUser)
            {

                FWriteQueue.Clear();

                if (FStream != null)
                {
                    FStream.Close();
                    #if Desktop
                    FStream.Dispose();
                    #endif
                }

            }

            base.Free(dispodedByUser);

        }

        #endregion

        #region Properties

        internal Queue<MessageBuffer> WriteQueue
        {
            get { return FWriteQueue; }
        }

        internal bool WriteQueueHasItems
        {
            get { return FWriteQueueHasItems; }
            set { FWriteQueueHasItems = value; }
        }

        internal bool ReadCanEnqueue
        {
            get { return FReadCanEnqueue; }
            set { FReadCanEnqueue = value; }
        }

        internal int ReadCount
        {
            get { return FReadCount; }
            set { FReadCount = value; }
        }

        internal object SyncReadCount
        {
            get { return FSyncReadCount; }
        }

        internal bool Active
        {
            get { return !Disposed; }
        }

        internal ICryptoTransform Encryptor
        {
            get { return FEncryptor; }
            set { FEncryptor = value; }
        }

        internal ICryptoTransform Decryptor
        {
            get { return FDecryptor; }
            set { FDecryptor = value; }
        }

        internal Stream Stream
        {
            get { return FStream; }
            set { FStream = value; }
        }

        internal DateTime LastAction
        {
            get { return FLastAction; }
            set { FLastAction = value; }
        }

        internal Socket Socket
        {
            get { return FSocket; }
        }

        protected BaseSocketConnectionHost Host
        {
            get { return FHost; }
        }

        #endregion

        #region ISocketConnection Members

        #region Properties

        public object CustomData
        {
            get { return FCustomData; }
            set { FCustomData = value; }
        }

        //pzg 080702
        public ushort SendSeq
        {
            get { FSendSeq = (ushort)(FSendSeq + 1); if (FSendSeq > 65535) { FSendSeq = 1; } return FSendSeq; }
        }

        //pzg 091217
        public Dictionary<int, byte[]> BufferDataList
        {
            get { return FBufferDataList; }
            //set { FBufferDataList = value; }
        }

        public byte[] Header
        {
            get { return FHeader; }
            set { FHeader = value; }
        }

        public EncryptType EncryptType
        {
            get { return FEncryptType; }
        }

        public CompressionType CompressionType
        {
            get { return FCompressionType; }
        }

        public HostType HostType
        {
            get { return FHost.HostType; }
        }

        public IPEndPoint LocalEndPoint
        {
            get { return (IPEndPoint)FSocket.LocalEndPoint; }
        }

        public IPEndPoint RemoteEndPoint
        {
            get { return (IPEndPoint)FSocket.RemoteEndPoint; }
        }

        public IntPtr SocketHandle
        {
            get { return FSocket.Handle; }
        }

        public string ConnectionId
        {
            get { return FId; }
        }

        #endregion

        #region Abstract Methods

        public abstract IClientSocketConnection AsClientConnection();
        public abstract IServerSocketConnection AsServerConnection();

        #endregion

        #region Send Methods

        public void BeginSend(byte[] buffer)
        {
            FHost.BeginSend(this, buffer);
        }

        #endregion

        #region Receive Methods

        public void BeginReceive()
        {
            FHost.BeginReceive(this);
        }

        #endregion

        #region Disconnect Methods

        public void BeginDisconnect()
        {
            FHost.BeginDisconnect(this, null);
        }

        internal void BeginDisconnect(Exception ex)
        {
            FHost.BeginDisconnect(this, ex);
        }

        #endregion

        #endregion

    }

}
