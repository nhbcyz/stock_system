using System;
using System.Net.Sockets;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Server connection implementation.
    /// </summary>
    internal class ServerSocketConnection : BaseSocketConnection, IServerSocketConnection
    {

        #region Constructor

        internal ServerSocketConnection(BaseSocketConnectionHost host, Socket socket, EncryptType encryptType, CompressionType compressionType, byte[] header)
            : base(host, socket, encryptType, compressionType, header)
        {
            //-----
        }

        #endregion

        #region ISocketConnection Members

        public override IClientSocketConnection AsClientConnection()
        {
            return null;
        }

        public override IServerSocketConnection AsServerConnection()
        {
            return (this as IServerSocketConnection);
        }

        #endregion

        #region IServerSocketConnection Members

        public void BeginSendToAll(byte[] buffer)
        {
            Host.BeginSendToAll(this, buffer);
        }

        public void BeginSendTo(ISocketConnection connection, byte[] buffer)
        {
            Host.BeginSendTo((BaseSocketConnection)connection, buffer);
        }

        public ISocketConnection GetConnectionById(string connectionId)
        {
            return Host.GetConnectionById(connectionId);
        }

        #endregion

    }

}
