using System;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Base event arguments for connection events.
    /// </summary>
    public class ConnectionEventArgs : EventArgs
    {

        #region Fields

        private ISocketConnection FConnection;
        private object FCustomData;

        #endregion

        #region Constructor

        public ConnectionEventArgs(ISocketConnection connection)
        {
            FConnection = connection;
        }

        #endregion

        #region Properties

        public ISocketConnection Connection
        {
            get { return FConnection; }
        }

        internal object CustomData
        {
            get { return FCustomData; }
            set { FCustomData = value; }
        }

        #endregion

    }

}
