using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
//using System.Net.Security;
//using System.Security.Cryptography.X509Certificates;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Client socket creator.
    /// </summary>
    internal class SocketConnector : BaseSocketConnectionCreator
    {

        #region Fields

        private Socket FSocket;
        private IPEndPoint FRemoteEndPoint;

        private Timer FReconnectTimer;
        private int FReconnectAttempts;
        private int FReconnectAttemptInterval;

        private int FReconnectAttempted;

        private bool FIsReconnect = false;
        #endregion

        #region Constructor

        /// <summary>
        /// Base SocketConnector creator.
        /// </summary>
        /// <param name="host">
        /// Host.
        /// </param>
        /// <param name="remoteEndPoint">
        /// The remote endpoint to connect.
        /// </param>
        /// <param name="encryptType">
        /// Encrypt type.
        /// </param>
        /// <param name="compressionType">
        /// Compression type.
        /// </param>
        /// <param name="cryptoService">
        /// CryptoService. if null, will not be used.
        /// </param>
        /// <param name="localEndPoint">
        /// Local endpoint. if null, will be any address/port.
        /// </param>
        public SocketConnector(BaseSocketConnectionHost host, IPEndPoint remoteEndPoint, EncryptType encryptType, CompressionType compressionType, ICryptoService cryptoService, int reconnectAttempts, int reconnectAttemptInterval, IPEndPoint localEndPoint)
            : base(host, localEndPoint, encryptType, compressionType, cryptoService)
        {
            try
            {
                #if Desktop
                FReconnectTimer = new Timer(new TimerCallback(ReconnectSocketConnection));
                #else
                FReconnectTimer = new Timer(new TimerCallback(ReconnectSocketConnection), this, reconnectAttemptInterval, reconnectAttemptInterval); ;
                #endif

                FRemoteEndPoint = remoteEndPoint;
                FReconnectAttempts = reconnectAttempts;
                FReconnectAttemptInterval = reconnectAttemptInterval;

                FReconnectAttempted = -1;
            }
            catch(Exception ee)
            {
               // HZ3000.PublicFunction.Log.WriteLog("HZDrawPublish.SocketConnector", "WriteLog", ee.Message, HZ3000.PublicFunction.Log.LogLevel.LError);
            }
        }

        #endregion

        #region Free

        protected override void Free(bool dispodedByUser)
        {
            if (dispodedByUser)
            {
                if (FSocket != null)
                {
                    FSocket.Close();
                }
               
            }
            base.Free(dispodedByUser);
        }

        #endregion

        #region Methods

        #region Start

        internal override void Start()
        {
            if (!Disposed)
            {
                BeginConnect();
            }
        }

        #endregion

        #region BeginConnect

        /// <summary>
        /// Begin the connection with host.
        /// </summary>
        internal void BeginConnect()
        {
            try
            {
                FSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                FSocket.Bind(LocalEndPoint);
                FSocket.BeginConnect(FRemoteEndPoint, new AsyncCallback(BeginConnectCallback), this);
            }
            catch (Exception exOut)
            {
                Stop();
                Host.FireOnException(exOut);
                //HZ3000.PublicFunction.Log.WriteLog("HZDrawPublish", "WriteLog", exOut.Message, HZ3000.PublicFunction.Log.LogLevel.LError);
            }
        }

        #endregion

        #region BeginConnectCallback

        /// <summary>
        /// Connect callback!
        /// </summary>
        /// <param name="ar"></param>
        internal void BeginConnectCallback(IAsyncResult ar)
        {
            if (!Disposed)
            {
                BaseSocketConnection connection = null;
                SocketConnector connector = null;
                try
                {
                    connector = (SocketConnector)ar.AsyncState;
                    connector.Socket.EndConnect(ar);
                    //----- Adjust buffer size!
                    #if Desktop
                    connector.Socket.ReceiveBufferSize = Host.SocketBufferSize;
                    connector.Socket.SendBufferSize = Host.SocketBufferSize;
                    #endif
                    connection = new ClientSocketConnection(Host, this, connector.Socket, this.EncryptType, this.CompressionType, Host.Header);
                    //----- Initialize!
                    Host.AddSocketConnection(connection);
                    InitializeConnection(connection);
                   // Host.FireOnConnected(connection);
                }
                catch (Exception exOut)
                {
                    if (connection != null)
                    {
                        try
                        {
                            connection.BeginDisconnect(exOut);
                        }
                        catch (Exception exInn)
                        {
                            Host.FireOnException(exInn);
                        }
                    }
                    else
                    {                        
                        Host.FireOnException(exOut);
                        FReconnectAttempted++;
                        Reconnect(false);
                    }
                }
            }
        }

        #endregion

        #region Stop

        internal override void Stop()
        {
            Dispose();
        }

        #endregion

        #region Reconnect

        internal void Reconnect(bool resetAttempts)
        {
            
            if (!Disposed)
            {

                if (resetAttempts)
                {
                    FReconnectAttempted = 0;
                }

                if (FReconnectAttempts > 0)
                {

                    if (FReconnectAttempted < FReconnectAttempts)
                    {
                        FReconnectTimer.Change(FReconnectAttemptInterval, FReconnectAttemptInterval);
                    }
                    else
                    {
                        Stop();
                        Host.FireOnException(new ReconnectAttemptsException("Max reconnect attempts reached"));
                    }
                }
                else
                {
                    Stop();
                }
            }
        }

        #endregion

        #region ReconnectSocketConnection

        private void ReconnectSocketConnection(Object stateInfo)
        {
            try
            {
                lock (this.thislock)
                {
                    if (!FIsReconnect)
                    {
                        FIsReconnect = true;
                        FReconnectTimer.Change(Timeout.Infinite, Timeout.Infinite);
                        BeginConnect();
                    }
                }
            }
            catch(Exception ee)
            {
                //HZ3000.PublicFunction.Log.WriteLog("HZDrawPublish.ReconnectSocketConnection", "WriteLog", ee.Message, HZ3000.PublicFunction.Log.LogLevel.LError);
            }
        }

        #endregion

        #endregion

        #region Properties

        internal Socket Socket
        {
            get { return FSocket; }
        }

        #endregion

    }

}
