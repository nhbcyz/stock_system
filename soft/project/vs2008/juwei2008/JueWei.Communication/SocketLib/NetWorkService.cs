﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HZDrawPublish.Message;
using HZDrawPublish.Socket;
using HZDrawPublish.Service;
using XY.SystemEx.SocketsEx;

namespace HZDrawPublish.Socket
{
    public class NetWorkService
    {
      //  private static HZMessageBase FRetArr = null; //发送后得到返回结果,临时使用,后续应该删除
        private  HZMessageBase FRetArr = null; //发送后得到返回结果,临时使用,后续应该删除   用静态的根本没法做多线程，这里是我修改的

  //      public static HZMessageBase Send(WorkService service, byte[] buff, int TimeOuts = 10000)
        public HZMessageBase Send(WorkService service, byte[] buff, int TimeOuts)
        {
            try
            {
                FRetArr = null;
                SocketClientService scs = service.Client.SocketService as SocketClientService;

                scs.RegisteredService("SynchronousSendData", new OnEventMessage(SynchronousSendDataEvent));
                System.Threading.AutoResetEvent autoEvent = new System.Threading.AutoResetEvent(false);
                System.Threading.Timer aTimer = new System.Threading.Timer(new System.Threading.TimerCallback(CallbackCheckData), autoEvent, 0, 20);

                SocketClientService.SendDataByteStruct data;
                data.aTimer = aTimer;
                data.sendBuffer = buff;
                data.dueTime = 0;

                if (!scs.SendBuffer(data))
                {
                    return null;
                }

                if (!autoEvent.WaitOne(TimeOuts, false))
                {
                    aTimer.Dispose();
                }

                aTimer.Dispose();


                return FRetArr;
            }
            catch (Exception)
            {

                throw;
            }
        }

    //    private static void SynchronousSendDataEvent(HZMessageBase msgbase)
        private void SynchronousSendDataEvent(HZMessageBase msgbase)
        {
            try
            {
                FRetArr = msgbase;
            }
            catch
            {

            }
        }

       // private static void CallbackCheckData(Object stateInfo)
        private  void CallbackCheckData(Object stateInfo)
        {
            if (FRetArr != null)
            {
                System.Threading.AutoResetEvent autoEvent = (System.Threading.AutoResetEvent)stateInfo;
                autoEvent.Set();
            }
        }
    }
}
