using System;
using System.Net.Sockets;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Client socket connection implementation.
    /// </summary>
    internal class ClientSocketConnection : BaseSocketConnection, IClientSocketConnection
    {

        #region Fields

        private SocketConnector FCreator;

        #endregion

        #region Constructor

        internal ClientSocketConnection(BaseSocketConnectionHost host, BaseSocketConnectionCreator creator, Socket socket, EncryptType encryptType, CompressionType compressionType, byte[] header)
            : base(host, socket, encryptType, compressionType, header)
        {

            FCreator = creator as SocketConnector;
        }

        #endregion

        #region Properties

        internal SocketConnector Creator
        {
            get { return FCreator; }
        }

        #endregion

        #region ISocketConnection Members

        public override IClientSocketConnection AsClientConnection()
        {
            return (this as IClientSocketConnection);
        }

        public override IServerSocketConnection AsServerConnection()
        {
            return null;
        }

        #endregion

        #region IClientSocketConnection Members

        public void BeginReconnect()
        {
            Host.BeginReconnect(this);
        }

        #endregion

    }

}
