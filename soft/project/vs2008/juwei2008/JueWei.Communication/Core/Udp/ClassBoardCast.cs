﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;

//局域网广播类


namespace HZDrawPublish.Service
{
    public class ClassBoardCast
    {
        IPEndPoint bcIPEndPoint = null;
        UdpClient bcUdpClient = null;
        public ClassBoardCast(int port)
        {
            _UdpPort = port;
            bcIPEndPoint = new IPEndPoint(IPAddress.Broadcast, port);
            bcUdpClient = new UdpClient();
        }

        private int _UdpPort;

        public int UdpPort
        {
            get { return _UdpPort; }
            set { _UdpPort = value; }
        }

        //发送自己的信息到广播地址
        public void BoardCast()
        {
            byte[] buff = UdpInfo.SerializeInfo(EmInfoType.On, "");
            bcUdpClient.Send(buff, buff.Length, bcIPEndPoint);
        }

        //用户退出时，发送消息至广播地址
        public void UserQuit()
        {
            byte[] buff = UdpInfo.SerializeInfo(EmInfoType.Off, "");
            bcUdpClient.Send(buff, buff.Length, bcIPEndPoint);
        }

        //收到别人上线的通知时，回复对方，以便对方将自己加入在线用户列表
        public void BCReply(string ipReply)
        {
            byte[] buff = UdpInfo.SerializeInfo(EmInfoType.Reply, "");
            bcUdpClient.Send(buff, buff.Length, bcIPEndPoint);
        }

        #region 失效代码
        ///// <summary>
        ///// 用户锁定或者解锁，发送广播告之

        ///// </summary>
        ///// <param name="Lock_Flag">是否锁定</param>
        //public void Lock(SysStructClass ssc)
        //{
        //    GetLocalIP();
        //    string lockInfo = ":LOCK:" + ssc.IP + ":" + ssc.UnLock.ToString();
        //    byte[] bufLock = Encoding.Default.GetBytes(lockInfo);

        //    bcUdpClient.Send(bufLock, bufLock.Length, bcIPEndPoint);
        //} 
        #endregion
    }
}
