﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Sockets;
using System.Net;
using System.Collections.ObjectModel;
using System.Threading;
using HZDrawPublish.Message;
using HZDrawPublish.Core.Network;
using HZDrawPublish.Core.Udp;

namespace HZDrawPublish.Service
{
    // 定义 UdpState类

    public class UdpState
    {
        public UdpClient udpClient;
        public IPEndPoint ipEndPoint;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        public int counter = 0;
    }

    public class UdpModel
    {
        public MyUdpClient udpclient;
        public IPEndPoint localipe;
        public IPEndPoint remoteipe;
        public IPEndPoint broadcast;
    }

    public class UdpService
    {
        private bool _isStarted = false;
        private List<NetworkElement> FNodes = null;
        System.Threading.AutoResetEvent autoEvent = new System.Threading.AutoResetEvent(false);
        System.Threading.Timer FTimer = null;
        MyUdpClient FUdpClient = null;
        //private List<MyUdpClient> FUdpClients = null;
        private List<IPEndPoint> bcIPEndPoints = null;
        private List<UdpModel> UdpModels = null;
        public UdpService(int port)
        {
           // FUdpClient = new MyUdpClient();
            FNodes = new List<NetworkElement>();
            bcIPEndPoints = new List<IPEndPoint>();
            UdpModels = new List<UdpModel>();
            foreach (var item in NetUtility.GetNetInfo())
            {
                IPAddress temp = null;
                try
                {
                    temp = null;
                    if (IPAddress.TryParse(item.Ip.Split(':')[0],out temp))
                    {
                        if ((item.Ip != "0.0.0.0") && (NetUtility.IsIP(item.Ip)))
                        {
                            UdpModel um = new UdpModel();
                            IPEndPoint ip = new IPEndPoint(IPAddress.Parse(item.Ip), port);
                            bcIPEndPoints.Add(ip);
                            um.localipe = ip;        
                            MyUdpClient tempudp = new MyUdpClient(new IPEndPoint(IPAddress.Parse(item.Ip), 6520));//6524
                           // MyUdpClient tempudp = new MyUdpClient(new IPEndPoint(IPAddress.Parse(item.Ip), 0));
                            um.udpclient = tempudp;
                           // um.broadcast = new IPEndPoint(IPAddress.Parse(item.BroadCastAddr), 0);
                            UdpModels.Add(um);
                        }
                    }
                    else
                    {

                    }
                }
                catch (System.Exception ex)
                {
                    //Utilities.Utility.WriteLog("1在构造函数UdpService中" + ex.Message.ToString() + "\n");
                }
            }
            //bcIPEndPoint = new IPEndPoint(IPAddress.Broadcast, port);
            _isStarted = false;
        }

        /// <summary>
        /// 刷新发布网络节点
        /// </summary>
        /// <param name="TimeOuts">等待应答时间，默认是5s</param>
        public List<NetworkElement> RefreshNodes(int TimeOuts)
        {
            try
            {
                int port = 5000;
                UpdateNetwork(port);
                _isStarted = false;
             //   FNodes.Clear();
                FNodes = new List<NetworkElement>();
                FTimer = new System.Threading.Timer(new System.Threading.TimerCallback(CallbackCheckData), autoEvent, 0, 250);
                if (!autoEvent.WaitOne(TimeOuts, false))
                {
                    FTimer.Dispose();
                }
                FTimer.Dispose();
                return FNodes;
            }
            catch (Exception ex)
            {
                //Utilities.Utility.WriteLog("RefreshNodes:" + ex.Message.ToString());
            }

            return null;

        }
        /// <summary>
        /// 处理接收UDP广播报文
        /// </summary>
        /// <param name="stateInfo"></param>
        private void CallbackCheckData(Object stateInfo)
        {
            lock (this)
            {
                if (!_isStarted)
                {
                    BroadCast();
                    _isStarted = true;
                    return;
                }
                try
                {
                    foreach (var um in UdpModels)
                    {
                        try
                        {

                            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 0);
                            if (um.udpclient.Available <= 0) return;
                            if (um.udpclient.Client == null) return;
                            byte[] buff = um.udpclient.Receive(ref ipEndPoint);
                            string userInfo = Encoding.ASCII.GetString(buff);
                            string msgHead = userInfo.Substring(0, 6);//消息前6位为消息类型标识符

                            string msgBody = userInfo.Substring(6);//第7位开始为消息实体内容
                            UdpMessage msg = new UdpMessage();
                            if (msg.Parse(buff))
                            {
                               // Utilities.Utility.WriteLog("收到UDP回应包:" + Utilities.Utility.byteToHexStr(buff) + "\n");
                                if (msg.Head == ":REPY:")
                                {
                                    if (msg.IpList.Count > 0)
                                    {
                                        foreach (string item in msg.IpList)
                                        {
                                            bool bExists = false;
                                            foreach (NetworkElement temp in FNodes)//检测IP是否已存在，如果已存在，没必要再添加
                                            {
                                                if (temp.IP == item)
                                                {
                                                    bExists = true;
                                                    break;
                                                }
                                            }
                                            if (!bExists)//如果是没有添加过的IP
                                            {
                                                NetworkElement ssc = new NetworkElement(msg.ComputerName, item, msg.Os);
                                                FNodes.Add(ssc);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Utilities.Utility.WriteLog("CallbackCheckData" + ex.Message.ToString());
                        }
                    }
   
                }
                catch(Exception ex)
                {
                   // Utilities.Utility.WriteLog(ex.Message.ToString());
                }
            }
        }
        private void BroadCast()
        {
            //byte[] buff = UdpInfo.SerializeInfo(EmInfoType.On, Utilities.Utility.OsInfo);

            //foreach (var item in UdpModels)
            //{
            //    try
            //    {
            //        item.udpclient.Send(buff, buff.Length, new IPEndPoint(IPAddress.Broadcast,10301));
            //        Thread.Sleep(40);
            //    }
            //    catch (System.Exception ex)
            //    {
                	
            //    }
            //}
            
        }

        private bool bHasUpdateNetwork = false;
        private void UpdateNetwork(int port)
        {            

            if (bHasUpdateNetwork)
                return;
            else
                bHasUpdateNetwork = true;
            FNodes = new List<NetworkElement>();
            bcIPEndPoints = new List<IPEndPoint>();
            UdpModels = new List<UdpModel>();
            foreach (var item in NetUtility.GetNetInfo())
            {
                IPAddress temp = null;
                try
                {
                    temp = null;
                    if (IPAddress.TryParse(item.Ip.Split(':')[0], out temp))
                    {
                        if ((item.Ip != "0.0.0.0") && (NetUtility.IsIP(item.Ip)))
                        {
                            UdpModel um = new UdpModel();
                            IPEndPoint ip = new IPEndPoint(IPAddress.Parse(item.Ip), port);
                            bcIPEndPoints.Add(ip);
                            um.localipe = ip;
                            MyUdpClient tempudp = new MyUdpClient(new IPEndPoint(IPAddress.Parse(item.Ip), 6524));
                            // MyUdpClient tempudp = new MyUdpClient(new IPEndPoint(IPAddress.Parse(item.Ip), 0));
                            um.udpclient = tempudp;
                            // um.broadcast = new IPEndPoint(IPAddress.Parse(item.BroadCastAddr), 0);
                            UdpModels.Add(um);
                        }
                    }
                    else
                    {

                    }
                }
                catch (System.Exception ex)
                {
                    //Utilities.Utility.WriteLog("在UpdateNetwork中" + ex.Message.ToString() + "\n");
                }
            }
        }

        ~UdpService()
        {
            foreach (var item in UdpModels)
            {
                if (item.udpclient != null)
                {
                    item.udpclient.Close();
                }  
            }
            if (FUdpClient != null)
            {
                FUdpClient.Close();
            }
            
        }

        public void Dispose()
        {
            foreach (var item in UdpModels)
            {
                if (item.udpclient != null)
                {
                    item.udpclient.Close();
                }             
            }
            if (FUdpClient != null)
            {
                FUdpClient.Close();
            }
        }
    }
}
