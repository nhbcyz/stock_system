﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HZDrawPublish.Service;

namespace HZDrawPublish.Core
{
    /// <summary>
    /// 数据传输
    /// </summary>
    public class TransContext
    {
        private DataTranService FService;
        private WorkService FWorker = null;

        /// <summary>
        /// 数据传输上下文构造类,根据业务类型来选择相应的策略,保证能调用到正确的业务处理逻辑
        /// </summary>
        /// <param name="bussiness">业务类型</param>
        /// <param name="elment">网络传输对象,包括IP等相关信息</param>
        public TransContext(EmBussiness bussiness, NetworkElement elment)
        {
            DataTranService service = null;
            int port = 51302;
            if (FWorker == null)
            {
                FWorker = new WorkService(elment, port);           
            }
            
            switch (bussiness)
            {
                case EmBussiness.SendFile:
                    service = new SendFileService();
                    break;
                case EmBussiness.ReqFile:
                    service = new ReqFileService();
                    break;
                case EmBussiness.SetType:
                    service = new SetTypeService();
                    break;
                case EmBussiness.ReqType:
                    service = new ReqTypeService();
                    break;
                default:
                    break;
            }
            FService = service;

        }
        
        public EmOperationResult GetResult(ITrans trans)
        {
            return FService.Process(trans, FWorker);
            //return EmOperationResult.Success;
        }

        /// <summary>
        /// 获得文件发布进度
        /// </summary>
        /// <returns></returns>
        public double GetPercentage()
        {
            return ((SendFileService)FService).Percentage;
        }

        /// <summary>
        /// 获取是否发送完毕
        /// </summary>
        /// <returns></returns>
        public bool GetSendOver()
        {
            return ((SendFileService)FService).bSendOver;
        }

        /// <summary>
        /// 获取发布结果
        /// </summary>
        /// <returns></returns>
        public bool  GetPublishOver(out EmOperationResult result)
        {
            result = ((SendFileService)FService).PublishResult;
            if (result == EmOperationResult.None)
                return false;
            else
                return true;
        }

        /// <summary>
        /// 获得心跳帧的状态
        /// </summary>
        /// <returns></returns>
        public int GetHeartBeatState()
        {
            return ((SendFileService)FService).nHeartBeatState;
        }

        /// <summary>
        /// 获得心跳信息列表
        /// </summary>
        /// <returns></returns>
        public List<HeartBeatInfo> GetHeartBeatInfoList()
        {
            List<HeartBeatInfo> list = new List<HeartBeatInfo>();
            int nCount = ((SendFileService)FService).HeartBeatInfoList.Count;
            for (int i = 0; i < nCount;i++ )
            {
                list.Add(((SendFileService)FService).HeartBeatInfoList[i].Clone());
            }
            ((SendFileService)FService).HeartBeatInfoList.RemoveRange(0,nCount);

            return list;
        }
    }
}
