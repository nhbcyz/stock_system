﻿using System.Collections.Generic;

namespace HZDrawPublish.Core
{
    public interface ITrans
    {
    }

    public interface ITransFile : ITrans
    {
        List<string> Files { get; set; }
    }

    public interface ISetType :ITrans
    {
        List<EmMechine> Mechines { get; set; }
    }



    public class TransFileModel : ITransFile
    {
        public TransFileModel(List<string> files)
        {
            this.Files = files;
        }
        private List<string> _files;
        public List<string> Files
        {
            get
            {
                return _files;
            }
            set
            {
                _files = value;
            }
        }
    }

    public class ServerTypeModel : ISetType
    {
        public ServerTypeModel(List<EmMechine> mechines)
        {
            _mechines = mechines;
        }
        private List<EmMechine> _mechines;
        public List<EmMechine> Mechines
        {
            get
            {
                return _mechines;
            }
            set
            {
                _mechines = value;
            }
        }
    }


}
