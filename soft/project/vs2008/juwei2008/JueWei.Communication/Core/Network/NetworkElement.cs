﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HZDrawPublish.Service
{
    public partial class NetworkElement
    {
        /// <summary>
        /// 计算机名字
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// 附加信息
        /// </summary>
        public string AddInfo { get; set; }


        public override string ToString()
        {
            return Name;
        }

        public NetworkElement()
        {

        }
        public NetworkElement(string name, string ip, string addinfo)
        {
            this.Name = name;
            this.IP = ip;
            this.AddInfo = addinfo;
        }
    }
}
