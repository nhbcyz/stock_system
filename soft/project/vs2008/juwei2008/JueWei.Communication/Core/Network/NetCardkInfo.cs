﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Net;
using System.Text.RegularExpressions;

namespace HZDrawPublish.Core.Network
{
    internal class NetCardInfo
    {
        public string Name { get; set; }

        public string Ip { get; set; }

        public string SubNet { get; set; }

        public string BroadCastAddr { get; set; }

    }

    internal class NetUtility
    {
        public static List<NetCardInfo> GetNetInfo()
        {
            List<NetCardInfo> nets = new List<NetCardInfo>();

            try
            {
                //本地计算机上的网络接口的对象,我的电脑里面以太网网络连接有两个虚拟机的接口和一个本地接口
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in nics)
                {      
                    bool Pd1 = (adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet); //判断是否是以太网连接
                    
                    if (adapter.OperationalStatus == OperationalStatus.Up)
                    {
                        IPInterfaceProperties ip = adapter.GetIPProperties();     //IP配置信息                       
                        if (ip.UnicastAddresses.Count > 0)
                        {
                            foreach (var item in ip.UnicastAddresses)
                            {
                                NetCardInfo cardinfo = new NetCardInfo();
                                cardinfo.Name = adapter.Name;
                                cardinfo.Ip = item.Address.ToString();
                                if (item.IPv4Mask != null)
                                {
                                    cardinfo.SubNet = item.IPv4Mask.ToString();
                                } 
                                IPAddress temp = null;
                                try
                                {

                                    if (IPAddress.TryParse(item.Address.ToString().Split(':')[0], out temp))
                                    {
                                        nets.Add(cardinfo);
                                        //cardinfo.BroadCastAddr = ComputeBroadCastAddr(item.Address.ToString(), cardinfo.SubNet);
                                    }
                                }
                                catch
                                {

                                }
                                
                            }
                        }
                        else
                        {//如果没有IP地址,则不加入到队列中去
                            continue;
                        }
                    }
                }
            }
            catch
            {

            }
            return nets;
        }

        /// <summary>
        /// 根据IP地址和子网掩码计算广播地址
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="subnet"></param>
        /// <returns></returns>
        private static string ComputeBroadCastAddr(string ipAddress, string subnetMask)
        {
            try
            {
                byte[] ip = IPAddress.Parse(ipAddress).GetAddressBytes();
                byte[] sub = IPAddress.Parse(subnetMask).GetAddressBytes();

                // 广播地址=子网按位求反 再 或IP地址
                for (int i = 0; i < ip.Length; i++)
                {
                    ip[i] = (byte)((~sub[i]) | ip[i]);
                }
                return new IPAddress(ip).ToString();
            }
            catch (System.Exception ex)
            {
                return "255.255.255.255";
            }

        }


        public static bool IsIP(string ip)
        {
            //判断是否为IP
            return Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }
    }
}
