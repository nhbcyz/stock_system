﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

//user define dll

using XY.SystemEx.SocketsEx;
using HZDrawPublish.Message;
using System.Runtime.InteropServices;
using HZDrawPublish.Core;
using HZDrawPublish.Utilities;

namespace HZDrawPublish.Service
{

    #region Delegates

    public delegate void OnEventDelegate(string eventMessage);
    public delegate void OnEventMessage(HZMessageBase Msg);
    //public delegate void OnEventDelegate(string eventMessage, ConnectionEventArgs ce);

    #endregion


    public class SocketClientService : BaseSocketService
    {
        #region Const Var
        public const string TIMEOUT_ERROR = "###TIMEOUT_ERROR###";
        public const string SERVICE_ERROR = "###SERVICE_ERROR###";
        private const int HEART_TIMEOUT = 1000 * 60;
        #endregion

        //临时保存buffer
        byte[] tempbuffer;
        //上次接收数据的长度
        int lastlength =0;
        public struct SendDataStringStruct
        {
            public string servName;
            public ushort encoderType;
            public string sendStr;
            public ushort errorNo;
            public System.Threading.Timer aTimer;
            public int dueTime;
        }

        public struct SendDataByteStruct
        {
            public byte[] sendBuffer;
            public System.Threading.Timer aTimer;
            public int dueTime;
        }

        public struct receDataByteStruct
        {
            public string servName;
            public ushort dataLen;
            public ushort encoderType;
            public ushort errorNo;
            public byte[] receBuffer;
        }

        class regServiceInfo
        {
            public OnEventMessage serviceEvent;
            //private byte[] FReceData;//接收的数据

            public regServiceInfo(OnEventMessage value)
            {
                serviceEvent = value;
            }

            private static byte[] EncryptDecryptData(byte[] data, byte[] key)
            {
                int keyLen = key.Length;
                for (int i = 0; i < data.Length; i++)
                {
                    data[i] = (byte)(data[i] ^ key[i % keyLen]);
                }
                return data;
            }

            public void receDataFunc(HZMessageBase msg)
            {
                if (serviceEvent != null)
                {
                    serviceEvent.Invoke(msg);//处理数据
                }
            }
        }

        private OnEventDelegate FOnEventDelegate;
        private Dictionary<string, regServiceInfo> FRegDelegate;
        private SocketClient FSocketClient;

        private bool FisConnSocketTimeOut;//等待连接时标识

        public bool isReConnection = true;//是否断开后重新连接
        System.Threading.Timer timerHeart = null;

        System.Net.IPEndPoint FRemoteEndPoint;
        EncryptType FEncryptType;
        CompressionType FCompressionType;

        object thislock = new object();

        public SocketClient SocketClient
        {
            get { return FSocketClient; }
            set { FSocketClient = value; }
        }


        public SocketClientService()
        {
            FisConnSocketTimeOut = false;
            FOnEventDelegate = null;
            if (FRegDelegate == null)
                FRegDelegate = new Dictionary<string, regServiceInfo>();
        }

        public SocketClientService(OnEventDelegate eventDelegate)
        {
            FisConnSocketTimeOut = false;
            FOnEventDelegate = eventDelegate;
            if (FRegDelegate == null)
                FRegDelegate = new Dictionary<string, regServiceInfo>();

            timerHeart = new System.Threading.Timer(new System.Threading.TimerCallback(CallbackHeart), this, HEART_TIMEOUT, System.Threading.Timeout.Infinite);
        }

        private void initService()
        {
            lock (FRegDelegate)
            {
                foreach (DictionaryEntry entry in (IDictionary)FRegDelegate)
                {
                    regServiceInfo tmp = (regServiceInfo)entry.Value;
                }
            }
        }

        /*注册服务与处理这个服务的委托*/
        public bool RegisteredService(string serviceName, OnEventMessage eventDelegate)
        {
            try
            {
                if (!FRegDelegate.ContainsKey(serviceName))
                {
                    regServiceInfo regService = new regServiceInfo(eventDelegate);
                    lock (FRegDelegate)
                    {
                        FRegDelegate[serviceName] = regService;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void CallbackHeart(Object stateInfo)
        {
        }

        private void CallbackSocketConn(Object stateInfo)
        {
            FisConnSocketTimeOut = true;
        }

        /*等待连接*/
        public bool waitConnection(int dueTime)
        {
            try
            {
                FisConnSocketTimeOut = false;
                System.Threading.Timer aTimer = new System.Threading.Timer(new System.Threading.TimerCallback(CallbackSocketConn), this, dueTime, System.Threading.Timeout.Infinite);

                while ((FSocketClient.GetSocketConnections().Length == 0) && (!FisConnSocketTimeOut))
                    System.Windows.Forms.Application.DoEvents();
                aTimer.Dispose();
                if (FisConnSocketTimeOut && (FSocketClient.GetSocketConnections().Length == 0))
                {
                    //   System.Windows.Forms.MessageBox.Show("连接超时，请检查网络连接！", "提示信息");
                   // HZ3000.PublicFunction.Log.WriteLog("HZDrawPublish", "WriteLog", "连接超时，请检查网络连接！  ", HZ3000.PublicFunction.Log.LogLevel.LError);
                    return false;
                }
            }
            catch(Exception ee)
            {
               // HZ3000.PublicFunction.Log.WriteLog("HZDrawPublish", "WriteLog", "连接超时，请检查网络连接！  "+ee.Message, HZ3000.PublicFunction.Log.LogLevel.LError);
                return false;
            }
            return true;
        }

        private void Event(string eventMessage)
        {
            if (FOnEventDelegate != null)
            {
                FOnEventDelegate.Invoke(eventMessage);
            }
        }

        private void EventMessage(HZFileMessage.PacketBody msg)
        {

            try
            {
               // ((regServiceInfo)(this.FRegDelegate[msg.Command])).serviceEvent.Invoke(msg);
            }
            catch
            {
                //System.Windows.Forms.MessageBox.Show("没有注册服务：" + msg.CommandName, "提示信息");
            }

        }

        public override void OnConnected(ConnectionEventArgs e)
        {
            FRemoteEndPoint = e.Connection.RemoteEndPoint;
            this.FCompressionType = e.Connection.CompressionType;
            this.FEncryptType = e.Connection.EncryptType;
            Event("建立连接 - " + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")");
        }
        public override void OnSent(MessageEventArgs e)
        {
            Event("发送消息 - " + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")");
            ProSendPackag(e.Buffer, e);
          //  Event("传输数据 - " + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ") ：" + DateTime.Now.ToString(), e);
        }
        private void ProSendPackag(byte[] bufferTmp, MessageEventArgs e)
        {
            if (bufferTmp.Length < 96)
            {
               // Event("发送数据--数据长度不够" + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")", e);
                return;
            }
        }
        private void ProSendRepeatData(byte[] pBuff, MessageEventArgs e)
        {
            int index = GetThreadIndex(e);
            string content;
            content = Encoding.Default.GetString(pBuff);
            //Event("第" + index.ToString() + "号线程发送数据:" + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")" + content + DateTime.Now.ToString() + "\n", e);
        }
        private void ProSendFileData(byte[] pBuff, MessageEventArgs e)
        {
            int index = GetThreadIndex(e);
           // Event("第" + index.ToString() + "号线程准备发送文件:" + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")" + DateTime.Now.ToString() + "\n", e);
        }
        private void ProRequestFile(byte[] pBuff, MessageEventArgs e)
        {
            int index = GetThreadIndex(e);
           // Event("第" + index.ToString() + "号线程请求文件:" + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")" + DateTime.Now.ToString() + "\n", e);
        }
        public override void OnReceived(MessageEventArgs e)
        {
            try
            {
                if (tempbuffer == null)//缓存不存在，则认为是新的数据包
                {
                    progresspackag(e.Buffer, e);
                }
                else //缓存已存在
                {
                    if (e.Buffer.Length + tempbuffer.Length  < (lastlength +4)) //还是未接收完数据
                    {
                        byte[] temp = new byte[tempbuffer.Length + e.Buffer.Length];
                        Array.Copy(tempbuffer, 0, temp, 0, tempbuffer.Length);
                        Array.Copy(e.Buffer, 0, temp, tempbuffer.Length, e.Buffer.Length);
                        tempbuffer = new byte[tempbuffer.Length + e.Buffer.Length];
                        Array.Copy(temp, tempbuffer, temp.Length);
                    }
                    else if (e.Buffer.Length + tempbuffer.Length == (lastlength + 4)) //数据接收完成
                    {
                        byte[] temp = new byte[lastlength +4];
                        Array.Copy(tempbuffer, 0, temp, 0, tempbuffer.Length);
                        Array.Copy(e.Buffer, 0, temp, tempbuffer.Length, e.Buffer.Length);
                        progresspackag(temp, e);
                    }
                    else //数据接收出错了
                    {
                        //暂时把最新的包发出去
                        progresspackag(e.Buffer,e);
                    }
                }
            }
            catch
            {
                progresspackag(e.Buffer, e);
            }

        }

        public override void OnDisconnected(DisconnectedEventArgs e)
        {
            Event("断开连接");
            if (isReConnection)
            {
                initService();
                //this.FSocketClient.AddConnector(FRemoteEndPoint,
                //        FEncryptType, FCompressionType, null);
            }
        }

        private void progresspackag(byte[] bufferTmp, MessageEventArgs e)
        {
            HZMessageBase msgbase = new HZMessageBase();
            if (!msgbase.XY_ParseMsgPacketHead(ref bufferTmp))
            {
                return;
            }
            //string strhex = Utility.byteToHexStr(bufferTmp);
            try
            {
                //if (FRegDelegate.ContainsKey(msgbase.CommandName))
                //    ((regServiceInfo)(this.FRegDelegate[msgbase.CommandName])).receDataFunc(msgbase);
                //else if (msgbase.CommandName.ToLower() == "serprocvoice")
                //    ((regServiceInfo)(this.FRegDelegate["serprocvoice"])).receDataFunc(msgbase);
                //else if (msgbase.CommandName.ToLower() == "serstopvoice")
                //    ((regServiceInfo)(this.FRegDelegate["serstopvoice"])).receDataFunc(msgbase);
                //else
                //{
                //    lastlength = 0;
                //    tempbuffer = null;
                //if (msgbase.Command == EmHzFileCommand.SendFileSilceSrv || msgbase.Command == EmHzFileCommand.SendFileOverviewSrv)
                //{
                //    ((regServiceInfo)(this.FRegDelegate["ReceiveFile"])).receDataFunc(msgbase);
                //}
                //else
                {
                    ((regServiceInfo)(this.FRegDelegate["SynchronousSendData"])).receDataFunc(msgbase);
                }    
                //}
            }
            catch
            {
                throw new RegisterServiceError();
            }
        }

        private int GetThreadIndex(MessageEventArgs e)
        {
            int index = 0;
            return index;
        }

        private void ProRepeatData(byte[] pBuff, MessageEventArgs e)
        {
            int index = GetThreadIndex(e);
            string content;
            content = Encoding.Default.GetString(pBuff);
            //Event("第" + index.ToString() + "号线程接收数据:" + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")" + content + DateTime.Now.ToString() + "\n", e);
        }

        private void ProFileData(byte[] pBuff, MessageEventArgs e)
        {
            int index = GetThreadIndex(e);
            string content = Encoding.Default.GetString(pBuff);
            //if (content.Contains("OK"))
            //   // Event("第" + index.ToString() + "号线程:发送文件成功" + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")" + DateTime.Now.ToString() + "\n", e);
            //else
            //{
            //    //Event("第" + index.ToString() + "号线程:发送文件失败" + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")" + DateTime.Now.ToString() + "\n", e);
            //}
        }

        private void ProSendFileAck(byte[] pBuff, MessageEventArgs e)
        {
            int index = GetThreadIndex(e);
           // Event("第" + index.ToString() + "号线程接收文件成功:" + e.Connection.RemoteEndPoint.Address.ToString() + "(" + e.Connection.RemoteEndPoint.Port.ToString() + ")" + DateTime.Now.ToString() + "\n", e);      
        }

        public bool SendBuffer(SendDataByteStruct ByteData)
        {
            BaseSocketConnection[] bsc = FSocketClient.GetSocketConnections();

            if (bsc.Length == 0)
            {
                initService();
                this.FSocketClient.AddConnector(FRemoteEndPoint, FEncryptType, FCompressionType, null);

                if (ByteData.aTimer != null)
                {
                    ByteData.aTimer.Change(ByteData.dueTime + 30, System.Threading.Timeout.Infinite);
                }
                waitConnection(1500);

                bsc = FSocketClient.GetSocketConnections();
                
                if (bsc.Length == 0)
                {
                    if (ByteData.aTimer != null)
                    {
                        ByteData.aTimer.Change(1000, System.Threading.Timeout.Infinite);//如果还没有建立，则取消等待
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }

            ISocketConnection Connection = bsc[bsc.Length - 1];
            while (Connection == null)
            {
                ArrayList ar = new ArrayList(bsc);
                ar.RemoveAt(bsc.Length - 1);
                bsc = (BaseSocketConnection[])ar.ToArray(typeof(BaseSocketConnection));
                if (bsc.Length == 0)
                {
                    initService();
                    this.FSocketClient.AddConnector(FRemoteEndPoint, FEncryptType, FCompressionType, null);

                    if (ByteData.aTimer != null)
                    {
                        ByteData.aTimer.Change(ByteData.dueTime + 30, System.Threading.Timeout.Infinite);
                    }
                    waitConnection(1500);

                    bsc = FSocketClient.GetSocketConnections();

                    if (bsc.Length == 0)
                    {
                        if (ByteData.aTimer != null)
                        {
                            ByteData.aTimer.Change(1000, System.Threading.Timeout.Infinite);//如果还没有建立，则取消等待
                        }
                        return false;
                    }
                    else
                    {
                        return false;
                    }
                }

                Connection = bsc[bsc.Length - 1];
            }
            try
            {
                if (Connection.RemoteEndPoint == null)
                {
                    return false;
                }
                Connection.BeginSend(ByteData.sendBuffer);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private int GetPacketLength(byte[] arr)
        {
            int length = 0;
            byte[] temp = new byte[4];
            try
            {
                if (arr != null)
                {
                    Array.Copy(arr, temp, 4);
                    length = temp[0] + (temp[1] << 8) + (temp[2] << 16) + (temp[3] << 32);
                    BitConverter.ToInt32(temp, 0);
                }
            }
            catch
            {
                return 0;
            }
            return length;
        }
    }
}
