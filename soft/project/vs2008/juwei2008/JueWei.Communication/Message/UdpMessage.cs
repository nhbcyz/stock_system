﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HZDrawPublish.Message
{
    public class UdpMessage
    {
        private string _Head;

        public string Head
        {
            get { return _Head; }
            set { _Head = value; }
        }

        private string _ComputerName;

        public string ComputerName
        {
            get { return _ComputerName; }
            set { _ComputerName = value; }
        }

        private List<string> _IpList;

        public List<string> IpList
        {
            get { return _IpList; }
            set { _IpList = value; }
        }

        private string _Os;

        public string Os
        {
            get { return _Os; }
            set { _Os = value; }
        }

        public UdpMessage()
        {

        }

        public bool Parse(byte[] buff)
        {
            try
            {
                string userInfo = Encoding.ASCII.GetString(buff);
                ParseHead(ref userInfo);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private void ParseHead(ref string msg)
        {
            this._Head = msg.Substring(0, 6);//消息前6位为消息类型标识符
            msg = msg.Substring(6);//第7位开始为消息实体内容
            ParseComputerName(ref msg);

        }
        private void ParseComputerName(ref string msg)
        {
            this._ComputerName = msg.Substring(0, msg.IndexOf(':'));
            msg = msg.Substring(_ComputerName.Length +1);
            ParseOsInfo(ref msg);
        }

        private void ParseOsInfo(ref string msg)
        {
            this._Os = msg.Substring(msg.LastIndexOf(':'));
            msg = msg.Substring(0, msg.LastIndexOf(':'));
            ParseIp(ref msg);
        }

        private void ParseIp(ref string msg)
        {
            this._IpList = new List<string>();
            _IpList.AddRange(msg.Split(':'));
        }

    }
}
