﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HZDrawPublish.Core.Network;
using HZDrawPublish.Service;
using System.Net;
using System.Net.Sockets;
using HZDrawPublish.Message;

namespace HZDrawPublish.Core.Udp
{

    public abstract class BaseUdp
    {

        public BaseUdp(int port)
        {
            _Port = port;
        }

        private int _Port;

        public int Port
        {
            get { return _Port; }
            set { _Port = value; }
        }


    }

    /// <summary>
    /// 单个UDP实例
    /// </summary>
    public class SingletonUdp 
    {   
        private List<NetworkElement> FNodes = null;
        private bool _isSendBroad = false; //是否已经发送过广播
        protected UdpClient FUdpClient = null;

        public System.Threading.AutoResetEvent autoEvent = new System.Threading.AutoResetEvent(false);
        public System.Threading.Timer FTimer = null;
        public IPEndPoint bcIPEndPoint = null;


        internal SingletonUdp(string ip,int port)
        {
            FUdpClient = new System.Net.Sockets.UdpClient();
            FNodes = new List<NetworkElement>();
            IPAddress net = new IPAddress(Encoding.Default.GetBytes(ip));
            bcIPEndPoint = new IPEndPoint(net, port);
            _isSendBroad = false;
        }

        public List<NetworkElement> Refresh(int TimeOuts)
        {
            FTimer = new System.Threading.Timer(new System.Threading.TimerCallback(CallbackCheckData), autoEvent, 0, 250);
            if (!autoEvent.WaitOne(TimeOuts, false))
            {
                FTimer.Dispose();
            }
            FTimer.Dispose();

            return FNodes;
        }

        private void CallbackCheckData(Object stateInfo)
        {
            lock (this)
            {
                if (!_isSendBroad)
                {
                    BroadCast();
                    _isSendBroad = true;
                    return;
                }
                try
                {
                    IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 0);
                    if (FUdpClient.Available <= 0 || FUdpClient.Client == null) return;
                    byte[] buff = FUdpClient.Receive(ref ipEndPoint);
                    UdpMessage msg = new UdpMessage();
                    if (msg.Parse(buff))
                    {
                        AddChild(msg);
                    }
                }
                finally
                {
                }
            }
        }
        /// <summary>
        /// 将IP添加到队列中
        /// </summary>
        /// <param name="msg"></param>
        private void AddChild(UdpMessage msg)
        {
            if (msg != null)
            {
                if (msg.Head == ":REPY:")
                {
                    foreach (string item in msg.IpList)
                    {
                        if (!Exist(item))//如果是没有添加过的IP
                        {
                            NetworkElement ssc = new NetworkElement(msg.ComputerName, item, msg.Os);
                            FNodes.Add(ssc);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 是否存在相同IP
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        private bool Exist(string ip)
        {
            bool bExists = false;
            foreach (NetworkElement temp in FNodes)//检测IP是否已存在，如果已存在，没必要再添加
            {
                if (temp.IP == ip)
                {
                    bExists = true;
                    break;
                }
            }

            return bExists;
        }

        /// <summary>
        /// 广播
        /// </summary>
        /// <param name="bcIPEndPoint"></param>
        protected void BroadCast()
        {
            //byte[] buff = UdpInfo.SerializeInfo(EmInfoType.On, Utilities.Utility.OsInfo);

            //FUdpClient.Send(buff, buff.Length, bcIPEndPoint);
        }
    }
}
