﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace HZDrawPublish.Core.Udp
{
    public class MyUdpClient : UdpClient
    {
        public MyUdpClient()
            : base()
        {
            //Calls the protected Client property belonging to the UdpClient base class.
            System.Net.Sockets.Socket s = this.Client;
            //Uses the Socket returned by Client to set an option that is not available using UdpClient.
            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
           // s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontRoute, 1);
        }

        public MyUdpClient(IPEndPoint ipLocalEndPoint)
            : base(ipLocalEndPoint)
        {
            //Calls the protected Client property belonging to the UdpClient base class.
            System.Net.Sockets.Socket s = this.Client;
            //Uses the Socket returned by Client to set an option that is not available using UdpClient.
            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontRoute, 0);
        }
    }
}
