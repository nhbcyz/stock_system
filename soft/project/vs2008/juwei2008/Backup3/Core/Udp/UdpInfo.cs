﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace HZDrawPublish.Service
{
    public enum EmInfoType
    {
        On,
        Reply,
        Off
    }
    public class UdpInfo
    {
        public static byte[] SerializeInfo(EmInfoType type,string addinfo)
        {
            byte[] buff = null;
            string info = string.Empty;
            try
            {
                switch (type)
                {
                    case EmInfoType.On:
                        info = ":USER:";
                        break;
                    case EmInfoType.Reply:
                        info = ":REPY:";
                        break;
                    case EmInfoType.Off:
                        info = ":QUIT:";
                        break;
                    default:
                        break;
                }

                //info += Utility.HostName + ":" + Utility.GetLocalIP();
                //if (!string.IsNullOrEmpty(addinfo))
                //{
                //    info += ":" + addinfo;
                //} 
                buff = Encoding.Default.GetBytes(info);
            }
            catch
            {
            }
            return buff;
        }
    }
}
