﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XY.SystemEx.SocketsEx;
using System.Threading;
using System.Net;
using HZDrawPublish.Core;
using HZDrawPublish.Message;

namespace HZDrawPublish.Service
{
    public class WorkService
    {
        #region Fields

        private bool _boolAutoStartup = false;
        private int FConnectionCount;
        //private bool _autoStartService = false;
        private bool _autoWriteLog = false;
        private SocketClientService FNetSocketService;
        private SocketClient FSocketClient;
        public OnEventDelegate FEvent;
       // private OnExceptionDelegate ExEvent; //Socket Exception Event
        private int _port = 10302;
        //private int _udpport = 6524;
        //private TransContext FContext;
        private NetworkElement FRemoteElement;

        #endregion Fields

        #region Properties


        public SocketClient Client
        { get { return FSocketClient; } }

        public bool AutoStartup
        {
            get { return _boolAutoStartup; }
            set
            {
                _boolAutoStartup = value;
            }
        }

        #endregion Properties

        #region 回调事件处理

        public void Event(string eventMessage)
        {
            Event(eventMessage, null, false);
        }

        public void Event(string eventMessage, ConnectionEventArgs connectArgs)
        {
            if (this._autoWriteLog)
                Event(eventMessage, connectArgs, false);
        }

        public void Event(string eventMessage, ConnectionEventArgs connectArgs, bool ex)
        {
            //if (lstMessages.InvokeRequired)
            //{
            //    eventMessage = eventMessage + "    " + DateTime.Now.ToLongTimeString();
            //    lstMessages.BeginInvoke(new OnEventDelegate(delegate(string s, ConnectionEventArgs ces) { Event(s, ces); }), eventMessage, connectArgs);
            //}
            //else
            {
                if (eventMessage.Contains("建立连接"))
                {
                    Interlocked.Increment(ref FConnectionCount);
                    if (connectArgs != null)
                    {
                        string lvistr = connectArgs.Connection.RemoteEndPoint.Address.ToString() + ":" + connectArgs.Connection.RemoteEndPoint.Port.ToString();
                        //if (!lvClients.Items.ContainsKey(lvistr))
                        //{
                        //    lvClients.Items.Add(new ListViewItem(connectArgs.Connection.RemoteEndPoint.Address.ToString() + ":" + connectArgs.Connection.RemoteEndPoint.Port.ToString()));
                        //}
                    }
                }

                if (eventMessage.Contains("断开连接"))
                {
                    Interlocked.Decrement(ref FConnectionCount);
                    //if (connectArgs != null)
                    //{
                    //    string lvistr = connectArgs.Connection.RemoteEndPoint.Address.ToString() + ":" + connectArgs.Connection.RemoteEndPoint.Port.ToString();
                    //    for (int i = lvClients.Items.Count; i > 0; i--)
                    //    {
                    //        if (lvClients.Items[i - 1].Text == lvistr)
                    //            lvClients.Items.RemoveAt(i - 1);
                    //    }
                    //}
                }

                // lblConnects.Text = "已经连接的客户端【" + FConnectionCount.ToString() + "】";

                string[] s = eventMessage.Split('\n');

                for (int i = s.Length - 1; i >= 0; i--)
                {
                    //lstMessages.BeginUpdate();
                    //lstMessages.Items.Insert(0, s[i]);
                    //lstMessages.EndUpdate();
                }
            }
        } 


        #endregion

        public WorkService(NetworkElement element, int nserverport)
        {
            _port = nserverport;
            FRemoteElement = element;
            Init();
        }

        private void Init()
        {
            this._autoWriteLog = true;
            //this._autoStartService = true;

            FEvent = new OnEventDelegate(Event);
            FConnectionCount = 0;

            FNetSocketService = new SocketClientService(FEvent);
            HZMessageHeader xTmp = new HZMessageHeader();


            FSocketClient = new SocketClient(FNetSocketService);
            FSocketClient.OnException += new OnExceptionDelegate(OnException);

            FNetSocketService.SocketClient = FSocketClient;
            IPEndPoint ipendpoint = new IPEndPoint(IPAddress.Parse(FRemoteElement.IP), _port);
            FSocketClient.AddConnector(ipendpoint,EncryptType.etNone,CompressionType.ctNone,null,200,1000);
            
            
        }

        public void OnException(Exception ex)
        {
            Event("出现异常! - " + ex.Message, null, true);
            Event("------------------------------------------------", null, true);
        }

        /// <summary>
        /// 开始连接
        /// </summary>
        /// <returns></returns>
        public bool Connect()
        {
            return FNetSocketService.waitConnection(50000);  //change  by zhoutao  5000-》50000
        }

        /// <summary>
        /// 断开连接
        /// </summary>
        public void DisConnect()
        {
            FSocketClient.Stop();
            
            FSocketClient.Dispose();
        }

        public string PrintNameIP()
        {
            if (FRemoteElement != null)
                return FRemoteElement.Name+":"+ FRemoteElement.IP;
            return "";
        }
    }
}
