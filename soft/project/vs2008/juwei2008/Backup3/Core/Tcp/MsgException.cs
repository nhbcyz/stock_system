﻿///////////////////////////////////////////////////////////////////////////////////////
//消息异常分类

using System;

namespace HZDrawPublish.Service
{
    /// <summary>
    /// 命令名字长度错误
    /// </summary>
    public class CommandNameLengthError : Exception
    {
        public CommandNameLengthError() : base("命令名字长度错误") { }
        public CommandNameLengthError(string message) : base(message) { }
    }

    public class MessageHeaderSerializeError : Exception
    {
        public MessageHeaderSerializeError() : base("序列化消息头出错") { }
        public MessageHeaderSerializeError(string message) : base(message) { }
    }

    public class MessageSerializeError : Exception
    {
        public MessageSerializeError() : base("序列化消息出错") { }
        public MessageSerializeError(string message) : base(message) { } 
    }

    public class MessageSerializeLengthError : Exception
    {
        public MessageSerializeLengthError() : base("序列化消息时验证长度出错") { }
        public MessageSerializeLengthError(string message) : base(message) { }
    }

    public class MessageParseLengthError : Exception
    {
        public MessageParseLengthError() : base("解析消息时验证长度出错") { }
        public MessageParseLengthError(string message) : base(message) { }
    }

    public class MessageLengthErrorException : Exception
    {
        public MessageLengthErrorException() : base("接收消息长度错误") { }
        public MessageLengthErrorException(string message) : base(message) { }
    }

    public class BadHeaderException : Exception
    {
        public BadHeaderException() : base("错误的消息头") { }
        public BadHeaderException(string message) : base(message) { } 
    }

    public class BadTailException : Exception
    {
        public BadTailException() : base("错误的消息尾") { }
        public BadTailException(string message) : base(message) { }
    }

    public class EscapeCharException : Exception
    {
        public EscapeCharException() : base("字符转义出错！") { }
        public EscapeCharException(string message) : base(message) { }
    }

    public class WriteLogError : Exception
    {
        public WriteLogError() : base("写日志出错！") { }
        public WriteLogError(string message) : base(message) { }
    }

    public class RegisterServiceError : Exception
    {
        public RegisterServiceError() : base("没有注册服务！") { }
        public RegisterServiceError(string message) : base(message) { }
    }
}
