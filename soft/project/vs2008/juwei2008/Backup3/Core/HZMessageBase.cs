﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using HZDrawPublish.Utilities;


namespace HZDrawPublish.Service
{

    //命令状态字;0：错误，1：字符串，2：dataTable，3：文件，4：含大字段的dataTable
    public enum EnumCommandStatus
    {
        None = 0,
        Error,
        String,
        DataTable,
        File,
        Image,
        DataTableBlob
    }

    public enum EmHzFileCommand
    {
        SendFileOverview = 0x0101,
        SendFileOverviewAck = 0xf1f1,
        SendFileFromHZDraw = 0x0202,
        SendFileFromHZDrawAck = 0xf2f2,
        SetSrvType = 0x0303,
        SetSrvTypeAck = 0xf3f3,
        ReqSrvType = 0xf6f6,
        ReqSrvTypeAck = 0x0606,
        ReqFile = 0x0404,
        ReqFileAck = 0xf4f4,
        SendFileOverviewSrv=0xf5f5,
        SendFileOverviewSrvAck=0x0505,
        SendFileSilceSrv=0xf7f7,
        SendFileSliceSrvAck=0x0707,
        HeartBeatFrame = 0x0808,//add by zhoutao   心跳帧
        ErrorAck= 0x8888

    }

    public class HZMessageHeader
    {
        public const char DELIMITER_LIST = '|';
        public const string DELIMITER_LIST2 = "#|";
        //消息包头部前缀和尾巴，只读
        public readonly byte[] MSG_HEADER = new byte[1] { 0x02 };
        public readonly byte[] MSG_TAIL = new byte[2] { 0x0d,0x0a };

        public enum Veritify
        {
            Success = 0,
            Incomplete,
            Error,
        };

        public enum VeritifySerialize
        {
            Success = 0,
            Faild,
        }

        #region Field

        //总的包ID
        private Int16 _TotalId;
        //包的总长度

        private Int32 _FileLen;
        //分片ID
        private Int16 _SliceId;
        //分片长度
        private Int32 _PacketLen;//包长度

        //命令字

        private EmHzFileCommand _Command;
        //站号
        private long _StationId;

        //数据内容
        private byte[] _Data;

        private Int16 _Crc;
        #endregion

        #region Constructor

        /// <summary>
        /// 头构造函数，空参数，主要用于解析
        /// </summary>
        public HZMessageHeader()
        {
            _Data = null;
        }
        

        public void init(Int16 totalid, Int32 filelen, Int16 sliceid, Int32 packlen, EmHzFileCommand command,long stationid)
        {
            _TotalId = totalid;
            _FileLen = filelen;
            _SliceId = sliceid;
            _PacketLen = packlen;
            _Command = command;
            _StationId = stationid;
            _Data = null;
        }

        #endregion

        #region EnumVeritifyHeader

        public enum EnumVeritifyHeader
        {
            Success = 0,
            Incomplete,
            Error,
        };

        public enum VeritifyHeaderSerialize
        {
            Success = 0,
            Faild,
        }

        #endregion

        #region Method

        public int VerifyAndGetLength(byte[] pRecBuff, int nBuffLength, ref int nLength)
        {
            if (nBuffLength < nLength) return Convert.ToInt32(EnumVeritifyHeader.Incomplete);

            nLength = BitConverter.ToInt32(pRecBuff, 0);
            if (nLength > nBuffLength)
                return Convert.ToInt32(EnumVeritifyHeader.Error);

            return Convert.ToInt32(EnumVeritifyHeader.Success);
        }
        /// <summary>
        /// 解析头部
        /// </summary>
        /// <param name="pBuff">引用传递,消息buffer</param>
        /// <param name="nLength"></param>
        /// <param name="nUseLength"></param>
        /// <returns></returns>
        protected VeritifyHeaderSerialize ParseHeader(ref byte[] pBuff)
        {
            try
            {
                int startPos = MSG_HEADER.Length;
                for (int i = 0; i < MSG_HEADER.Length; i++)
                {
                    if (pBuff[i] != MSG_HEADER[i])
                    {
                        return VeritifyHeaderSerialize.Faild;
                    }
                }
                _TotalId = EndianBitConverter.Big.ToInt16(pBuff, startPos);
                _FileLen = EndianBitConverter.Big.ToInt32(pBuff, 2 + startPos);
                _SliceId = EndianBitConverter.Big.ToInt16(pBuff, 6 + startPos);
                _PacketLen = EndianBitConverter.Big.ToInt16(pBuff, 8 + startPos);
                _Command = (EmHzFileCommand)(EndianBitConverter.Big.ToUInt16(pBuff,12 + startPos));
                _StationId = EndianBitConverter.Big.ToInt64(pBuff, 14 + startPos);
                return VeritifyHeaderSerialize.Success;
            }
            catch
            { return VeritifyHeaderSerialize.Faild; }
        }


        /// <summary>
        /// 头部序列化
        /// </summary>
        /// <param name="pBuff">引用传值，头部封装好的buffer</param>
        /// <returns></returns>
        protected VeritifyHeaderSerialize SerializeHeader(out byte[] pBuff)
        {
            pBuff = new byte[22];
            try
            {
                
                int startpos = 0;

                //Array.Copy(MSG_HEADER, 0, pBuff, startpos, MSG_HEADER.Length);

                Array.Copy(EndianBitConverter.Big.GetBytes(TotalId),0,pBuff,startpos, 2);
                startpos += 2;
                Array.Copy(EndianBitConverter.Big.GetBytes(FileLen), 0, pBuff, startpos, 4);
                startpos += 4;

                Array.Copy(EndianBitConverter.Big.GetBytes(SliceId), 0, pBuff, startpos, 2);
                startpos += 2;

                Array.Copy(EndianBitConverter.Big.GetBytes(PacketLen), 0, pBuff, startpos, 4);
                startpos += 4;

                Array.Copy(EndianBitConverter.Big.GetBytes((Int16)Command), 0, pBuff, startpos, 2);
                startpos += 2;

                Array.Copy(EndianBitConverter.Big.GetBytes(StationId), 0, pBuff, startpos, 8);
                startpos += 8;

                return VeritifyHeaderSerialize.Success;
            }
            catch
            { return VeritifyHeaderSerialize.Faild; }
        }

        #endregion

        #region Properties

        public Int16 TotalId
        {
            get { return _TotalId; }
            set { _TotalId = value; }
        }

        public EmHzFileCommand Command
        {
            get { return _Command; }
            set { _Command = value; }
        }

        public Int32 FileLen
        {
            get { return _FileLen; }
            set { _FileLen = value; }
        }

        public byte[] Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        public Int16 SliceId
        {
            get { return _SliceId; }
            set
            {
                _SliceId = value;
            }
        }

        public Int32 PacketLen
        {
            get { return _PacketLen; }
            set
            {
                _PacketLen = value;
            }
        }

        public long StationId
        {
            get { return _StationId; }
            set { _StationId = value; }
        }

        public Int16 Crc
        {
            get
            {
                return _Crc;
            }
            set { _Crc = value; }
        }
        #endregion
    }

    public class HZMessageBase : HZMessageHeader
    {
        #region Constructor

        public HZMessageBase(Int16 totalid, Int32 filelen, Int16 sliceid, Int32 packlen, EmHzFileCommand command, long stationid)
            : base()
        {
            init(totalid,filelen,sliceid,packlen,command,stationid);
            //---
        }

        public HZMessageBase()
            : base()
        {
            //--
        }

        #endregion

        /// <summary>
        /// 克隆
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public HZMessageBase Clone()
        {
            HZMessageBase result = new HZMessageBase();
            result.TotalId = this.TotalId;
            result.FileLen = this.FileLen;
            result.SliceId = this.SliceId;
            result.Data = this.Data;
            result.PacketLen = this.PacketLen;
            result.StationId = this.StationId;
            result.Command = this.Command;
            return result;
        }

        private Veritify Verify(ref byte[] pInBuff)
        {
            //验证消息头

            for (int i = 0; i < MSG_HEADER.Length; i++)
            {
                //错误的消息头
                if (MSG_HEADER[i] != pInBuff[i])
                    return Veritify.Error;
            }
            //验证消息尾部
            for (int i = 0; i < MSG_TAIL.Length; i++)
            {
                if (MSG_TAIL[i] != pInBuff[pInBuff.Length + i - MSG_TAIL.Length])
                {
                    return Veritify.Error;
                }
            }
           //short c = 0;//Utility.GetCrc16(pInBuff, pInBuff.Length - 2);

            //short CRC = (short)(pInBuff[pInBuff.Length - 2] * 0x100 + pInBuff[pInBuff.Length - 1]);
            //if (c!=CRC)
            //{
            //    return Veritify.Error;
            //}
            return Veritify.Success;
        }

        #region 转义相关代码
        /// <summary>
        /// 对字符串进行转义
        /// </summary>
        /// <param name="strTarget"></param>
        /// <returns></returns>
        private string EscapeCharIn(string strTarget)
        {
            try
            {
                string sRes = "";
                for (int i = 0; i < strTarget.Length; i++)
                {
                    if (strTarget[i] == DELIMITER_LIST)
                    {
                        sRes += "\\" + DELIMITER_LIST;
                    }
                    else
                    {
                        sRes += strTarget[i];
                    }
                }
                return sRes;
            }
            catch
            {
                return strTarget;
            }
        }

        /// <summary>
        /// 对字符串进行反转义
        /// </summary>
        /// <param name="strTarget"></param>
        /// <returns></returns>
        private ArrayList EscapeCharOut(string strTarget)
        {
            try
            {
                ArrayList l = new ArrayList();
                string sTmp = "";
                for (int i = 0; i < strTarget.Length; i++)
                {
                    if (strTarget[i] == DELIMITER_LIST)
                    {
                        if (i == 0)
                        {
                            l.Add("");
                            break;
                        }
                        if ((strTarget[i - 1]) == '\\')//替换前面的那个'\'
                        {
                            sTmp = sTmp.Substring(0, sTmp.Length - 1) + DELIMITER_LIST;
                        }
                        else
                        {
                            l.Add(sTmp.Substring(0, sTmp.Length - 1));
                            sTmp = "";
                        }
                    }
                    else
                    {
                        sTmp += strTarget[i];
                    }
                }
                if ((l.Count > 0) || (sTmp != ""))//如果已经有分隔符号，则后面一定存在一个数据，反之，则为普通的字符串

                    l.Add(sTmp);
                return l;
            }
            catch
            {
                return null;
            }
        } 
        #endregion

        /// <summary>
        /// 序列化整个消息包
        /// </summary>
        /// <param name="totalid"></param>
        /// <param name="filelen"></param>
        /// <param name="sliceid"></param>
        /// <param name="packlen"></param>
        /// <param name="command"></param>
        /// <param name="stationid"></param>
        /// <param name="buffbody"></param>
        /// <param name="MsgBuff"></param>
        public void XY_SerializeMsgPacket(Int16 totalid, Int32 filelen, Int16 sliceid, EmHzFileCommand command, long stationid, byte[] buffbody, out byte[] MsgBuff)
        {
            MsgBuff = null;
            try
            {
                this.init(totalid, filelen, sliceid,buffbody.Length, command,stationid);

                //序列化消息头
                byte[] headerBuf;
                if (VeritifyHeaderSerialize.Success != SerializeHeader(out headerBuf))
                    throw new MessageHeaderSerializeError();

                //将消息头和消息体结合
                if ((buffbody != null) && (headerBuf != null))
                {
                    MsgBuff = new byte[buffbody.Length + headerBuf.Length + MSG_HEADER.Length + 2]; //数字2为crc校验长度
                    Array.Copy(MSG_HEADER, 0, MsgBuff, 0, MSG_HEADER.Length);//加头
                    Array.Copy(headerBuf, 0, MsgBuff, MSG_HEADER.Length, headerBuf.Length);//消息头

                    Array.Copy(buffbody, 0, MsgBuff, MSG_HEADER.Length + headerBuf.Length, buffbody.Length);//消息体

                    this.Crc = Utility.GetCrc16(MsgBuff, MsgBuff.Length-2);
                    Array.Copy(EndianBitConverter.Big.GetBytes(this.Crc), 0, MsgBuff, MSG_HEADER.Length + headerBuf.Length + buffbody.Length, 2);
                }
            }
            catch (Exception e)
            { throw e; }
        }

        /// <summary>
        /// 解析消息包头
        /// </summary>
        /// <param name="pBuff">socket接收的消息原包</param>
        /// <param name="pOutbuff">消息体数组</param>
        /// <param name="header">消息头对象，内部属性变量已填充</param>
        public bool XY_ParseMsgPacketHead(ref byte[] pRawBuff)
        {
            try
            {
                if (pRawBuff == null) return false;
                if (pRawBuff.Length < MSG_HEADER.Length)
                    throw new MessageLengthErrorException("接收消息包长度小于4,消息解析失败");

                //验证消息长度是否正确
                if (Veritify.Success != Verify(ref pRawBuff))
                {
                    throw new MessageParseLengthError();
                }

                //解析消息头部
                //if (VeritifyHeaderSerialize.Success != ParseHeader(ref pRawBuff))
                //    throw new MessageHeaderSerializeError();

                //复制消息体到引用参数数组，即消息体

                Data = new byte[pRawBuff.Length - MSG_TAIL.Length - MSG_HEADER.Length];
                Array.Copy(pRawBuff, MSG_HEADER.Length, Data, 0, Data.Length);

                return true;
            }
            catch
            {
                //throw e;
                return false;
            }
        }

    }
}
