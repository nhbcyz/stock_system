﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HZDrawPublish.Core
{
    /// <summary>
    /// 业务类型
    /// </summary>
    public enum EmBussiness
    {
        SendFile,//发送文件
        ReqFile,//请求文件
        SetType, //设置服务器类型
        ReqType  //获取服务器类型
    }


    /// <summary>
    /// 机器类型
    /// </summary>
    public enum EmMechine
    {
        RDS=0x01,
        CMS=0x02,
        STATION=0x04,
        HISDATASRV=0x08,
        LOGSRV=0x10,
        DTS=0x20,
    }


    /// <summary>
    /// 处理结果
    /// </summary>
    public enum EmOperationResult
    {
        Success,
        Fail,
        Timeout,
        ZipError,
        ConnectFail,
        UnKnowError,
        None
    }

    public class SliceFileModel
    {
        public int Length { get; set; }
        public Array Content { get; set; }
        public int ID { get; set; }
    }
}
