﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HZDrawPublish.Service;
using System.IO;
using HZDrawPublish.Message;
using System.Runtime.InteropServices;
using HZDrawPublish.Socket;
using XY.SystemEx.SocketsEx;
using System.Threading;
using HZDrawPublish.Utilities;

namespace HZDrawPublish.Core
{
    /// <summary>
    /// 业务逻辑抽象类
    /// </summary>
    public abstract class DataTranService
    {
       // public byte[] Head = new byte[4] { 0x4a, 0x4b, 0x5a, 0x5b };//
        public abstract EmOperationResult Process(ITrans trans,WorkService service);

        #region 失效代码

        //protected unsafe byte[] ProcUnsafeBuffer(long stationid, Int16 totalid, uint filelen, Int16 id, Int16 command, int sendlen, byte[] content, ref int len)
        //{
        //    var incoming = new byte[1100];
        //    fixed (byte* inBuf = incoming)
        //    {
        //        byte* outBuf = HZFileMessage.Serialize(stationid, totalid, filelen, id, command, sendlen, content, ref len);

        //        for (int i = 0; i < incoming.Length; i++)
        //        {
        //            incoming[i] = outBuf[i];
        //        }
        //    }

        //    return incoming;
        //}
        #endregion
    }

    public class HeartBeatInfo
    {
        public int type;
        public int result;
        public string info;

        public static  List<HeartBeatInfo> AnalyzeHeartBeatInfo(byte[] bts)
        {
            List<HeartBeatInfo> hbiList = new List<HeartBeatInfo>();
            
            if (bts.Length > 2)
            {
                int index=0;
                while(bts.Length >= 2+66*(index+1))
                {
                    HeartBeatInfo hbi = new HeartBeatInfo();
                    hbi.type = bts[2 + index * 66];
                    hbi.result = bts[3 + index * 66];
                    hbi.info = Bytes2String(bts, 4 + index * 66, 64);
                    hbiList.Add(hbi);
                    index++;
                }
            }
            return hbiList;
        }

        public HeartBeatInfo Clone()
        {
            HeartBeatInfo hbi = new HeartBeatInfo();
            hbi.type = type;
            hbi.result = result;
            hbi.info = info;

            return hbi;
        }

        public static string Bytes2String(byte[] bts,int index,int nCount)
        {
            int validLen = 0;
            for (int i = index; i < index + nCount; i++)
            {
                if (bts[i] == 0)//如果遇到最后一个字符
                {
                    
                    break;
                }
                validLen++;
            }
            return System.Text.Encoding.Default.GetString(bts, index, validLen);
        }
    }

    /// <summary>
    /// 发送文件类
    /// </summary>
    public class SendFileService : DataTranService
    {
        #region 失效代码
        //        public override EmOperationResult Process(ITrans trans, WorkService service)
        //        {
        //            string tempfilename = "", fullname = "",tempfolder="",tempzipfolder="";
        //            if (!service.Connect())
        //            {
        //                return EmOperationResult.ConnectFail;
        //            }
        //            try
        //            {
        //                TransFileModel model = trans as TransFileModel;
        //                if (model == null)
        //                {
        //                    return EmOperationResult.Fail;
        //                }
        //                tempfilename = DateTime.Now.Ticks.ToString();//生成的临时的文件名,不包括全路径和扩展名
        //                FileProc.CreateFolder("temp");
        //                tempfolder = "temp\\" + tempfilename + "\\" + tempfilename; //建立实际内容文件夹,用于存放实际的文件和文件夹
        //                tempzipfolder = "temp\\" + tempfilename; //临时的文件夹，用于存放实际内容文件夹和压缩包,在该函数结束时将删除该文件夹

        //                if (!this.FileClassify(model, tempfolder))
        //                {
        //                    return EmOperationResult.ZipError;
        //                }

        //                if (!this.Compress("temp\\" + tempfilename, tempfilename))
        //                {
        //                    return EmOperationResult.ZipError;
        //                }

        //                fullname = tempfolder + ".zip";
        //                List<SliceFileModel> list = FileProc.File2Slice(fullname);
        //                if (list == null || list.Count == 0)
        //                {
        //                    return EmOperationResult.UnKnowError;
        //                }
        //                FileInfo fi = new FileInfo(fullname);
        //                //打包文件预览信息
        //                int totalId = list.Count; //总ID
        //                byte[] sendcontent = new byte[0];

        //                #region 失效代码
        //                //int nlen = 0;
        //                //var incoming = new byte[1024];
        //                //incoming = ProcUnsafeBuffer(AppCommon.Instance.StationID, (short)totalId, (uint)fi.Length, (short)totalId, (short)EmHzFileCommand.SendFileOverview, 0, sendcontent, ref nlen);
        //                //byte[] buff = new byte[nlen];
        //                //Array.Copy(incoming, buff, nlen);
        //                //byte[] buffer = NetWorkService.Send(service, buff); 
        //                #endregion

        //                //HZFileMessage.PacketBody pb = new HZFileMessage.PacketBody();
        //                HZMessageBase msgbase = new HZMessageBase();
        //                byte[] msgbuff = null;
        //                msgbase.XY_SerializeMsgPacket((short)totalId, (int)fi.Length, (short)totalId, EmHzFileCommand.SendFileOverview, AppCommon.Instance.StationID, sendcontent, out msgbuff);
        //                HZMessageBase retmsgbase = null;
        //                int retrycount = 3;
        //                string hex = Utility.byteToHexStr(msgbuff);
        //                while (true)
        //                {
        //                    if (retrycount<=0)
        //                    {
        //                        return EmOperationResult.Fail;
        //                    }
        //                    retmsgbase = NetWorkService.Send(service, msgbuff);
        //                    if (retmsgbase == null)
        //                    {
        //                        retrycount--;
        //                        continue;
        //                    }
        //                    else
        //                    {
        //                        break;
        //                    }
        //                }
        //                for (int i = 0; i < list.Count; i++)
        //                {
        //                    msgbuff = null;
        //                    msgbase = new HZMessageBase();
        //                    sendcontent = new byte[list[i].Length];
        //                    Array.Copy(list[i].Content, sendcontent, list[i].Length);
        //                    msgbase.XY_SerializeMsgPacket((short)totalId, (int)fi.Length, (short)list[i].ID, EmHzFileCommand.SendFileFromHZDraw, AppCommon.Instance.StationID, sendcontent, out msgbuff);
        //                    retrycount = 3;
        //                    retmsgbase = null;
        //                    while (true)
        //                    {
        //                        if (retrycount <= 0)
        //                        {
        //                            return EmOperationResult.Fail;
        //                        }
        //                        retmsgbase = NetWorkService.Send(service, msgbuff);
        //                        if (retmsgbase == null)
        //                        {
        //                            retrycount--;
        //                            continue;
        //                        }
        //                        Int16 nextid = EndianBitConverter.Big.ToInt16(retmsgbase.Data, 0);
        //                        if (nextid != (list[i].ID +1))
        //                        {
        //                            retrycount--;
        //                            continue;
        //                        }
        //                        else
        //                        {
        //                            break;
        //                        }
        //                    }
        //                }
        //                return EmOperationResult.Success;
        //#region 失效代码
        //                        //if (HZFileMessage.Parse(buffer, (buffer.Length), ref pb) == 0)
        //                //{
        //                //    nlen = 0;
        //                //    incoming = null;
        //                //    buff = null;
        //                //    retrycount = 3;
        //                //    buffer = null;

        //                //    //发送文件分片
        //                //    foreach (var item in list)
        //                //    {
        //                //        sendcontent = null;
        //                //        sendcontent = new byte[item.Length];
        //                //        Array.Copy(item.Content, sendcontent, item.Length);
        //                //        while (true)
        //                //        {
        //                //            if (retrycount <= 0)
        //                //            {
        //                //                return EmOperationResult.Fail;
        //                //            }
        //                //            incoming = new byte[1200];


        //                //            incoming = ProcUnsafeBuffer(AppCommon.Instance.StationID, (short)totalId, (uint)fi.Length, (short)item.ID, (short)EmHzFileCommand.SendFileFromHZDraw, sendcontent.Length, sendcontent, ref nlen);
        //                //            buff = new byte[nlen];
        //                //            Array.Copy(incoming, buff, nlen);
        //                //            pb = new HZFileMessage.PacketBody();
        //                //            string hexstr = Utilities.Utility.byteToHexStr(buff);
        //                //            buffer = NetWorkService.Send(service, buff);
        //                //            if (buffer == null)
        //                //            {
        //                //                retrycount--;
        //                //                continue;
        //                //            }
        //                //            if (HZFileMessage.Parse(buffer, (buffer.Length), ref pb) == -1)
        //                //            {
        //                //                retrycount--;
        //                //                continue;
        //                //            }
        //                //            nextid = BitConverter.ToInt16(pb.Content, 0);

        //                //            if (nextid != (item.ID))
        //                //            {
        //                //                retrycount--;
        //                //                continue;
        //                //            }
        //                //            else
        //                //            {
        //                //                break;
        //                //            }
        //                //        }
        //                //    }
        //                //} 
        //    #endregion
        //            }
        //            catch (Exception)
        //            {
        //                return EmOperationResult.UnKnowError;
        //            }
        //            finally
        //            {
        //                service.DisConnect();
        //                if (Directory.Exists(tempzipfolder))
        //                {
        //                    FileProc.Delete(tempzipfolder);
        //                   // Directory.Delete(tempzipfolder, true);
        //                }
        //            }
        //        } 
        #endregion

        public double Percentage = 0;//add by zhoutao
        public bool bSendOver = false;
        public int nHeartBeatState = -2;//发送心跳帧后返回的状态
        public List<HeartBeatInfo> HeartBeatInfoList = new List<HeartBeatInfo>();//心跳信息
        public EmOperationResult PublishResult = EmOperationResult.None;

        public override EmOperationResult Process(ITrans trans, WorkService service)
        {
            PublishResult = EmOperationResult.None;
            string tempfilename = "", fullname = "", tempfolder = "", tempzipfolder = "";                
            if (!service.Connect())
            {

               // HZ3000.PublicFunction.Log.WriteLog("HZDrawPublish", "WriteLog", "连接失败  " + service.PrintNameIP(), HZ3000.PublicFunction.Log.LogLevel.LError);
                PublishResult = EmOperationResult.ConnectFail;
                return EmOperationResult.ConnectFail;
            }
            try
            {
                TransFileModel model = trans as TransFileModel;
                if (model == null || model.Files.Count == 0)
                {
                    PublishResult = EmOperationResult.Fail;
                    return EmOperationResult.Fail;
                }
                //tempfilename = CreateTempPath(); //创建临时路径,确定打包路径
                //fullname = "temp\\" + tempfilename + ".zip";
                fullname = model.Files[0].Substring(0,model.Files[0].LastIndexOf("\\")) + ".zip";
                if (!this.Compress(model.Files[0], fullname))
                {
                    PublishResult = EmOperationResult.ZipError;
                    return EmOperationResult.ZipError;
                }

                List<SliceFileModel> list = FileProc.File2Slice(fullname);
                if (list == null || list.Count == 0)
                {
                    PublishResult =  EmOperationResult.UnKnowError;
                    return EmOperationResult.UnKnowError;
                }
                FileInfo fi = new FileInfo(fullname);
                //打包文件预览信息
                int totalId = list.Count; //总ID
                byte[] sendcontent = new byte[0];
                HZMessageBase msgbase = new HZMessageBase();
                byte[] msgbuff = null;
                msgbase.XY_SerializeMsgPacket((short)totalId, (int)fi.Length, (short)totalId, EmHzFileCommand.SendFileOverview, AppCommon.Instance.StationID, sendcontent, out msgbuff);
                HZMessageBase retmsgbase = null;
                int retrycount = 30;
                string hex = Utility.byteToHexStr(msgbuff);
                NetWorkService networkservice = new NetWorkService();
                while (true)
                {
                    if (retrycount <= 0)
                    {
                        list = null;
                       // HZ3000.PublicFunction.Log.WriteLog("HZDrawPublish", "WriteLog","发报头无回应  "+ service.PrintNameIP(), HZ3000.PublicFunction.Log.LogLevel.LError);
                        PublishResult = EmOperationResult.Fail;
                        return EmOperationResult.Fail;
                    }
                   // retmsgbase = NetWorkService.Send(service, msgbuff);
                    int timeout = 10000;
                    retmsgbase = networkservice.Send(service, msgbuff, timeout);
                    if (retmsgbase == null)
                    {
                        retrycount--;
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                #region 发文件

                for (int i = 0; i < list.Count; i++)
                {
                    msgbuff = null;
                    msgbase = new HZMessageBase();
                    sendcontent = new byte[list[i].Length];
                    Array.Copy(list[i].Content, sendcontent, list[i].Length);
                    msgbase.XY_SerializeMsgPacket((short)totalId, (int)fi.Length, (short)list[i].ID, EmHzFileCommand.SendFileFromHZDraw, AppCommon.Instance.StationID, sendcontent, out msgbuff);
                    retrycount = 30;
                    retmsgbase = null;
                    
                    while (true)
                    {
                        if (retrycount <= 0)
                        {
                            list = null;
                           // HZ3000.PublicFunction.Log.WriteLog("HZDrawPublish", "WriteLog", "发包30都返回超时  " + service.PrintNameIP(), HZ3000.PublicFunction.Log.LogLevel.LError);
                            PublishResult = EmOperationResult.Fail;
                            return EmOperationResult.Fail;
                        }

                        //retmsgbase = networkservice.Send(service, msgbuff);
                       /* if (i < list.Count - 1)
                            retmsgbase = networkservice.Send(service, msgbuff);
                        else
                        {
                            bSendOver = true;
                            double size = list.Count * 0.154;
                            if(size<50)
                            // retmsgbase = NetWorkService.Send(service, msgbuff,2*60*1000);//add by zhoutao
                                retmsgbase = networkservice.Send(service, msgbuff, 5 * 60 * 1000);//add by zhoutao
                            else if(size <100)
                                retmsgbase = networkservice.Send(service, msgbuff, 10 * 60 * 1000);//add by zhoutao
                            else
                                retmsgbase = networkservice.Send(service, msgbuff, 20 * 60 * 1000);//add by zhoutao
                        }
                        */

                        if (retmsgbase == null)
                        {
                            retrycount--;
                            continue;
                        }
                        if (retmsgbase.Command == EmHzFileCommand.ErrorAck)
                        {
                            list = null;
                            PublishResult = EmOperationResult.Fail;
                            return EmOperationResult.Fail;
                        }
                        else
                        {
                            if (i == list.Count - 1)//如果是最后一帧
                            {
                                bSendOver = true;
                                break;
                            }
                        }

                        Int16 nextid = EndianBitConverter.Big.ToInt16(retmsgbase.Data, 0);
                        if (nextid != (list[i].ID + 1))
                        {
                            retrycount--;
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                    Percentage = ((double)(i + 1)) / ((double)list.Count);
                }
                #endregion

                #region 发心跳帧
                bool bErrorAck = false;
                totalId = 1;
                byte[] content = new byte[0];
                msgbase = new HZMessageBase();
                msgbuff = null;
                msgbase.XY_SerializeMsgPacket((short)totalId, 0, (short)totalId, EmHzFileCommand.HeartBeatFrame, AppCommon.Instance.StationID, content, out msgbuff);
                retmsgbase = null;
           //     networkservice = new NetWorkService();
                nHeartBeatState = -1;
                for (int i = 0; i < 20 * 60;i++ )//因为Send中默认间隔1s发一次，所以这里算起来是1200s，也就是20分钟
                {
                    retrycount = 10;
                    while (true)//每组都发3次心跳帧，如果有返回进行下一组。如果某一组发的心跳帧超过3次没有回应，那么表明对方无回应
                    {
                        if (retrycount <= 0)
                        {
                            PublishResult = EmOperationResult.Fail;
                            return EmOperationResult.Fail;
                        }
                        int timeout = 5000;
                        retmsgbase = networkservice.Send(service, msgbuff,timeout);
                        if (retmsgbase == null)
                        {
                            retrycount--;
                            continue;
                        }
                        else
                        {
                            if (retmsgbase.Command == EmHzFileCommand.ErrorAck)
                            {
                                bErrorAck = true;
                            }

                            HeartBeatInfoList.AddRange(HeartBeatInfo.AnalyzeHeartBeatInfo(retmsgbase.Data));
                            if (retmsgbase.Data.Length > 0)
                            {

                                nHeartBeatState = retmsgbase.Data[0];

                                if (nHeartBeatState == 0)
                                {
                                    if (bErrorAck)
                                    {
                                        PublishResult = EmOperationResult.Fail;
                                        return EmOperationResult.Fail;
                                    }
                                    else
                                    {
                                        PublishResult = EmOperationResult.Success;
                                        return EmOperationResult.Success;
                                    }
                                }
                                else if (nHeartBeatState == 2)
                                {
                                }
                                break;
                            }
                            else
                            {
                                retrycount--;
                                continue;
                            }
                            
                        }
                        
                    }
                    Thread.Sleep(1000);
                }
                
                
                #endregion

                list = null;
                PublishResult = EmOperationResult.Success;
                return EmOperationResult.Success;
            }
            catch (Exception ex)
            {
                PublishResult = EmOperationResult.UnKnowError;
                return EmOperationResult.UnKnowError;
            }
            finally
            {
                service.DisConnect();
                //if (File.Exists(fullname))
                //{
                //    FileProc.Delete(fullname);
                //}
            }
        }

        private string CreateTempPath()
        {
            string tempfilename = DateTime.Now.Ticks.ToString();//生成的临时的文件名,不包括全路径和扩展名
            //临时临时路径
            FileProc.CreateFolder("temp");
            //FileProc.CreateFolder("temp\\" + tempfilename);
            return tempfilename;
        }


        /// <summary>
        /// 压缩和打包
        /// </summary>
        /// <param name="model"></param>
        /// <param name="destfilename">临时文件目录</param>
        /// <param name="tempfolder">临时的文件夹,全路径</param>
        /// <param name="tempfilename">临时的文件名字,不包括扩展名和全路径</param>
        /// <returns></returns>
        private bool Compress(string tempfolder, string tempfilename)
        {
            try
            {
                //GZipResult zipret = GZip.Compress(tempfolder, tempfilename, tempfilename);
                //if (zipret.Errors) return false;
                //string tozipfile = tempfolder + "\\" + tempfilename;
                //string zipedfile = tozipfile + ".zip";
                //ZipClass.ZipFile(tozipfile, zipedfile);
                //ZipClass.ZipFileDirectory(tempfolder, tempfilename);
                //if (File.Exists(tozipfile))
                //{
                //    File.Delete(tozipfile);
                //}
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 文件分类和移动到目标文件夹
        /// </summary>
        /// <param name="model"></param>
        /// <param name="destfolder">目标文件夹</param>
        private bool FileClassify(TransFileModel model,string destfolder)
        {
            if (model.Files != null && model.Files.Count > 0)
            {
                try
                {
                    foreach (var item in model.Files)
                    {
                        string tempitem = item.ToUpper();
                        int tempindex = tempitem.IndexOf(AppCommon.Instance.AppPath);
                        if (tempindex==0)//如果文件时基于hz3000目录的则进行分类
                        {
                            string substr = tempitem.Substring(AppCommon.Instance.AppPath.Length);
                            string tempdestsubstr = destfolder;
                            while (true)
                            {
                                if (substr.IndexOf("\\") > 0)//找到了一个次级文件夹
                                {
                                    string tempsubstr = substr.Substring(0, substr.IndexOf("\\"));

                                    tempdestsubstr += "\\" + tempsubstr;
                                    FileProc.CreateFolder(tempdestsubstr);
                                    substr = substr.Substring(substr.IndexOf("\\")+1);
                                }
                                else//已经是文件名字了,则将文件复制到temp目录中。
                                {
                                    File.Copy(tempitem, tempdestsubstr + "\\" + substr);
                                    break;
                                }
                            }                                           
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }
            return false;
        }

    }

    public class ReqFileService : DataTranService
    {
        string tempfilename = "", fullname = "", tempfolder = "", tempzipfolder = "";

        public override EmOperationResult Process(ITrans trans, WorkService service)
        {
            if (!service.Connect())
            {
                return EmOperationResult.ConnectFail;
            }
            try
            {
                int totalId = 1; //总ID
                byte[] sendcontent = new byte[0];
                HZMessageBase msgbase = new HZMessageBase();
                byte[] msgbuff = null;
                HZMessageBase retmsgbase = null;

                #region 发送文件请求信息
                msgbase.XY_SerializeMsgPacket((short)totalId, 0, (short)totalId, EmHzFileCommand.ReqFile, AppCommon.Instance.StationID, sendcontent, out msgbuff);
                int retrycount = 3;
                NetWorkService networkservice = new NetWorkService();
                while (true)
                {
                    if (retrycount <= 0)
                    {
                        return EmOperationResult.Fail;
                    }
                    int timeout = 5000;
                   // retmsgbase = NetWorkService.Send(service, msgbuff);
                    retmsgbase = networkservice.Send(service, msgbuff, timeout);
                    if (retmsgbase == null)
                    {
                        retrycount--;
                        continue;
                    }
                    else
                    {
                        break;
                    }
                } 
                #endregion

                #region 等待接收服务器端发文件预览信息
                //retrycount = 3;
                //while (true)
                //{
                //    if (retrycount <= 0)
                //    {
                //        return EmOperationResult.Fail;
                //    }
                //    if (!autoEvent.WaitOne(5000, false))
                //    {
                //        aTimer.Dispose();
                //    }

                //    aTimer.Dispose();

                //    if (FRetMsg == null)
                //    {
                //        retrycount--;
                //        continue;
                //    }
                //    else
                //    {
                //        if (FRetMsg.Command == EmHzFileCommand.SendFileOverviewSrv)
                //        {
                //            break;
                //        }
                //        else
                //        {
                //            retrycount--;
                //            continue;
                //        }
                //    }
                //} 
                #endregion

                #region 发送文件预览信息应答
                if (retmsgbase.TotalId == 0 || retmsgbase.FileLen <= 0)
                {
                    return EmOperationResult.Fail;
                }

                byte[] filecontent = new byte[retmsgbase.FileLen];
                int lastcontentlength = 0; //已经收到的文件分片长度
                retrycount = 3;
                retmsgbase = null;
                msgbase.XY_SerializeMsgPacket((short)totalId, 0, (short)totalId, EmHzFileCommand.SendFileOverviewSrvAck, AppCommon.Instance.StationID, sendcontent, out msgbuff);
                while (true)
                {
                    if (retrycount <= 0)
                    {
                        return EmOperationResult.Fail;
                    }

                  //  retmsgbase = NetWorkService.Send(service, msgbuff);
                    int timeout = 5000;
                    retmsgbase = networkservice.Send(service, msgbuff, timeout);
                    if (retmsgbase == null)
                    {
                        retrycount--;
                        continue;
                    }
                    else
                    {
                        if (retmsgbase.SliceId == 1 && retmsgbase.Command == EmHzFileCommand.SendFileSilceSrv)
                        {
                            Array.Copy(retmsgbase.Data, lastcontentlength, filecontent, 0, retmsgbase.Data.Length);
                            lastcontentlength += retmsgbase.Data.Length;
                            break;
                        }
                        else
                        {
                            retrycount--;
                            continue;
                        }
                    }
                } 
                #endregion

                #region 接收所有的文件分片

                int totallen = retmsgbase.TotalId;
                for (short i = 1; i < totallen; i++)
                {
                    retrycount = 3;
                    retmsgbase = null;
                    sendcontent = EndianBitConverter.Big.GetBytes((short)(i+1));
                    msgbase.XY_SerializeMsgPacket((short)totalId, 2, (short)totalId, EmHzFileCommand.SendFileSliceSrvAck, AppCommon.Instance.StationID, sendcontent, out msgbuff);
                    while (true)
                    {
                        if (retrycount <= 0)
                        {
                            return EmOperationResult.Fail;
                        }
                       // retmsgbase = NetWorkService.Send(service, msgbuff);
                        int timeout = 5000;
                        retmsgbase = networkservice.Send(service, msgbuff,timeout);
                        if (retmsgbase == null)
                        {
                            retrycount--;
                            continue;
                        }
                        else
                        {
                            if ((retmsgbase.Command == EmHzFileCommand.SendFileSilceSrv) && (retmsgbase.SliceId == i + 1))
                            {
                                Array.Copy(retmsgbase.Data, 0, filecontent, lastcontentlength, retmsgbase.Data.Length);
                                lastcontentlength += retmsgbase.Data.Length;
                                break;
                            }
                            else
                            {
                                retrycount--;
                                continue;
                            }
                        }
                    }
                } 
                #endregion
                
                //处理接收后的文件
                tempfilename = DateTime.Now.Ticks.ToString();
                FileProc.CreateFolder("temp");
                tempzipfolder = "temp\\" + tempfilename; //临时的文件夹，用于存放实际内容文件夹和压缩包,在该函数结束时将删除该文件夹
                FileProc.CreateFolder(tempzipfolder);
                fullname = tempzipfolder + "\\" + tempfilename + ".zip";
                File.WriteAllBytes(fullname, filecontent);

                
                return EmOperationResult.Success;
            }
            catch
            {
                return EmOperationResult.UnKnowError;
            }
            finally
            {
                service.DisConnect();
            }
        }

    }

    public class SetTypeService:DataTranService
    {
        public override EmOperationResult Process(ITrans trans, WorkService service)
        {
            if (!service.Connect())
            {
                return EmOperationResult.ConnectFail;
            }
            try
            {
                ServerTypeModel stm = trans as ServerTypeModel;
                if (stm == null)
                {
                    return EmOperationResult.Fail;
                }
                if (stm.Mechines == null || stm.Mechines.Count == 0)
                {
                    return EmOperationResult.Fail;
                }
                short t = 0;
                foreach (var item in stm.Mechines)
                {
                    t |= (short)item;
                }
                byte[] content = EndianBitConverter.Big.GetBytes(t);
                short totalId = 1;

                HZMessageBase msgbase = new HZMessageBase();
                byte[] msgbuff = null;
                msgbase.XY_SerializeMsgPacket(totalId, content.Length - 1, totalId, EmHzFileCommand.SetSrvType, AppCommon.Instance.StationID, content, out msgbuff);
                int retrycount = 3;
                HZMessageBase retmsgbase = null;
                NetWorkService networkservice = new NetWorkService();
                while (true)
                {
                    if (retrycount <= 0)
                    {
                        return EmOperationResult.Fail;
                    }
                   // retmsgbase = NetWorkService.Send(service, msgbuff);
                    int timeout = 5000;
                    retmsgbase = networkservice.Send(service, msgbuff, timeout);
                    if (retmsgbase == null)
                    {
                        retrycount--;
                        continue;
                    }
                    else if (retmsgbase.Command == EmHzFileCommand.ErrorAck)
                    {
                        retrycount--;
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                return EmOperationResult.Success;
                #region 失效代码
                //var incoming = new byte[1024];
                //incoming = ProcUnsafeBuffer(AppCommon.Instance.StationID, totalId, (uint)content.Length - 1, totalId, (short)EmHzFileCommand.SetSrvType, content.Length - 1, content, ref nlen);
                //byte[] buff = new byte[nlen];
                //Array.Copy(incoming, buff, nlen);
                //if (HZFileMessage.Parse(buffer, (buffer.Length), ref pb) == 0)
                //{
                //    if (pb.Content.Length != 1)
                //    {
                //        return EmOperationResult.Fail;
                //    }
                //    if ((EmHzFileCommand)pb.Command != EmHzFileCommand.SetSrvTypeAck)
                //    {
                //        return EmOperationResult.Fail;
                //    }
                //    if (pb.Content[0] != 0)
                //    {
                //        return EmOperationResult.Fail;
                //    }
                //} 
                #endregion
            }
            catch
            {
                return EmOperationResult.Fail;
            }
            finally
            {
                service.DisConnect();
            }
        }
    }

    public class ReqTypeService : DataTranService
    {
        public override EmOperationResult Process(ITrans trans, WorkService service)
        {
            if (!service.Connect())
            {
                return EmOperationResult.ConnectFail;
            }
            try
            {
                short totalId = 1;
                byte[] content = new byte[0];
                HZMessageBase msgbase = new HZMessageBase();
                byte[] msgbuff = null;
                msgbase.XY_SerializeMsgPacket(totalId,0, totalId, EmHzFileCommand.ReqSrvType, AppCommon.Instance.StationID, content, out msgbuff);
                int retrycount = 3;
                HZMessageBase retmsgbase = null;
                NetWorkService networkservice = new NetWorkService();
                while (true)
                {
                    if (retrycount <= 0)
                    {
                        return EmOperationResult.Fail;
                    }
                   // retmsgbase = NetWorkService.Send(service, msgbuff);
                    int timeout = 5000;
                    retmsgbase = networkservice.Send(service, msgbuff, timeout);
                    if (retmsgbase == null)
                    {
                        retrycount--;
                        continue;
                    }
                    else if (retmsgbase.Command == EmHzFileCommand.ErrorAck)
                    {
                        retrycount--;
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                ServerTypeModel stm = trans as ServerTypeModel;
                if (stm == null)
                {
                    return EmOperationResult.Fail;
                }
                if (stm.Mechines == null)
                {
                    stm.Mechines = new List<EmMechine>();
                }
                stm.Mechines.Clear();
                List<EmMechine> ls = new List<EmMechine>();
                short t = EndianBitConverter.Big.ToInt16(retmsgbase.Data, 0);
                for (int i = 0; i < Enum.GetNames(typeof(EmMechine)).GetLength(0); i++)
                {
                    if ((t & (short)(Math.Pow(2,i))) == 1)
                    {
                        ls.Add((EmMechine)(short)(Math.Pow(2,i)));
                    }
                }
                return EmOperationResult.Success;
            }
            catch
            {
                return EmOperationResult.Fail;
            }
            finally
            {
                service.DisConnect();
            }
        }
    }
}
