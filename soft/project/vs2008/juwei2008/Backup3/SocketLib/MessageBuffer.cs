using System;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Contains original buffer and the read/send buffer and offset.
    /// </summary>
    internal class MessageBuffer
    {

        #region Fields

        private byte[] FRawBuffer;
        private byte[] FPacketBuffer;
        private int FPacketOffSet;

        #endregion

        #region Constructor

        public MessageBuffer(int bufferSize)
        {
            
            FPacketBuffer = null;

            if (bufferSize > 0)
            {
                FPacketBuffer = new byte[bufferSize];
            }

            FPacketOffSet = 0;
            FRawBuffer = null;

        }

        public MessageBuffer(ref byte[] rawBuffer, ref byte[] packetBuffer)
        {
            FRawBuffer = rawBuffer;
            FPacketBuffer = packetBuffer;
            FPacketOffSet = 0;
        }

        #endregion

        #region Methods

        #region GetPacketMessage

        /// <summary>
        /// Gets a packet message!
        /// </summary>
        /// <param name="connection">
        /// Socket connection.
        /// </param>
        /// <param name="buffer">
        /// Data.
        /// </param>
        public static MessageBuffer GetPacketMessage(BaseSocketConnection connection, ref byte[] buffer)
        {

            byte[] workBuffer = null;

            workBuffer = CryptUtils.EncryptData(connection, buffer);

            if (connection.Header != null && connection.Header.Length >= 0)
            //if(connection.Header == null)  //by nhb
            {
                //----- Need header!
                int headerSize = connection.Header.Length + 4;
                byte[] result = new byte[workBuffer.Length + headerSize];

                int messageLength = result.Length;

                //----- Header!
                for (int i = 0; i < connection.Header.Length; i++)
                {
                    result[i] = connection.Header[i];
                }
                
                //----- Length!
                //result[connection.Header.Length] = Convert.ToByte((messageLength & 0xFF00) >> 8);
                //result[connection.Header.Length + 1] = Convert.ToByte(messageLength & 0xFF);
                result[connection.Header.Length] = Convert.ToByte((messageLength & 0xFF000000) >> 24);
                result[connection.Header.Length + 1] = Convert.ToByte((messageLength & 0xFF0000) >> 16);
                result[connection.Header.Length + 2] = Convert.ToByte((messageLength & 0xFF00) >> 8);
                result[connection.Header.Length + 3] = Convert.ToByte(messageLength & 0xFF);
                Array.Copy(workBuffer, 0, result, headerSize, workBuffer.Length);

                return new MessageBuffer(ref buffer, ref result);

            }
            else
            {
                //----- No header!
                return new MessageBuffer(ref buffer, ref workBuffer);
            }

        }

        #endregion

        #region GetRawBuffer

        /// <summary>
        /// Get the buffer from packet message!
        /// </summary>
        /// <param name="messageLength">
        /// Message offset.
        /// </param>
        /// <param name="headerSize">
        /// Service header size.
        /// </param>
        public byte[] GetRawBuffer(int messageLength, int headerSize)
        {

            //----- Get Raw Buffer!
            byte[] result = null;

            result = new byte[messageLength - headerSize];
            Array.Copy(FPacketBuffer, headerSize, result, 0, result.Length);

            //----- Adjust Packet Buffer!
            byte[] packetBuffer = new byte[FPacketBuffer.Length - messageLength];
            Array.Copy(FPacketBuffer, messageLength, packetBuffer, 0, packetBuffer.Length);

            FPacketBuffer = packetBuffer;
            FPacketOffSet = FPacketOffSet - messageLength;

            return result;

        }

        #endregion

        #region Resize

        /// <summary>
        /// Resize the buffer.
        /// </summary>
        /// <param name="newLength">
        /// The new length of buffer.
        /// </param>
        public void Resize(int newLength)
        {
            byte[] buf = new byte[newLength];
            if (FPacketBuffer.Length > newLength)
            {
                Array.Copy(FPacketBuffer, 0, buf, 0, newLength);
            }
            else
            {
                Array.Copy(FPacketBuffer, 0, buf, 0, FPacketBuffer.Length);
            }
            FPacketBuffer = buf;

            //Array.Resize(ref FPacketBuffer, newLength);
        }

        #endregion

        #endregion

        #region Properties

        public byte[] RawBuffer
        {
            get { return FRawBuffer; }
            set { FRawBuffer = value; }
        }

        public byte[] PacketBuffer
        {
            get { return FPacketBuffer; }
            set { FPacketBuffer = value; }
        }

        public int PacketOffSet
        {
            get { return FPacketOffSet; }
            set { FPacketOffSet = value; }
        }

        public int PacketRemaining
        {
            get { return FPacketBuffer.Length - FPacketOffSet; }
        }

        public int PacketLength
        {
            get { return FPacketBuffer.Length; }
        }

        #endregion

    }

}
