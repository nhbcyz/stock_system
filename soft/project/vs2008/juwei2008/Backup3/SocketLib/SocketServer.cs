using System;
//using System.Threading;
using System.Net;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Server connection host.
    /// </summary>
    public class SocketServer : BaseSocketConnectionHost
    {

        #region Constructor

        public SocketServer(ISocketService socketService)
            : base(HostType.htServer, socketService, null, 2048, 16384, 0, 0, 60000, 60000)
        {
            //-----
        }

        public SocketServer(ISocketService socketService, byte[] header)
            : base(HostType.htServer, socketService, header, 2048, 16384, 0, 0, 60000, 60000)
        {
            //-----
        }

        public SocketServer(ISocketService socketService, byte[] header, int socketBufferSize, int messageBufferSize, int minThreads, int maxThreads)
            : base(HostType.htServer, socketService, header, socketBufferSize, messageBufferSize, minThreads, maxThreads, 60000, 60000)
        {
            //-----
        }

        public SocketServer(ISocketService socketService, byte[] header, int socketBufferSize, int messageBufferSize, int minThreads, int maxThreads, int idleCheckInterval, int idleTimeOutValue)
            : base(HostType.htServer, socketService, header, socketBufferSize, messageBufferSize, minThreads, maxThreads, idleCheckInterval, idleTimeOutValue)
        {
            //-----
        }

        #endregion

        #region Methods

        #region BeginReconnect

        internal override void BeginReconnect(ClientSocketConnection connection) { }

        #endregion

        #region BeginSendToAll

        internal override void BeginSendToAll(ServerSocketConnection connection, byte[] buffer)
        {

            if (!Disposed)
            {

                BaseSocketConnection[] items = GetSocketConnections();

                if (items != null)
                {

                    foreach (BaseSocketConnection cnn in items)
                    {

                        if (connection != cnn)
                        {
                            BeginSend(cnn, buffer);
                        }

                    }
                }
            }
        }

        #endregion

        #region BeginSendTo

        internal override void BeginSendTo(BaseSocketConnection connection, byte[] buffer)
        {

            if (!Disposed)
            {
                BeginSend(connection, buffer);
            }

        }

        #endregion

        #region GetConnectionById

        internal override BaseSocketConnection GetConnectionById(string connectionId)
        {

            BaseSocketConnection result = null;

            if (!Disposed)
            {
                result = GetSocketConnectionById(connectionId);
            }

            return result;

        }

        #endregion

        #region AddListener

        /// <summary>
        /// Add the server connector (SocketListener).
        /// </summary>
        /// <param name="localEndPoint"></param>
        public void AddListener(IPEndPoint localEndPoint)
        {
            AddListener(localEndPoint, EncryptType.etNone, CompressionType.ctNone, null, 5, 2);
        }

        public void AddListener(IPEndPoint localEndPoint, EncryptType encryptType, CompressionType compressionType, ICryptoService cryptoService)
        {
            AddListener(localEndPoint, encryptType, compressionType, cryptoService, 5, 2);
        }

        public void AddListener(IPEndPoint localEndPoint, EncryptType encryptType, CompressionType compressionType, ICryptoService cryptoService, byte backLog, byte acceptThreads)
        {
            if (!Disposed)
            {
                AddCreator(new SocketListener(this, localEndPoint, encryptType, compressionType, cryptoService, backLog, acceptThreads));
            }
        }

        #endregion

        #region Stop

        public override void Stop()
        {

            if (!Disposed)
            {

                StopCreators();
                StopConnections();

            }

            base.Stop();

        }

        #endregion

        #endregion

    }

}
