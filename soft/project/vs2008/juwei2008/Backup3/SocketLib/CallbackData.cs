using System;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Keeps connection event information between callbacks
    /// </summary>
    internal class CallbackData
    {

        #region Fields

        private BaseSocketConnection FConnection;
        private MessageBuffer FBuffer;

        #endregion

        #region Constructor

        public CallbackData(BaseSocketConnection connection, MessageBuffer buffer)
        {
            FConnection = connection;
            FBuffer = buffer;
        }

        #endregion

        #region Properties

        public BaseSocketConnection Connection
        {
            get { return FConnection; }
        }

        public MessageBuffer Buffer
        {
            get { return FBuffer; }
        }

        #endregion

    }

}
