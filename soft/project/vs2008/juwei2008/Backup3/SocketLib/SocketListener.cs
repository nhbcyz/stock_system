using System;
using System.Net;
using System.Net.Sockets;
//using System.Net.Security;
//using System.Security.Cryptography.X509Certificates;
//using System.Threading;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Server socket connector.
    /// </summary>
    internal class SocketListener : BaseSocketConnectionCreator
    {

        #region Fields

        private Socket FSocket;
        private byte FBackLog;
        private byte FAcceptThreads;

        //private ManualResetEvent allDone = new ManualResetEvent(false);

        #endregion

        #region Constructor

        /// <summary>
        /// Base SocketListener creator.
        /// </summary>
        /// <param name="host">
        /// Host.
        /// </param>
        /// <param name="localEndPoint">
        /// Local endpoint to be used.
        /// </param>
        /// <param name="encryptType">
        /// Encryption to be used.
        /// </param>
        /// <param name="compressionType">
        /// Compression to be used.
        /// </param>
        /// <param name="cryptoService">
        /// CryptoService. if null, will not be used.
        /// </param>
        /// <param name="backLog">
        /// Socket backlog queue number.
        /// </param>
        /// <param name="acceptThreads">
        /// Number of accept events to be used.
        /// </param>
        public SocketListener(BaseSocketConnectionHost host, IPEndPoint localEndPoint, EncryptType encryptType, CompressionType compressionType, ICryptoService cryptoService, byte backLog, byte acceptThreads)
            : base(host, localEndPoint, encryptType, compressionType, cryptoService)
        {
            FBackLog = backLog;
            FAcceptThreads = acceptThreads;
        }

        #endregion

        #region Free

        protected override void Free(bool dispodedByUser)
        {

            if (dispodedByUser)
            {
                FSocket.Close();
            }

            base.Free(dispodedByUser);
        }

        #endregion

        #region Methods

        #region Start

        internal override void Start()
        {
            if (!Disposed)
            {
                try
                {
                    FSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    FSocket.Bind(LocalEndPoint);
                    FSocket.Listen(FBackLog);

                    //----- Begin accept new connections!
                    for (int i = 1; i <= FAcceptThreads; i++)
                    {
                        FSocket.BeginAccept(new AsyncCallback(BeginAcceptCallback), this);
                    }
                }
                catch (Exception exOut)
                {
                    Stop();
                    Host.FireOnException(exOut);
                }
            }
        }

        #endregion

        #region BeginAcceptCallback

        /// <summary>
        /// Accept callback!
        /// </summary>
        internal void BeginAcceptCallback(IAsyncResult ar)
        {

            if (!Disposed)
            {
                SocketListener listener = null;
                Socket acceptedSocket = null;
                ServerSocketConnection connection = null;
                try
                {
                    listener = (SocketListener)ar.AsyncState;
                    //----- Get accepted socket!
                    acceptedSocket = listener.Socket.EndAccept(ar);
                    //----- Adjust buffer size!
                    #if Desktop
                    acceptedSocket.ReceiveBufferSize = Host.SocketBufferSize;
                    acceptedSocket.SendBufferSize = Host.SocketBufferSize;
                    #endif

                    //---- Continue to accept!
                    listener.Socket.BeginAccept(new AsyncCallback(BeginAcceptCallback), listener);
                    connection = new ServerSocketConnection(Host, acceptedSocket, this.EncryptType, this.CompressionType, Host.Header);
                    //----- Initialize!
                    Host.AddSocketConnection(connection);
                    InitializeConnection(connection);
                }
                catch (Exception exOut)
                {
                    if (connection != null)
                    {
                        try
                        {
                            connection.BeginDisconnect(exOut);
                        }
                        catch (Exception exInn)
                        {
                            Host.FireOnException(exInn);
                        }
                    }
                    else
                    {
                        Host.FireOnException(exOut);
                    }
                }
            }
        }

        #endregion

        #region Stop

        internal override void Stop()
        {
            Dispose();
        }

        #endregion

        #endregion

        #region Properties

        public byte BackLog
        {
            get { return FBackLog; }
        }

        public byte AcceptThreads
        {
            get { return FAcceptThreads; }
        }

        internal Socket Socket
        {
            get { return FSocket; }
        }

        #endregion

    }

}
