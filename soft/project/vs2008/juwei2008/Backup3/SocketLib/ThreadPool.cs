using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net.Sockets;


namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Callback items for HostThreadPool.
    /// </summary>
    internal class WaitCallbackItem
    {

        #region Fields

        private WaitCallback FWaitCallback;
        private ConnectionEventArgs FEventArgs;

        #endregion

        #region Constructor

        public WaitCallbackItem(WaitCallback waitCallback, ConnectionEventArgs e)
        {
            FWaitCallback = waitCallback;
            FEventArgs = e;
        }

        #endregion

        #region Properties

        public WaitCallback WaitCallBack
        {
            get { return FWaitCallback; }
        }

        public ConnectionEventArgs EventArgs
        {
            get { return FEventArgs; }
        }

        #endregion

    }

    /// <summary>
    /// Host thread pool for connection events.
    /// </summary>
    public class HostThreadPool: BaseClass
    {

        #region Fields

        private object FSyncThreads;
        private int FNumThreads;
        private int FActiveThreads;

        private int FMaxThreads;
        private int FMinThreads;
        private BaseSocketConnectionHost FHost;

        private List<Thread> FThreads;
        private Queue<WaitCallbackItem> FCallbackItems;

        private bool FActive;
        private object FSyncActive;

        #endregion

        #region Constructor

        public HostThreadPool(BaseSocketConnectionHost host, int minThreads, int maxThreads, int idleCheckInterval, int idleTimeOutValue)
        {

            FHost = host;

            FMaxThreads = maxThreads;
            FMinThreads = minThreads;

            FActiveThreads = 0;
            FNumThreads = 0;
            FSyncThreads = new object();

            FThreads = new List<Thread>(minThreads);
            FCallbackItems = new Queue<WaitCallbackItem>(50);

            FActive = false;
            FSyncActive = new object();

        
        }

        #endregion

        #region Free

        protected override void Free(bool dispodedByUser)
        {

            if (dispodedByUser)
            {
                FCallbackItems.Clear();
            }

            base.Free(dispodedByUser);
        }

        #endregion


        #region Methods

        #region Start

        public void Start()
        {
        
            if (!Disposed)
            {

                Active = true;

                for (int i = 0; i < FMinThreads; i++)
                {
                    AddThread();
                }
            
            }

        }

        #endregion

        #region AddThread

        private void AddThread()
        {

            Thread t = new Thread(Execute);
            t.IsBackground = true;
            t.Priority = ThreadPriority.Normal;

            FThreads.Add(t);
            FNumThreads++;

            t.Start();

        }

        #endregion

        #region Stop

        public void Stop()
        {

            if (!Disposed)
            {

                Active = false;
                
                lock (FCallbackItems)
                {
                    Monitor.PulseAll(FCallbackItems);
                }

                lock (FThreads)
                {
                 
                    if (FThreads.Count > 0)
                    {
                    
                        for (int i = 0; i < FThreads.Count; i++)
                        {
                            FThreads[i].Join(5000);
                        }

                    }

                }

            }

        }

        #endregion

        #region Execute

        private void Execute()
        {

            WaitCallbackItem item = null;

            while (Active)
            {

                item = null;

                lock (FCallbackItems)
                {
                    
                    if (FCallbackItems.Count > 0)
                    {
                        //----- if has items, dequeue!
                        item = FCallbackItems.Dequeue();
                    }
                    else
                    {
                        //----- if has no items, wait!
                        Monitor.Wait(FCallbackItems);
                    }

                }

                if (item != null)
                {

                    lock(FSyncThreads)
                    {

                        //---- Increment active threads!
                        FActiveThreads++;

                        if (FActiveThreads < FMaxThreads)
                        {

                            if (FActiveThreads == FNumThreads && FNumThreads < FMaxThreads)
                            {
                                //---- Add thread!
                                AddThread();
                            }

                        }

                    }

                    BaseSocketConnection connection = (BaseSocketConnection) item.EventArgs.Connection;

                    try
                    {
                        //----- Execute the callback method!
                        item.WaitCallBack(item.EventArgs);
                    }
                    catch (Exception exOut)
                    {
                        
                        if (item.EventArgs is DisconnectedEventArgs)
                        {
                            //----- Disconnecting!
                            FHost.FireOnException(exOut);
                        }
                        else
                        {

                            try
                            {
                                connection.BeginDisconnect(exOut);
                            }
                            catch (Exception exInn)
                            {
                                FHost.FireOnException(exInn);
                            }

                        }

                    }

                    lock (FSyncThreads)
                    {
                        //----- Decrement active threads!
                        FActiveThreads--;
                    }
                    
                }
            
            }

            lock (FSyncThreads)
            {
                FNumThreads--;
            }

        }

        #endregion

        #region Enqueue

        public void Enqueue(WaitCallback waitCallback, ConnectionEventArgs e)
        {

            lock (FCallbackItems)
            {
                FCallbackItems.Enqueue(new WaitCallbackItem(waitCallback, e));
                Monitor.Pulse(FCallbackItems);
            }
        
        }

        #endregion

        #endregion



        #region Properties

        public bool Active
        {
            set
            {
                lock (FSyncActive)
                {
                    FActive = value;
                }

            }

            get
            {
                
                bool result = false;

                lock (FSyncActive)
                {
                    result = FActive;
                }                    

                return result;

            }
        }

        #endregion

    }

}
