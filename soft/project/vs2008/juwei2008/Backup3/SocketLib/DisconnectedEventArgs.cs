using System;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Disconnect event arguments for disconnected event.
    /// </summary>
    public class DisconnectedEventArgs : ConnectionEventArgs
    {

        #region Fields

        private Exception FException;

        #endregion

        #region Constructor

        public DisconnectedEventArgs(ISocketConnection connection, Exception exception)
            : base(connection)
        {
            FException = exception;
        }

        #endregion

        #region Properties

        public Exception Exception
        {
            get { return FException; }
        }

        #endregion

    }

}
