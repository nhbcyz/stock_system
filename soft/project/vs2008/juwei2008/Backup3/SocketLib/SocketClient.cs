using System;
//using System.Threading;
using System.Net;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Socket client host.
    /// </summary>
    public class SocketClient : BaseSocketConnectionHost
    {

        #region Constructor

        public SocketClient(ISocketService socketService)
            : base(HostType.htClient, socketService, null, 8192, 16384, 0, 0, 60000, 60000)
        {
            //-----
        }

        public SocketClient(ISocketService socketService, byte[] header)
            : base(HostType.htClient, socketService, header, 8192, 16384, 0, 0, 60000, 60000)
        {
            //-----
        }

        public SocketClient(ISocketService socketService, byte[] header, int socketBufferSize, int messageBufferSize, int minThreads, int maxThreads)
            : base(HostType.htClient, socketService, header, socketBufferSize, messageBufferSize, minThreads, maxThreads, 60000, 60000)
        {
            //-----
        }

        public SocketClient(ISocketService socketService, byte[] header, int socketBufferSize, int messageBufferSize, int minThreads, int maxThreads, int idleCheckInterval, int idleTimeOutValue)
            : base(HostType.htClient, socketService, header, socketBufferSize, messageBufferSize, minThreads, maxThreads, idleCheckInterval, idleTimeOutValue)
        {
            //-----
        }

        #endregion

        #region Methods

        #region BeginReconnect

        /// <summary>
        /// Reconnects the connection adjusting the reconnect timer.
        /// </summary>
        /// <param name="connection">
        /// Connection.
        /// </param>
        /// <param name="sleepTimeOutValue">
        /// Sleep timeout before reconnect.
        /// </param>
        internal override void BeginReconnect(ClientSocketConnection connection)
        {
            if (!Disposed)
            {
                connection.Creator.Reconnect(true);
            }

        }

        #endregion

        #region BeginSendToAll

        internal override void BeginSendToAll(ServerSocketConnection connection, byte[] buffer) { }

        #endregion

        #region BeginSendTo

        internal override void BeginSendTo(BaseSocketConnection connectionTo, byte[] buffer) { }

        #endregion

        #region GetConnectionByHandle

        internal override BaseSocketConnection GetConnectionById(string connectionId) { return null; }

        #endregion

        #region AddClient

        /// <summary>
        /// Adds the client connector (SocketConnector).
        /// </summary>
        /// <param name="remoteEndPoint"></param>
        public void AddConnector(IPEndPoint remoteEndPoint)
        {
            AddConnector(remoteEndPoint, EncryptType.etNone, CompressionType.ctNone, null, 0, 0, new IPEndPoint(IPAddress.Any, 0));
        }

        public void AddConnector(IPEndPoint remoteEndPoint, EncryptType encryptType, CompressionType compressionType, ICryptoService cryptoService)
        {
            AddConnector(remoteEndPoint, encryptType, compressionType, cryptoService, 0, 0, new IPEndPoint(IPAddress.Any, 0));
        }

        public void AddConnector(IPEndPoint remoteEndPoint, EncryptType encryptType, CompressionType compressionType, ICryptoService cryptoService, int reconnectAttempts, int reconnectAttemptInterval)
        {
            AddCreator(new SocketConnector(this, remoteEndPoint, encryptType, compressionType, cryptoService, reconnectAttempts, reconnectAttemptInterval, new IPEndPoint(IPAddress.Any, 0)));
        }

        public void AddConnector(IPEndPoint remoteEndPoint, EncryptType encryptType, CompressionType compressionType, ICryptoService cryptoService, int reconnectAttempts, int reconnectAttemptInterval, IPEndPoint localEndPoint)
        {
            if (!Disposed)
            {
                AddCreator(new SocketConnector(this, remoteEndPoint, encryptType, compressionType, cryptoService, reconnectAttempts, reconnectAttemptInterval, localEndPoint));
            }
        }

        #endregion

        #region Stop

        public override void Stop()
        {

            if (!Disposed)
            {
                StopConnections();
                StopCreators();//pzg 080717 将这两行代码更换顺序
                
            }

            base.Stop();

        }

        #endregion

        #endregion

    }

}
