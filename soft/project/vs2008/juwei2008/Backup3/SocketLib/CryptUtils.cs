using System;
using System.IO;
//using System.IO.Compression;
using System.Security.Cryptography;
using System.Diagnostics;

namespace XY.SystemEx.SocketsEx
{

    /// <summary>
    /// Crypt tools.
    /// </summary>
    internal static class CryptUtils
    {

        #region CreateSymmetricAlgoritm

        /// <summary>
        /// Creates an asymmetric algoritm.
        /// </summary>
        /// <param name="encryptType">
        /// Encrypt type.
        /// </param>
        public static SymmetricAlgorithm CreateSymmetricAlgoritm(EncryptType encryptType)
        {

            SymmetricAlgorithm result = null;

            switch (encryptType)
            {

                case EncryptType.etTripleDES:
                    {

                        result = new TripleDESCryptoServiceProvider();

                        result.KeySize = 192;
                        result.BlockSize = 64;

                        break;
                    }

                case EncryptType.etRijndael:
                    {

                        result = new RijndaelManaged();

                        result.KeySize = 256;
                        result.BlockSize = 256;

                        break;
                    }

            }

            if (result != null)
            {
                
                result.Mode = CipherMode.CBC;
                result.Padding = PaddingMode.ISO10126;

            }

            return result;

        }

        #endregion

        #region EncryptDataForAuthenticate

        /// <summary>
        /// Encrypts using default padding.
        /// </summary>
        /// <param name="buffer">
        /// Data to be rncrypted
        /// </param>
        public static byte[] EncryptDataForAuthenticate(SymmetricAlgorithm sa, byte[] buffer, PaddingMode padding)
        {

            using (MemoryStream ms = new MemoryStream())
            {

                sa.Padding = padding;

                using (CryptoStream cs = new CryptoStream(ms, sa.CreateEncryptor(), CryptoStreamMode.Write))
                {

                    sa.Padding = padding;
                    cs.Write(buffer, 0, buffer.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();

                }

            }

        }

        #endregion

        #region DecryptDataForAuthenticate

        /// <summary>
        /// Encrypts using default padding.
        /// </summary>
        /// <param name="buffer">
        /// Data to be rncrypted
        /// </param>
        public static byte[] DecryptDataForAuthenticate(SymmetricAlgorithm sa, byte[] buffer, PaddingMode padding)
        {

            using (MemoryStream ms = new MemoryStream(buffer))
            {
                
                sa.Padding = padding;

                using (CryptoStream cs = new CryptoStream(ms, sa.CreateDecryptor(), CryptoStreamMode.Read))
                using (BinaryReader b = new BinaryReader(cs))
                {
                    ms.Position = 0;
                    sa.Padding = padding;
                    return b.ReadBytes(16384);
                }

            }

        }

        #endregion

        #region EncryptData

        /// <summary>
        /// Encrypts the data.
        /// </summary>
        /// <param name="connection">
        /// Connection information.
        /// </param>
        /// <param name="buffer">
        /// Data to be encrypted.
        /// </param>
        /// <param name="signOnly">
        /// Indicates is encrypt method only uses symmetric algoritm.
        /// </param>
        public static byte[] EncryptData(BaseSocketConnection connection, byte[] buffer)
        {

            if (
                 (connection.EncryptType == EncryptType.etSSL && connection.CompressionType == CompressionType.ctNone) ||
                 (connection.EncryptType == EncryptType.etNone && connection.CompressionType == CompressionType.ctNone)
                )
            {
                return buffer;
            }
            else
            { 
                /*
                using(MemoryStream ms = new MemoryStream())
                {

                    CryptoStream cs = null;
                    //GZipStream gs = null;

                    switch (connection.EncryptType)
                    {

                        case EncryptType.etNone:
                        case EncryptType.etSSL:
                            {
                                break;
                            }

                        case EncryptType.etBase64:
                            {
                                cs = new CryptoStream(ms, new ToBase64Transform(), CryptoStreamMode.Write);
                                break;
                            }

                        default:
                            {
                                cs = new CryptoStream(ms, connection.Encryptor, CryptoStreamMode.Write);
                                break;
                            }
                    }

                    //switch (connection.CompressionType)
                    //{
                    //    case CompressionType.ctGZIP:
                    //        {
                    //            if (cs != null)
                    //            {
                    //                gs = new GZipStream(cs, CompressionMode.Compress, true);
                    //            }
                    //            else
                    //            {
                    //                gs = new GZipStream(ms, CompressionMode.Compress, true);
                    //            }
                    //            break;
                    //        }
                    //}
                    //if (gs != null)
                    //{
                    //    gs.Write(buffer, 0, buffer.Length);
                    //    gs.Close();
                    //    gs.Dispose();
                    //}
                    //else
                    //{
                        cs.Write(buffer, 0, buffer.Length);
                    //}

                    if (cs != null)
                    {
                        cs.FlushFinalBlock();
                        cs.Close();
                        cs.Dispose();
                    }
                    byte[] result = ms.ToArray();
                    return result;
                }*/
                return buffer;
            }
        }

        #endregion

        #region DecryptData

        /// <summary>
        /// Decrypts the data.
        /// </summary>
        /// <param name="connection">
        /// Connection information.
        /// </param>
        /// <param name="buffer">
        /// Data to be encrypted.
        /// </param>
        /// <param name="maxBufferSize">
        /// Max buffer size accepted.
        /// </param>
        public static byte[] DecryptData(BaseSocketConnection connection, ref byte[] buffer, int maxBufferSize)
        {

            if (
                 (connection.EncryptType == EncryptType.etSSL && connection.CompressionType == CompressionType.ctNone) ||
                 (connection.EncryptType == EncryptType.etNone && connection.CompressionType == CompressionType.ctNone)
                )
            {
                return buffer;
            }
            else
            {
                /*
                MemoryStream ms = new MemoryStream(buffer);
                CryptoStream cs = null;
                //GZipStream gs = null;

                switch (connection.EncryptType)
                {

                    case EncryptType.etNone:
                    case EncryptType.etSSL:
                        {
                            break;
                        }

                    case EncryptType.etBase64:
                        {
                            cs = new CryptoStream(ms, new FromBase64Transform(), CryptoStreamMode.Read);
                            break;
                        }

                    default:
                        {
                            cs = new CryptoStream(ms, connection.Decryptor, CryptoStreamMode.Read);
                            break;
                        }
                }
                BinaryReader b = null;

                //switch (connection.CompressionType)
                //{
                //    case CompressionType.ctGZIP:
                //        {
                //            if (cs != null)
                //            {
                //                gs = new GZipStream(cs, CompressionMode.Decompress, true);
                //            }
                //            else
                //            {
                //                gs = new GZipStream(ms, CompressionMode.Decompress, true);
                //            }
                //            break;
                //        }
                //}
                //if (gs != null)
                //{
                //    b = new BinaryReader(gs);
                //}
                //else
                //{
                    b = new BinaryReader(cs);
                //}
                byte[] result = b.ReadBytes(maxBufferSize);
                b.BaseStream.Close();
                b.Close();
                return result;
                */
                return buffer;
            }
        }
        #endregion

    }

}
