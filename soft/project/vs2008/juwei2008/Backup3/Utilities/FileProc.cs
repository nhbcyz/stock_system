﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using HZDrawPublish.Core;
using System.IO;
using System.Runtime.InteropServices;

namespace HZDrawPublish.Utilities
{
    public class FileProc
    {
        /// <summary>
        /// 文件分片
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<SliceFileModel> File2Slice(string path)
        {
            List<SliceFileModel> list = new List<SliceFileModel>();
            try
            {
                if (File.Exists(path))
                {
                    using (var fileStream = new FileStream(path, FileMode.Open))
                    {
                        //尚未读取的文件内容长度
                        long left = fileStream.Length;
                        //每次读取的文件长度
                        int maxlength = AppCommon.Instance.FileSliceLength;
                        //读取位置
                        int start = 0;
                        //分片序号
                        int index = 0;
                        //
                        byte[] bs = null;
                        //实际长度
                        int num = 0;
                        //当文件未读取长度大于0时，不断进行读取  
                        while (left > 0)
                        {
                            index++;
                            fileStream.Position = start;
                            num = 0;
                            if (left < maxlength)
                            {
                                bs = new byte[left];
                                num = fileStream.Read(bs,0,Convert.ToInt32(left));
                            }
                            else
                            {
                                bs = new byte[maxlength];
                                num = fileStream.Read(bs, 0, maxlength);
                            }

                            if (num == 0)
                            {
                                break;
                            }
                            SliceFileModel sfm = new SliceFileModel() { Length = num,ID=index };
                            sfm.Content = new byte[bs.Length];
                            Array.Copy(bs, sfm.Content, bs.Length);
                            list.Add(sfm);
                            start += num;
                            left -= num;
                        }
                    }
                }
            }
            catch (Exception)
            {
                list = null;
                throw;
            }

            return list;
        }

        public static void CreateFolder(string folder)
        {
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
        }


        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto, Pack = 1)]
        public struct SHFILEOPSTRUCT
        {
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.U4)]
            public int wFunc;
            public string pFrom;
            public string pTo;
            public short fFlags;
            [MarshalAs(UnmanagedType.Bool)]
            public bool fAnyOperationsAborted;
            public IntPtr hNameMappings;
            public string lpszProgressTitle;
        }

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        private static extern int SHFileOperation(ref SHFILEOPSTRUCT FileOp);

        public const int FO_DELETE = 3;
        public const int FOF_ALLOWUNDO = 0x40;
        public const int FOF_NOCONFIRMATION = 0x10;

        /// <summary>
        /// 强制删除文件或文件夹,文件不会存放于回收站
        /// </summary>
        /// <param name="file"></param>
        public static void Delete(string file)
        {
            var shf = new SHFILEOPSTRUCT();
            shf.wFunc = FO_DELETE;
            shf.fFlags = FOF_NOCONFIRMATION; 
            shf.pFrom = file + '\0' + '\0'; //File / Folder to delete
            SHFileOperation(ref shf);
        }
    }
}
