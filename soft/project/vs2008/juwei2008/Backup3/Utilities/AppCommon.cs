﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace HZDrawPublish.Utilities
{
    public class AppCommon
    {
        private static AppCommon _instance = null;

        public static AppCommon Instance
        {
            get
            {
                if (_instance == null)
                { _instance = new AppCommon(); }
                return _instance;
            }
        }

        private AppCommon()
        {

        }

        public string AppPath
        {
            get
            {
                DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
                return di.FullName.ToString().ToUpper() ;
            }
        }

        /// <summary>
        /// 文件分片长度
        /// </summary>
        public int FileSliceLength
        {
            get
            {
                return 8168*20;
            }
        }

        public long StationID
        {
            get { return 111111111111; }
        }
    }
}
