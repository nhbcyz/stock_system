﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JueWei.Model
{
    [Serializable]
    public class ims_product_order
    {
        
        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// 物料编号
        /// </summary>		
        private string _materialno;
        public string Materialno
        {
            get { return _materialno; }
            set { _materialno = value; }
        }
        /// <summary>
        /// 订单编号
        /// </summary>		
        private string _orderno;
        public string Orderno
        {
            get { return _orderno; }
            set { _orderno = value; }
        }
        /// <summary>
        /// 完成日期
        /// </summary>		
        private DateTime _completedate;
        public DateTime Completedate
        {
            get { return _completedate; }
            set { _completedate = value; }
        }
        /// <summary>
        /// 移动类型
        /// </summary>		
        private string _mobiletype;
        public string Mobiletype
        {
            get { return _mobiletype; }
            set { _mobiletype = value; }
        }
       /// <summary>
        /// 库存地点
        /// </summary>		
        private string _stockaddr;
        public string Stockaddr
        {
            get { return _stockaddr; }
            set { _stockaddr = value; }
        }
       /// <summary>
        /// 库存地点
        /// </summary>		
        private string _factory;
        public string Factory
        {
            get { return _factory; }
            set { _factory = value; }
        }
        /// <summary>
        /// 修改时间
        /// </summary>		
        private int _basicnum;
        public int Basicnum
        {
            get { return _basicnum; }
            set { _basicnum = value; }
        }
        /// <summary>
        /// 修改时间
        /// </summary>		
        private int _scannum = 0;
        public int Scannum
        {
            get { return _scannum; }
            set { _scannum = value; }
        }
        /// <summary>
        /// 关联设备
        /// </summary>		
        private string _basicunit;
        public string Basicunit
        {
            get { return _basicunit; }
            set { _basicunit = value; }
        }


        /// <summary>
        /// 关联设备
        /// </summary>		
        private string _sapverifyno;
        public string Sapverifyno
        {
            get { return _sapverifyno; }
            set { _sapverifyno = value; }
        }

        /// <summary>
        /// 关联设备
        /// </summary>		
        private string _sapverifytime;
        public string Sapverifytime
        {
            get { return _sapverifytime; }
            set { _sapverifytime = value; }
        }
        public ims_product_order() { }
        public ims_product_order(int id, string orderno, DateTime completedate, string materialno, 
            string mobiletype, string stockaddr, string factory, int basicnum, string basicunit,
            int scannum, string sapverifyno, string sapverifytime)
        {
            _id = id;
            _materialno = materialno;
            _orderno = orderno;
            _completedate = completedate;
            _mobiletype = mobiletype;
            _stockaddr = stockaddr;
            _factory = factory;
            _basicnum = basicnum;
            _basicunit = basicunit;
            _scannum = scannum;
            _sapverifyno = sapverifyno;
            _sapverifytime = sapverifytime;
        }
    }
}
