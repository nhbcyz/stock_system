﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using JueWei.DBUtility;
using JueWei.Common;
using System.Data;
using JueWei.Bussiness;
using WHC.OrderWater.Commons;
using System.Text;
using Newtonsoft.Json;

namespace JueWei.WebService
{
    /// <summary>
    /// ReqSchedule 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。
    // [System.Web.Script.Services.ScriptService]
    public class ReqSchedule : System.Web.Services.WebService
    {
        [WebMethod]
        public string ReqDayOrder()
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;
            DataSet ds = BLL.ims_devices.GetList("");
            return JsonHelper.DataTable2Json(ds.Tables[0]);
        }

        [WebMethod]
        public string PostProduct(string data)
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;

            try
            {
                Model.ims_instock_record record = ScanDataTransfer.Str2Record(data);
                if (record == null)
                {
                    return "-1";
                }
                if (!BLL.ims_instock_record.Insert(record))
                {
                    LogHelper.Error(string.Format("写入入库记录时失败,物料编号为:{0}\n", record.Materialno));
                    return "-1";
                }
            }
            catch (System.Exception ex)
            {
                return "-1";
            }

            return "0";
        }

        [WebMethod]
        public string ReqHasScanedProduct()
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;
            DataSet ds = BLL.ims_instock_record.GetHasScanedProductNum(DateTime.Now);
            return JsonHelper.DataTable2Json(ds.Tables[0]);
        }


        [WebMethod]
        public int GetDeviceState(string deviceid)
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;
            try
            {
                Model.ims_devices d = BLL.ims_devices.SelectById(int.Parse(deviceid));
                if (d != null)
                {
                    return d.Devicestate;
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            
            return 0;
        }

        [WebMethod]
        public void DeviceReqStop(string deviceid)
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;
            try
            {
                Model.ims_devices d = BLL.ims_devices.SelectById(int.Parse(deviceid));
                if (d != null)
                {
                    d.Devicestate = 0;
                    BLL.ims_devices.Update(d);
                }
            }
            catch (System.Exception ex)
            {

            }
        }

        [WebMethod]
        public string GetNextShopOrder(string deviceid)
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;
            try
            {
                Model.ims_settings _ims_settings = BLL.ims_settings.SelectById("current_order");
                if (_ims_settings == null)
                {
                    return "1"; //当前为启动扫码工作
                }

                JueWei.Bussiness.OrderSetting _order_setting = JsonConvert.DeserializeObject<Bussiness.OrderSetting>(_ims_settings.Setting_data);
                if (_order_setting == null)
                {
                    return "1";//当前为启动扫码工作
                }

                if (_order_setting.SelectDate == "" || _order_setting.LineNo == "")
                {
                    return "1";//当前为启动扫码工作
                }

                DataSet set = GetNextOrder(deviceid, _order_setting);
                if (CheckDsIsNull(set)) //判断本门店当品已经扫码完成
                {
                    return "0";//扫码已完成
                }

                return JsonHelper.DataTable2Json(set.Tables[0]); ;
            }
            catch (System.Exception ex)
            {
                return "5"; //未知异常
            }
        }

        [WebMethod]
        public string PostOrder(string deviceid, string shopno, string data)
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;
            try
            {
                if (string.IsNullOrEmpty(deviceid) || string.IsNullOrEmpty(shopno) || string.IsNullOrEmpty(data))
                {
                    return "-1"; //参数错误
                }
                Model.ims_instock_record record = ScanDataTransfer.Str2Record(data);
                if (record == null)
                {
                    return "-2"; //扫码参数错误
                }

                string sql = string.Format("update ims_orders set scannum = scannum + 1 where materialno = '{0}' and shopno = '{1}' and deviceid = {2} and state = 0 ;", record.Materialno, shopno, deviceid);
                if (DbHelperMySQL.ExecuteSql(sql) > 0)
                {
                    sql = string.Format("update ims_orders set state = 1 where materialno = '{0}' and shopno = '{1}' and deviceid = {2} and  scannum = verifynum;", record.Materialno, shopno, deviceid);
                    if (DbHelperMySQL.ExecuteSql(sql) > 0)
                    {
                        return "6";
                    }
                    else
                    {
                        return GetNextShopOrder(deviceid);
                    }
                }
                else
                {
                    sql = string.Format("select count(1) from ims_orders where materialno = '{0}' and shopno = '{1}' and deviceid = {2} and state = 0 ;", record.Materialno, shopno, deviceid);
                    DataSet ds = DbHelperMySQL.Query(sql);
                    if (ds != null && ds.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        return "6";
                    }
                    else
                    {
                        return "-3"; //入库失败
                    }
                   
                }
                

            }
            catch (Exception)
            {
                return "-4"; //未知异常
            }

        }
        private bool CheckDsIsNull(DataSet set)
        {         
            if (set == null || set.Tables[0].Rows.Count == 0)
            {
                return true; //当前线路没有待扫码的订单
            }
            return false;
        }

        private DataSet GetNextOrder(string deviceid, JueWei.Bussiness.OrderSetting _order_setting)
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;
            return BLL.ims_orders.GetCurrentNoScanOrderByDeviceID(_order_setting.SelectDate, _order_setting.LineNo, Convert.ToInt32(deviceid));
        }

        [WebMethod]
        public string Test1()
        {
            DbHelperMySQL.connectionString = GlobalVar.DBConnString;
            DataSet ds = BLL.ims_devices.GetList("");
            return JsonHelper.DataTable2Json(ds.Tables[0]);
        }
    }
}
