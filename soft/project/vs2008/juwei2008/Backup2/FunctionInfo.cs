﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JueWei.Bussiness
{
    [Serializable]
    public class FunctionInfo : BaseEntity
    {
        public FunctionInfo()
        {

        }

        public virtual string ControlID { get; set; }
        public int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual int PID { get; set; }
    }


}
