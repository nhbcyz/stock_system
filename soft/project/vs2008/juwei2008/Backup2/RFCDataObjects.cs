﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JueWei.Bussiness
{
    public class RFC_BaseObj
    {

    }

    public class RFC_MATNR : RFC_BaseObj
    {
        public string MATNR { get; set; }
        public string MAKTX { get; set; }
        public string VRKME { get; set; }
        public string MEINS { get; set; }
        public float ZWEIGHT { get; set; }
    }

    public class RFC_KUNNR : RFC_BaseObj
    {
        public string KUNNR { get; set; }
        public string NAME1 { get; set; }
        public string MDLXR { get; set; }
        public string MDTEL { get; set; }
        public string STREET { get; set; }
        public string ZYLX { get; set; }
        public string TZYLX { get; set; }
        public string ZYXS { get; set; }
    }

    public class RFC_AUFNR : RFC_BaseObj
    {
        public string AUFNR { get; set; }
        public string GLTRP { get; set; }
        public string MATNR { get; set; }
        public string BWART { get; set; }
        public string LGORT { get; set; }
        public string WERKS { get; set; }
        public float SBMNG { get; set; }
        public string SBMEH { get; set; }
    }

    public class RFC_AUFNR_OUT : RFC_BaseObj
    {
        public string AUFNR { get; set; }
        public string GLTRP { get; set; }
        public string MATNR { get; set; }
        public string BWART { get; set; }
        public string LGORT { get; set; }
        public string WERKS { get; set; }
        public float SBMNG { get; set; }
        public string SBMEH { get; set; }

        public string Error { get; set; }
        public string MBLNR { get; set; }
        public string MJAHR { get; set; }
    }

    public class RFC_GOODSMVT : RFC_BaseObj
    {

        public string AUFNR { get; set; }
        public string GLTRP { get; set; }
        public string MATNR { get; set; }
        public string BWART { get; set; }
        public string LGORT { get; set; }
        public string WERKS { get; set; }
        public float SBMNG { get; set; }
        public string SBMEH { get; set; }
    }

    public class RFC_EBELN : RFC_BaseObj
    {
        public string EBELN { get; set; }
        public string POSNR { get; set; }
        public string MATNR { get; set; }
        public string MAKTX { get; set; }
        public string LFDAT { get; set; }
        public string BLDAT { get; set; }
        public string WADAT { get; set; }
        public float LFIMG { get; set; }
        public string VRKME { get; set; }
        public string KUNNR { get; set; }
        public string NAME1 { get; set; }
        public string STREET { get; set; }

    }

    public class RFC_Comm_Obj_Out
    {
        public bool Error { get; set; }
        public string ErrorMsg { get; set; }
        public List<RFC_BaseObj> Data { get; set; }
        public RFC_Comm_Obj_Out()
        {
            Error = false;
            ErrorMsg = "";
            Data = null;
        }
    }
}
