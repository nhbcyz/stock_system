﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAP.Middleware.Connector;
using System.Data;

namespace JueWei.Bussiness
{
    public class RFCComm : IRFCComm
    {
        #region IRFCComm 成员

        public RFC_Comm_Obj_Out ZMM_GET_MATNR(string FacNo, string DateStr)
        {
            RFC_Comm_Obj_Out rfcdata = new RFC_Comm_Obj_Out();
            RfcRepository repo = prd.Repository;
            try
            {
                
                IRfcFunction companyBapi = repo.CreateFunction("ZMM_GET_MATNR");   //调用函数名
                companyBapi.SetValue("IM_WERKS", FacNo);   //设置Import的参数
                companyBapi.SetValue("IM_DATUM", DateStr);   //设置Import的参数
                companyBapi.Invoke(prd);   //执行函数
                rfcdata.Error = Error(companyBapi.GetValue("EM_RETRUN").ToString());
                rfcdata.ErrorMsg = companyBapi.GetValue("EM_MESSAGE").ToString();
                if (!rfcdata.Error)
                {
                    return ErrorMsg(rfcdata.ErrorMsg);
                }
                IRfcTable table = companyBapi.GetTable("ET_MATNR");
                DataTable dt = GetDataTableFromRFCTable(table);
                rfcdata.Data = new List<RFC_BaseObj>();
                for (int i = 0; i < table.RowCount; i++)
                {
                    DataRow dr = dt.Rows[i];
                    RFC_MATNR matnr = new RFC_MATNR();
                    matnr.MATNR = dr["MATNR"].ToString();
                    matnr.MAKTX = dr["MAKTX"].ToString();
                    matnr.VRKME = dr["VRKME"].ToString();
                    matnr.MEINS = dr["MEINS"].ToString();
                    float dweight = 0.0f;
                    if (dr["ZWEIGHT"] != null && dr["ZWEIGHT"].ToString() != "")
                    {
                        float.TryParse(dr["ZWEIGHT"].ToString(), out dweight);
                    }
                    matnr.ZWEIGHT = dweight;
                    rfcdata.Data.Add(matnr);
                }
                return rfcdata;
            }
            catch (System.Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
            finally
            {
                prd = null;
                repo = null;
            }
        }

        public RFC_Comm_Obj_Out ZMM_GET_KUNNR(string FacNo, string DateStr)
        {
            RFC_Comm_Obj_Out rfcdata = new RFC_Comm_Obj_Out();
            RfcRepository repo = prd.Repository;
            try
            {

                IRfcFunction companyBapi = repo.CreateFunction("ZMM_GET_KUNNR");   //调用函数名
                companyBapi.SetValue("IM_WERKS", FacNo);   //设置Import的参数
                companyBapi.SetValue("IM_DATUM", DateStr);   //设置Import的参数
                companyBapi.Invoke(prd);   //执行函数
                rfcdata.Error = Error(companyBapi.GetValue("EM_RETRUN").ToString());
                rfcdata.ErrorMsg = companyBapi.GetValue("EM_MESSAGE").ToString();
                if (!rfcdata.Error)
                {
                    return ErrorMsg(rfcdata.ErrorMsg);
                }
                IRfcTable table = companyBapi.GetTable("ET_KUNNR");
                DataTable dt = GetDataTableFromRFCTable(table);
                rfcdata.Data = new List<RFC_BaseObj>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    RFC_KUNNR kunnr = new RFC_KUNNR();
                    kunnr.KUNNR = dr["KUNNR"].ToString();
                    kunnr.NAME1 = dr["NAME1"].ToString();
                    kunnr.MDLXR = dr["MDLXR"].ToString();
                    kunnr.MDTEL = dr["MDTEL"].ToString();
                    kunnr.STREET = dr["STREET"].ToString();
                    kunnr.ZYLX = dr["ZYLX"].ToString();
                    kunnr.TZYLX = dr["TZYLX"].ToString();
                    kunnr.ZYXS = dr["ZYXS"].ToString();
                    rfcdata.Data.Add(kunnr);
                }
                return rfcdata;
            }
            catch (System.Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
            finally
            {
                prd = null;
                repo = null;
            }
        }

        public RFC_Comm_Obj_Out ZMM_GET_AUFNR(string FacNo, string DateStr, string MaterialNo)
        {
            RFC_Comm_Obj_Out rfcdata = new RFC_Comm_Obj_Out();
            RfcRepository repo = prd.Repository;
            try
            {

                IRfcFunction companyBapi = repo.CreateFunction("ZMM_GET_AUFNR");   //调用函数名
                companyBapi.SetValue("IM_WERKS", FacNo);   //设置Import的参数
                companyBapi.SetValue("IM_GLTRP", DateStr);   //设置Import的参数
                companyBapi.SetValue("IM_MATNR", MaterialNo);   //设置Import的参数
                companyBapi.Invoke(prd);   //执行函数
                rfcdata.Error = Error(companyBapi.GetValue("EM_RETRUN").ToString());
                rfcdata.ErrorMsg = companyBapi.GetValue("EM_MESSAGE").ToString();
                if (!rfcdata.Error)
                {
                    return ErrorMsg(rfcdata.ErrorMsg);
                }
                IRfcTable table = companyBapi.GetTable("ET_AUFNR");
                DataTable dt = GetDataTableFromRFCTable(table);
                rfcdata.Data = new List<RFC_BaseObj>();
                for (int i = 0; i < table.RowCount; i++)
                {
                    RFC_AUFNR aufnr = new RFC_AUFNR();
                    DataRow dr = dt.Rows[i];
                    aufnr.AUFNR = dr["AUFNR"].ToString();
                    aufnr.GLTRP = dr["GLTRP"].ToString();
                    aufnr.MATNR = dr["MATNR"].ToString();
                    aufnr.BWART = dr["BWART"].ToString();
                    aufnr.LGORT = dr["LGORT"].ToString();
                    aufnr.WERKS = dr["WERKS"].ToString();
                    string mng = dr["SBMNG"].ToString();
                    float fm = 0.0f;
                    float.TryParse(mng,out fm);
                    aufnr.SBMNG = fm;
                    aufnr.SBMEH = dr["SBMEH"].ToString();
                    rfcdata.Data.Add(aufnr);
                }
                return rfcdata;
            }
            catch (System.Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
            finally
            {
                prd = null;
                repo = null;
            }
        }

        public RFC_Comm_Obj_Out ZMM_GOODSMVT_CREATE(List<RFC_AUFNR> goods)
        {
            RFC_Comm_Obj_Out rfcdata = new RFC_Comm_Obj_Out();
            RfcRepository repo = prd.Repository;
            try
            {

                IRfcFunction companyBapi = repo.CreateFunction("ZMM_GOODSMVT_CREATE");   //调用函数名    

                IRfcTable table = companyBapi.GetTable("IT_AUFNR");
                for (int i = 0; i < goods.Count; i++)
                {
                    table.Append();
                    table.SetValue("AUFNR", goods[i].AUFNR);
                    table.SetValue("GLTRP", goods[i].GLTRP);
                    table.SetValue("MATNR", goods[i].MATNR);
                    table.SetValue("BWART", goods[i].BWART);
                    table.SetValue("LGORT", goods[i].LGORT);
                    table.SetValue("WERKS", goods[i].WERKS);
                    table.SetValue("SBMNG", goods[i].SBMNG);
                    table.SetValue("SBMEH", goods[i].SBMEH);
                }
                //  table.SetValue("MAXROWS", goods.Count);
                companyBapi.Invoke(prd);   //执行函数
                rfcdata.Error = Error(companyBapi.GetValue("EM_RETRUN").ToString());
                rfcdata.ErrorMsg = companyBapi.GetValue("EM_MESSAGE").ToString();
                rfcdata.Data = new List<RFC_BaseObj>();
                IRfcTable table2 = companyBapi.GetTable("IT_AUFNR");
                DataTable dt = GetDataTableFromRFCTable(table2);

                for (int i = 0; i < table.RowCount; i++)
                {
                    RFC_AUFNR_OUT aufnr = new RFC_AUFNR_OUT();
                    DataRow dr = dt.Rows[i];
                    aufnr.AUFNR = dr["AUFNR"].ToString();
                    aufnr.GLTRP = dr["GLTRP"].ToString();
                    aufnr.MATNR = dr["MATNR"].ToString();
                    aufnr.BWART = dr["BWART"].ToString();
                    aufnr.LGORT = dr["LGORT"].ToString();
                    aufnr.WERKS = dr["WERKS"].ToString();
                    string mng = dr["SBMNG"].ToString();
                    float fm = 0.0f;
                    float.TryParse(mng, out fm);
                    aufnr.SBMNG = fm;
                    aufnr.SBMEH = dr["SBMEH"].ToString();
                    aufnr.MBLNR = dr["MBLNR"].ToString();
                    aufnr.MJAHR = dr["MJAHR"].ToString();
                    rfcdata.Data.Add(aufnr);
                }
                return rfcdata;
            }
            catch (System.Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
            finally
            {
                prd = null;
                repo = null;
            }
        }

        public RFC_Comm_Obj_Out ZMM_GET_EBELN(string FacNo, string DateStr)
        {
            RFC_Comm_Obj_Out rfcdata = new RFC_Comm_Obj_Out();
            RfcRepository repo = prd.Repository;
            try
            {

                IRfcFunction companyBapi = repo.CreateFunction("ZMM_GET_EBELN");   //调用函数名
                companyBapi.SetValue("IM_WERKS", FacNo);   //设置Import的参数
                companyBapi.SetValue("IM_LFDAT", DateStr);   //设置Import的参数
                companyBapi.SetValue("IM_MATNR", "");
                companyBapi.SetValue("IM_VBELN", ""); 
                companyBapi.Invoke(prd);   //执行函数
                rfcdata.Error = Error(companyBapi.GetValue("EM_RETRUN").ToString());
                rfcdata.ErrorMsg = companyBapi.GetValue("EM_MESSAGE").ToString();
                if (!rfcdata.Error)
                {
                    return ErrorMsg(rfcdata.ErrorMsg);
                }
                IRfcTable table = companyBapi.GetTable("ET_EBELN");
                DataTable dt = GetDataTableFromRFCTable(table);
                rfcdata.Data = new List<RFC_BaseObj>();
                for (int i = 0; i < table.RowCount; i++)
                {
                    RFC_EBELN ebeln = new RFC_EBELN();
                    DataRow dr = dt.Rows[i];
                    ebeln.EBELN = dr["EBELN"].ToString();
                    ebeln.POSNR = dr["POSNR"].ToString();
                    ebeln.MATNR = dr["MATNR"].ToString();
                    ebeln.MAKTX = dr["MAKTX"].ToString();
                    ebeln.LFDAT = dr["LFDAT"].ToString();
                    ebeln.BLDAT = dr["BLDAT"].ToString();
                    ebeln.WADAT = dr["WADAT"].ToString();
                    string mng = dr["LFIMG"].ToString();
                    float fm = 0.0f;
                    float.TryParse(mng, out fm);
                    ebeln.LFIMG = fm;

                    ebeln.VRKME = dr["VRKME"].ToString();
                    ebeln.KUNNR = dr["KUNNR"].ToString();
                    ebeln.NAME1 = dr["NAME1"].ToString();
                    ebeln.STREET = dr["STREET"].ToString();
                    rfcdata.Data.Add(ebeln);
                }
                return rfcdata;
            }
            catch (System.Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
            finally
            {
                prd = null;
                repo = null;
            }
        }

        public RFC_Comm_Obj_Out ZMM_VL02N(List<RFC_EBELN> goods)
        {
            RFC_Comm_Obj_Out rfcdata = new RFC_Comm_Obj_Out();
            RfcRepository repo = prd.Repository;
            try
            {

                IRfcFunction companyBapi = repo.CreateFunction("ZMM_VL02N");   //调用函数名    
                IRfcTable table = companyBapi.GetTable("ET_EBELN");
                for (int i = 0; i < goods.Count; i++)
                {
                    table.Append();
                    table.SetValue("EBELN", goods[i].EBELN);
                    table.SetValue("POSNR", goods[i].POSNR);
                    table.SetValue("MATNR", goods[i].MATNR);
                    table.SetValue("MAKTX", goods[i].MAKTX);
                    table.SetValue("LFDAT", goods[i].LFDAT);
                    table.SetValue("BLDAT", goods[i].BLDAT);
                    table.SetValue("WADAT", goods[i].WADAT);
                    table.SetValue("LFIMG", goods[i].LFIMG);
                    table.SetValue("VRKME", goods[i].VRKME);
                    table.SetValue("KUNNR", goods[i].KUNNR);
                    table.SetValue("NAME1", goods[i].NAME1);
                    table.SetValue("STREET", goods[i].STREET);
                }

                //DataTable dt2 = GetDataTableFromRFCTable(table);
                //table.SetValue("MAXROWS", goods.Count);
                companyBapi.Invoke(prd);   //执行函数
                IRfcTable table2 = companyBapi.GetTable("ET_EBELN");
                DataTable dt = GetDataTableFromRFCTable(table2);
                rfcdata.Error = Error(companyBapi.GetValue("EM_RETRUN").ToString());
                rfcdata.ErrorMsg = companyBapi.GetValue("EM_MESSAGE").ToString();
                if (!rfcdata.Error)
                {
                    return ErrorMsg(rfcdata.ErrorMsg,rfcdata.Data);
                }
                return rfcdata;
            }
            catch (System.Exception ex)
            {
                return ErrorMsg(ex.Message);
            }
            finally
            {
                prd = null;
                repo = null;
            }
        }

        #endregion

        RfcDestination prd = null;
        public RFCComm()
        {
            IDestinationConfiguration ID = new MyBackendConfig();

            RfcDestinationManager.RegisterDestinationConfiguration(ID);

            prd = RfcDestinationManager.GetDestination("PRD_000");

            RfcDestinationManager.UnregisterDestinationConfiguration(ID);
        }


        public RFCComm(string host,string systemno,string user,string password,string client)
        {
            IDestinationConfiguration ID = new MyBackendConfig(host,systemno,user,password,client);

            RfcDestinationManager.RegisterDestinationConfiguration(ID);

            prd = RfcDestinationManager.GetDestination("PRD_000");

            RfcDestinationManager.UnregisterDestinationConfiguration(ID);
        }


        private RFC_Comm_Obj_Out ErrorMsg(string error)
        {
            return new RFC_Comm_Obj_Out()
            {
                Error = false,
                ErrorMsg = error,
                Data = null
            };
        }

        private RFC_Comm_Obj_Out ErrorMsg(string error,List<RFC_BaseObj> data)
        {
            return new RFC_Comm_Obj_Out()
            {
                Error = false,
                ErrorMsg = error,
                Data = data
            };
        }

        private bool Error(string error)
        {
            if (error == "S")
            {
                return true;
            }
            return false;
        }


        public DataTable GetDataTableFromRFCTable(IRfcTable myrfcTable)
        {
            DataTable loTable = new DataTable();
            int liElement = 0;
            for (liElement = 0; liElement <= myrfcTable.ElementCount - 1; liElement++)
            {
                RfcElementMetadata metadata = myrfcTable.GetElementMetadata(liElement);
                loTable.Columns.Add(metadata.Name);

            }
            foreach (IRfcStructure Row in myrfcTable)
            {
                DataRow ldr = loTable.NewRow();
                for (liElement = 0; liElement <= myrfcTable.ElementCount - 1; liElement++)
                {
                    RfcElementMetadata metadata = myrfcTable.GetElementMetadata(liElement);
                    ldr[metadata.Name] = Row.GetString(metadata.Name);
                }
                loTable.Rows.Add(ldr);

            }
            return loTable;
        }
    }

    public class MyBackendConfig : IDestinationConfiguration
    {

        public RfcConfigParameters GetParameters(String destinationName)
        {

            //if ("PRD_000".Equals(destinationName))
            {

                RfcConfigParameters parms = new RfcConfigParameters();

                parms.Add(RfcConfigParameters.AppServerHost, Host);   //SAP主机IP

                parms.Add(RfcConfigParameters.SystemNumber, SystemNumber);  //SAP实例

                parms.Add(RfcConfigParameters.User, User);  //用户名

                parms.Add(RfcConfigParameters.Password, Password);  //密码

                parms.Add(RfcConfigParameters.Client, Client);  // Client

                parms.Add(RfcConfigParameters.Language, "ZH");  //登陆语言

                parms.Add(RfcConfigParameters.PoolSize, "5");

                parms.Add(RfcConfigParameters.MaxPoolSize, "10");

                parms.Add(RfcConfigParameters.IdleTimeout, "60");

                return parms;

            }

        }

        public MyBackendConfig()
        {

        }
        private string _host = "192.168.10.7";
        public string Host
        {
            get { return _host; }
            set { _host = value; }
        }

        private string _systemnumber = "00";
        public string SystemNumber
        {
            get { return _systemnumber; }
            set { _systemnumber = value; }
        }

        private string _user = "LIC";
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        private string _password = "lc@12345";
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _client = "800";
        public string Client
        {
            get { return _client; }
            set { _client = value; }
        }

        public MyBackendConfig(string host, string systemno, string user, string password, string client)
        {
            this.Host = host;
            this.SystemNumber = systemno;
            this.User = user;
            this.Password = password;
            this.Client = client;
        }

        public bool ChangeEventsSupported()
        {

            return false;

        }

        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

    }
}
