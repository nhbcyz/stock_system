﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHC.OrderWater.Commons;

namespace JueWei.Bussiness
{
    public class ScanDataTransfer
    {
        public static ICommunicationData Str2CommunicationData(string str)
        {
            StandablePdaData data = new StandablePdaData();

            string[] s = str.Split(',');
            if (s.Length != 4)
            {
               // LogHelper.Error(string.Format("接收到非法数据:{0}\n", str));
                return null;

            }
            try
            {
                data.materilano = s[0];
                data.spec = s[1];
                data.weight = Convert.ToDecimal(s[2]);
                data.unit = s[3];
            }
            catch (System.Exception ex)
            {
                //LogHelper.Error(string.Format("接收到非法数据:{0}\n", str));
                return null;
            }
            return data;
        }

        public static Model.ims_instock_record PdaData2Record(ICommunicationData data)
        {
            StandablePdaData pdadata = data as StandablePdaData;
            if (pdadata == null)
            {
                return null;
            }
            Model.ims_dict_product product = BLL.ims_dict_product.SelectModelByWhereOrderByID("materialNo =" + pdadata.materilano, true);
            if (product == null)
            {
                LogHelper.Error(string.Format("接收到非法数据:{0}\n", pdadata.ToString()));
                return null;
            }
            Model.ims_instock_record record = new JueWei.Model.ims_instock_record()
            {
                Materialno = product.Materialno,
                Productid = product.Id,
                Spec = pdadata.spec,
                Weight = pdadata.weight,
                Unit = pdadata.unit,
                Createtime = DateTime.Now
            };
            return record;
        }

        public static Model.ims_instock_record ProductData2Record(ProductData pdadata)
        {
            if (pdadata == null)
            {
                return null;
            }
            Model.ims_dict_product product = BLL.ims_dict_product.SelectModelByWhereOrderByID("materialNo =" + pdadata.materialno, true);
            if (product == null)
            {
                LogHelper.Error(string.Format("接收到非法数据:{0}\n", pdadata.ToString()));
                return null;
            }
            Model.ims_instock_record record = new JueWei.Model.ims_instock_record()
            {
                Materialno = product.Materialno,
                Productid = product.Id,
                Spec = pdadata.spec,
                Weight = Convert.ToDecimal(pdadata.weight),
                Unit = pdadata.unit,
                Createtime = DateTime.Now
            };
            return record;
        }

        public static Model.ims_instock_record Str2Record(string str)
        {
            ICommunicationData data = Str2CommunicationData(str);
            if (data != null)
            {
                return PdaData2Record(data);
            }
            return null;
        }

    }
}
