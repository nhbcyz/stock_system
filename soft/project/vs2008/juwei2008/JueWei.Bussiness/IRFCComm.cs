﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JueWei.Bussiness
{
    public interface IRFCComm
    {
        RFC_Comm_Obj_Out ZMM_GET_MATNR(string FacNo, string DateStr);
        RFC_Comm_Obj_Out ZMM_GET_KUNNR(string FacNo, string DateStr);
        RFC_Comm_Obj_Out ZMM_GET_AUFNR(string FacNo, string DateStr, string MaterialNo);
        RFC_Comm_Obj_Out ZMM_GOODSMVT_CREATE(List<RFC_AUFNR> goods);
        RFC_Comm_Obj_Out ZMM_GET_EBELN(string FacNo, string DateStr);
        RFC_Comm_Obj_Out ZMM_VL02N(List<RFC_EBELN> goods);
    }
}
