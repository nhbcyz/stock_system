﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JueWei.Bussiness.protocols
{
    public interface IBaseProtocol
    {
        object Serialize(object data);
        object DeSerialize(object data);
    }


}
