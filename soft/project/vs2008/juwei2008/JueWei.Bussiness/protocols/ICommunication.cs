﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JueWei.Bussiness.protocols
{
    public interface ICommunication
    {
    }

    public interface ISyncCommunication : ICommunication
    {
        bool SendData(object data);
        bool ReceiveData(object data);
    }

    public interface IAsyncCommunication : ICommunication
    {

    }


    public interface ITcpSyncCommnunication : ISyncCommunication
    {

    }

    public interface SerialPortCommnunication : ISyncCommunication
    {

    }

}
