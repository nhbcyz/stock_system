using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using JueWei.Main.Controls;
using JueWei.Common;
using System.IO;
using Aspose.Cells;
using JueWei.Main.Common;
using WHC.OrderWater.Commons;
using WHC.OrderWater.Commons.Threading;
using System.Threading;
using JueWei.Bussiness;

namespace JueWei.Main
{
    public partial class OrderInStockFrm : BaseForm
    {
        private int _lasselectindex = 0; //最后选中索引
        private bool _modifying = false; //界面正在刷新
        private SearchCondition _conditions = new SearchCondition();
        private QueuedBackgroundWorker worker;
        private DataSet _alldevices = null;
        private string importfilename = string.Empty;
        private int _lastDeviceIndex = -1;
        public OrderInStockFrm()
        {
            InitializeComponent();
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }

            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            DeleteData(id);
        }

        private void DeleteData(string id)
        {
            if (!BLL.ims_orders.Delete(int.Parse(id)))
            {
                MessageBox.Show("删除失败");
                return;
            }
            this.pager1.PageCount = BindData();
        }
        private void EditData()
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                MessageUtil.ShowTips("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            Model.ims_orders mpro = BLL.ims_orders.SelectById(int.Parse(id));

            if (mpro == null)
            {
                MessageUtil.ShowTips("获取数据失败");
                return;
            }
            if (mpro.State > 0)
            {
                MessageUtil.ShowTips("订单已扫码完成,不再允许修改重量");
                return;
            }
            ModifyOrderFrm frm = new ModifyOrderFrm(mpro, false);
            frm.ShowDialog();
            _modifying = true;
            this.pager1.PageCount = BindData();
            BindPage();
        }

        /// <summary>
        /// 页数变化时调用绑定数据方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private int pager1_EventPaging(EventPagingArg e)
        {
            return BindData();
        }

        private void BindPage()
        {
            this.pager1.PageCurrent = 1;

            this.pager1.Bind();
        }

        private int BindData()
        {
            int recordcount = 0;
            string strSql = CombineSql();
            if (this.pager1.PageCurrent == 0)
            {
                this.pager1.PageCurrent = 1;
            }
            DataSet ds = BLL.ims_orders.GetList(this.pager1.PageCurrent, this.pager1.PageSize, "", strSql, ref recordcount);
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = ds.Tables[0];
            if (dataGridViewX1.RowCount <= _lasselectindex)
            {
                _lasselectindex = 0;
            }
            else
            {
                dataGridViewX1.Rows[_lasselectindex].Selected = true;
            }
           

            return recordcount;
        }

        private void dataGridViewX1_DoubleClick(object sender, EventArgs e)
        {
            _lasselectindex = dataGridViewX1.CurrentRow.Index;
            EditData();
        }

        private void btn_import_Click(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            if (ConfigHelper.GetConfigInt("DataSource") == 0)
            {
                ImportFromExcelTh();
            }
            else
            {
                ImportFromSapTh();
            }
            //ImportFromExcelTh();
            //ImportFromSapTh();
        }

        private void ImportFromExcelTh()
        {
            if (MessageUtil.ShowYesNoAndWarning("导入订单会清除以前的订单,是否确认?") != DialogResult.Yes)
            {
                return;
            }
            OrderBussiness.DeleteAllOrder();
            importfilename = OpenFile();
            if (string.IsNullOrEmpty(importfilename))
            {
                return;
            }
            worker = new QueuedBackgroundWorker();
            worker.IsBackground = true;
            worker.Threads = 1;
            worker.ProcessingMode = ProcessingMode.FIFO;
            worker.DoWork += new QueuedWorkerDoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunQueuedWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.RunWorkerAsync("import_order");
        }

        private void ImportFromSapTh()
        {
            if (MessageUtil.ShowYesNoAndWarning("导入订单会清除以前的订单,是否确认?") != DialogResult.Yes)
            {
                return;
            }
            OrderBussiness.DeleteAllOrder();
            worker = new QueuedBackgroundWorker();
            worker.IsBackground = true;
            worker.Threads = 1;
            worker.ProcessingMode = ProcessingMode.FIFO;
            worker.DoWork += new QueuedWorkerDoWorkEventHandler(worker_DoWork);
            worker.RunWorkerCompleted += new RunQueuedWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.RunWorkerAsync("ImportFromSAP");
            Thread.Sleep(1000);
            BindData();
            this.pager1.Bind();
        }

        private void ImportFromSAP()
        {
            try
            {
                IRFCComm rfccomm = new RFCComm(ConfigHelper.GetConfigString("RFCHost"),
                                                ConfigHelper.GetConfigString("RFCSN"),
                                                ConfigHelper.GetConfigString("RFCUser"),
                                                EncodeHelper.DesDecrypt(ConfigHelper.GetConfigString("RFCPassword")),
                                                ConfigHelper.GetConfigString("RFCClient"));

                if (Math.Abs(dtinput_select.Value.Subtract(DateTime.Now).Days) > 30)
                {
                    return;
                }

                RFC_Comm_Obj_Out rfcdata = rfccomm.ZMM_GET_EBELN(ConfigHelper.GetConfigString("RFCFactoryNo"), dtinput_select.Value.ToString("yyyyMMdd"));
                if (rfcdata == null)
                {
                    MessageUtil.ShowTips("从SAP导入数据失败");
                    return;
                }
                if (!rfcdata.Error)
                {
                    MessageUtil.ShowTips(string.Format("从SAP导入数据失败,错误:{0}", rfcdata.ErrorMsg));
                    return;
                }
                if (rfcdata.Data == null)
                {
                    MessageUtil.ShowTips(string.Format("从SAP导入数据失败,错误:{0}", "SAP没有返回数据"));
                    return;
                }
                OrderBussiness.InsertExchangeOrder(rfcdata.Data);
               
                

            }
            catch (System.Exception ex)
            {
                MessageUtil.ShowTips(ex.Message);
            }


        }


        private void worker_DoWork(object sender, QueuedWorkerDoWorkEventArgs e)
        {
            //DataTable dt = ImportFromExcel(importfilename);
           // OrderBussiness.Insert(dt);
          //  ImportFromSAP();
            if (ConfigHelper.GetConfigInt("DataSource") == 0)
            {
                DataTable dt = ImportFromExcel(importfilename);
                OrderBussiness.Insert(dt);
            }
            else
            {
                ImportFromSAP();
            }
            // Thread.Sleep(5000);
        }
        private void worker_RunWorkerCompleted(object sender, QueuedWorkerCompletedEventArgs e)
        {
            //btn_search_Click(null, null);
            //this.pager1.PageCount = BindData();
            //BindPage();
           
        }
        private string OpenFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.RestoreDirectory = false;
            openFileDialog.InitialDirectory = ConfigHelper.GetConfigString("LastImportPath");
            openFileDialog.Filter = "excel 03文件|*.xls|excel 07文件|*.xlsx";

            openFileDialog.FilterIndex = 1;
            
            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return "";
            }
            FileInfo f = new FileInfo(openFileDialog.FileName);
            ConfigHelper.SetAppSetting("LastImportPath", Path.GetDirectoryName(openFileDialog.FileName), AppCommon.Instance._Config);
            return openFileDialog.FileName;
        }

        private DataTable ImportFromExcel(string filename)
        {
            Workbook workbook = new Workbook(filename);

            Cells cells = workbook.Worksheets[0].Cells;

            DataTable dt = cells.ExportDataTableAsString(8, 0, cells.MaxDataRow, cells.MaxColumn + 1, false);
            return dt;
            
        }

        private void OrderInStockFrm_Load(object sender, EventArgs e)
        {
            InitDevices();
            //初始化右键菜单
            InitContextMenu();
            //初始化查询参数
            InitQueryParams();

            this.dtinput_select.Value = DateTime.Now.AddDays(1);
            this.pager1.PageCurrent = 1;
            this.pager1.PageSize = 30;

            this.pager1.NMax = BLL.ims_dict_product.GetRecordCount(""); ;

            //激活OnPageChanged事件
            pager1.EventPaging += new EventPagingHandler(pager1_EventPaging);
            this.pager1.Bind();
        }

        private void dataGridViewX1_SelectionChanged(object sender, EventArgs e)
        {
            if (!_modifying)
            {
                _modifying = false;
                if (dataGridViewX1.CurrentRow != null)
                {
                    _lasselectindex = dataGridViewX1.CurrentRow.Index;
                }
                
            }
        }

        private void InitContextMenu()
        {           
            if (_alldevices == null || _alldevices.Tables[0].Rows.Count <= 0)
            {
                return;
            }

            foreach (DataRow dr in _alldevices.Tables[0].Rows)
            {
                ToolStripMenuItem tsmi = new System.Windows.Forms.ToolStripMenuItem();
                tsmi.Name = "tsmi_device_" + dr["id"];
                tsmi.Size = new System.Drawing.Size(152, 22);
                tsmi.Text = dr["devicename"].ToString();
                tsmi.Tag = dr["id"].ToString();
                tsmi.Click+=new EventHandler(tsmi_Click);
                this.tsmi_alloc.DropDownItems.Add(tsmi);
            }
        }

        private void InitDevices()
        {
            _alldevices = BLL.ims_devices.GetList(" devicestate <> 1 and devicetype <> 2");
        }
        private void InitQueryParams()
        {
            #region 分配状态
            cb_allocstate.Items.Clear();
            cb_allocstate.Items.Add("所有");
            cb_allocstate.Items.Add("未分配");
            cb_allocstate.Items.Add("已分配");
            cb_allocstate.SelectedIndex = 0;
            #endregion

            #region 订单状态
            cb_orderstate.Items.Clear();
            cb_orderstate.Items.Add("所有");
            cb_orderstate.Items.Add("未入库");
            cb_orderstate.Items.Add("已扫码");
            cb_orderstate.Items.Add("已称重");
            cb_orderstate.Items.Add("已出库");
            cb_orderstate.SelectedIndex = 0;
            #endregion

            #region 设备
            if (_alldevices == null || _alldevices.Tables[0].Rows.Count <= 0)
            {
                return;
            }
            cb_devices.Items.Clear();
            cb_devices.Items.Add(new ComboboxItem() { Text = "所有",Value = 0});
            foreach (DataRow dr in _alldevices.Tables[0].Rows)
            {
                cb_devices.Items.Add(new ComboboxItem() { Text = dr["devicename"].ToString(),Value = dr["id"].ToString() });
            }
            cb_devices.SelectedIndex = 0;
            #endregion

        }

        private void tsmi_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.SelectedRows.Count <= 0)
            {
                MessageUtil.ShowTips("请先选择要分配的数据");
                return;
            }
            int nsucccount = 0,nfailedcount = 0; //成功更新条数

            ToolStripMenuItem tsmi = sender as ToolStripMenuItem;
            if (tsmi != null && tsmi.Tag != null)
            {
                int deviceid = Convert.ToInt32(tsmi.Tag.ToString());
                if (deviceid > 0)
                {
                    for (int i = 0; i < dataGridViewX1.SelectedRows.Count; i++)
                    {
                        DataGridViewRow row = dataGridViewX1.SelectedRows[i];
                        int orderid = Convert.ToInt32(row.Cells[0].Value);
                        //业务层分配订单
                        if (OrderBussiness.SceduleOrder(orderid, deviceid))
                        {
                            nsucccount++;
                        }
                        else { nfailedcount++; }
                    }
                }
            }
            MessageUtil.ShowTips(string.Format("分配成功,分配结果,选中{0}条,成功{1}条,失败{2}条", dataGridViewX1.SelectedRows.Count,nsucccount, nfailedcount));
        }

        /// <summary>
        /// 搜索订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_search_Click(object sender, EventArgs e)
        {
            this.pager1.PageCount = BLL.ims_orders.GetRecordCount(CombineSql());
            this.pager1.Bind();
        }

        private string CombineSql()
        {
            StringBuilder sbstr = new StringBuilder();
            string strsql = string.Empty;
            if (this._lasselectindex > 0)
            {
                sbstr.AppendFormat(" deviceid = {0} ", (cb_devices.SelectedItem as ComboboxItem).Value.ToString());
            }
            if (sbstr.ToString().Length > 0)
            {
                if (cb_allocstate.SelectedIndex == 1)
                {
                    sbstr.AppendFormat(" and deviceid = {0} ", 0);
                }
                else if (cb_allocstate.SelectedIndex == 2)
                {
                    sbstr.AppendFormat(" and deviceid <> {0} ", 0);
                }
            }
            else
            {
                if (cb_allocstate.SelectedIndex == 1)
                {
                    sbstr.AppendFormat(" deviceid = {0} ", 0);
                }
                else if (cb_allocstate.SelectedIndex == 2)
                {
                    sbstr.AppendFormat(" deviceid <> {0} ", 0);
                }
            }
            if (sbstr.Length > 0)
            {
                if (cb_orderstate.SelectedIndex != 0)
                {
                    sbstr.AppendFormat(" and state = {0} ", cb_orderstate.SelectedIndex - 1);
                }
            }
            else
            {
                if (cb_orderstate.SelectedIndex != 0)
                {
                    sbstr.AppendFormat(" state = {0} ", cb_orderstate.SelectedIndex - 1);
                }
            }
            if (sbstr.Length > 0)
            {
                if (!string.IsNullOrEmpty(tb_materialno.Text.Trim()))
                {
                    sbstr.AppendFormat(" and materialno like '%{0}%' ", tb_materialno.Text.Trim());
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(tb_materialno.Text.Trim()))
                {
                    sbstr.AppendFormat(" materialno like '%{0}%' ", tb_materialno.Text.Trim());
                }
            }

            if (sbstr.Length > 0)
            {
                if (!string.IsNullOrEmpty(tb_shopname.Text.Trim()))
                {
                    sbstr.AppendFormat(" and shopname like '%{0}%' ", tb_shopname.Text.Trim());
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(tb_shopname.Text.Trim()))
                {
                    sbstr.AppendFormat(" shopname like '%{0}%' ", tb_shopname.Text.Trim());
                }
            }


            return sbstr.ToString();
        }

        private void btn_exportall_Click(object sender, EventArgs e)
        {

        }

        private void cb_devices_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._lastDeviceIndex = cb_devices.SelectedIndex;
        }


    }
}