using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class InstockEditFrm : BaseForm
    {
        private Model.ims_orders _mproduct = null;
        private bool _adding = false;
        public InstockEditFrm()
        {
            InitializeComponent();
        }

        public InstockEditFrm(Model.ims_orders mproduct, bool adding)
        {
            this._mproduct = mproduct;
            this._adding = adding;
            InitializeComponent();
        }

        private void UpdateUI()
        {
            if (!_adding)
            {
                if (this._mproduct == null)
                {
                    return;
                }
                this.tb_productnum.Text = this._mproduct.Weight.ToString(".00");
            }
            else {
                this._mproduct = new JueWei.Model.ims_orders();
                this.tb_productnum.Text = "1";
            }
            DataTable dt = BLL.ims_dict_product.GetList("").Tables[0];
            this.cbproducts.DataSource = dt;
            cbproducts.DisplayMember = "materialNo";
            cbproducts.ValueMember = "id";

        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (!_adding)
            {
                Close();
            }
            string strproductnum = tb_productnum.Text.Trim();
            if (string.IsNullOrEmpty(strproductnum))
            {
                MessageUtil.ShowTips("请输入产品数量");
                return;
            }
            int productnum = 0;
            if (!int.TryParse(strproductnum, out productnum))
            {
                MessageUtil.ShowTips("请输入正确的产品数量");
                return;
            }
            if (productnum <= 0)
            {
                MessageUtil.ShowTips("请输入大于0的产品数量");
                return;
            }
            if (cbproducts.SelectedIndex < 0)
            {
                MessageUtil.ShowTips("请选择单品");
                return;
            }
            int pid = Convert.ToInt32(cbproducts.SelectedValue);
            Model.ims_dict_product mp = BLL.ims_dict_product.SelectById(pid);
            if (mp == null)
            {
                MessageUtil.ShowTips("查询单品出错");
                return;
            }

            Model.ims_operation_log log = new JueWei.Model.ims_operation_log();
            log.Note = string.Format("用户手动添加入库数据,物料编号{0},产品数量{1}}", mp.Materialno, productnum);
            log.Userid = Portal.gc.LoginInfo.Id;
            log.Username = Portal.gc.LoginInfo.Username;
            BLL.ims_operation_log.Insert(log);

            for (int i = 0; i < productnum; i++)
            {
                Model.ims_instock_record instock = new JueWei.Model.ims_instock_record();
                instock.Materialno = mp.Materialno;
                instock.Productid = mp.Id;
                instock.Spec = mp.Spec;
                instock.Unit = mp.Unit;
                instock.Weight = mp.Weight;
                instock.Createtime = DateTime.Now;
                BLL.ims_instock_record.Insert(instock);
            }
            this.Close();
        }

        private void OrderEditFrm_Load(object sender, EventArgs e)
        {
            UpdateUI();
        }
    }
}