﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class FrmLogHistroy : BaseDock
    {
        public FrmLogHistroy()
        {
            InitializeComponent();
        }

        private void SearchControl_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnSearch_Click(null, null);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnDeleteMonthLog_Click(object sender, EventArgs e)
        {
            try
            {
                //BLLFactory<LoginLog>.Instance.DeleteMonthLog();
                this.BindData();
            }
            catch (Exception exception)
            {
                LogHelper.Error(exception);
                MessageExUtil.ShowError(exception.Message);
            }
        }
        private void chkUseDate_CheckedChanged(object sender, EventArgs e)
        {
            this.EnableDate(this.chkUseDate.Checked);
        }
        private void EnableDate(bool enable)
        {
            this.dateTimePicker1.Enabled = enable;
            this.dateTimePicker2.Enabled = enable;
        }
        public void BindData()
        {
            //this.winGridViewPager1.DisplayColumns = "ID,RealName,LoginName,Note,LastUpdated";
            //this.winGridViewPager1.AddColumnAlias("ID", "编号");
            //this.winGridViewPager1.AddColumnAlias("LoginName", "登录名");
            //this.winGridViewPager1.AddColumnAlias("RealName", "真实名称");
            //this.winGridViewPager1.AddColumnAlias("Note", "日志信息");
            //this.winGridViewPager1.AddColumnAlias("LastUpdated", "记录日期");
            SearchCondition condition = new SearchCondition();
            condition.AddCondition("LoginName", this.txtLoginName.Text, SqlOperator.Like).AddCondition("RealName", this.txtRealName.Text, SqlOperator.Like).AddCondition("Note", this.txtNote.Text, SqlOperator.Like);
            if (this.chkUseDate.Checked)
            {
                condition.AddCondition("LastUpdated", Convert.ToDateTime(this.dateTimePicker1.Value.ToString("yyyy-MM-dd")), SqlOperator.MoreThanOrEqual, true).AddCondition("LastUpdated", Convert.ToDateTime(this.dateTimePicker2.Value.AddDays(1.0).ToString("yyyy-MM-dd")), SqlOperator.LessThanOrEqual, true);
            }
            //string str = condition.BuildConditionSql().Replace("Where", "");
            //List<LoginLogInfo> list = BLLFactory<LoginLog>.Instance.FindWithPager(str, this.winGridViewPager1.PagerInfo);
            //this.winGridViewPager1.DataSource = new SortableBindingList<LoginLogInfo>(list);
        }
    }
}
