namespace JueWei.Main
{
    partial class ProductOffStockFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_tosapall = new DevComponents.DotNetBar.ButtonX();
            this.btn_exportall = new DevComponents.DotNetBar.ButtonX();
            this.btn_selectorder = new DevComponents.DotNetBar.ButtonX();
            this.dtinput_select = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_generateship = new DevComponents.DotNetBar.ButtonX();
            this.cb_noscanexchange = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.btn_entercomplete = new DevComponents.DotNetBar.ButtonX();
            this.cb_shops = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btn_crab_product = new DevComponents.DotNetBar.ButtonX();
            this.cb_vechele = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_offstock = new DevComponents.DotNetBar.ButtonX();
            this.btn_weight = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_exchange2sap = new DevComponents.DotNetBar.ButtonX();
            this.cbExchangeNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.btn_reselectOrder = new DevComponents.DotNetBar.ButtonX();
            this.btn_query = new DevComponents.DotNetBar.ButtonX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.cb_weightshops = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dataGridViewX2 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_state = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtinput_select)).BeginInit();
            this.groupPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            this.groupPanel4.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel2
            // 
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btn_tosapall);
            this.groupPanel2.Controls.Add(this.btn_exportall);
            this.groupPanel2.Controls.Add(this.btn_selectorder);
            this.groupPanel2.Controls.Add(this.dtinput_select);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupPanel2.Location = new System.Drawing.Point(0, 0);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(932, 72);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 1;
            this.groupPanel2.Text = "选择订单";
            // 
            // btn_tosapall
            // 
            this.btn_tosapall.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_tosapall.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_tosapall.Location = new System.Drawing.Point(743, 4);
            this.btn_tosapall.Name = "btn_tosapall";
            this.btn_tosapall.Size = new System.Drawing.Size(75, 23);
            this.btn_tosapall.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_tosapall.TabIndex = 16;
            this.btn_tosapall.Text = "全部过账";
            this.btn_tosapall.Visible = false;
            this.btn_tosapall.Click += new System.EventHandler(this.btn_tosapall_Click);
            // 
            // btn_exportall
            // 
            this.btn_exportall.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_exportall.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_exportall.Location = new System.Drawing.Point(642, 4);
            this.btn_exportall.Name = "btn_exportall";
            this.btn_exportall.Size = new System.Drawing.Size(75, 23);
            this.btn_exportall.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_exportall.TabIndex = 13;
            this.btn_exportall.Text = "导出所有";
            this.btn_exportall.Click += new System.EventHandler(this.btn_exportall_Click);
            // 
            // btn_selectorder
            // 
            this.btn_selectorder.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_selectorder.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_selectorder.Location = new System.Drawing.Point(251, 4);
            this.btn_selectorder.Name = "btn_selectorder";
            this.btn_selectorder.Size = new System.Drawing.Size(75, 23);
            this.btn_selectorder.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_selectorder.TabIndex = 2;
            this.btn_selectorder.Text = "选择";
            this.btn_selectorder.Click += new System.EventHandler(this.btn_selectorder_Click);
            // 
            // dtinput_select
            // 
            // 
            // 
            // 
            this.dtinput_select.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtinput_select.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtinput_select.ButtonDropDown.Visible = true;
            this.dtinput_select.IsPopupCalendarOpen = false;
            this.dtinput_select.Location = new System.Drawing.Point(108, 4);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dtinput_select.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dtinput_select.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtinput_select.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.MonthCalendar.DisplayMonth = new System.DateTime(2016, 10, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtinput_select.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtinput_select.MonthCalendar.TodayButtonVisible = true;
            this.dtinput_select.Name = "dtinput_select";
            this.dtinput_select.Size = new System.Drawing.Size(111, 21);
            this.dtinput_select.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtinput_select.TabIndex = 1;
            this.dtinput_select.Value = new System.DateTime(2016, 10, 9, 20, 55, 14, 0);
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(10, 4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 0;
            this.labelX2.Text = "选择时间";
            // 
            // groupPanel3
            // 
            this.groupPanel3.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.btn_generateship);
            this.groupPanel3.Controls.Add(this.cb_noscanexchange);
            this.groupPanel3.Controls.Add(this.labelX6);
            this.groupPanel3.Controls.Add(this.btn_entercomplete);
            this.groupPanel3.Controls.Add(this.cb_shops);
            this.groupPanel3.Controls.Add(this.labelX1);
            this.groupPanel3.Controls.Add(this.btn_crab_product);
            this.groupPanel3.Controls.Add(this.cb_vechele);
            this.groupPanel3.Controls.Add(this.labelX3);
            this.groupPanel3.Controls.Add(this.dataGridViewX1);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupPanel3.Location = new System.Drawing.Point(0, 72);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(932, 225);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 2;
            this.groupPanel3.Text = "拣货阶段";
            // 
            // btn_generateship
            // 
            this.btn_generateship.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_generateship.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_generateship.Location = new System.Drawing.Point(440, 16);
            this.btn_generateship.Name = "btn_generateship";
            this.btn_generateship.Size = new System.Drawing.Size(86, 23);
            this.btn_generateship.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_generateship.TabIndex = 15;
            this.btn_generateship.Text = "生成物流记录";
            this.btn_generateship.Click += new System.EventHandler(this.btn_generateship_Click);
            // 
            // cb_noscanexchange
            // 
            this.cb_noscanexchange.DisplayMember = "Text";
            this.cb_noscanexchange.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_noscanexchange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_noscanexchange.FormattingEnabled = true;
            this.cb_noscanexchange.ItemHeight = 15;
            this.cb_noscanexchange.Location = new System.Drawing.Point(396, 50);
            this.cb_noscanexchange.Name = "cb_noscanexchange";
            this.cb_noscanexchange.Size = new System.Drawing.Size(130, 21);
            this.cb_noscanexchange.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_noscanexchange.TabIndex = 14;
            this.cb_noscanexchange.Visible = false;
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(343, 48);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(47, 23);
            this.labelX6.TabIndex = 13;
            this.labelX6.Text = "交货单";
            this.labelX6.Visible = false;
            // 
            // btn_entercomplete
            // 
            this.btn_entercomplete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_entercomplete.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_entercomplete.Location = new System.Drawing.Point(532, 50);
            this.btn_entercomplete.Name = "btn_entercomplete";
            this.btn_entercomplete.Size = new System.Drawing.Size(75, 23);
            this.btn_entercomplete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_entercomplete.TabIndex = 9;
            this.btn_entercomplete.Text = "拣货完成";
            this.btn_entercomplete.Tooltip = "确认拣货完成";
            this.btn_entercomplete.Click += new System.EventHandler(this.btn_entercomplete_Click);
            // 
            // cb_shops
            // 
            this.cb_shops.DisplayMember = "Text";
            this.cb_shops.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_shops.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_shops.FormattingEnabled = true;
            this.cb_shops.ItemHeight = 15;
            this.cb_shops.Location = new System.Drawing.Point(108, 48);
            this.cb_shops.Name = "cb_shops";
            this.cb_shops.Size = new System.Drawing.Size(229, 21);
            this.cb_shops.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_shops.TabIndex = 6;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(10, 48);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 5;
            this.labelX1.Text = "门店";
            // 
            // btn_crab_product
            // 
            this.btn_crab_product.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_crab_product.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_crab_product.Location = new System.Drawing.Point(252, 16);
            this.btn_crab_product.Name = "btn_crab_product";
            this.btn_crab_product.Size = new System.Drawing.Size(75, 23);
            this.btn_crab_product.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_crab_product.TabIndex = 2;
            this.btn_crab_product.Text = "开始拣货";
            this.btn_crab_product.Click += new System.EventHandler(this.btn_crab_product_Click);
            // 
            // cb_vechele
            // 
            this.cb_vechele.DisplayMember = "Text";
            this.cb_vechele.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_vechele.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_vechele.FormattingEnabled = true;
            this.cb_vechele.ItemHeight = 15;
            this.cb_vechele.Location = new System.Drawing.Point(108, 16);
            this.cb_vechele.Name = "cb_vechele";
            this.cb_vechele.Size = new System.Drawing.Size(111, 21);
            this.cb_vechele.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_vechele.TabIndex = 1;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(10, 14);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 0;
            this.labelX3.Text = "运输路线";
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column13,
            this.Column1,
            this.Column14,
            this.Column2,
            this.Column3,
            this.Column4});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewX1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(0, 77);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.RowTemplate.Height = 23;
            this.dataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX1.Size = new System.Drawing.Size(926, 124);
            this.dataGridViewX1.TabIndex = 7;
            this.dataGridViewX1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridViewX1_RowPostPaint);
            // 
            // Column13
            // 
            this.Column13.HeaderText = "序号";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "shopno";
            this.Column1.HeaderText = "店铺编号";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "exchangeno";
            this.Column14.HeaderText = "出货单";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "shopname";
            this.Column2.HeaderText = "店铺名称";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "status";
            this.Column3.HeaderText = "状态";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "statusno";
            this.Column4.HeaderText = "状态编码";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            // 
            // btn_offstock
            // 
            this.btn_offstock.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_offstock.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_offstock.Location = new System.Drawing.Point(642, 17);
            this.btn_offstock.Name = "btn_offstock";
            this.btn_offstock.Size = new System.Drawing.Size(75, 23);
            this.btn_offstock.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_offstock.TabIndex = 8;
            this.btn_offstock.Text = "出库";
            this.btn_offstock.Click += new System.EventHandler(this.btn_offstock_Click);
            // 
            // btn_weight
            // 
            this.btn_weight.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_weight.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_weight.Location = new System.Drawing.Point(561, 17);
            this.btn_weight.Name = "btn_weight";
            this.btn_weight.Size = new System.Drawing.Size(75, 23);
            this.btn_weight.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_weight.TabIndex = 4;
            this.btn_weight.Text = "称重";
            this.btn_weight.Click += new System.EventHandler(this.btn_weight_Click);
            // 
            // groupPanel4
            // 
            this.groupPanel4.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.splitContainer1);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupPanel4.Location = new System.Drawing.Point(0, 297);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(932, 173);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 3;
            this.groupPanel4.Text = "称重&出库";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btn_exchange2sap);
            this.splitContainer1.Panel1.Controls.Add(this.cbExchangeNo);
            this.splitContainer1.Panel1.Controls.Add(this.labelX5);
            this.splitContainer1.Panel1.Controls.Add(this.btn_reselectOrder);
            this.splitContainer1.Panel1.Controls.Add(this.btn_query);
            this.splitContainer1.Panel1.Controls.Add(this.labelX4);
            this.splitContainer1.Panel1.Controls.Add(this.cb_weightshops);
            this.splitContainer1.Panel1.Controls.Add(this.btn_offstock);
            this.splitContainer1.Panel1.Controls.Add(this.btn_weight);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewX2);
            this.splitContainer1.Size = new System.Drawing.Size(926, 149);
            this.splitContainer1.TabIndex = 11;
            // 
            // btn_exchange2sap
            // 
            this.btn_exchange2sap.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_exchange2sap.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_exchange2sap.Location = new System.Drawing.Point(726, 15);
            this.btn_exchange2sap.Name = "btn_exchange2sap";
            this.btn_exchange2sap.Size = new System.Drawing.Size(75, 23);
            this.btn_exchange2sap.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_exchange2sap.TabIndex = 14;
            this.btn_exchange2sap.Text = "过账";
            this.btn_exchange2sap.Visible = false;
            this.btn_exchange2sap.Click += new System.EventHandler(this.btn_exchange2sap_Click);
            // 
            // cbExchangeNo
            // 
            this.cbExchangeNo.DisplayMember = "Text";
            this.cbExchangeNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbExchangeNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbExchangeNo.FormattingEnabled = true;
            this.cbExchangeNo.ItemHeight = 15;
            this.cbExchangeNo.Location = new System.Drawing.Point(320, 18);
            this.cbExchangeNo.Name = "cbExchangeNo";
            this.cbExchangeNo.Size = new System.Drawing.Size(130, 21);
            this.cbExchangeNo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbExchangeNo.TabIndex = 13;
            this.cbExchangeNo.SelectedIndexChanged += new System.EventHandler(this.cbExchangeNo_SelectedIndexChanged);
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(279, 17);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(47, 23);
            this.labelX5.TabIndex = 12;
            this.labelX5.Text = "交货单";
            // 
            // btn_reselectOrder
            // 
            this.btn_reselectOrder.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_reselectOrder.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_reselectOrder.Location = new System.Drawing.Point(807, 17);
            this.btn_reselectOrder.Name = "btn_reselectOrder";
            this.btn_reselectOrder.Size = new System.Drawing.Size(75, 23);
            this.btn_reselectOrder.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_reselectOrder.TabIndex = 3;
            this.btn_reselectOrder.Text = "重新拣货";
            this.btn_reselectOrder.Click += new System.EventHandler(this.btn_reselectOrder_Click);
            // 
            // btn_query
            // 
            this.btn_query.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_query.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_query.Location = new System.Drawing.Point(491, 17);
            this.btn_query.Name = "btn_query";
            this.btn_query.Size = new System.Drawing.Size(64, 23);
            this.btn_query.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_query.TabIndex = 11;
            this.btn_query.Text = "查询";
            this.btn_query.Click += new System.EventHandler(this.btn_query_Click);
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(10, 15);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(47, 23);
            this.labelX4.TabIndex = 10;
            this.labelX4.Text = "门店";
            // 
            // cb_weightshops
            // 
            this.cb_weightshops.DisplayMember = "Text";
            this.cb_weightshops.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cb_weightshops.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_weightshops.FormattingEnabled = true;
            this.cb_weightshops.ItemHeight = 15;
            this.cb_weightshops.Location = new System.Drawing.Point(46, 15);
            this.cb_weightshops.Name = "cb_weightshops";
            this.cb_weightshops.Size = new System.Drawing.Size(229, 21);
            this.cb_weightshops.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cb_weightshops.TabIndex = 10;
            this.cb_weightshops.SelectedIndexChanged += new System.EventHandler(this.cb_weightshops_SelectedIndexChanged);
            // 
            // dataGridViewX2
            // 
            this.dataGridViewX2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX2.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dataGridViewX2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column11,
            this.Column10,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column12,
            this.Column9,
            this.col_state});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewX2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewX2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridViewX2.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewX2.Name = "dataGridViewX2";
            this.dataGridViewX2.ReadOnly = true;
            this.dataGridViewX2.RowTemplate.Height = 23;
            this.dataGridViewX2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX2.Size = new System.Drawing.Size(926, 95);
            this.dataGridViewX2.TabIndex = 9;
            this.dataGridViewX2.DoubleClick += new System.EventHandler(this.dataGridViewX2_DoubleClick);
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "id";
            this.Column5.HeaderText = "编号";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "shopno";
            this.Column11.HeaderText = "店铺编号";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Visible = false;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "orderid";
            this.Column10.HeaderText = "订单编号";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "shopname";
            this.Column6.HeaderText = "店铺";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "materialno";
            this.Column7.HeaderText = "物料编号";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "productname";
            this.Column8.HeaderText = "商品名称";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "productnum";
            this.Column12.HeaderText = "商品数量";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "weight";
            this.Column9.HeaderText = "重量";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // col_state
            // 
            this.col_state.DataPropertyName = "state";
            this.col_state.HeaderText = "状态";
            this.col_state.Name = "col_state";
            this.col_state.ReadOnly = true;
            // 
            // ProductOffStockFrm
            // 
            this.ClientSize = new System.Drawing.Size(932, 470);
            this.Controls.Add(this.groupPanel4);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.groupPanel2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProductOffStockFrm";
            this.Text = "ProductOffStock";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProductOffStockFrm_FormClosing);
            this.Load += new System.EventHandler(this.ProductOffStockFrm_Load);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtinput_select)).EndInit();
            this.groupPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            this.groupPanel4.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtinput_select;
        private DevComponents.DotNetBar.ButtonX btn_selectorder;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_vechele;
        private DevComponents.DotNetBar.ButtonX btn_crab_product;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private DevComponents.DotNetBar.ButtonX btn_weight;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_shops;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private DevComponents.DotNetBar.ButtonX btn_offstock;
        private DevComponents.DotNetBar.ButtonX btn_entercomplete;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_weightshops;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevComponents.DotNetBar.ButtonX btn_query;
        private DevComponents.DotNetBar.ButtonX btn_reselectOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_state;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbExchangeNo;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cb_noscanexchange;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private DevComponents.DotNetBar.ButtonX btn_exportall;
        private DevComponents.DotNetBar.ButtonX btn_generateship;
        private DevComponents.DotNetBar.ButtonX btn_exchange2sap;
        private DevComponents.DotNetBar.ButtonX btn_tosapall;
    }
}