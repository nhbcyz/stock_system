using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;
using JueWei.Common;
using System.IO;
using Aspose.Cells;
using JueWei.Main.Controls;
using JueWei.Main.Common;
using System.Reflection;

namespace JueWei.Main
{
    /// <summary>
    /// 产品入库统计
    /// </summary>
    public partial class ProductInStockStasticFrm : BaseForm
    {
        private int _lasselectindex = 0; //最后选中索引
        private bool _modifying = false; //界面正在刷新
        private SearchCondition _conditions = new SearchCondition();

        private DataSet _alldevices = null;
        public ProductInStockStasticFrm()
        {
            InitializeComponent();
        }


        private void BindData()
        {
            DateTime start = dtinput_start.Value;
            DateTime end = dtinput_end.Value;
            DataSet ds = BLL.ims_instock_record.GetProductSumWeightByDateTime(start.ToString("yyyy-MM-dd HH:mm:ss"), end.ToString("yyyy-MM-dd HH:mm:ss"));
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = ds.Tables[0];
            if (dataGridViewX1.RowCount <= _lasselectindex)
            {
                _lasselectindex = 0;
            }
            
            dataGridViewX1.Rows[_lasselectindex].Selected = true;
        }

        private void dataGridViewX1_SelectionChanged(object sender, EventArgs e)
        {
            if (!_modifying)
            {
                _modifying = false;
                if (dataGridViewX1.CurrentRow != null)
                {
                    _lasselectindex = dataGridViewX1.CurrentRow.Index;
                }
                
            }
        }

        private void InitQueryParams()
        {
            DateTime dt = DateTime.Now;
            dtinput_start.Value = new DateTime(dt.Year,dt.Month,dt.Day,0,0,0);
            dtinput_end.Value = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }

        /// <summary>
        /// 搜索订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_search_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void ProductInStockStasticFrm_Load(object sender, EventArgs e)
        {
            //初始化查询参数
            InitQueryParams();
            BindData();
        }

        private void btn_export_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.RestoreDirectory = false;
            dlg.Filter = "excel 03文件|*.xls|excel 07文件|*.xlsx";
            string fName = string.Empty;
            if (dlg.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            fName = dlg.FileName;
            if (string.IsNullOrEmpty(fName))
            {
                return;
            }
            //ExcelHelper.DataSetToExcel(this.dataGridViewX1.DataSource as DataTable, fName);
            if (ExportExcelWithAspose(this.dataGridViewX1.DataSource as DataTable, fName))
            {
                MessageUtil.ShowTips("导出成功");
                return;
            }
            MessageUtil.ShowTips("导出失败");
        }

        public bool ExportExcelWithAspose(DataTable dt, string path)
        {
            
            bool succeed = false;
            if (dt != null)
            {
                try
                {
                    Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                    Aspose.Cells.Worksheet cellSheet = workbook.Worksheets[0];
                    //为单元格添加样式    
                    Aspose.Cells.Style style = workbook.Styles[workbook.Styles.Add()];
                    //设置居中
                    style.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
                    //设置背景颜色
                    style.ForegroundColor = System.Drawing.Color.FromArgb(153, 204, 0);
                    style.Pattern = BackgroundType.Solid;
                    style.Font.IsBold = true;
                    int rowIndex = 0;
                    int colIndex = 0;
                    int colCount = 2;
                    int rowCount = dt.Rows.Count;
                    //列名的处理
                    //列名的处理
                    cellSheet.Cells[rowIndex, colIndex].PutValue("物料-描述");
                    colIndex++;
                    cellSheet.Cells[rowIndex, colIndex].PutValue("入库数量（kg）");
                    colIndex++;


                    rowIndex++;
                    rowIndex++;
                    for (int i = 0; i < rowCount; i++)
                    {
                        colIndex = 0;
                        for (int j = 0; j < colCount; j++)
                        {
                            if (j == 1)
                            {
                                cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Rows[i][j + 1].ToString());
                                colIndex++;
                                continue;
                            }
                            cellSheet.Cells[rowIndex, colIndex].PutValue(dt.Rows[i][j].ToString());
                            colIndex++;
                        }
                        rowIndex++;
                    }

                    //总重量
                    rowIndex++;
                    string Sum = dt.Compute("sum(weights)", "true").ToString();
                    cellSheet.Cells[rowIndex, 1].PutValue(Sum);


                    cellSheet.AutoFitColumns();
                    path = Path.GetFullPath(path);
                    workbook.Save(path);
                    succeed = true;
                }
                catch (Exception ex)
                {
                    succeed = false;
                }
            }
            return succeed;
        }
    }
}