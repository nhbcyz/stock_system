using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class DeviceItemEditFrm : BaseForm
    {
        private Model.ims_devices _mproduct = null;
        private bool _adding = false;
        public DeviceItemEditFrm()
        {
            InitializeComponent();
        }

        public DeviceItemEditFrm(Model.ims_devices mproduct, bool adding)
        {
            this._mproduct = mproduct;
            this._adding = adding;
            InitializeComponent();
        }

        private void UpdateUI()
        {
            if (!_adding)
            {
                if (this._mproduct == null)
                {
                    return;
                }
                this.tb_serialno.Text = this._mproduct.Serialno;
                this.tb_devname.Text = this._mproduct.Devicename;
                this.tb_mac.Text = this._mproduct.Mac;
                this.tb_ip.Text = this._mproduct.Ipaddr;
                tx_port.Text = this._mproduct.Commport.ToString();
                if (this._mproduct.Devicetype < 0 || this._mproduct.Devicetype > 2)
                {
                    this._mproduct.Devicetype = 0;
                }
                this.cb_devtype.SelectedIndex = this._mproduct.Devicetype;
                if (this._mproduct.Devicestate < 0 || this._mproduct.Devicestate > 2)
                {
                    this._mproduct.Devicestate = 0;
                }
                
                this.cb_devstate.SelectedIndex = this._mproduct.Devicestate;
            }
            else {
                this._mproduct = new JueWei.Model.ims_devices();
                this.tb_serialno.Text = "";
                this.tb_devname.Text = "";
                this.tb_mac.Text = "";
                this.tb_ip.Text = "";
                this.tx_port.Text = "";
                this.cb_devtype.SelectedIndex = 0;
                this.cb_devstate.SelectedIndex = 0;
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tb_serialno.Text.Trim()) ||
                    string.IsNullOrEmpty(tb_devname.Text.Trim()) ||
                    string.IsNullOrEmpty(tb_mac.Text.Trim()) ||
                    string.IsNullOrEmpty(tb_ip.Text.Trim())
                    )
                {
                    MessageExUtil.ShowWarning("参数不能为空");
                    return;
                }
                if (cb_devtype.SelectedIndex < 0)
                {
                    MessageExUtil.ShowWarning("请选择设备类型");
                    return;
                }
                if (cb_devstate.SelectedIndex < 0)
                {
                    MessageExUtil.ShowWarning("请选择设备状态");
                    return;
                }
                string strport = tx_port.Text.Trim();
                int port = 0;
                if (!string.IsNullOrEmpty(strport))
                {
                    if (!int.TryParse(strport, out port))
                    {
                        MessageExUtil.ShowWarning("请输入正确的端口");
                        return;
                    }
                }
                _mproduct.Serialno = tb_serialno.Text.TrimEnd();
                _mproduct.Devicename = tb_devname.Text.TrimEnd();
                _mproduct.Mac = tb_mac.Text.TrimEnd();
                _mproduct.Ipaddr = tb_ip.Text.TrimEnd();
                _mproduct.Commport = port;
                _mproduct.Devicestate = cb_devstate.SelectedIndex;
                _mproduct.Devicetype = cb_devtype.SelectedIndex;
                if (_adding)
                {
                    if (!BLL.ims_devices.Insert(_mproduct))
                    {
                        MessageExUtil.ShowError("新增失败");
                        return;
                    }
                }
                else {
                    if (!BLL.ims_devices.Update(_mproduct))
                    {
                        MessageExUtil.ShowError("保存失败");
                        return;
                    }
                }

            }
            catch (System.Exception ex)
            {
                MessageExUtil.ShowError("保存失败");
                LogHelper.Error(typeof(DeviceItemEditFrm), ex);
            }

            this.Close();
        }

        private void DeviceItemEditFrm_Load(object sender, EventArgs e)
        {
            UpdateUI();
        }
    }
}