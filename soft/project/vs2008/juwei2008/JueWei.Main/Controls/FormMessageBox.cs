﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Runtime.InteropServices;


namespace WHC.OrderWater.Commons
{
    /// <summary>
    /// 枚举，描述消息窗口加载的形式
    /// </summary>
    public enum LoadMode
    {
        /// <summary>
        /// 警告
        /// </summary>
        Warning,


        /// <summary>
        /// 错误
        /// </summary>
        Error,


        /// <summary>
        /// 提示
        /// </summary>
        Prompt
    }


    /// <summary>
    /// 消息提示窗口
    /// </summary>
    public partial class FormMessageBox : Form
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public FormMessageBox()
        {
            InitializeComponent();
        }

        public FormMessageBox(string message, LoadMode mode)
        {
            this.ShowMessage = message;
            this.FormMode = mode;
            InitializeComponent();
        }


        #region ***********************字 段***********************


        /// <summary>
        /// 窗体加载模式
        /// </summary>
        private LoadMode FormMode = LoadMode.Prompt;


        /// <summary>
        /// 显示的消息正文
        /// </summary>
        private string ShowMessage = null;


        #endregion*************************************************




        #region ***********************事 件***********************


        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMessageBox_Load(object sender, EventArgs e)
        {
            this.lblTitle.Text = "提示";
            if (FormMode == LoadMode.Error)
            {
                this.lblTitle.Text = "错误";
                //this.plShow.BackgroundImage = global::CommonApp.Properties.Resources.error;    // 更换背景
            }
            else if (FormMode == LoadMode.Warning)
            {
                this.lblTitle.Text = "警告";
                //this.plShow.BackgroundImage = global::CommonApp.Properties.Resources.warning;  // 更换背景
            }
            else
            {
                //this.plShow.BackgroundImage = global::CommonApp.Properties.Resources.Prompt;   // 更换背景
            }


            this.lblTitle.Text = ShowMessage;


            int x = Screen.PrimaryScreen.WorkingArea.Right - this.Width;
            int y = Screen.PrimaryScreen.WorkingArea.Bottom - this.Height;
            this.Location = new Point(x, y);//设置窗体在屏幕右下角显示



            this.ShowInTaskbar = false;
        }

        #endregion*************************************************

        private int ncount = 50;
        private void timer1_Tick(object sender, EventArgs e)
        {
            ncount--;
            if (ncount <= 0)
            {
                timer1.Enabled = false;
                this.Close();
            }
        }


    }
}


