namespace JueWei.Main
{
    partial class NormalSetFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.tb_orderweightdiff = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_qrpath = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tb_exportpath = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnqr_view = new DevComponents.DotNetBar.ButtonX();
            this.btn_exportview = new DevComponents.DotNetBar.ButtonX();
            this.btn_save = new DevComponents.DotNetBar.ButtonX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.cbDataSource = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.tb_rfcpwd = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.tb_rfcuser = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lb_user = new DevComponents.DotNetBar.LabelX();
            this.tb_client = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.tb_host = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(54, 57);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(101, 23);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "二维码生成路径:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(80, 22);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "称重误差:";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(54, 102);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(101, 23);
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "出货单默认路径";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // tb_orderweightdiff
            // 
            // 
            // 
            // 
            this.tb_orderweightdiff.Border.Class = "TextBoxBorder";
            this.tb_orderweightdiff.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_orderweightdiff.Location = new System.Drawing.Point(188, 26);
            this.tb_orderweightdiff.Name = "tb_orderweightdiff";
            this.tb_orderweightdiff.PreventEnterBeep = true;
            this.tb_orderweightdiff.Size = new System.Drawing.Size(113, 21);
            this.tb_orderweightdiff.TabIndex = 3;
            this.tb_orderweightdiff.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_orderweightdiff_KeyPress);
            // 
            // tb_qrpath
            // 
            // 
            // 
            // 
            this.tb_qrpath.Border.Class = "TextBoxBorder";
            this.tb_qrpath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_qrpath.Location = new System.Drawing.Point(188, 61);
            this.tb_qrpath.Name = "tb_qrpath";
            this.tb_qrpath.PreventEnterBeep = true;
            this.tb_qrpath.ReadOnly = true;
            this.tb_qrpath.Size = new System.Drawing.Size(305, 21);
            this.tb_qrpath.TabIndex = 4;
            // 
            // tb_exportpath
            // 
            // 
            // 
            // 
            this.tb_exportpath.Border.Class = "TextBoxBorder";
            this.tb_exportpath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_exportpath.Location = new System.Drawing.Point(188, 102);
            this.tb_exportpath.Name = "tb_exportpath";
            this.tb_exportpath.PreventEnterBeep = true;
            this.tb_exportpath.ReadOnly = true;
            this.tb_exportpath.Size = new System.Drawing.Size(305, 21);
            this.tb_exportpath.TabIndex = 5;
            // 
            // btnqr_view
            // 
            this.btnqr_view.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnqr_view.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnqr_view.Location = new System.Drawing.Point(517, 61);
            this.btnqr_view.Name = "btnqr_view";
            this.btnqr_view.Size = new System.Drawing.Size(75, 23);
            this.btnqr_view.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnqr_view.TabIndex = 6;
            this.btnqr_view.Text = "浏览";
            this.btnqr_view.Click += new System.EventHandler(this.btnqr_view_Click);
            // 
            // btn_exportview
            // 
            this.btn_exportview.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_exportview.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_exportview.Location = new System.Drawing.Point(517, 99);
            this.btn_exportview.Name = "btn_exportview";
            this.btn_exportview.Size = new System.Drawing.Size(75, 23);
            this.btn_exportview.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_exportview.TabIndex = 7;
            this.btn_exportview.Text = "浏览";
            this.btn_exportview.Click += new System.EventHandler(this.btn_exportview_Click);
            // 
            // btn_save
            // 
            this.btn_save.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_save.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_save.Location = new System.Drawing.Point(478, 338);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_save.TabIndex = 8;
            this.btn_save.Text = "保存";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(54, 151);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(101, 23);
            this.labelX4.TabIndex = 9;
            this.labelX4.Text = "业务数据来源:";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // cbDataSource
            // 
            this.cbDataSource.DisplayMember = "Text";
            this.cbDataSource.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbDataSource.FormattingEnabled = true;
            this.cbDataSource.ItemHeight = 15;
            this.cbDataSource.Items.AddRange(new object[] {
            this.comboItem2,
            this.comboItem1});
            this.cbDataSource.Location = new System.Drawing.Point(188, 151);
            this.cbDataSource.Name = "cbDataSource";
            this.cbDataSource.Size = new System.Drawing.Size(121, 21);
            this.cbDataSource.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbDataSource.TabIndex = 10;
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Excel导入";
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "SAP";
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.tb_rfcpwd);
            this.groupPanel1.Controls.Add(this.labelX8);
            this.groupPanel1.Controls.Add(this.tb_rfcuser);
            this.groupPanel1.Controls.Add(this.lb_user);
            this.groupPanel1.Controls.Add(this.tb_client);
            this.groupPanel1.Controls.Add(this.labelX6);
            this.groupPanel1.Controls.Add(this.tb_host);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(67, 180);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(515, 138);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 11;
            this.groupPanel1.Text = "SAP通讯参数";
            // 
            // tb_rfcpwd
            // 
            // 
            // 
            // 
            this.tb_rfcpwd.Border.Class = "TextBoxBorder";
            this.tb_rfcpwd.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_rfcpwd.Location = new System.Drawing.Point(280, 55);
            this.tb_rfcpwd.Name = "tb_rfcpwd";
            this.tb_rfcpwd.PasswordChar = '*';
            this.tb_rfcpwd.PreventEnterBeep = true;
            this.tb_rfcpwd.Size = new System.Drawing.Size(122, 21);
            this.tb_rfcpwd.TabIndex = 7;
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(230, 54);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(44, 23);
            this.labelX8.TabIndex = 6;
            this.labelX8.Text = "密码";
            // 
            // tb_rfcuser
            // 
            // 
            // 
            // 
            this.tb_rfcuser.Border.Class = "TextBoxBorder";
            this.tb_rfcuser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_rfcuser.Location = new System.Drawing.Point(280, 21);
            this.tb_rfcuser.Name = "tb_rfcuser";
            this.tb_rfcuser.PreventEnterBeep = true;
            this.tb_rfcuser.Size = new System.Drawing.Size(122, 21);
            this.tb_rfcuser.TabIndex = 5;
            // 
            // lb_user
            // 
            // 
            // 
            // 
            this.lb_user.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb_user.Location = new System.Drawing.Point(230, 20);
            this.lb_user.Name = "lb_user";
            this.lb_user.Size = new System.Drawing.Size(44, 23);
            this.lb_user.TabIndex = 4;
            this.lb_user.Text = "用户名";
            // 
            // tb_client
            // 
            // 
            // 
            // 
            this.tb_client.Border.Class = "TextBoxBorder";
            this.tb_client.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_client.Location = new System.Drawing.Point(76, 56);
            this.tb_client.Name = "tb_client";
            this.tb_client.PreventEnterBeep = true;
            this.tb_client.Size = new System.Drawing.Size(122, 21);
            this.tb_client.TabIndex = 3;
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(10, 55);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(60, 23);
            this.labelX6.TabIndex = 2;
            this.labelX6.Text = "客户端";
            // 
            // tb_host
            // 
            // 
            // 
            // 
            this.tb_host.Border.Class = "TextBoxBorder";
            this.tb_host.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tb_host.Location = new System.Drawing.Point(76, 18);
            this.tb_host.Name = "tb_host";
            this.tb_host.PreventEnterBeep = true;
            this.tb_host.Size = new System.Drawing.Size(122, 21);
            this.tb_host.TabIndex = 1;
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(10, 17);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(60, 23);
            this.labelX5.TabIndex = 0;
            this.labelX5.Text = "IP地址";
            // 
            // NormalSetFrm
            // 
            this.ClientSize = new System.Drawing.Size(617, 373);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.cbDataSource);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_exportview);
            this.Controls.Add(this.btnqr_view);
            this.Controls.Add(this.tb_exportpath);
            this.Controls.Add(this.tb_qrpath);
            this.Controls.Add(this.tb_orderweightdiff);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.Name = "NormalSetFrm";
            this.ShowIcon = false;
            this.Text = "参数设置";
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_orderweightdiff;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_qrpath;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_exportpath;
        private DevComponents.DotNetBar.ButtonX btnqr_view;
        private DevComponents.DotNetBar.ButtonX btn_exportview;
        private DevComponents.DotNetBar.ButtonX btn_save;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbDataSource;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_host;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_rfcpwd;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_rfcuser;
        private DevComponents.DotNetBar.LabelX lb_user;
        private DevComponents.DotNetBar.Controls.TextBoxX tb_client;
        private DevComponents.DotNetBar.LabelX labelX6;
    }
}