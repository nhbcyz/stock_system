using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using JueWei.Common;
using WHC.OrderWater.Commons;
using JueWei.Main.Common;

namespace JueWei.Main
{
    public partial class WaitWeightFrm : BaseForm
    {
        private decimal _orderweight = 0.0m;
        private decimal _weightOfBox = 0.0m;
        private string _selectDate = string.Empty;
        private string _shopno = string.Empty;
        private string _exchangeno = string.Empty;
        private decimal _packetweight = 0m;
        private int _failedcount = 0;
        public WaitWeightFrm()
        {
            InitializeComponent();
            _weightOfBox = ConfigHelper.GetConfigDecimal("BoxHeavy");
        }

        public WaitWeightFrm(decimal orderweight,string selectDate,string shopno,string exchangeno)
        { 
            InitializeComponent();
            _orderweight = orderweight;
            _exchangeno = exchangeno;
            this.tb_orderweight.Text = orderweight.ToString();
            _packetweight = BLL.ims_offstock.GetSumPacketWeightByShopNoAndDt(selectDate, shopno,_exchangeno);
            this.tb_packetweight.Text = _packetweight.ToString();
            _weightOfBox = ConfigHelper.GetConfigDecimal("BoxHeavy");
            _selectDate = selectDate;
            _shopno = shopno;
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void WaitWeightFrm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.DialogResult = DialogResult.Cancel;
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tb_weight.Text.Trim()))
            {
                MessageUtil.ShowTips("请输入重量");
                return;
            }
            if (string.IsNullOrEmpty(tb_boxnum.Text.Trim()))
            {
                MessageUtil.ShowTips("请输入框数");
                return;
            }

            int boxnum = 0;
            int.TryParse(tb_boxnum.Text.Trim(), out boxnum);
            if (boxnum <= 0)
            {
                MessageUtil.ShowTips("请输入大于0的框数");
                return;
            }
            //框重
            decimal packedweight = boxnum * _weightOfBox;

           
            decimal v = 0.0m;
            decimal.TryParse(tb_weight.Text.Trim(), out v);
            //净重
            decimal netweight = v - packedweight - _packetweight;
            if (netweight <= 0)
            {
                MessageUtil.ShowTips("输入数据有误,去皮重后重量小于0");
                return;
            }
            decimal diff = netweight - _orderweight;
            if (diff > ConfigHelper.GetConfigDecimal("OrderDiffWeight") || diff < 0)
            //if (Math.Abs((decimal)netweight / _orderweight - 1) > weightdiff)
            {
                MessageUtil.ShowTips(String.Format("实际重量{0},订单重量{1},相差超过设定值,请重新称重", v, _orderweight));
                _failedcount++;
                if (_failedcount >= 3)
                {
                    btn_weight.Visible = true;
                }
                return;
            }

            //更新所有的框数
            UpdateShopOrderToBoxNum(boxnum, v,_shopno, _exchangeno, Convert.ToDateTime(_selectDate));

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void UpdateShopOrderToBoxNum(int boxnum,decimal offweight,string shopno,string exchangeno, DateTime scandate)
        {
            DateTime weightdt = DateTime.Now;
            //string strWhere = string.Format(" DATE_FORMAT(orderdate, '%Y-%m-%d') = '{0}' and state = 0 and shopno ='{1}' and exchangeno='{2}' ", scandate.ToString("yyyy-MM-dd"), shopno,exchangeno);
            //int recordcount = 0;
           // IList<Model.ims_offstock> stocks = BLL.ims_offstock.GetListByPage(1, 1000, strWhere, ref recordcount);
            DataSet ds = BLL.ims_offstock.GetListByWhere(_shopno, exchangeno, scandate.ToString("yyyy-MM-dd"), 0);
            if (ds == null || ds.Tables[0].Rows.Count <= 0)
            {
                return;
            }
            ProductOffStockBussiness.UpdateShopOrderToBoxNum(ds, boxnum, offweight);
            //foreach (DataRow dr in ds.Tables[0].Rows)
            //{
            //    Model.ims_offstock item = BLL.ims_offstock.SelectById(int.Parse(dr["id"].ToString()));
            //    if (item == null)
            //    {
            //        continue;
            //    }
            //    item.Boxnum = boxnum;
            //    item.Offsumweight = offweight;
            //    if (!BLL.ims_offstock.Update(item))
            //    {
            //        MessageUtil.ShowError("出现错误");
            //        return;
            //    }
            //}
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up: // 分号
                    // ..
                    this.tb_weight.Focus();
                    return true;

                case Keys.Down: // 引号
                    this.tb_boxnum.Focus();
                    return true;
                case Keys.Enter:
                    btn_ok_Click(null, null);
                    return true;
                case Keys.Escape:
                    btn_cancel_Click(null, null);
                    return true;
                    
                default:
                    return false;
            }

            return false;
        }

        private void btn_weight_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageUtil.ShowYesNoAndWarning("系统将记录本次操作,是否确认直接称重通过?");
            if (dr != DialogResult.Yes)
            {
                return;
            }
            if (string.IsNullOrEmpty(tb_weight.Text.Trim()))
            {
                MessageUtil.ShowTips("请输入重量");
                return;
            }
            if (string.IsNullOrEmpty(tb_boxnum.Text.Trim()))
            {
                MessageUtil.ShowTips("请输入框数");
                return;
            }

            int boxnum = 0;
            int.TryParse(tb_boxnum.Text.Trim(), out boxnum);
            if (boxnum <= 0)
            {
                MessageUtil.ShowTips("请输入大于0的框数");
                return;
            }
            decimal v = 0.0m;
            decimal.TryParse(tb_weight.Text.Trim(), out v);

            //更新所有的框数
            UpdateShopOrderToBoxNum(boxnum, v, _shopno, _exchangeno, Convert.ToDateTime(_selectDate));

            Model.ims_operation_log log = new JueWei.Model.ims_operation_log();
            log.Note = string.Format("用户直接称重通过,门店编号{0},出货单号{1},订单日期{2},毛重{3}",_shopno,_exchangeno,_selectDate,v);
            log.Userid = Portal.gc.LoginInfo.Id;
            log.Username = Portal.gc.LoginInfo.Username;
            BLL.ims_operation_log.Insert(log);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}