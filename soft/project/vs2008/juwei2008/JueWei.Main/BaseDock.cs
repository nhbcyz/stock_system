namespace JueWei.Main
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using WHC.OrderWater.Commons;

    public class BaseDock : UserControl
    {
        private IContainer components = null;

        public BaseDock()
        {
            this.InitializeComponent();
        }

        private void BaseForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                this.FormOnLoad();
            }
        }

        private void BaseForm_Load(object sender, EventArgs e)
        {
            if (!base.DesignMode)
            {
                this.Cursor = Cursors.WaitCursor;
                try
                {
                    this.FormOnLoad();
                }
                catch (Exception exception)
                {
                    this.ProcessException(exception);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public virtual void FormOnLoad()
        {
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(BaseForm));
            base.SuspendLayout();
            base.AutoScaleDimensions = new SizeF(6f, 12f);
//            base.AutoScaleMode = AutoScaleMode.None;
            base.ClientSize = new Size(0x124, 0x10a);
            base.Name = "BaseForm";
            this.Text = "BaseForm";
            base.Load += new EventHandler(this.BaseForm_Load);
            base.KeyUp += new KeyEventHandler(this.BaseForm_KeyUp);
            base.ResumeLayout(false);
        }

        protected override void OnControlAdded(ControlEventArgs e)
        {
            if (!base.DesignMode)
            {
                ComponentResourceManager manager = new ComponentResourceManager(typeof(BaseDock));
                base.OnControlAdded(e);
            }
        }

        public void ProcessException(Exception ex)
        {
            this.WriteException(ex);
            MessageBox.Show(ex.Message);
        }

        public void WriteException(Exception ex)
        {
            LogHelper.Error(ex);
            MessageExUtil.ShowError(ex.Message);
        }
    }
}

