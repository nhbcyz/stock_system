using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using JueWei.Main.Common;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class WeightLogonFrm : DevComponents.DotNetBar.OfficeForm
    {
        public WeightLogonFrm()
        {
            InitializeComponent();
        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            if (this.cmbzhanhao.Text.Length == 0)
            {
                MessageExUtil.ShowTips("请输入帐号");
                this.cmbzhanhao.Focus();
            }
            else
            {
                try
                {
                    string userName = this.cmbzhanhao.Text.Trim();
                    string pwd = this.tbPass.Text;
                    Model.ims_users user = BLL.ims_users.VerifyUser(userName, pwd);
                    if (user != null)
                    {
                        IList<Model.ims_acl_function> functionsByUser = BLL.ims_acl_function.GetListByUserName(user.Username);
                        if ((functionsByUser != null) && (functionsByUser.Count > 0))
                        {
                            foreach (Model.ims_acl_function info2 in functionsByUser)
                            {
                                if (!AppCommon.Instance.FunctionDict.ContainsKey(info2.Controlid))
                                {
                                    AppCommon.Instance.FunctionDict.Add(info2.Controlid, info2);
                                }
                            }
                        }
                        if (!AppCommon.Instance.FunctionDict.ContainsKey("ModifyWeight"))
                        {
                            AppCommon.Instance.FunctionDict.Clear();
                            MessageUtil.ShowError("该用户不具备称重权限,请重新登陆");
                            this.tbPass.Text = "";
                            return;
                        }
                        AppCommon.Instance.WeightLogonDt = DateTime.Now;
                        Model.ims_logon_log info3 = new Model.ims_logon_log
                        {
                            Logonname = user.Username,
                            Realname = user.Fullname,
                            Userid = user.Id,
                            Note = "称重用户登录"
                        };
                        BLL.ims_logon_log.Insert(info3);
                        base.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        MessageExUtil.ShowTips("用户帐号密码不正确");
                        base.DialogResult = DialogResult.Cancel;
                        this.tbPass.Text = "";
                    }
                }
                catch (Exception exception)
                {
                    MessageExUtil.ShowError(exception.Message);
                }
            }
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}