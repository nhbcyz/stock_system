using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using JueWei.Main.Controls;

namespace JueWei.Main
{
    public partial class UserManageFrm : BaseForm
    {
        private int _lasselectindex = 0; //最后选中索引
        private bool _modifying = false; //界面正在刷新

        public UserManageFrm()
        {
            InitializeComponent();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            AddData();
        }

        private void AddData()
        {
            UserEditFrm frm = new UserEditFrm(null, true);
            frm.ShowDialog();
            _modifying = true;
            BindData();
        }
        private void btn_edit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                return;
            }

            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            DeleteData(id);
        }

        private void DeleteData(string id)
        {
            if (!BLL.ims_users.Delete(int.Parse(id)))
            {
                MessageBox.Show("删除失败");
                return;
            }
            BindData();
        }
        private void EditData()
        {
            if (dataGridViewX1.CurrentRow == null)
            {
                MessageBox.Show("没有选中行");
                return;
            }
            string id = dataGridViewX1.CurrentRow.Cells[0].Value.ToString();
            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            Model.ims_users muser = BLL.ims_users.SelectById(int.Parse(id));

            if (muser == null)
            {
                MessageBox.Show("获取数据失败");
                return;
            }
            UserEditFrm frm = new UserEditFrm(muser, false);
            frm.ShowDialog();
            _modifying = true;
            BindData();
        }

        /// <summary>
        /// 页数变化时调用绑定数据方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private int pager1_EventPaging(EventPagingArg e)
        {
            return BindData();
        }

        private void BindPage()
        {
            this.pager1.PageCurrent = 1;

            this.pager1.Bind();
        }

        private int BindData()
        {
            int recordcount = 0;
            DataSet ds = BLL.ims_users.GetList(this.pager1.PageCurrent, this.pager1.PageSize, " id asc ", " id != 1 ", ref recordcount);
            dataGridViewX1.AutoGenerateColumns = false;
            dataGridViewX1.DataSource = ds.Tables[0];
            dataGridViewX1.Rows[_lasselectindex].Selected = true;

            return recordcount;
        }

        private void dataGridViewX1_DoubleClick(object sender, EventArgs e)
        {
            _lasselectindex = dataGridViewX1.CurrentRow.Index;
            EditData();
        }

        private void dataGridViewX1_SelectionChanged(object sender, EventArgs e)
        {
            if (!_modifying)
            {
                _modifying = false;
                if (this.dataGridViewX1.CurrentRow == null)
                {
                    _lasselectindex = 0;
                    return;
                }
                _lasselectindex = dataGridViewX1.CurrentRow.Index;
            }
        }

        private void UserManageFrm_Load(object sender, EventArgs e)
        {
            this.pager1.PageCurrent = 1;
            this.pager1.PageSize = 30;
            this.pager1.NMax = BLL.ims_devices.GetRecordCount(""); ;
            //激活OnPageChanged事件
            pager1.EventPaging += new EventPagingHandler(pager1_EventPaging);
            this.pager1.Bind();
        }
    }
}