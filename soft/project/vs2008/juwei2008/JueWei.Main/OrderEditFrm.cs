using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using WHC.OrderWater.Commons;

namespace JueWei.Main
{
    public partial class OrderEditFrm : BaseForm
    {
        private Model.ims_orders _mproduct = null;
        private bool _adding = false;
        public OrderEditFrm()
        {
            InitializeComponent();
        }

        public OrderEditFrm(Model.ims_orders mproduct, bool adding)
        {
            this._mproduct = mproduct;
            this._adding = adding;
            InitializeComponent();
        }

        private void UpdateUI()
        {
            if (!_adding)
            {
                if (this._mproduct == null)
                {
                    return;
                }
                this.tb_weight.Text = this._mproduct.Weight.ToString(".00");
            }
            else {
                this._mproduct = new JueWei.Model.ims_orders();
                this.tb_weight.Text = "0.0";
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.tb_weight.Text.Trim())
                    )
                {
                    MessageExUtil.ShowWarning("参数不能为空");
                    return;
                }

                decimal weight = 0.0M;
                if (!decimal.TryParse(tb_weight.Text.Trim(),out weight))
                {
                    MessageExUtil.ShowWarning("重量输入错误");
                    return;
                }
                _mproduct.Weight = weight;
                if (_adding)
                {
                    if (!BLL.ims_orders.Insert(_mproduct))
                    {
                        MessageExUtil.ShowError("新增失败");
                        return;
                    }
                }
                else
                {
                    if (!BLL.ims_orders.Update(_mproduct))
                    {
                        MessageExUtil.ShowError("保存失败");
                        return;
                    }
                }

            }
            catch (System.Exception ex)
            {
                MessageExUtil.ShowError("保存失败");
                LogHelper.Error(typeof(DeviceItemEditFrm), ex);
            }

            this.Close();
        }

        private void OrderEditFrm_Load(object sender, EventArgs e)
        {
            UpdateUI();
        }
    }
}