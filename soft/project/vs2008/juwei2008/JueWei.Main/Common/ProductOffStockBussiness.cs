﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using JueWei.DBUtility;

namespace JueWei.Main.Common
{
    public class ProductOffStockBussiness
    {
        public static bool BatchInsertReocrds(DataSet ds,string scantime,string orderdate)
        {
            if (ds == null || ds.Tables.Count <= 0)
            {
                return false;
            }
            StringBuilder sb = new StringBuilder();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                sb.AppendFormat("insert into ims_offstock (deviceid,materialno,shopno,orderid,scandate,state,orderdate,weight,productnum,exchangeno) ");
                sb.AppendFormat("values({0},'{1}','{2}',{3},'{4}',0,'{5}',{6},{7},'{8}');", item["deviceid"], item["materialno"], item["shopno"], item["id"], scantime, orderdate, item["weight"], item["verifynum"], item["exchangeno"]);
                
            }
            int k = DbHelperMySQL.ExecuteSql(sb.ToString());
            if (k > 0)
            {
                return true;
            }
            return false;
        }

        public static bool BatchChangeOrderState(DataSet ds, int state)
        {
            if (ds == null || ds.Tables.Count <= 0)
            {
                return false;
            }
            StringBuilder sb = new StringBuilder();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                int id = Convert.ToInt32(item["id"]);
                sb.AppendFormat("update ims_orders set state = {0} where id={1};", state, id);
            }
            int k = DbHelperMySQL.ExecuteSql(sb.ToString());
            if (k > 0)
            {
                return true;
            }
            return true;
        }

        public static bool UpdateShopOrderToBoxNum(DataSet ds,int boxnum, decimal offweight)
        {
            if (ds == null || ds.Tables.Count <= 0)
            {
                return false;
            }
            StringBuilder sb = new StringBuilder();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                sb.AppendFormat("update ims_offstock set boxnum = {0},offsumweight={1} where id={2};", boxnum, offweight, item["id"]);
            }

            int k = DbHelperMySQL.ExecuteSql(sb.ToString());
            if (k > 0)
            {
                return true;
            }
            return true;
        }

        public static bool UpdateShopOrderToOffStockState(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0)
            {
                return false;
            }
            StringBuilder sb = new StringBuilder();
            string offstockdt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                sb.AppendFormat("update ims_offstock set state = 2,offstockdate='{0}' where id={1};", offstockdt, item["id"]);
            }

            int k = DbHelperMySQL.ExecuteSql(sb.ToString());
            if (k > 0)
            {
                return true;
            }
            return true;
        }

        /// <summary>
        /// 获取已发物料的数量
        /// </summary>
        /// <param name="materialno"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int GetOffStockByMaterialNo(string materialno, DateTime dt)
        {
            return BLL.ims_offstock.QueryOffStockNum(dt.ToString("yyyy-MM-dd"), materialno, 2);
        }
    }
}
