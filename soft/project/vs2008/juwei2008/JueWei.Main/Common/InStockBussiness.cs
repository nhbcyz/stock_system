﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using WHC.OrderWater.Commons;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using JueWei.Bussiness;

namespace JueWei.Main.Common
{
    class ScanData
    {
        public string MaterialNo { get; set; }
        public string Spec { get; set; }
        public string Unit { get; set; }
        public decimal Weight { get; set; }

    }
    class InStockBussiness
    {
        private Thread theTh = null;
        List<Model.ims_devices> devices = new List<JueWei.Model.ims_devices>();
        List<CommnunicationItem> communications = new List<CommnunicationItem>();
        private List<Model.ims_instock_record> records = new List<JueWei.Model.ims_instock_record>();
        private object theObj = new object();
        private bool _stoped = true;

        public InStockBussiness()
        {
            devices.Clear();
            IList<Model.ims_devices> _devices = BLL.ims_devices.GetAllList();
            if (_devices != null)
            {
                foreach (var item in _devices)
                {
                    if (item.Devicetype == 1 && !string.IsNullOrEmpty(item.Ipaddr) && item.Commport != 0 && item.Devicestate == 2)
                    {
                        devices.Add(item);
                        CommnunicationItem conn = new CommnunicationItem(item.Ipaddr, item.Commport);
                        communications.Add(conn);
                        ProcessReceiveDataDelegate d = new ProcessReceiveDataDelegate(ProcessData);
                        conn.RegDelegate(ProcessData);
                    }
                }
            }

            theTh = new Thread(new ThreadStart(Process));      
        }
        public void Start()
        { 
            if (!_stoped)
            {
                return;
            }
            _stoped = false;
            if (theTh != null)
            {
                theTh.Start();
            }
        }
        private void Process()
        {
            foreach (var item in communications)
            {
                item.Start();
            }
            while (true)
            {
                if (_stoped)
                {
                    break;
                }
                //判断缓存队列是否为空
                List<Model.ims_instock_record>  temp = new List<JueWei.Model.ims_instock_record>();
                lock (theObj)
                {
                    if (this.records.Count > 0)
                    {
                        foreach (var item in records)
                        {
                            temp.Add(Clone<Model.ims_instock_record >(item));  
                        }
                        records.Clear();
                    }
                }
                if (temp.Count > 0)
                {
                    foreach (var item in temp)
                    {
                        if (!BLL.ims_instock_record.Insert(item))
                        {
                            LogHelper.Error(string.Format("写入入库记录时失败,物料编号为:{0}\n", item.Materialno));
                        }
                    }
                }
                //Todo:判断连接是否中断,并进行重新链接
                foreach (var item in communications)
                {
                    item.CheckConnection();
                    if (item.CheckConnectionState())
                    {
                        if (item.CheckErrorState() && !item.bShowTips)
                        {
                            item.bShowTips = true ;
                            item.nErrorCount = 0;
                            FormMessageBox frm = new FormMessageBox(string.Format("设备:{0},通讯中断", item.ServerIP),LoadMode.Error);
                           // FormAnimator ani = new FormAnimator(frm, FormAnimator.AnimationMethod.Blend,FormAnimator.AnimationDirection.Down, 3000,true);
                            frm.Show();
                            
                        }
                        item.Stop();
                        item.Start();
                    }
                }
                Thread.Sleep(50);
            }
        }

        public void Stop()
        {
            _stoped = true;
            foreach (var item in communications)
            {
                item.Stop();
            }
        }

        private void ProcessData(ICommunicationData data)
        {
            StandablePdaData pdadata = data as StandablePdaData;
            if (pdadata == null)
            {
                return;
            }
            Model.ims_dict_product product = BLL.ims_dict_product.SelectModelByWhereOrderByID("materialNo =" + pdadata.materilano,true);
            if (product == null)
            {
                LogHelper.Error(string.Format("接收到非法数据:{0}\n", pdadata.ToString()));
                return;
            }
            Model.ims_instock_record record = new JueWei.Model.ims_instock_record()
            {
                Materialno = product.Materialno,
                Productid = product.Id,
                Spec = pdadata.spec,
                Weight = pdadata.weight,
                Unit = pdadata.unit,
                Createtime = DateTime.Now
            };
            lock (theObj)
            {
                records.Add(record);
            }
        }

        /// <summary>
        /// 深度复制对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="RealObject"></param>
        /// <returns></returns>
        public T Clone<T>(T RealObject)
        {
            using (Stream objectStream = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(objectStream, RealObject);
                objectStream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(objectStream);
            }
        }
    }


}
