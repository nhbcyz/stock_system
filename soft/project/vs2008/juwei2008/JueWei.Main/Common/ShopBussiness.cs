﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using JueWei.DBUtility;
using JueWei.Bussiness;

namespace JueWei.Main.Common
{
    class ShopBussiness
    {
        public static int Update(DataTable dt)
        {
            if (dt == null)
            {
                return 0;
            }
            List<string> sqllist = new List<string>();
            string sqlStr = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string shopno = dt.Rows[i][1].ToString();
                string shiporder = dt.Rows[i][37].ToString();

                if (string.IsNullOrEmpty(shopno) || string.IsNullOrEmpty(shiporder))
                {
                    continue;
                }
                int sort_id = Convert.ToInt32(shiporder.Substring(1));
                DataSet ds = BLL.ims_dict_shop.GetList(" shopno ='" + shopno + "' ");

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    //已存在店铺编号
                    sqlStr = "update ims_dict_shop set ";
                    sqlStr += "shopname ='" + dt.Rows[i][11].ToString() + "',";
                    sqlStr += "contacter ='" + dt.Rows[i][23].ToString() + "',";
                    sqlStr += "tel ='" + dt.Rows[i][24].ToString() + "',";
                    sqlStr += "vehiclelineno ='" + dt.Rows[i][35].ToString() + "', ";
                    sqlStr += "vehiclename ='" + dt.Rows[i][36].ToString() + "', ";
                    sqlStr += "shiporder ='" + dt.Rows[i][37].ToString() + "', ";
                    sqlStr += "sort_id ='" + sort_id.ToString() + "', ";
                    sqlStr += "addr ='" + dt.Rows[i][14].ToString() + "' ";
                    sqlStr += " where shopno = '" + dt.Rows[i][1].ToString() + "'";
                    sqllist.Add(sqlStr);
                }
                else
                {
                    //不存在店铺编号
                    sqlStr = "insert into ims_dict_shop (shopno,shopname,contacter,tel,vehiclelineno,vehiclename,shiporder,addr,sort_id) values ";
                    sqlStr += "('" + dt.Rows[i][1].ToString() + "','";
                    sqlStr += "" + dt.Rows[i][11].ToString() + "','";
                    sqlStr += "" + dt.Rows[i][23].ToString() + "','";
                    sqlStr += "" + dt.Rows[i][24].ToString() + "','";
                    sqlStr += "" + dt.Rows[i][35].ToString() + "','";
                    sqlStr += "" + dt.Rows[i][36].ToString() + "','";
                    sqlStr += "" + dt.Rows[i][37].ToString() + "','";
                    sqlStr += "" + dt.Rows[i][14].ToString() + "','";
                    sqlStr += "" + sort_id.ToString() + "')";
                    sqllist.Add(sqlStr);
                }
            }
            int k = DbHelperMySQL.ExecuteSqlTran(sqllist);
            return k;
        }

        public static int Update(List<RFC_BaseObj> shops)
        {
            if (shops == null)
            {
                return 0;
            }
            List<string> sqllist = new List<string>();
            string sqlStr = string.Empty;
            for (int i = 0; i < shops.Count; i++)
            {
                RFC_KUNNR kunnr = shops[i] as RFC_KUNNR;
                string shopno = kunnr.KUNNR;
                string shiporder = kunnr.ZYXS;

                if (string.IsNullOrEmpty(shopno) || string.IsNullOrEmpty(shiporder))
                {
                    continue;
                }

                int sort_id = Convert.ToInt32(shiporder.Substring(1));
                DataSet ds = BLL.ims_dict_shop.GetList(" shopno ='" + shopno + "' ");

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    //已存在店铺编号
                    sqlStr = "update ims_dict_shop set ";
                    sqlStr += "shopname ='" + kunnr.NAME1 + "',";
                    sqlStr += "contacter ='" + kunnr.MDLXR + "',";
                    sqlStr += "tel ='" + kunnr.MDTEL + "',";
                    sqlStr += "vehiclelineno ='" + kunnr.ZYLX + "', ";
                    sqlStr += "vehiclename ='" + kunnr.TZYLX + "', ";
                    sqlStr += "shiporder ='" + shiporder + "', ";
                    sqlStr += "sort_id ='" + sort_id.ToString() + "', ";
                    sqlStr += "addr ='" + kunnr.STREET + "' ";
                    sqlStr += " where shopno = '" + shopno + "'";
                    sqllist.Add(sqlStr);
                }
                else
                {
                    //不存在店铺编号
                    sqlStr = "insert into ims_dict_shop (shopno,shopname,contacter,tel,vehiclelineno,vehiclename,shiporder,addr,sort_id) values ";
                    sqlStr += "('" + shopno + "','";
                    sqlStr += "" + kunnr.NAME1 + "','";
                    sqlStr += "" + kunnr.MDLXR + "','";
                    sqlStr += "" + kunnr.MDTEL + "','";
                    sqlStr += "" + kunnr.ZYLX + "','";
                    sqlStr += "" + kunnr.TZYLX + "','";
                    sqlStr += "" + shiporder + "','";
                    sqlStr += "" + kunnr.STREET + "','";
                    sqlStr += "" + sort_id.ToString() + "')";
                    sqllist.Add(sqlStr);
                }
            }
            int k = DbHelperMySQL.ExecuteSqlTran(sqllist);
            return k;
        }
    }
}
