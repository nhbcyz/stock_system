﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace JueWei.Main.Common
{
    public class StockBussiness
    {
        /// <summary>
        /// 获取当天和昨天库存
        /// </summary>
        /// <param name="materialno"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int GetStockByMaterialNo(string materialno,DateTime dt)
        {
            int todaynum = BLL.ims_instock_record.GetInstockRecordByMaterialnoAndDt(materialno, dt, 0);
            int yesterdaynum = BLL.ims_instock_record.GetInstockRecordByMaterialnoAndDt(materialno, dt.AddDays(-1), 0);
            int recordcount = 0;
            int productordernum = 0;
            IList<Model.ims_product_order> productorders = BLL.ims_product_order.GetListByPage(1,100,string.Format(" materialNo = '{0}' and DATE_FORMAT(completedate,'%Y-%m-%d') = '{1}' ", materialno, dt.ToString("yyyy-MM-dd")),ref recordcount);
            if (productorders != null && productorders.Count > 0)
            {
                foreach (var item in productorders)
                {
                    productordernum += item.Scannum;
                }
            }
            int totalinstocknum = todaynum + yesterdaynum + productordernum;

            return totalinstocknum;
        }

        /// <summary>
        /// 获取已发物料的数量
        /// </summary>
        /// <param name="materialno"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int GetOffStockByMaterialNo(string materialno, DateTime dt)
        {
            return BLL.ims_offstock.QueryOffStockNum(dt.ToString("yyyy-MM-dd"), materialno, 2);
        }

    }
}
