﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using JueWei.Common;

namespace JueWei.Main.Common
{
    public class WeightLoginBussiness
    {
        private Thread theTh = null;
        private int expiredMin = 30;
        private bool _stoped = true;
        public WeightLoginBussiness()
        {
            theTh = new Thread(new ThreadStart(Process));
            expiredMin = ConfigHelper.GetConfigInt("WeightLoginExpired");
        }

        public void Start()
        {
            if (!_stoped)
            {
                return;
            }
            _stoped = false;
            if (theTh != null)
            {
                theTh.Start();
            }
        }
        private void Process()
        {
            while (true)
            {
                if (_stoped)
                {
                    return;
                }
                if (AppCommon.Instance.WeightLogonDt == null)
                {
                    Thread.Sleep(1000);
                    continue;
                }
                if (AppCommon.Instance.FunctionDict.Count > 0)
                {
                    DateTime dt = (DateTime)AppCommon.Instance.WeightLogonDt;
                    if (dt.AddMinutes(expiredMin) < DateTime.Now)
                    {
                        AppCommon.Instance.FunctionDict.Clear();
                    }
                }
                Thread.Sleep(1000);
            }
        }

        public void Stop()
        {
            _stoped = true;
        }
    }
}
