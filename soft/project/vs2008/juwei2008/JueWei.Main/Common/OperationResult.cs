﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JueWei.Main.Common
{
    public class OperationResult
    {
        public bool Result { get; set; }
        public string ErrorMsg { get; set; }
    }
}
