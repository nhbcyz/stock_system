﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HZDrawPublish.Service;
using XY.SystemEx.SocketsEx;
using System.Net;
using WHC.OrderWater.Commons;
using JueWei.Bussiness;

namespace JueWei.Main.Common
{
    public delegate void ProcessReceiveDataDelegate(ICommunicationData data);

    //调试字符串
    //02 33 30 30 30 30 30 2C 30 34 4A 2C 31 2C 6B 67 0d 0a
    class CommnunicationItem
    {
        public string ServerIP { get; set; }
        public int ServerPort { get; set; }

        private SocketClient _socketClient;
        private SocketClientService _socketService = null;
        private ProcessReceiveDataDelegate FOnEventDelegate;
        private List<HZMessageBase> _listData = new List<HZMessageBase>();
        private int nDisconnect = 0;
        public int nErrorCount = 0; //错误次数/用于判断通讯是否恢复,从而通知界面报警
        public bool bShowTips = false;

        public CommnunicationItem(string ip, int port)
        {
            this.ServerIP = ip;
            this.ServerPort = port;
        }

        public void RegDelegate(ProcessReceiveDataDelegate de)
        {
            this.FOnEventDelegate = de;
        }
        public void Start()
        {
            _socketService = new SocketClientService();
            _socketClient = new SocketClient(_socketService);
            _socketClient.OnException += new OnExceptionDelegate(_socketClient_OnException);
            IPEndPoint ipendpoint = new IPEndPoint(IPAddress.Parse(ServerIP), ServerPort);


            _socketService.SocketClient = _socketClient;
            _socketClient.AddConnector(ipendpoint);
             _socketClient.Start();
            _socketService.RegisteredService("SynchronousSendData", new OnEventMessage(SynchronousSendDataEvent));
            _socketService.waitConnection(3000);
            nDisconnect = 0;
            nErrorCount = 0;
        }

        public void Stop()
        {
            if (_socketClient != null)
            {
                _socketClient.Stop();
            }
        }

        public void _socketClient_OnException(Exception ex)
        {
            if (_socketClient != null)
            {
                _socketClient.Stop();
            }
            nDisconnect = 0;
            _socketService.waitConnection(15000);
        }

        public bool CheckConnectionState()
        {
            return nDisconnect >= 3;
        }
        public bool CheckErrorState()
        {
            return nDisconnect >= 50;
        }
        public void CheckConnection()
        {
            if (_socketClient.GetSocketConnections().Length == 0)
            {
                 nDisconnect++;
                 nErrorCount++;
            }
            else
            {
                nDisconnect = 0;
                nErrorCount = 0;
                bShowTips = false;
            }
        }

        private void SynchronousSendDataEvent(HZMessageBase msgbase)
        {
            try
            {
                string str = System.Text.Encoding.ASCII.GetString(msgbase.Data); 
                if (this.FOnEventDelegate != null)
                {
                    //this.FOnEventDelegate.Invoke(
                    if (string.IsNullOrEmpty(str))
                    {
                        return;
                    }
                    ICommunicationData data = Str2CommunicationData(str);
                    if (data == null)
                    {
                        return;
                    }
                    this.FOnEventDelegate.Invoke(data);
                }
            }
            catch
            {

            }
        }

        private ICommunicationData Str2CommunicationData(string str)
        {
            StandablePdaData data = new StandablePdaData();

            string[] s = str.Split(',');
            if (s.Length != 4)
            {
                LogHelper.Error(string.Format("接收到非法数据:{0}\n",str));
                return null;
                
            }
            try
            {
                data.materilano = s[0];
                data.spec = s[1];
                data.weight = Convert.ToDecimal(s[2]);
                data.unit = s[3];
            }
            catch (System.Exception ex)
            {
                LogHelper.Error(string.Format("接收到非法数据:{0}\n", str));
                return null;
            }
 
            return data;
        }
    }
}
