#ifndef YAOHUA_WEIGHT_COMMUNICATION_H
#define YAOHUA_WEIGHT_COMMUNICATION_H

#ifdef DLL_EXPORT
#define WIN_SHARE_FLAG __declspec(dllexport)
#else
#define WIN_SHARE_FLAG __declspec(dllimport)
#endif

/*
函数说明：打开串口
参数说明:port 串口号，baud 波特率
*/
extern "C" WIN_SHARE_FLAG bool open_serialport(int port,int baud);

/*
函数说明：获取数据
参数说明: value 值，type 类型（1毛重，2皮重，3净重），unit 单位（1 kg，2 lb）
*/
extern "C" WIN_SHARE_FLAG bool get_data(float &value,int &type,int &unit,char* err);

/*
函数说明：获取报文
*/
extern "C" WIN_SHARE_FLAG bool get_message(char *buf,int buf_size,int &msg_size);

/*
函数说明：关闭串口
*/
extern "C" WIN_SHARE_FLAG void close_serialport();

#endif // YAOHUA_WEIGHT_COMMUNICATION_H
