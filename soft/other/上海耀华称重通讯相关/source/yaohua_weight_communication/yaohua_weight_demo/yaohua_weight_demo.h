#ifndef YAOHUA_WEIGHT_DEMO_H
#define YAOHUA_WEIGHT_DEMO_H

#include <QListWidget>
#include <QLabel>
#include <QTimer>
#include <QComboBox>
#include <QDialog>

class yaohua_weight_demo : public QDialog
{
	Q_OBJECT

public:
	yaohua_weight_demo(QWidget *parent = 0, Qt::WFlags flags = 0);
	~yaohua_weight_demo();

private:
	void show_message( char* buff,int len,int type);

	QListWidget* m_list;
	QLabel* m_label;
	QTimer* m_timer;
	QComboBox *box_port;
	QComboBox *box_baut;
	bool m_b_is_open;
	QPushButton* m_btn ;

	public slots:
		void slot_clicked();
		void slot_timeout();
};

#endif // YAOHUA_WEIGHT_DEMO_H
