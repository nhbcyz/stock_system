#include "yaohua_weight_demo.h"
#include <QtGui>
#include "yaohua_weight_export.h"

yaohua_weight_demo::yaohua_weight_demo(QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
{
	setFixedSize(600,400);
	QVBoxLayout* layout = new QVBoxLayout;
	QHBoxLayout* layout_1 = new QHBoxLayout;
	box_port = new QComboBox;
	box_baut = new QComboBox;
    m_label = new QLabel;
	m_list = new QListWidget;
	m_timer = new QTimer;
	m_btn = new QPushButton(tr("开始读取"));
	layout_1->addWidget(box_port);
	layout_1->addWidget(box_baut);
	layout_1->addWidget(m_btn);
	layout_1->addStretch();
	layout->addLayout(layout_1);
	layout->addWidget(m_label);
	layout->addWidget(m_list);
	setLayout(layout);

	connect(m_btn,SIGNAL(clicked()),this,SLOT(slot_clicked()));
	connect(m_timer,SIGNAL(timeout()),this,SLOT(slot_timeout()));
	setWindowTitle(tr("测试程序"));
	for (int i=1; i<=10; ++i)
	{
		QString str = QString("COM") + QString::number(i);
		box_port->addItem(str);
	}

	box_baut->addItem("1200");
	box_baut->addItem("2400");
	box_baut->addItem("4800");
	box_baut->addItem("9600");
	box_baut->setCurrentIndex(3);

	m_b_is_open = false;
}

yaohua_weight_demo::~yaohua_weight_demo()
{

}

void yaohua_weight_demo::slot_clicked()
{
	int port = box_port->currentText().remove("COM").toInt();
	int baud = box_baut->currentText().toInt();
	if(!m_b_is_open)
	{
		if(open_serialport(port,baud))
		{
			m_b_is_open = true;
			m_btn->setText(tr("停止"));
			m_timer->start(500);
		}
		else
		{
			QMessageBox box;
			QString str = tr("打开COM");
			str += QString::number(port);
			str += tr("失败");
			box.setText(str);
			box.exec();
		}
	}
}

void yaohua_weight_demo::slot_timeout()
{
	if(m_b_is_open)
	{
		float value;
		int type;
		int unit;
		char err[1024] = {0};
		char msg_buf[1024] = {0};
		if(get_data(value,type,unit,err))
		{
			//参数说明: value 值，type 类型（1毛重，2皮重，3净重），unit 单位（1 kg，2 lb）
			QString str;
			if(type == 1)
				str = tr("毛重: ");
			else if(type == 2)
				str = tr("皮重: ");
			else if(type == 3)
				str = tr("净重: ");

			str += QString::number(value);

			if(unit == 1)
				str += tr(" kg");
			else if(unit == 2)
				str += tr(" lb");

			m_label->setText(str);

			int msg_size = 0;
			if(get_message(msg_buf,1024,msg_size))
			{
				show_message(msg_buf,msg_size,1);
			}
		}
		else
			m_label->clear();
	}
}

void yaohua_weight_demo::show_message( char* buff,int len,int type)
{
	static QColor send_color(0,255,0),recv_color(0,0,255);
	QString msg,msg_temp;

	if(type == 0)//send
		msg += "send[";
	else
		msg += "recv[";

	msg_temp.sprintf("%d",len);
	msg += msg_temp;
	msg += "] ";

	for (int i=0; i<len; ++i)
	{
		msg_temp.sprintf("%02x ",buff[i]);
		msg += msg_temp;
	}

	m_list->addItem(msg);
	int row = m_list->count();
	m_list->setCurrentRow(row-1);
	if(type == 0)
		m_list->currentItem()->setTextColor(send_color);
	else
		m_list->currentItem()->setTextColor(recv_color);
}
