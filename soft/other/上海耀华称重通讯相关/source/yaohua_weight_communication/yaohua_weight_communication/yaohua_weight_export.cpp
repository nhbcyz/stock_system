#define DLL_EXPORT
#include "yaohua_weight_export.h"
#include "yaohua_communication.h"

yaohua_communication g_comm;

extern "C" WIN_SHARE_FLAG bool open_serialport(int port,int baud)
{
	return g_comm.open(port,baud);
}

extern "C" WIN_SHARE_FLAG bool get_data(float &value,int &type,int &unit,char *err)
{
	QString str_err;
	if(g_comm.get_data(value,type,unit,str_err))
	{
		return true;
	}
	else
	{
		if(err != NULL)
			strcpy(err,str_err.toStdString().c_str());

		return false;
	}
}

extern "C" WIN_SHARE_FLAG bool get_message(char *buf,int buf_size,int &msg_size)
{
	return g_comm.get_message(buf,buf_size,msg_size);
}

extern "C" WIN_SHARE_FLAG void close_serialport()
{
	g_comm.close();
}
