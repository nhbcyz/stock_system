#include "yaohua_communication.h"
#include <QDebug>

//参数：value 值，type 类型（1毛重，2皮重，3净重），unit 单位（1 kg，2 lb）
bool yaohua_protocol::proc_data(const QByteArray &arr,float &value,int &type,int &unit,QString &err)
{
	if(arr.size() > QString("ww000.000kg").size() + 2)
	{
		err = QObject::tr("报文格式错误: 长度超出范围");
		return false;
	}

	QString str(arr);
	QString str_type,str_unit;

	if(str.startsWith("ww"))//毛重
	{
		str_type = "ww";
		type = enuMaozhong;
	}
	else if(str.startsWith("wn"))//皮重
	{
		str_type = "wn";
		type = enuPizhong;
	}
	else if(str.startsWith("wt"))//净重
	{
		str_type = "wt";
		type = enuJingzhong;
	}
	else
	{
		err = QObject::tr("报文格式错误: 称重类型错误");
		return false;
	}
	
	if(str.endsWith("kg"))
	{
		str_unit = "kg";
		unit = enuKg;
	}
	else if(str.endsWith("lb"))
	{
		str_unit = "lb";
		unit = enuPizhong;
	}
	else
	{
		err = QObject::tr("报文格式错误: 单位错误");
		return false;
	}

	str.remove(str_type);
	str.remove(str_unit);
	bool b;
	value = str.toFloat(&b);
	if(!b)
	{
		err = QObject::tr("报文格式错误：数据转换错误");
		return false;
	}

	return true;
}

bool yaohua_communication::open(int port,int nbaud)
{
	serialPortName = "COM" + QString::number(port);
	serialPort.setPortName(serialPortName);
	serialPort.setBaudRate(nbaud);

	if (!serialPort.open(QIODevice::ReadOnly)) 
	{
		qDebug() << QObject::tr("Failed to open port %1, error: %2").arg(serialPortName).arg(serialPort.error()) << endl;
		return false;
	}
	else
		return true;
}

bool yaohua_communication::get_data(float &value,int &type,int &unit,QString &err)
{
	QByteArray readData;
	if(read(readData,err))
	{
		m_msg = readData;
		return m_protocol.proc_data(readData,value,type,unit,err);
	}
	else
		return false;
}

void yaohua_communication::close()
{
	serialPort.close();
}

bool yaohua_communication::get_message(char *buf,int buf_size,int &msg_size)
{
	if(NULL == buf)
		return false;

	if(buf_size < m_msg.size())
		return false;

	msg_size = m_msg.size();
	memcpy(buf,m_msg.data(),m_msg.size());

	return true;
}

bool yaohua_communication::read(QByteArray &readData,QString &err)
{
	readData = serialPort.readAll();
	serialPort.waitForReadyRead(200);//超时200ms

	while (serialPort.waitForReadyRead(20))//判断读完
		readData.append(serialPort.readAll());

	if (serialPort.error() == QSerialPort::ReadError) 
	{
		err = QObject::tr("Failed to read from port %1, error: %2").arg(serialPortName).arg(serialPort.errorString());
		return false;
	}
	else if (serialPort.error() == QSerialPort::TimeoutError && readData.isEmpty()) 
	{
		err = QObject::tr("No data was currently available for reading from port %1").arg(serialPortName);
		return false;
	}
	else
	{
		return true;
	}
}

