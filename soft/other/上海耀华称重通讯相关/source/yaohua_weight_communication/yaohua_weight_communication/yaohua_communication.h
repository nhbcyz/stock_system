#pragma once

#include <QtSerialPort/QSerialPort>

enum {enuMaozhong = 1,enuPizhong,enuJingzhong};
enum {enuKg = 1,enuLb};

class yaohua_protocol
{
public:
	bool proc_data(const QByteArray &arr,float &value,int &type,int &unit,QString &err);
};

class yaohua_communication
{
public:
	bool get_data(float &value,int &type,int &unit,QString &err);//参数：value 值，type 类型（1毛重，2皮重，3净重），unit 单位（1 kg，2 lb）
	bool open(int port,int nbaud);
	void close();
	bool get_message(char *buf,int buf_size,int &msg_size);

private:
	bool read(QByteArray &readData,QString &err);

	QSerialPort serialPort;
	QString serialPortName;
	yaohua_protocol m_protocol;
	QByteArray m_msg;
};

