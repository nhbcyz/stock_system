﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace TestProtocol
{
    public class protocol_yaohua_weight_communication
    {
        public const string DllPath = @"yaohua_weight_communication.dll";
        [DllImport(DllPath, EntryPoint = "open_serialport", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool open_serialport(int port, int baud);


        [DllImport(DllPath, EntryPoint = "get_data", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool get_data(ref float value, ref int datatype, ref int unit, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] byte[] err);

        [DllImport(DllPath, EntryPoint = "close_serialport", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern void close_serialport();

        private bool portstate = false;
        private int nport = 0;
        private int nbaund = 0;
        public protocol_yaohua_weight_communication()
        {
            portstate = false;
        }

        public bool OpenSerialPort(int port, int baund)
        {
            nport = port;
            nbaund = baund;
            portstate = open_serialport(port, baund);
            return portstate;
        }


        public bool GetVal(ref float v, ref int datatype, ref int unit, byte[] err)
        {
            bool ret = get_data(ref v, ref datatype, ref unit, err);
            int ncount = 30;
            while (!ret && ncount > 0)
            {
                ret = get_data(ref v, ref datatype, ref unit, err);
                ncount--;
                System.Threading.Thread.Sleep(50);
            }
            if (!ret)
            {
                open_serialport(nport, nbaund);
            }
            return ret;
        }

        public void CloseSerialPort()
        {
            close_serialport();
        }

    }
}
