﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestProtocol
{
    public partial class Form1 : Form
    {
        protocol_yaohua_weight_communication _port_protocl = new protocol_yaohua_weight_communication();
        public Form1()
        {
            InitializeComponent();
            bool ret = _port_protocl.OpenSerialPort(4, 9600);
            if (!ret)
            {
                MessageBox.Show("串口打开失败!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = "";
            float v = 0.0f;
            int datatype = 0;
            int unit = 0;
            byte[] err = new byte[255];
            bool ret = _port_protocl.GetVal(ref v, ref datatype, ref unit, err);
            string error = System.Text.Encoding.Default.GetString(err);
            if (!ret)
            {
                MessageBox.Show(error);
                return;
            }
            this.textBox1.Text = string.Format("重量等于:{0}",v);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _port_protocl.CloseSerialPort();
        }
    }
}
